﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Заполняет переданный список кассовыми книгами, доступными организации
//
// Параметры:
//    Организация - СправочникСсылка.Организации - Организация, по которой производится поиск кассовых книг
//    СписокКассовыхКниг - СписокЗначений - Заполняемый список
//
Процедура КассовыеКнигиОрганизации(Организация, СписокКассовыхКниг) Экспорт
	
	СписокКассовыхКниг.Очистить();
	
	Если ЗначениеЗаполнено(Организация) Тогда
		
		СписокКассовыхКниг.Добавить(ПустаяСсылка(), НСтр("ru = '<Основная кассовая книга организации>'"));
		
		Запрос = Новый Запрос("
		|ВЫБРАТЬ
		|	КассовыеКниги.Ссылка КАК КассоваяКнига
		|ИЗ
		|	Справочник.КассовыеКниги КАК КассовыеКниги
		|ГДЕ
		|	КассовыеКниги.Владелец = &Организация
		|");
		
		Запрос.УстановитьПараметр("Организация", Организация);
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			СписокКассовыхКниг.Добавить(Выборка.КассоваяКнига);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УП 2.0.9
// Выполняет создание кассовых книг и заполняет ими листы кассовых книг
//
Процедура СоздатьКассовыеКниги(Параметры) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	КассовыеОрдера.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ НеобработанныеЛисты
	|ИЗ
	|	Документ.ЛистКассовойКниги.КассовыеОрдера КАК КассовыеОрдера
	|	
	|ГДЕ
	|	КассовыеОрдера.Валюта = ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка)
	|	И КассовыеОрдера.Ссылка.Удалить_ПоПодразделению
	|	И КассовыеОрдера.Ссылка.Проведен
	|;
	|/////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	КассовыеОрдера.Ссылка.Организация               КАК Организация,
	|	КассовыеОрдера.Ссылка.Удалить_Подразделение     КАК Подразделение,
	|	КассовыеОрдера.Документ.Касса                   КАК Касса
	|ПОМЕСТИТЬ ОбособленныеКассы
	|ИЗ
	|	Документ.ЛистКассовойКниги.КассовыеОрдера КАК КассовыеОрдера
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		НеобработанныеЛисты
	|	ПО
	|		НеобработанныеЛисты.Ссылка = КассовыеОрдера.Ссылка
	|;
	|///////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	ОбособленныеКассы.Организация                КАК Организация,
	|	ОбособленныеКассы.Подразделение              КАК Подразделение,
	|	ОбособленныеКассы.Касса                      КАК Касса,
	|	ЕСТЬNULL(КассовыеКниги.Ссылка, НЕОПРЕДЕЛЕНО) КАК КассоваяКнига
	|ИЗ
	|	ОбособленныеКассы КАК ОбособленныеКассы
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Справочник.КассовыеКниги КАК КассовыеКниги
	|	ПО
	|		КассовыеКниги.Ссылка = ОбособленныеКассы.Касса.КассоваяКнига
	|;
	|///////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	ДокументКассоваяКнига.Ссылка                    КАК Ссылка,
	|	ДокументКассоваяКнига.Организация               КАК Организация,
	|	ДокументКассоваяКнига.Удалить_Подразделение     КАК Подразделение
	|ИЗ
	|	Документ.ЛистКассовойКниги КАК ДокументКассоваяКнига
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		НеобработанныеЛисты
	|	ПО
	|		НеобработанныеЛисты.Ссылка = ДокументКассоваяКнига.Ссылка
	|;
	|///////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	КассовыеОрдера.Ссылка                                 КАК Ссылка,
	|	ЕСТЬNULL(СУММА(ДенежныеСредства.СуммаРеглПриход), 0)  КАК СуммаПоступления,
	|	ЕСТЬNULL(СУММА(ДенежныеСредства.СуммаРеглРасход), 0)  КАК СуммаВыдачи
	|ИЗ
	|	Документ.ЛистКассовойКниги.КассовыеОрдера КАК КассовыеОрдера
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрНакопления.ДенежныеСредстваНаличные.Обороты(,, Регистратор) КАК ДенежныеСредства
	|	ПО
	|		ДенежныеСредства.Регистратор = КассовыеОрдера.Документ
	|	
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		НеобработанныеЛисты
	|	ПО
	|		НеобработанныеЛисты.Ссылка = КассовыеОрдера.Ссылка
	|
	|СГРУППИРОВАТЬ ПО
	|	КассовыеОрдера.Ссылка
	|;
	|////////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ДокументКассоваяКнига.Ссылка                                          КАК Ссылка,
	|	ЕСТЬNULL(СУММА(ДенежныеСредстваОстатки.СуммаРеглКонечныйОстаток), 0)  КАК СуммаКонечныйОстаток
	|ИЗ
	|	Документ.ЛистКассовойКниги КАК ДокументКассоваяКнига
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрНакопления.ДенежныеСредстваНаличные.ОстаткиИОбороты(, , Авто, , ) КАК ДенежныеСредстваОстатки
	|	ПО
	|		НАЧАЛОПЕРИОДА(КассовыеОрдера.Ссылка.Дата, ДЕНЬ) = ДенежныеСредстваОстатки.ПериодДень
	|		И ДенежныеСредстваОстатки.Касса В
	|			(ВЫБРАТЬ
	|				ОбособленныеКассы.Касса
	|			ИЗ
	|				ОбособленныеКассы КАК ОбособленныеКассы
	|			ГДЕ
	|				ОбособленныеКассы.Подразделение = ДокументКассоваяКнига.Удалить_Подразделение)
	|	
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		НеобработанныеЛисты
	|	ПО
	|		НеобработанныеЛисты.Ссылка = ДокументКассоваяКнига.Ссылка
	|
	|СГРУППИРОВАТЬ ПО
	|	ДокументКассоваяКнига.Ссылка
	|;
	|/////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	КассовыеОрдера.Ссылка КАК Ссылка,
	|	КассовыеОрдера.НомерСтроки КАК НомерСтроки,
	|	КассовыеОрдера.Документ.Валюта КАК Валюта
	|ИЗ
	|	Документ.ЛистКассовойКниги.КассовыеОрдера КАК КассовыеОрдера
	|	
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		НеобработанныеЛисты
	|	ПО
	|		НеобработанныеЛисты.Ссылка = КассовыеОрдера.Ссылка
	|;
	|/////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	НАЧАЛОПЕРИОДА(ДенежныеСредства.Период, ДЕНЬ) КАК Период,
	|	ДенежныеСредства.Организация КАК Организация,
	|	ДенежныеСредства.Касса.Подразделение КАК Подразделение,
	|	СУММА(
	|		ВЫБОР КОГДА ДенежныеСредства.СуммаРегл > 0 ТОГДА
	|			ДенежныеСредства.СуммаРегл
	|		ИНАЧЕ
	|			0
	|		КОНЕЦ
	|	) КАК Приход,
	|	СУММА(
	|		ВЫБОР КОГДА ДенежныеСредства.СуммаРегл < 0 ТОГДА
	|			-ДенежныеСредства.СуммаРегл
	|		ИНАЧЕ
	|			0
	|		КОНЕЦ
	|	) КАК Расход,
	|	МАКСИМУМ(СтатьиДДС.КорреспондирующийСчет) КАК КорреспондирующийСчет
	|ИЗ
	|	РегистрНакопления.ДенежныеСредстваНаличные КАК ДенежныеСредства
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Справочник.СтатьиДвиженияДенежныхСредств КАК СтатьиДДС
	|	ПО
	|		СтатьиДДС.Ссылка = ДенежныеСредства.СтатьяДвиженияДенежныхСредств
	|	
	|ГДЕ
	|	ДенежныеСредства.СуммаРегл <> 0
	|	И Не ДенежныеСредства.Регистратор ССЫЛКА Документ.ВводОстатков
	|	И ДенежныеСредства.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПереоценкаДенежныхСредств)
	|	И ДенежныеСредства.Касса В 
	|		(ВЫБРАТЬ
	|			ОбособленныеКассы.Касса
	|		ИЗ
	|			ОбособленныеКассы)
	|	
	|СГРУППИРОВАТЬ ПО
	|	ДенежныеСредства.Период,
	|	ДенежныеСредства.Организация,
	|	ДенежныеСредства.Касса
	|");
	
	Результат = Запрос.ВыполнитьПакет();
	
	ВыборкаЛистов = Результат[0].Выбрать();
	ВыборкаЛистов.Следующий();
	
	КэшКассовыхКниг = Новый ТаблицаЗначений;
	КэшКассовыхКниг.Колонки.Добавить("КассоваяКнига");
	КэшКассовыхКниг.Колонки.Добавить("Организация");
	КэшКассовыхКниг.Колонки.Добавить("Подразделение");
	
	ВыборкаКасс = Результат[2].Выбрать();
	Пока ВыборкаКасс.Следующий() Цикл
		
		Если ВыборкаКасс.КассоваяКнига = Неопределено Тогда
		
			СтруктураПоиска = Новый Структура("Организация, Подразделение", ВыборкаКасс.Организация, ВыборкаКасс.Подразделение);
			МассивКниг = КэшКассовыхКниг.НайтиСтроки(СтруктураПоиска);
			Если МассивКниг.Количество() Тогда
				КассоваяКнига = МассивКниг[0].КассоваяКнига;
			Иначе // поиск в базе
				КассоваяКнига = Справочники.КассовыеКниги.НайтиПоНаименованию(Строка(ВыборкаКасс.Подразделение), , ВыборкаКасс.Организация);
				
				Если Не ЗначениеЗаполнено(КассоваяКнига) Тогда
					НоваяКнига = Справочники.КассовыеКниги.СоздатьЭлемент();
					НоваяКнига.Владелец = ВыборкаКасс.Организация;
					НоваяКнига.Наименование = Строка(ВыборкаКасс.Подразделение);
					НоваяКнига.СтруктурноеПодразделение = НоваяКнига.Наименование;
					НоваяКнига.ИспользоватьПрефикс = Ложь;
					ОбновлениеИнформационнойБазы.ЗаписатьДанные(НоваяКнига);
					
					КассоваяКнига = НоваяКнига.Ссылка;
				КонецЕсли;
				
				НоваяСтрока = КэшКассовыхКниг.Добавить();
				НоваяСтрока.КассоваяКнига = КассоваяКнига;
				НоваяСтрока.Организация = ВыборкаКасс.Организация;
				НоваяСтрока.Подразделение = ВыборкаКасс.Подразделение;
			КонецЕсли;
		
			Попытка
				
				НачатьТранзакцию();
				
				Блокировка = Новый БлокировкаДанных;
				ЭлементБлокировки = Блокировка.Добавить("Справочник." + ВыборкаКасс.Касса.Метаданные().Имя);
				ЭлементБлокировки.УстановитьЗначение("Ссылка", ВыборкаКасс.Касса);
				Блокировка.Заблокировать();
				
				СправочникОбъект = ВыборкаКасс.Касса.ПолучитьОбъект();
				Если СправочникОбъект = Неопределено Тогда
					ОтменитьТранзакцию();
					Продолжить;
				КонецЕсли;
				
				СправочникОбъект.КассоваяКнига = КассоваяКнига;
				СправочникОбъект.ЭтоКассаОбособленногоПодразделения = Истина;
				
				Если СправочникОбъект.Модифицированность() Тогда
					ОбновлениеИнформационнойБазы.ЗаписатьДанные(СправочникОбъект);
				КонецЕсли;
				
				ЗафиксироватьТранзакцию();
				
			Исключение
				
				ОтменитьТранзакцию();
				ТекстСообщения = НСтр("ru = 'Не удалось обработать справочник: %Ссылка% по причине: %Причина%'");
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%", ВыборкаКасс.Касса);
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
				ЗаписьЖурналаРегистрации(ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(), УровеньЖурналаРегистрации.Предупреждение,
					, ВыборкаКасс.Касса, ТекстСообщения);
				
			КонецПопытки;
		Иначе
			СтруктураПоиска = Новый Структура("Организация, Подразделение", ВыборкаКасс.Организация, ВыборкаКасс.Подразделение);
			МассивКниг = КэшКассовыхКниг.НайтиСтроки(СтруктураПоиска);
			Если НЕ МассивКниг.Количество() Тогда
				НоваяСтрока = КэшКассовыхКниг.Добавить();
				НоваяСтрока.КассоваяКнига = ВыборкаКасс.КассоваяКнига;
				НоваяСтрока.Организация = ВыборкаКасс.Организация;
				НоваяСтрока.Подразделение = ВыборкаКасс.Подразделение;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	ВалютаРегламентированногоУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	
	ВыборкаДокументов = Результат[3].Выбрать();
	
	ВыборкаОборотов = Результат[4].Выбрать();
	ВыборкаОстатков = Результат[5].Выбрать();
	ВыборкаВалют = Результат[6].Выбрать();
	ВыборкаКурсовыхРазниц = Результат[7].Выбрать();
	
	Пока ВыборкаДокументов.Следующий() Цикл
		
		СтруктураПоиска = Новый Структура("Организация, Подразделение", ВыборкаДокументов.Организация, ВыборкаДокументов.Подразделение);
		МассивКниг = КэшКассовыхКниг.НайтиСтроки(СтруктураПоиска);
		Если МассивКниг.Количество() Тогда
			КассоваяКнига = МассивКниг[0].КассоваяКнига;
		Иначе
			Продолжить;
		КонецЕсли;
			
		Попытка
			
			НачатьТранзакцию();
			
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("Документ." + ВыборкаДокументов.Ссылка.Метаданные().Имя);
			ЭлементБлокировки.УстановитьЗначение("Ссылка", ВыборкаДокументов.Ссылка);
			Блокировка.Заблокировать();
			
			ДокументОбъект = ВыборкаДокументов.Ссылка.ПолучитьОбъект();
			Если ДокументОбъект = Неопределено Тогда
				ОтменитьТранзакцию();
				Продолжить;
			КонецЕсли;
			
			ДокументОбъект.КассоваяКнига = КассоваяКнига;
			
			СтруктураПоиска = Новый Структура;
			СтруктураПоиска.Вставить("Ссылка", ВыборкаДокументов.Ссылка);
			Если ВыборкаОборотов.НайтиСледующий(СтруктураПоиска) Тогда
				ДокументОбъект.СуммаПоступления = ВыборкаОборотов.СуммаПоступления;
				ДокументОбъект.СуммаВыдачи = ВыборкаОборотов.СуммаВыдачи;
			КонецЕсли;
			Если ВыборкаОстатков.НайтиСледующий(СтруктураПоиска) Тогда
				ДокументОбъект.СуммаКонечныйОстаток = ВыборкаОстатков.СуммаКонечныйОстаток;
			КонецЕсли;
			
			СтруктураПоиска = Новый Структура("Ссылка", ВыборкаДокументов.Ссылка);
			Пока ВыборкаВалют.НайтиСледующий(СтруктураПоиска) Цикл
				ДокументОбъект.КассовыеОрдера[ВыборкаВалют.НомерСтроки - 1].Валюта = ВыборкаВалют.Валюта;
			КонецЦикла;
			ВыборкаВалют.Сбросить();
			
			СтруктураПоиска = Новый Структура("Период, Организация, Подразделение",
				НачалоДня(ДокументОбъект.Дата), ДокументОбъект.Организация, ДокументОбъект.Удалить_Подразделение);
			НомерПоследнейСтроки = ДокументОбъект.КассовыеОрдера.Количество() - 1;
			Если НомерПоследнейСтроки >= 0 Тогда
				НомерЛиста = ДокументОбъект.КассовыеОрдера[НомерПоследнейСтроки].НомерЛиста;
				Пока ВыборкаКурсовыхРазниц.НайтиСледующий(СтруктураПоиска) Цикл
					НоваяСтрока = ДокументОбъект.КассовыеОрдера.Добавить();
					НоваяСтрока.НомерЛиста = НомерЛиста;
					НоваяСтрока.Приход = ВыборкаКурсовыхРазниц.Приход;
					ДокументОбъект.СуммаПоступления = ДокументОбъект.СуммаПоступления + ВыборкаКурсовыхРазниц.Приход;
					НоваяСтрока.Расход = ВыборкаКурсовыхРазниц.Расход;
					ДокументОбъект.СуммаВыдачи = ДокументОбъект.СуммаВыдачи + ВыборкаКурсовыхРазниц.Расход;
					НоваяСтрока.Валюта = ВалютаРегламентированногоУчета;
					НоваяСтрока.КорреспондирующийСчет = ВыборкаКурсовыхРазниц.КорреспондирующийСчет;
				КонецЦикла;
				ВыборкаКурсовыхРазниц.Сбросить();
			КонецЕсли;
			
			Если ДокументОбъект.Модифицированность() Тогда
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(ДокументОбъект);
			КонецЕсли;
			
			ЗафиксироватьТранзакцию();
			
		Исключение
			
			ОтменитьТранзакцию();
			
			ТекстСообщения = НСтр("ru = 'Не удалось обработать документ: %Ссылка% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%", ВыборкаДокументов.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			
			ЗаписьЖурналаРегистрации(ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(), УровеньЖурналаРегистрации.Предупреждение,
				, ВыборкаДокументов.Ссылка, ТекстСообщения);
			
		КонецПопытки;
	КонецЦикла;
	
	Параметры.ОбработкаЗавершена = (ВыборкаЛистов.Количество = 0);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли