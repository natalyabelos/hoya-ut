﻿
&НаСервере
Процедура ЗаполнитьБоксыНаСервере()
	Если ЗначениеЗаполнено(ЭтаФорма.НомерС) И ЗначениеЗаполнено(ЭтаФорма.НомерПо) И ЭтаФорма.НомерПо > ЭтаФорма.НомерС И ЗначениеЗаполнено(ЭтаФорма.Склад) тогда
		Запрос = Новый запрос;
		запрос.Текст = "ВЫБРАТЬ
		               |	fs_БоксыКлиента.Ссылка КАК Бокс
		               |ИЗ
		               |	Справочник.fs_БоксыКлиента КАК fs_БоксыКлиента
		               |ГДЕ
		               |	НЕ fs_БоксыКлиента.ПометкаУдаления
		               |	И fs_БоксыКлиента.НомерЧисло >= &НомерС
		               |	И fs_БоксыКлиента.НомерЧисло <= &НомерПо
		               |	И fs_БоксыКлиента.Склад = &Склад";
		Запрос.УстановитьПараметр("НомерС",ЭтаФорма.НомерС);
		Запрос.УстановитьПараметр("НомерПо",ЭтаФорма.НомерПо);
		Запрос.УстановитьПараметр("Склад",ЭтаФорма.Склад);

		ТЗ = Запрос.Выполнить().Выгрузить();
		
		Об = РеквизитФормыВЗначение("Объект");
		ТЧ = Об.БоксыКлиента;
		Если Этаформа.ОчиститьСписок тогда
			ТЧ.Загрузить(ТЗ);
		иначе
			Для каждого СтрокаТЗ из ТЗ цикл
				НоваяСтрока = ТЧ.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрока,СтрокаТЗ);
			КонецЦикла;
		КонецЕсли;
		
		ЗначениеВРеквизитФормы(ТЧ,"Объект.БоксыКлиента");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьБоксы(Команда)
	ЗаполнитьБоксыНаСервере();
КонецПроцедуры
