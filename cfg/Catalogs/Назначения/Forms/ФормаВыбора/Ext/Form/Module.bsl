﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Если используется выбор групп и элементов, то форма вызвана из платформенного поиска 
	Если Не Параметры.Свойство("Источник",Источник)
		И Не Параметры.Свойство("ИмяОбъекта",ИмяОбъекта)
		И ЗначениеЗаполнено(Параметры.Номенклатура) Тогда
		ВызватьИсключение НСтр("ru='Не реализован выбор назначения из этой формы. Необходимо настроить связи параметров выбора'"); 	
	КонецЕсли; 	

	Параметры.Свойство("Распоряжение",Распоряжение);
	Параметры.Свойство("Ячейка",Ячейка);
	Параметры.Свойство("ВидОперации",ВидОперации);	
	Параметры.Свойство("РежимВыбораНазначений",РежимВыбора);
	
	Если ЗначениеЗаполнено(ИмяОбъекта) Тогда
		Если ИмяОбъекта = "Обработка.ПроверкаКоличестваТоваровВДокументе" 
			Или ИмяОбъекта = "Обработка.ВводРасхожденийПоРезультатамПриемкиПолучателем"
			Или ИмяОбъекта = "Обработка.ПроверкаКоличестваТоваровВПриходномОрдере" Тогда
			Элементы.Потребность.Видимость = Ложь;
			Элементы.Обеспечено.Видимость = Ложь;	
		ИначеЕсли ИмяОбъекта = "Обработка.РедактированиеСпецификацииСтрокиЗаказа" Тогда
			Элементы.Потребность.Видимость = Истина;
			Элементы.Обеспечено.Видимость = Истина;	
		КонецЕсли;	
	ИначеЕсли Источник <> Неопределено Тогда
		Если ТипЗнч(Источник) = Тип("ДокументСсылка.ПриходныйОрдерНаТовары") 
				И ЗначениеЗаполнено(Распоряжение)
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОтборРазмещениеТоваров") 
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.РасходныйОрдерНаТовары") Тогда
			Элементы.Потребность.Видимость = Истина;
		ИначеЕсли ТипЗнч(Источник) = Тип("ДокументСсылка.ПересчетТоваров") 
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОрдерНаОтражениеИзлишковТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОрдерНаОтражениеНедостачТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОрдерНаОтражениеПорчиТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.СписаниеНедостачТоваров")	
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ПорчаТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОприходованиеИзлишковТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ПересортицаТоваров")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ВводОстатков")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.КорректировкаПоступления") 
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ВозвратТоваровПоставщику")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.КорректировкаРеализации") 
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ОтражениеРезультатовПроверкиОрдераНаТовары")
			Или ТипЗнч(Источник) = Тип("ДокументСсылка.ПересортицаТоваров")
			Или РежимВыбора = "Все" Тогда
		Иначе
			Элементы.Потребность.Видимость = Истина;
			Элементы.Обеспечено.Видимость = Истина;
		КонецЕсли;	
	Иначе
		Элементы.Потребность.Видимость = Ложь;
		Элементы.Обеспечено.Видимость = Ложь;
	КонецЕсли;	
	
	УстановитьТекстЗапросаДинамическогоСписка();
	УстановитьПараметрыДинамическогоСписка(Параметры);
	
	Если Параметры.Свойство("ВариантыВыбора") Тогда
		НастроитьФормуПоВариантамВыбора();
	Иначе
		НастроитьФормуПоПараметрам();
	КонецЕсли;

	Заголовок = НСтр("ru = 'Выбор назначения'");
	Если ЗначениеЗаполнено(Параметры.Номенклатура) Тогда
		ТекстНоменклатура = СтрЗаменить(НСтр("ru = 'Номенклатура: %1'"), "%1", Параметры.Номенклатура);
		Если ЗначениеЗаполнено(Параметры.Характеристика) Тогда
			ТекстНоменклатура = ТекстНоменклатура + ", "
				+ СтрЗаменить(НСтр("ru = 'Характеристика: %1'"), "%1", Параметры.Характеристика);
		КонецЕсли;
		Заголовок = Заголовок + " (" + ТекстНоменклатура + ")";
	КонецЕсли;
	МодификацияКонфигурацииПереопределяемый.ПриСозданииНаСервере(ЭтаФорма, СтандартнаяОбработка, Отказ);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ПереключательВариантыВыбораПриИзменении(Элемент)
	НастроитьФормуПоВариантуВыбораНазначения();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура УстановитьПараметрыДинамическогоСписка(Параметры)
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ОтборПоРеквизитамЗаказа", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "МассивЗаказов", Новый Массив());
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"Склад",
		Параметры.Склад);
		
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"СкладОтправитель",
		Параметры.СкладОтправитель);
		
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"Номенклатура",
		Параметры.Номенклатура);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"Характеристика",
		Параметры.Характеристика);
		
	ТаблицаТоваров = Новый ТаблицаЗначений;
	ТаблицаТоваров.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаТоваров.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	
	СтрокаТовара = ТаблицаТоваров.Добавить();
	ЗаполнитьЗначенияСвойств(СтрокаТовара, Параметры);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"ТаблицаТоваров",
		ТаблицаТоваров);
		
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"Подразделение",
		Параметры.Подразделение);
		
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"Назначение",
		Параметры.ТекущаяСтрока);
		
	Запрос = Новый Запрос();
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ВЫБОР
		|		КОГДА СпрНоменклатура.ТипНоменклатуры 
		|			= ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Работа) 
		|		ТОГДА
		|			ИСТИНА
		|		ИНАЧЕ
		|			ЛОЖЬ
		|	КОНЕЦ                            КАК ЭтоРабота
		|ИЗ
		|	Справочник.Номенклатура КАК СпрНоменклатура
		|ГДЕ
		|	СпрНоменклатура.Ссылка = &Ссылка";
	Запрос.УстановитьПараметр("Ссылка", Параметры.Номенклатура);
	РезультатЗапроса = Запрос.Выполнить();
	Если РезультатЗапроса.Пустой() Тогда
		ЭтоРабота = Ложь;
	Иначе
		Выборка = РезультатЗапроса.Выбрать();
		Выборка.Следующий();
		ЭтоРабота = Выборка.ЭтоРабота;
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"ЭтоРабота", ЭтоРабота);
		
	Если Параметры.Свойство("ВключатьЗаказыДавальцев") Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
			"ВключатьЗаказыДавальцев",
			Параметры.ВключатьЗаказыДавальцев);
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
			"ВключатьЗаказыДавальцев",
			Истина);
	КонецЕсли;
	
	Если Параметры.Отбор.Свойство("ДвиженияПоСкладскимРегистрам") Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
		"ДвиженияПоСкладскимРегистрам",
		Параметры.Отбор.ДвиженияПоСкладскимРегистрам);
	КонецЕсли;
	
	ТолькоСкладские = Параметры.Свойство("ТолькоСкладскиеНазначения");
	
	ЕстьОтправитель = ЗначениеЗаполнено(Параметры.СкладОтправитель);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоНаличиюУОтправителя", ЕстьОтправитель);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоВсемСкладам",      Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоВсейНоменклатуре", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоРаспоряжению",     Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "Распоряжение", Распоряжение);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ТолькоСкладскиеНазначения", ТолькоСкладские);

КонецПроцедуры


&НаСервере
Процедура УстановитьТекстЗапросаДинамическогоСписка()
	
	Если ТипЗнч(Источник) = Тип("ДокументСсылка.ПриходныйОрдерНаТовары")
		И ЗначениеЗаполнено(Распоряжение) И Параметры.Свойство("ВариантыВыбора") Тогда
		
		Список.ТекстЗапроса = Справочники.Назначения.ТекстЗапросаНазначенийРасширенный();
		
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "Регистратор", Источник);
		
		Возврат;
		
	КонецЕсли;

	ПараметрыФормированияЗапроса = Справочники.Назначения.ПараметрыФормированияЗапросаДоступныхНазначений();
	ПараметрыФормированияЗапроса.РежимВыбора = РежимВыбора;
	ПараметрыФормированияЗапроса.ВидОперации = ВидОперации;	
	ПараметрыФормированияЗапроса.Источник = Источник;
	Если ЗначениеЗаполнено(ИмяОбъекта) Тогда
		Список.ТекстЗапроса = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(ИмяОбъекта).ТекстЗапросаДоступныхНазначений(ПараметрыФормированияЗапроса);
	ИначеЕсли Источник <> Неопределено Тогда
		Список.ТекстЗапроса = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Источник).ТекстЗапросаДоступныхНазначений(ПараметрыФормированияЗапроса);
	Иначе
		Список.ТекстЗапроса = 
		"ВЫБРАТЬ
		|	Назначения.Ссылка КАК Назначение
		|ИЗ
		|	Справочник.Назначения КАК Назначения";
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
	"Распоряжение",
	Распоряжение);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
	"Регистратор",
	Источник);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список,
	"Ячейка",
	Ячейка);

КонецПроцедуры

#Область НастройкаФормы

&НаСервере
Процедура НастроитьФормуПоПараметрам()
	
	ПереключательВариантыВыбора = 2; // потребности склада-получателя
	
	ИспользоватьОбеспечениеСверхПотребности = ПолучитьФункциональнуюОпцию("НеКонтролироватьОбособленноеОбеспечениеСверхПотребности");
	
	Если Параметры.РежимВыбораНазначений = "Расширенный" И ИспользоватьОбеспечениеСверхПотребности Тогда
		Элементы.ПереключательВариантыВыбора.Видимость = Истина;
		Элементы.ВНаличииОтправитель.Видимость = ЗначениеЗаполнено(Параметры.СкладОтправитель);
	Иначе
		Элементы.ПереключательВариантыВыбора.Видимость = Ложь;
		Элементы.ВНаличииОтправитель.Видимость = Ложь;
	КонецЕсли;
	
	НастроитьФормуПоВариантуВыбораНазначения();
	
	// Для перемещений обособленных товаров не имеет смысла переключатель "По всем складам", так как перемещение идет на конкретный склад.
	Если ТипЗнч(Источник) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
		ЭлементСпискаРежимов = Элементы.ПереключательВариантыВыбора.СписокВыбора.НайтиПоЗначению(3); // значение "По всем складам"
		Элементы.ПереключательВариантыВыбора.СписокВыбора.Удалить(ЭлементСпискаРежимов);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НастроитьФормуПоВариантамВыбора()
	
	СписокВыбора = Элементы.ПереключательВариантыВыбора.СписокВыбора;
	
	СписокВыбора.Очистить();
	
	Если Параметры.ВариантыВыбора.Найти(0) <> Неопределено И ЗначениеЗаполнено(Распоряжение) Тогда
		СписокВыбора.Добавить(0, НСтр("ru = 'Назначения данного товара'"));
	КонецЕсли;
	Если Параметры.ВариантыВыбора.Найти(1) <> Неопределено Тогда
		СписокВыбора.Добавить(1, НСтр("ru = 'Наличие на складе-отправителе'"));
	КонецЕсли;
	Если Параметры.ВариантыВыбора.Найти(2) <> Неопределено Тогда
		СписокВыбора.Добавить(2, НСтр("ru = 'Потребность склада-получателя'"));
	КонецЕсли;
	Если Параметры.ВариантыВыбора.Найти(3) <> Неопределено Тогда
		СписокВыбора.Добавить(3, НСтр("ru = 'Потребность всех складов'"));
	КонецЕсли;
	Если Параметры.ВариантыВыбора.Найти(4) <> Неопределено Тогда
		СписокВыбора.Добавить(4, НСтр("ru = 'Все обособленные заказы'"));
	КонецЕсли;
	Если Параметры.ВариантыВыбора.Найти(5) <> Неопределено Тогда
		СписокВыбора.Добавить(5, НСтр("ru = 'Все назначения распоряжения'"));
	КонецЕсли;
	
	Элементы.ПереключательВариантыВыбора.Видимость = СписокВыбора.Количество() > 1;
	
	ПереключательВариантыВыбора = ?(СписокВыбора.Количество() > 0, СписокВыбора[0].Значение, 0);
	
	ИспользоватьОбеспечениеСверхПотребности = ПолучитьФункциональнуюОпцию("НеКонтролироватьОбособленноеОбеспечениеСверхПотребности");
	Если ИспользоватьОбеспечениеСверхПотребности Тогда
		Элементы.ПереключательВариантыВыбора.Видимость = Истина;
		Элементы.ВНаличииОтправитель.Видимость = ЗначениеЗаполнено(Параметры.СкладОтправитель);
	Иначе
		Элементы.ПереключательВариантыВыбора.Видимость = Ложь;
		Элементы.ВНаличииОтправитель.Видимость = Ложь;
	КонецЕсли;
	
	НастроитьФормуПоВариантуВыбораНазначения();
	
	Элементы.ВНаличииОтправитель.Видимость = ЗначениеЗаполнено(Параметры.СкладОтправитель);
	
КонецПроцедуры

&НаСервере
Процедура НастроитьФормуПоВариантуВыбораНазначения()

	ПоРаспоряжению     = ПереключательВариантыВыбора = 0 Или ПереключательВариантыВыбора = 5;
	ПоВсемСкладам      = ПереключательВариантыВыбора = 3;
	ПоВсейНоменклатуре = ПереключательВариантыВыбора = 4 Или ПереключательВариантыВыбора = 5;

	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоВсемСкладам",      ПоВсемСкладам);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоРаспоряжению",     ПоРаспоряжению);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПоВсейНоменклатуре", ПоВсейНоменклатуре);

	Шаблон = НСтр("ru = 'В наличии на ""%1""'");
	ТекстЗаголовка = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(Шаблон, Параметры.СкладОтправитель);
	Элементы.ВНаличииОтправитель.Заголовок = ТекстЗаголовка;

	Элементы.Количество.Видимость  = Ложь;
	Элементы.Обеспечено.Видимость  = Ложь;
	Элементы.Потребность.Видимость = Ложь;
	
	Если ПереключательВариантыВыбора = 0 Тогда // назначение товара по распоряжению
		
		Элементы.Количество.Видимость = Истина;
		
	ИначеЕсли ПереключательВариантыВыбора = 5 Тогда // назначения всех товаров распоряжения
		
	ИначеЕсли ПереключательВариантыВыбора = 1 Тогда // наличие на складе-отправителе
		
	ИначеЕсли ПереключательВариантыВыбора = 2 Тогда // потребность на складе-получателе
		
		Если ЗначениеЗаполнено(Параметры.Склад) Тогда
			Шаблон = НСтр("ru = 'Потребность на ""%1""'");
			Элементы.Потребность.Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(Шаблон, Параметры.Склад);
		КонецЕсли;
		Элементы.Потребность.Видимость = Истина;
		
	ИначеЕсли ПереключательВариантыВыбора = 3 Тогда // потребность на всех складах
		
		Элементы.Потребность.Заголовок = НСтр("ru = 'Потребность на всех складах'");
		Элементы.Потребность.Видимость = Истина;
		
	ИначеЕсли ПереключательВариантыВыбора = 4 Тогда // все заказы
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти







