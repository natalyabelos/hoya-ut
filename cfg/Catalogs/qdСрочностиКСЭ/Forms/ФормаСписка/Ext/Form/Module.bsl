﻿
&НаСервере
Процедура ОбновитьСписокUrgenciesНаСервере()
	qdОбменКСЭ.ЗагрузитьОбновитьСписок("Urgencies","qdСрочностиКСЭ");
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСписокUrgencies(Команда)
	ОбновитьСписокUrgenciesНаСервере();
КонецПроцедуры
