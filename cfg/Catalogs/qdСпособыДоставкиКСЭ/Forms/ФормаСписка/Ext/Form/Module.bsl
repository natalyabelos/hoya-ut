﻿
&НаСервере
Процедура ОбновитьСписокShippingMethodНаСервере()
	
	НастройкаОбменаКСЭ = qdОбменКСЭ.ПолучитьОсновнуюНастройкуОбменаКСЭ();
	Если НастройкаОбменаКСЭ = Неопределено Тогда 
		Возврат;
	КонецЕсли;
	
	Попытка
		Соединение	= qdОбменКСЭ.ПолучитьWSПрокси(НастройкаОбменаКСЭ);
	Исключение
		Сообщить("Неудачная попытка соединения.");
		Возврат;
	КонецПопытки;
	
	login 		= НастройкаОбменаКСЭ.login;
	password	= НастройкаОбменаКСЭ.password; 	
	URIПространстваИменСервиса  = НастройкаОбменаКСЭ.URIПространстваИменСервиса; 	
	
	ТипОбъектаXDTO	= Соединение.ФабрикаXDTO.Тип(URIПространстваИменСервиса, "Element");
	
	XDTOПакет		= Соединение.ФабрикаXDTO.Создать(ТипОбъектаXDTO); 
	XDTOПакет.Key	= "parameters";
	
	ЭлементСпискаXDTO = Соединение.ФабрикаXDTO.Создать(ТипОбъектаXDTO);
	ЭлементСпискаXDTO.Key 		= "Reference";
	ЭлементСпискаXDTO.Value 	= "ShippingMethods";
	ЭлементСпискаXDTO.ValueType	= "string";
	
	XDTOПакет.List.Добавить(ЭлементСпискаXDTO);
	
	Попытка
	Ответ = Соединение.GetReferenceData(
		login,
		password,
		XDTOПакет);
	Исключение
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ОписаниеОшибки());
		Возврат;
	КонецПопытки;
	
	Для каждого ОбъектXDTO Из Ответ.List Цикл
		
		ЭлементаKey = ОбъектXDTO.Key;
		ЭлементаValue = ОбъектXDTO.Value;
		
		НайденыйЭлемент = Справочники.qdСпособыДоставкиКСЭ.НайтиПоРеквизиту("Key", ЭлементаKey);	
		Если ЗначениеЗаполнено(НайденыйЭлемент) Тогда
			СправочникОбъект = НайденыйЭлемент.ПолучитьОбъект();
		Иначе 
			СправочникОбъект = Справочники.qdСпособыДоставкиКСЭ.СоздатьЭлемент();
		КонецЕсли;
		
		СправочникОбъект.Key 		  = ЭлементаKey;
		СправочникОбъект.Наименование = ЭлементаValue;
		
		Попытка
			СправочникОбъект.Записать();
		Исключение
			ТекстСообщения = "Не удалось создать/обновить элемент '"+Строка(ЭлементаValue)+"' справочника ShippingMethods. "+ОписаниеОшибки();
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);	
			Продолжить;
		КонецПопытки;			
		
	КонецЦикла;
	
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСписокShippingMethod(Команда)
	
	ОбновитьСписокShippingMethodНаСервере();
	
КонецПроцедуры
