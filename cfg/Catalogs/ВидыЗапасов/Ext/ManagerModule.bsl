﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Функция определяет реквизиты выбранного вида запасов.
//
// Параметры:
//  ВидЗапасов - СправочникСсылка.ВидыЗапасов - Ссылка на вид запасов
//
// Возвращаемое значение:
//	Структура - реквизиты выбранного вида запасов
//
Функция ПолучитьРеквизитыВидаЗапасов(ВидЗапасов) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ВидыЗапасов.РеализацияЗапасовДругойОрганизации КАК РеализацияЗапасовДругойОрганизации,
	|	ВидыЗапасов.Организация КАК Организация,
	|	ВидыЗапасов.Валюта КАК Валюта,
	|	ВидыЗапасов.ТипЗапасов КАК ТипЗапасов
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ВидыЗапасов
	|ГДЕ
	|	ВидыЗапасов.Ссылка = &ВидЗапасов
	|");
	
	Запрос.УстановитьПараметр("ВидЗапасов", ВидЗапасов);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		РеализацияЗапасовДругойОрганизации = Выборка.РеализацияЗапасовДругойОрганизации;
		Организация = Выборка.Организация;
		Валюта = Выборка.Валюта;
		ТипЗапасов = Выборка.ТипЗапасов;
	Иначе
		РеализацияЗапасовДругойОрганизации = Ложь;
		Организация = Справочники.Организации.ПустаяСсылка();
		Валюта = Справочники.Валюты.ПустаяСсылка();
		ТипЗапасов = Перечисления.ТипыЗапасов.ПустаяСсылка();
	КонецЕсли;
	
	СтруктураРеквизитов = Новый Структура("РеализацияЗапасовДругойОрганизации, Организация, Валюта, ТипЗапасов", 
		РеализацияЗапасовДругойОрганизации,
		Организация,
		Валюта,
		ТипЗапасов);
	
	Возврат СтруктураРеквизитов;

КонецФункции

// Функция формирует строку наименования вида запаса.
//
// Параметры:
//	СправочникОбъект - Вид запасов, для которого необходимо определить наименовние
//
// Возвращаемое значение:
//	Строка - Наименование вида запасов
//
Функция ПолучитьНаименованиеВидаЗапасов(СправочникОбъект) Экспорт
	
	Если СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяНДС Тогда
		СтрокаНалогообложение = НСтр("ru='Облагается НДС'");
	ИначеЕсли СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС Тогда
		СтрокаНалогообложение = НСтр("ru='Не облагается НДС'");
	ИначеЕсли СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД Тогда
		СтрокаНалогообложение = НСтр("ru='Облагается ЕНВД'");
	ИначеЕсли СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаНаЭкспорт Тогда
		СтрокаНалогообложение = НСтр("ru='Экспорт'");	
	Иначе
		СтрокаНалогообложение = "";
	КонецЕсли;
	
	Если СправочникОбъект.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляЗаказа Тогда
		АналитикаУчета = Строка(СправочникОбъект.Назначение);
	ИначеЕсли СправочникОбъект.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляСделки Тогда
		АналитикаУчета = НСтр("ru='Сделка'") + ": " + Строка(СправочникОбъект.Сделка);
	ИначеЕсли СправочникОбъект.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляМенеджера Тогда
		АналитикаУчета = НСтр("ru='Менеджер'") + ": " + Строка(СправочникОбъект.Менеджер);
	ИначеЕсли СправочникОбъект.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляПодразделения Тогда
		АналитикаУчета = НСтр("ru='Подразделение'") + ": " + Строка(СправочникОбъект.Подразделение);
	Иначе
		АналитикаУчета = "";
	КонецЕсли;
	
	Если СправочникОбъект.ТипЗапасов = Перечисления.ТипыЗапасов.КомиссионныйТовар Тогда
		Наименование = СокрЛП(СправочникОбъект.ТипЗапасов) + "; "
			+ ?(Не ПустаяСтрока(АналитикаУчета), АналитикаУчета + "; ", "")
			+ НСтр("ru='Комитент'") + ": " + Строка(СправочникОбъект.Комитент) + "; "
			+ ?(ЗначениеЗаполнено(СправочникОбъект.Соглашение), НСтр("ru='Соглашение'") + ": " + Строка(СправочникОбъект.Соглашение) + "; ", "")
			+ СтрокаНалогообложение + "; "
			+ НСтр("ru='Валюта'") + ": " + Строка(СправочникОбъект.Валюта);
	Иначе // собственный товар
		Наименование = СокрЛП(СправочникОбъект.ТипЗапасов) + "; "
			+ ?(Не ПустаяСтрока(АналитикаУчета), АналитикаУчета + "; ", "")
			+ ?(ЗначениеЗаполнено(СправочникОбъект.Поставщик), НСтр("ru='Поставщик'") + ": " + Строка(СправочникОбъект.Поставщик) + "; ", "")
			+ ?(Не ПустаяСтрока(СтрокаНалогообложение), СтрокаНалогообложение, "")
		;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СправочникОбъект.ГруппаФинансовогоУчета) Тогда
		Наименование = Наименование
			+ ?(Прав(СокрЛП(Наименование), 1) <> ";", "; ", "") 
			+ НСтр("ru='Группа'") + ": " + СправочникОбъект.ГруппаФинансовогоУчета;
	КонецЕсли;
		
	Если ЗначениеЗаполнено(СправочникОбъект.ГруппаПродукции) Тогда
		Наименование = Наименование
			+ ?(Прав(СокрЛП(Наименование), 1) <> ";", "; ", "") 
			+ НСтр("ru='Группа продукции'") + ": " + СправочникОбъект.ГруппаПродукции;
	КонецЕсли;	
	
	Если СправочникОбъект.РеализацияЗапасовДругойОрганизации Тогда
		Наименование = Наименование
			+ ?(Прав(СокрЛП(Наименование), 1) <> ";", "; ", "") 
			+ НСтр("ru='Запасы другой организации'");
	КонецЕсли;
		
	Если Прав(СокрЛП(Наименование), 1) =  ";" Тогда
		Наименование = Сред(СокрЛП(Наименование), 1, СтрДлина(СокрЛП(Наименование)) - 1);
	КонецЕсли;
		
	Возврат Наименование;

КонецФункции

// Функция получает вид запасов для текущего документа.
//
// Параметры:
//	Организация - СправочникСсылка.Организации - Организация документа
//	ХозяйственнаяОперация - ПеречислениеСсылка.ХозяйственныеОперации - Операция документа
//	РеквизитыДокумента - Структура или ВыборкаИзРезультатаЗапроса - Данные документа
//
// Возвращаемое значение:
//	СправочникСсылка.ВидыЗапасов - Найденный вид запасов
//
Функция ВидЗапасовДокумента(Организация, ХозяйственнаяОперация, РеквизитыДокумента = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если ЗначениеЗаполнено(РеквизитыДокумента) Тогда
		Реквизиты = Новый Структура("
			|Организация,
			|ТипЗапасов,
			|Комитент,
			|Контрагент,
			|Соглашение,
			|Договор,
			|Валюта,
			|НалогообложениеНДС,
			|НалогообложениеОрганизации,
			|Поставщик,
			|Предназначение,
			|Менеджер,
			|Подразделение,
			|Сделка,
			|Назначение,
			|ОбособленныйУчетТоваровПоСделке,
			|ВариантОбособленногоУчетаТоваров,
			|ВидЗапасов,
			|ГруппаФинансовогоУчета,
			|ГруппаПродукции,
			|ХозяйственнаяОперация
			|");
		ЗаполнитьЗначенияСвойств(Реквизиты, РеквизитыДокумента);
	Иначе
		Реквизиты = Неопределено;
	КонецЕсли;
	
	СтруктураВидЗапасов = СтруктураВидаЗапасов(Организация, ХозяйственнаяОперация, Реквизиты);
	
	МенеджерЗаписи = РегистрыСведений.АналитикаВидовЗапасов.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, СтруктураВидЗапасов);
	МенеджерЗаписи.Прочитать();
	
	Если МенеджерЗаписи.Выбран() Тогда
		Результат = МенеджерЗаписи.КлючАналитики;
	Иначе
		Результат = СоздатьКлючАналитики(СтруктураВидЗапасов);
	КонецЕсли;
	
	Возврат Результат;

КонецФункции

// Функция получает вид запасов для передачи товаров между организациями.
//
// Параметры:
//	ВидЗапасовВладельца - СправочникСсылка.ВидыЗапасов - Вид запасов организации - владельца
//	ОрганизацияПродавец - СправочникСсылка.Организации - Организация - продавец
//	ПередачаПодДеятельность - ПеречислениеСсылка.ТипыНалогообложенияНДС - Налогообложение деятельности
//
// Возвращаемое значение:
//	СправочникСсылка.ВидыЗапасов - Созданный вид запасов
//
Функция ВидЗапасовДляПередачиМеждуОрганизациями(ВидЗапасовВладельца, ОрганизацияПродавец, ПередачаПодДеятельность) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ВидыЗапасовВладельца.Ссылка КАК ВидЗапасовВладельца,
	|	ВидыЗапасовПродавца.Ссылка КАК ВидЗапасов,
	|
	|	Настройка.ОрганизацияПродавец КАК Организация,
	|	ВидыЗапасовВладельца.Организация КАК Комитент,
	|	ВидыЗапасовВладельца.Организация КАК Контрагент,
	|	Настройка.Валюта КАК Валюта,
	|	Настройка.СпособПередачиТоваров КАК СпособПередачиТоваров,
	|	Истина КАК РеализацияЗапасовДругойОрганизации,
	|
	|	ВидыЗапасовВладельца.Предназначение КАК Предназначение,
	|	ВидыЗапасовВладельца.Сделка КАК Сделка,
	|	ВидыЗапасовВладельца.Менеджер КАК Менеджер,
	|	ВидыЗапасовВладельца.Подразделение КАК Подразделение,
	|	ВидыЗапасовВладельца.Назначение КАК Назначение,
	|	&ПередачаПодДеятельность КАК НалогообложениеНДС,
	|
	|	ВЫБОР КОГДА Настройка.СпособПередачиТоваров В (
	|		ЗНАЧЕНИЕ(Перечисление.СпособыПередачиТоваров.ПередачаНаКомиссию),
	|		ЗНАЧЕНИЕ(Перечисление.СпособыПередачиТоваров.ПередачаНаКомиссиюВозврат))
	|	ТОГДА
	|		ВидыЗапасовВладельца.Организация
	|	ИНАЧЕ
	|		ВидыЗапасовВладельца.Поставщик
	|	КОНЕЦ КАК Поставщик,
	|	ВидыЗапасовВладельца.ТипЗапасов КАК ТипЗапасовВладельца,
	|	ВидыЗапасовВладельца.НалогообложениеНДС КАК НалогообложениеВладельца
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ВидыЗапасовВладельца
	|	
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		РегистрСведений.НастройкаПередачиТоваровМеждуОрганизациями КАК Настройка
	|	ПО
	|		ВидыЗапасовВладельца.Организация = Настройка.ОрганизацияВладелец
	|		И ВидыЗапасовВладельца.ТипЗапасов = Настройка.ТипЗапасов
	|		И Настройка.ОрганизацияПродавец = &ОрганизацияПродавец
	|		И Настройка.СпособПередачиТоваров <> ЗНАЧЕНИЕ(Перечисление.СпособыПередачиТоваров.НеПередается)
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Справочник.ВидыЗапасов КАК ВидыЗапасовПродавца
	|	ПО
	|		ВидыЗапасовВладельца.Ссылка = ВидыЗапасовПродавца.ВидЗапасовВладельца
	|		И Настройка.ОрганизацияПродавец = ВидыЗапасовПродавца.Организация
	|		И Настройка.СпособПередачиТоваров = ВидыЗапасовПродавца.СпособПередачиТоваров
	|		И Настройка.Валюта = ВидыЗапасовПродавца.Валюта
	|		И (ВидыЗапасовПродавца.НалогообложениеНДС = &ПередачаПодДеятельность
	|			ИЛИ Настройка.СпособПередачиТоваров = ЗНАЧЕНИЕ(Перечисление.СпособыПередачиТоваров.ПередачаНаКомиссию)
	|			ИЛИ Настройка.СпособПередачиТоваров = ЗНАЧЕНИЕ(Перечисление.СпособыПередачиТоваров.ПередачаНаКомиссиюВозврат)
	|			)
	|ГДЕ
	|	ВидыЗапасовВладельца.Ссылка = &ВидЗапасовВладельца
	|");
	
	Запрос.УстановитьПараметр("ОрганизацияПродавец", ОрганизацияПродавец);
	Запрос.УстановитьПараметр("ВидЗапасовВладельца", ВидЗапасовВладельца);
	ИспользоватьРаздельныйУчетПоНалогообложению = ПолучитьФункциональнуюОпцию("ИспользоватьРаздельныйУчетПоНалогообложению");
	Если ИспользоватьРаздельныйУчетПоНалогообложению Тогда
		Запрос.УстановитьПараметр("ПередачаПодДеятельность", ПередачаПодДеятельность);
	Иначе
		Запрос.УстановитьПараметр("ПередачаПодДеятельность", Перечисления.ТипыНалогообложенияНДС.ПустаяСсылка());
	КонецЕсли;	
	
	ВидЗапасов = Справочники.ВидыЗапасов.ПустаяСсылка();
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		
		Если ЗначениеЗаполнено(Выборка.ВидЗапасов) Тогда
			ВидЗапасов = Выборка.ВидЗапасов;
		Иначе
			СтруктураПараметры = Новый Структура("
				|Организация,
				|СпособПередачиТоваров,
				|Комитент,
				|Контрагент,
				|Валюта,
				|ВидЗапасовВладельца,
				|РеализацияЗапасовДругойОрганизации,
				|Предназначение,
				|Сделка,
				|Менеджер,
				|Подразделение,
				|Поставщик,
				|Назначение,
				|НалогообложениеНДС
				|");
			ЗаполнитьЗначенияСвойств(СтруктураПараметры, Выборка);
			
			Если Выборка.СпособПередачиТоваров = Перечисления.СпособыПередачиТоваров.ПередачаНаКомиссию
			 ИЛИ Выборка.СпособПередачиТоваров = Перечисления.СпособыПередачиТоваров.ПередачаНаКомиссиюВозврат Тогда
				Если Выборка.ТипЗапасовВладельца = Перечисления.ТипыЗапасов.КомиссионныйТовар Тогда
					НалогообложениеНДС = Выборка.НалогообложениеВладельца;
				Иначе
					НалогообложениеНДС = Справочники.Организации.НалогообложениеНДС(
						Выборка.Комитент,
						, // Склад
						// Дата
					);
				КонецЕсли;
			ИначеЕсли ИспользоватьРаздельныйУчетПоНалогообложению Тогда
				НалогообложениеНДС = ПередачаПодДеятельность;
			Иначе
				НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПустаяСсылка();
			КонецЕсли;
			
			СправочникОбъект = Справочники.ВидыЗапасов.СоздатьЭлемент();
			СправочникОбъект.Заполнить(СтруктураПараметры);
			СправочникОбъект.НалогообложениеНДС = НалогообложениеНДС;
			СправочникОбъект.Записать();
			
			ВидЗапасов = СправочникОбъект.Ссылка;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ВидЗапасов;
	
КонецФункции

// Функция получает вид запасов для возврата товаров от клиента.
//
// Параметры:
//	ВидЗапасовОтгрузки - СправочникСсылка.ВидыЗапасов - Вид запасов реализованного товара
//	Организация - СправочникСсылка.Организации - Организация в документе возврата товаров
//				- Неопределено - Организация берется из вида запасов отгрузки
//
// Возвращаемое значение:
//	СправочникСсылка.ВидыЗапасов - Созданный вид запасов
//
Функция ВидЗапасовДляВозвратаТоваровОтКлиента(ВидЗапасовОтгрузки, Организация = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ВидыЗапасов.Ссылка					КАК ВидЗапасов,
	|	ВидыЗапасов.Организация				КАК Организация,
	|	ВидыЗапасов.ТипЗапасов				КАК ТипЗапасов,
	|	ВидыЗапасов.Комитент				КАК Комитент,
	|	ВидыЗапасов.Контрагент				КАК Контрагент,
	|	ВидыЗапасов.Договор					КАК Договор,
	|	ВидыЗапасов.Соглашение				КАК Соглашение,
	|	ВидыЗапасов.Валюта					КАК Валюта,
	|	ВЫБОР КОГДА ВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|		ВидыЗапасов.Комитент
	|	ИНАЧЕ
	|		ВидыЗапасов.Поставщик
	|	КОНЕЦ КАК Поставщик,
	|	ВидыЗапасов.ГруппаФинансовогоУчета	КАК ГруппаФинансовогоУчета,
	|
	|	ВЫБОР КОГДА ВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.Товар) ТОГДА
	|		ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
	|	ИНАЧЕ
	|		ВидыЗапасов.НалогообложениеНДС
	|	КОНЕЦ КАК НалогообложениеНДС,
	|
	|	ВЫБОР КОГДА ВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|		ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПриемНаКомиссию)
	|	ИНАЧЕ
	|		ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ЗакупкаУПоставщика)
	|	КОНЕЦ КАК ХозяйственнаяОперация,
	|
	|	ВЫБОР КОГДА ВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.МатериалДавальца) ТОГДА
	|		ИСТИНА
	|	ИНАЧЕ
	|		ЛОЖЬ
	|	КОНЕЦ КАК МатериалДавальца,
	|
	|	ЕСТЬNULL(ВидыЗапасов.Сделка.ОбособленныйУчетТоваровПоСделке, ЛОЖЬ)		КАК ОбособленныйУчетТоваровПоСделке,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)				КАК НалогообложениеОрганизации,
	|	ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)					КАК Подразделение,
	|	ЗНАЧЕНИЕ(Справочник.Пользователи.ПустаяСсылка)							КАК Менеджер,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка)						КАК Сделка,
	|	ЗНАЧЕНИЕ(Перечисление.ВариантыОбособленногоУчетаТоваров.ПустаяСсылка)	КАК ВариантОбособленногоУчетаТоваров,
	|	ЗНАЧЕНИЕ(Справочник.Назначения.ПустаяСсылка)							КАК Назначение,
	|	ЛОЖЬ																	КАК РеализацияЗапасовДругойОрганизации,
	|	НЕОПРЕДЕЛЕНО															КАК ВидЗапасовВладельца
	|
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ВидыЗапасов
	|ГДЕ
	|	ВидыЗапасов.Ссылка = &ВидЗапасовОтгрузки
	|");
	
	Запрос.УстановитьПараметр("ВидЗапасовОтгрузки", ВидЗапасовОтгрузки);
	
	ВидЗапасов = Справочники.ВидыЗапасов.ПустаяСсылка();
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		
		Если Выборка.МатериалДавальца Тогда
			
			ВидЗапасов = Выборка.ВидЗапасов;
			
		Иначе
			
			ВидЗапасов = Справочники.ВидыЗапасов.ВидЗапасовДокумента(
				?(Организация = Неопределено, Выборка.Организация, Организация),
				Выборка.ХозяйственнаяОперация,
				Выборка);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ВидЗапасов;
	
КонецФункции

// Функция определяет вид запасов по умолчанию для передачи между организациями.
//
// Параметры:
//  ОрганизацияВладелец - СправочникСсылка.Организации - Ссылка на владельца
//  ОрганизацияПродавец - СправочникСсылка.Организации - Ссылка на продавца
//
// Возвращаемое значение:
//	СправочникСсылка.ВидыЗапасов - Найденный вид запасов
//
Функция ВидЗапасовДляПередачиМеждуОрганизациямиПоУмолчанию(ОрганизацияВладелец, ОрганизацияПродавец) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 2
	|	ВидыЗапасов.Ссылка КАК ВидЗапасов
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ВидыЗапасов
	|ГДЕ
	|	Не ВидыЗапасов.ПометкаУдаления
	|	И ВидыЗапасов.РеализацияЗапасовДругойОрганизации
	|	И ВидыЗапасов.Организация = &ОрганизацияПродавец
	|	И ВидыЗапасов.ВидЗапасовВладельца.Организация = &ОрганизацияВладелец
	|");
	
	Запрос.УстановитьПараметр("ОрганизацияВладелец", ОрганизацияВладелец);
	Запрос.УстановитьПараметр("ОрганизацияПродавец", ОрганизацияПродавец);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Количество() = 1 
	   И Выборка.Следующий()
	Тогда
		ВидЗапасов = Выборка.ВидЗапасов;
	Иначе
		ВидЗапасов = Справочники.ВидыЗапасов.ПустаяСсылка();
	КонецЕсли;
	
	Возврат ВидЗапасов;

КонецФункции

// Процедура устанавливает пометку на удаление для найденных элементов справочника.
//
// Параметры:
//	СтруктураПараметров - Структура - Параметры выбора элементов справочника
//	ПометкаУдаления - Булево - Признак установки пометки на удаление
//
Процедура УстановитьПометкуУдаления(СтруктураПараметров, ПометкаУдаления) Экспорт

	УстановитьПривилегированныйРежим(Истина);

	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Таблица.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.ВидыЗапасов КАК Таблица
	|ГДЕ
	|	Таблица.ПометкаУдаления <> &ПометкаУдаления
	|");
	Если СтруктураПараметров.Свойство("Организация") Тогда

		Запрос.УстановитьПараметр("Организация", СтруктураПараметров.Организация);
		Запрос.Текст = Запрос.Текст + " И (Таблица.Организация = &Организация ИЛИ Таблица.Комитент = &Организация ИЛИ Таблица.Поставщик = &Организация)";

	КонецЕсли;
	Если СтруктураПараметров.Свойство("Партнер") Тогда

		Запрос.УстановитьПараметр("Партнер", СтруктураПараметров.Партнер);
		Запрос.Текст = Запрос.Текст + " И (Таблица.Комитент = &Партнер ИЛИ Таблица.Поставщик = &Партнер)";

	КонецЕсли;
	Если СтруктураПараметров.Свойство("Соглашение") Тогда

		Запрос.УстановитьПараметр("Соглашение", СтруктураПараметров.Соглашение);
		Запрос.Текст = Запрос.Текст + " И Таблица.Соглашение = &Соглашение";

	КонецЕсли;
	Если СтруктураПараметров.Свойство("Договор") Тогда

		Запрос.УстановитьПараметр("Договор", СтруктураПараметров.Договор);
		Запрос.Текст = Запрос.Текст + " И Таблица.Договор = &Договор";

	КонецЕсли;
	Если СтруктураПараметров.Свойство("Назначение") Тогда

		Запрос.УстановитьПараметр("Назначение", СтруктураПараметров.Назначение);
		Запрос.Текст = Запрос.Текст + " И Таблица.Назначение = &Назначение";

	КонецЕсли;
	Запрос.УстановитьПараметр("ПометкаУдаления", ПометкаУдаления);

	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл

		Выборка.Ссылка.ПолучитьОбъект().УстановитьПометкуУдаления(ПометкаУдаления);

	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеИнформационнойБазы

Процедура ЗаполнитьНалогообложениеНДС() Экспорт
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьРаздельныйУчетПоНалогообложению") Тогда
	
		Запрос = Новый Запрос("
		|ВЫБРАТЬ
		|	ДанныеРегистра.КлючАналитики КАК КлючАналитики,
		|	ДанныеРегистра.Организация КАК Организация,
		|	ДанныеРегистра.ТипЗапасов КАК ТипЗапасов,
		|	ДанныеРегистра.НалогообложениеНДС КАК НалогообложениеНДС,
		|	ДанныеРегистра.Поставщик КАК Поставщик,
		|	ДанныеРегистра.Соглашение КАК Соглашение,
		|	ДанныеРегистра.Валюта КАК Валюта,
		|	ДанныеРегистра.АналитикаПредназначения КАК АналитикаПредназначения,
		|	ДанныеРегистра.УдалитьДеятельностьОблагаетсяЕНВД КАК УдалитьДеятельностьОблагаетсяЕНВД ,
		|	ДанныеРегистра.ГруппаФинансовогоУчета КАК ГруппаФинансовогоУчета,
		|	ДанныеРегистра.Контрагент КАК Контрагент,
		|	ДанныеРегистра.Договор КАК Договор
		|ИЗ
		|	РегистрСведений.АналитикаВидовЗапасов КАК ДанныеРегистра
		|ГДЕ
		|	ДанныеРегистра.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.Товар)
		|	И ДанныеРегистра.УдалитьДеятельностьОблагаетсяЕНВД 
		|	И ДанныеРегистра.НалогообложениеНДС = ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
		|	И ДанныеРегистра.Организация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
		|");
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СправочникОбъект = Выборка.КлючАналитики.ПолучитьОбъект();
			СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД;
			
			Попытка
				СправочникОбъект.Записать();
			Исключение
			КонецПопытки;
			
			МенеджерЗаписи = РегистрыСведений.АналитикаВидовЗапасов.СоздатьМенеджерЗаписи();
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
			МенеджерЗаписи.Удалить();
			
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
			МенеджерЗаписи.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД;
			МенеджерЗаписи.Записать(Ложь);
			
		КонецЦикла;
		
	КонецЕсли;
		
КонецПроцедуры

Процедура ЗаполнитьНалогообложениеНДСЗапасыДругойОрганизации() Экспорт
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьРаздельныйУчетПоНалогообложению") Тогда
	
		Запрос = Новый Запрос("
		|ВЫБРАТЬ
		|	ДанныеСправочника.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.ВидыЗапасов КАК ДанныеСправочника
		|ГДЕ
		|	ДанныеСправочника.УдалитьДеятельностьОблагаетсяЕНВД
		|	И ДанныеСправочника.РеализацияЗапасовДругойОрганизации
		|	И ДанныеСправочника.НалогообложениеНДС = ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
		|");
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СправочникОбъект = Выборка.Ссылка.ПолучитьОбъект();
			СправочникОбъект.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД;
			СправочникОбъект.Наименование = ПолучитьНаименованиеВидаЗапасов(СправочникОбъект);
			СправочникОбъект.ОбменДанными.Загрузка = Истина;
			СправочникОбъект.Записать();
			
		КонецЦикла;
		
	КонецЕсли;
		
КонецПроцедуры

// Обработчик обновления УТ 11.1.6.13
//
// Удаляет обособление по поставщику для видов запасов по работам
//
Процедура УдалитьОбособлениеПоПоставщикуДляРабот() Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Т.Организация,
	|	Т.ТипЗапасов,
	|	Т.НалогообложениеНДС,
	|	Т.Поставщик,
	|	Т.Соглашение,
	|	Т.Валюта,
	|	Т.АналитикаПредназначения,
	|	Т.УдалитьДеятельностьОблагаетсяЕНВД,
	|	Т.ГруппаФинансовогоУчета,
	|	Т.Контрагент,
	|	Т.Договор,
	|	Т.КлючАналитики
	|ИЗ
	|	РегистрСведений.АналитикаВидовЗапасов КАК Т
	|ГДЕ
	|	Т.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.Услуга)
	|	И Т.Поставщик <> НЕОПРЕДЕЛЕНО");
	Выборка = Запрос.Выполнить().Выбрать();
	
	НачатьТранзакцию();
	Пока Выборка.Следующий() Цикл
		
		ВидЗапасов = Выборка.КлючАналитики.ПолучитьОбъект();
		ВидЗапасов.Поставщик = Неопределено;
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(ВидЗапасов);
		
		МенеджерЗаписи = РегистрыСведений.АналитикаВидовЗапасов.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
		МенеджерЗаписи.Удалить();
		
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
		МенеджерЗаписи.Поставщик = Неопределено;
		МенеджерЗаписи.Записать(Истина);
		
	КонецЦикла;
	
	Если Выборка.Количество() > 0 Тогда
		Справочники.ВидыЗапасов.ЗаменитьДублиКлючейАналитики();
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ДанныеСправочника.Ссылка КАК Ссылка,
	|	ДанныеСправочника.Организация КАК Организация,
	|	ДанныеСправочника.ТипЗапасов КАК ТипЗапасов,
	|	ДанныеСправочника.Комитент КАК Комитент,
	|	ДанныеСправочника.Контрагент КАК Контрагент,
	|	ДанныеСправочника.Соглашение КАК Соглашение,
	|	ДанныеСправочника.Договор КАК Договор,
	|	ДанныеСправочника.Валюта КАК Валюта,
	|	ДанныеСправочника.НалогообложениеНДС КАК НалогообложениеНДС,
	|	Неопределено КАК НалогообложениеОрганизации,
	|	ВЫБОР КОГДА ДанныеСправочника.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|		ДанныеСправочника.Комитент
	|	ИНАЧЕ
	|		ДанныеСправочника.Поставщик
	|	КОНЕЦ КАК Поставщик,
	|	ДанныеСправочника.Предназначение КАК Предназначение,
	|	ДанныеСправочника.Менеджер КАК Менеджер,
	|	ДанныеСправочника.Подразделение КАК Подразделение,
	|	ДанныеСправочника.Сделка КАК Сделка,
	|	ДанныеСправочника.Назначение КАК Назначение,
	|	Ложь КАК ОбособленныйУчетТоваровПоСделке,
	|	Неопределено КАК ВариантОбособленногоУчетаТоваров,
	|	ДанныеСправочника.ГруппаФинансовогоУчета КАК ГруппаФинансовогоУчета
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ДанныеСправочника
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаВидовЗапасов КАК ДанныеРегистра
	|		ПО ДанныеСправочника.Ссылка = ДанныеРегистра.КлючАналитики
	|ГДЕ
	|	ДанныеСправочника.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.Услуга)
	|	И ДанныеРегистра.КлючАналитики ЕСТЬ NULL
	|	И НЕ ДанныеСправочника.ПометкаУдаления
	|");
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		СтруктураВидЗапасов = Справочники.ВидыЗапасов.СтруктураВидаЗапасов(
			Выборка.Организация,
			Неопределено, // Хозяйственная операция
			Выборка);
		
		МенеджерЗаписи = РегистрыСведений.АналитикаВидовЗапасов.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, СтруктураВидЗапасов);
		МенеджерЗаписи.КлючАналитики = Выборка.Ссылка;
		МенеджерЗаписи.Записать(Истина);
		
	КонецЦикла;
	
	ЗафиксироватьТранзакцию();
	
КонецПроцедуры

#КонецОбласти

#Область ЗаменаДублейВидовЗапасов

Процедура ЗаменитьДублиКлючейАналитики() Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДанныеСправочника.Ссылка КАК Ссылка,
	|	ДанныеСправочника.ПометкаУдаления,
	|	ДанныеСправочника.Организация,
	|	ДанныеСправочника.ТипЗапасов,
	|	ДанныеСправочника.НалогообложениеНДС,
	|	ДанныеСправочника.Соглашение,
	|	ДанныеСправочника.Валюта,
	|
	|	ВЫБОР КОГДА ДанныеСправочника.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|		ДанныеСправочника.Комитент
	|	ИНАЧЕ
	|		ВЫБОР КОГДА ДанныеСправочника.Поставщик ССЫЛКА Справочник.Партнеры
	|			И ДанныеСправочника.Поставщик <> ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка)
	|		ТОГДА
	|			ДанныеСправочника.Поставщик
	|		КОГДА ДанныеСправочника.Поставщик ССЫЛКА Справочник.Организации
	|			И ДанныеСправочника.Поставщик <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|		ТОГДА
	|			ДанныеСправочника.Поставщик	
	|		ИНАЧЕ
	|			Неопределено
	|		КОНЕЦ
	|	КОНЕЦ КАК Поставщик,
	|
	|	ВЫБОР КОГДА ДанныеСправочника.Предназначение = ЗНАЧЕНИЕ(Перечисление.ТипыПредназначенияВидовЗапасов.ПредназначенДляСделки) ТОГДА
	|		ДанныеСправочника.Сделка
	|	КОГДА ДанныеСправочника.Предназначение = ЗНАЧЕНИЕ(Перечисление.ТипыПредназначенияВидовЗапасов.ПредназначенДляМенеджера) ТОГДА
	|		ДанныеСправочника.Менеджер
	|	КОГДА ДанныеСправочника.Предназначение = ЗНАЧЕНИЕ(Перечисление.ТипыПредназначенияВидовЗапасов.ПредназначенДляПодразделения) ТОГДА
	|		ДанныеСправочника.Подразделение
	|	ИНАЧЕ
	|		Неопределено
	|	КОНЕЦ КАК АналитикаПредназначения
	|
	|ПОМЕСТИТЬ СвободныеВидыЗапасов
	|ИЗ
	|	Справочник.ВидыЗапасов КАК ДанныеСправочника
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрСведений.АналитикаВидовЗапасов КАК ДанныеРегистра
	|	ПО
	|		ДанныеСправочника.Ссылка = ДанныеРегистра.КлючАналитики
	|ГДЕ
	|	ДанныеРегистра.КлючАналитики ЕСТЬ NULL
	|	И Не ДанныеСправочника.РеализацияЗапасовДругойОрганизации
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СвободныеВидыЗапасов.Ссылка КАК Ссылка,
	|	СвободныеВидыЗапасов.ПометкаУдаления КАК ПометкаУдаления,
	|	Аналитика.КлючАналитики КАК КлючАналитики
	|ИЗ
	|	СвободныеВидыЗапасов КАК СвободныеВидыЗапасов
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		РегистрСведений.АналитикаВидовЗапасов КАК Аналитика
	|	ПО
	|		СвободныеВидыЗапасов.Организация = Аналитика.Организация
	|		И СвободныеВидыЗапасов.ТипЗапасов = Аналитика.ТипЗапасов
	|		И СвободныеВидыЗапасов.НалогообложениеНДС = Аналитика.НалогообложениеНДС
	|		И СвободныеВидыЗапасов.Поставщик = Аналитика.Поставщик
	|		И СвободныеВидыЗапасов.Соглашение = Аналитика.Соглашение
	|		И СвободныеВидыЗапасов.Валюта = Аналитика.Валюта
	|		И СвободныеВидыЗапасов.АналитикаПредназначения = Аналитика.АналитикаПредназначения
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ДанныеСправочника.Ссылка КАК Ссылка,
	|	ДанныеСправочника.ПометкаУдаления КАК ПометкаУдаления,
	|	ДанныеСправочника.Организация КАК ОрганизацияПродавец,
	|	ДанныеСправочника.НалогообложениеНДС КАК НалогообложениеНДС,
	|	СвободныеВидыЗапасов.Ссылка КАК ВидЗапасовВладельца
	|ИЗ
	|	СвободныеВидыЗапасов КАК СвободныеВидыЗапасов
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		РегистрСведений.АналитикаВидовЗапасов КАК Аналитика
	|	ПО
	|		СвободныеВидыЗапасов.Организация = Аналитика.Организация
	|		И СвободныеВидыЗапасов.ТипЗапасов = Аналитика.ТипЗапасов
	|		И СвободныеВидыЗапасов.НалогообложениеНДС = Аналитика.НалогообложениеНДС
	|		И СвободныеВидыЗапасов.Поставщик = Аналитика.Поставщик
	|		И СвободныеВидыЗапасов.Соглашение = Аналитика.Соглашение
	|		И СвободныеВидыЗапасов.Валюта = Аналитика.Валюта
	|		И СвободныеВидыЗапасов.АналитикаПредназначения = Аналитика.АналитикаПредназначения
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		Справочник.ВидыЗапасов КАК ДанныеСправочника
	|	ПО
	|		СвободныеВидыЗапасов.Ссылка = ДанныеСправочника.ВидЗапасовВладельца
	|");
	
	// Сформируем соответствие ключей аналитики.
	СоответствиеАналитик = Новый Соответствие;
	
	МассивРезультатов = Запрос.ВыполнитьПакет();
	// МассивРезультатов[0] - СвободныеВидыЗапасов
	РезультатЗапросаВидыЗапасовКУдалению = МассивРезультатов[1];
	РезультатЗапросаИнтеркампани = МассивРезультатов[2];
	
	Если Не РезультатЗапросаВидыЗапасовКУдалению.Пустой() Тогда
	
		// Собственные виды запасов.
		Выборка = РезультатЗапросаВидыЗапасовКУдалению.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СоответствиеАналитик.Вставить(Выборка.Ссылка, Выборка.КлючАналитики);
			
			Если Не Выборка.ПометкаУдаления Тогда
				СправочникОбъект = Выборка.Ссылка.ПолучитьОбъект();
				Попытка
					СправочникОбъект.УстановитьПометкуУдаления(Истина, Ложь);
				Исключение
				КонецПопытки;
			КонецЕсли;

		КонецЦикла;
		
		// Виды запасов интеркампани.
		Выборка = РезультатЗапросаИнтеркампани.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			ВидЗапасовВладельца = СоответствиеАналитик[Выборка.ВидЗапасовВладельца];
			ВидЗапасов = ВидЗапасовДляПередачиМеждуОрганизациями(
				ВидЗапасовВладельца,
				Выборка.ОрганизацияПродавец,
				Выборка.НалогообложениеНДС);
			Если ЗначениеЗаполнено(ВидЗапасов) Тогда
				СоответствиеАналитик.Вставить(Выборка.Ссылка, ВидЗапасов);
			КонецЕсли;
			
			Если Не Выборка.ПометкаУдаления Тогда
				СправочникОбъект = Выборка.Ссылка.ПолучитьОбъект();
				Попытка
					СправочникОбъект.УстановитьПометкуУдаления(Истина, Ложь);
				Исключение
				КонецПопытки;
			КонецЕсли;

		КонецЦикла;
		
		Исключения = Новый Массив;
		Если Константы.УчитыватьСебестоимостьТоваровПоВидамЗапасов.Получить() Тогда
			Исключения.Добавить(Метаданные.РегистрыСведений.СтоимостьТоваров);
		КонецЕсли;
		ОбщегоНазначенияУТ.ЗаменитьСсылки(СоответствиеАналитик, Исключения);
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область Прочее

Функция СтруктураВидаЗапасов(Организация, ХозяйственнаяОперация, РеквизитыДокумента) Экспорт
	
	СтруктураВидЗапасов = Новый Структура("
		|Организация,
		|ТипЗапасов,
		|Комитент,
		|Контрагент,
		|Соглашение,
		|Договор,
		|Валюта,
		|НалогообложениеНДС,
		|Поставщик,
		|Предназначение,
		|Менеджер,
		|Подразделение,
		|Сделка,
		|Назначение,
		|АналитикаПредназначения,
		|ГруппаФинансовогоУчета,
		|ГруппаПродукции
		|");
	СтруктураВидЗапасов.Организация = Организация;
	СтруктураВидЗапасов.Комитент = Неопределено;
	СтруктураВидЗапасов.Контрагент = Неопределено;
	СтруктураВидЗапасов.Поставщик = Неопределено;
	СтруктураВидЗапасов.Соглашение = Справочники.СоглашенияСПоставщиками.ПустаяСсылка();
	СтруктураВидЗапасов.Договор = Справочники.ДоговорыКонтрагентов.ПустаяСсылка();
	СтруктураВидЗапасов.Валюта = Справочники.Валюты.ПустаяСсылка();
	СтруктураВидЗапасов.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПустаяСсылка();
	СтруктураВидЗапасов.ТипЗапасов = Перечисления.ТипыЗапасов.Товар;
	
	СтруктураВидЗапасов.Сделка = Справочники.СделкиСКлиентами.ПустаяСсылка();
	СтруктураВидЗапасов.Менеджер = Справочники.Пользователи.ПустаяСсылка();
	СтруктураВидЗапасов.Подразделение = Справочники.СтруктураПредприятия.ПустаяСсылка();
	СтруктураВидЗапасов.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначениеНеОграничено;
	СтруктураВидЗапасов.АналитикаПредназначения = Неопределено;
	СтруктураВидЗапасов.ГруппаФинансовогоУчета = Справочники.ГруппыФинансовогоУчетаНоменклатуры.ПустаяСсылка();
	СтруктураВидЗапасов.ГруппаПродукции = Справочники.ГруппыАналитическогоУчетаНоменклатуры.ПустаяСсылка();
	
	Если РеквизитыДокумента <> Неопределено Тогда
		
		Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПриемНаКомиссию
		 ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровКомитенту
		 ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПередачаНаКомиссиюВДругуюОрганизацию
		 ИЛИ РеквизитыДокумента.ТипЗапасов = Перечисления.ТипыЗапасов.КомиссионныйТовар Тогда
			СтруктураВидЗапасов.ТипЗапасов = Перечисления.ТипыЗапасов.КомиссионныйТовар;
			СтруктураВидЗапасов.Комитент = РеквизитыДокумента.Поставщик;
			СтруктураВидЗапасов.Контрагент = ?(ЗначениеЗаполнено(РеквизитыДокумента.Контрагент), РеквизитыДокумента.Контрагент, Неопределено);
			СтруктураВидЗапасов.Соглашение = ?(ЗначениеЗаполнено(РеквизитыДокумента.Соглашение), РеквизитыДокумента.Соглашение, Справочники.СоглашенияСПоставщиками.ПустаяСсылка());
			СтруктураВидЗапасов.Договор = ?(ЗначениеЗаполнено(РеквизитыДокумента.Договор), РеквизитыДокумента.Договор, Справочники.ДоговорыКонтрагентов.ПустаяСсылка());
			СтруктураВидЗапасов.Валюта = РеквизитыДокумента.Валюта;
			СтруктураВидЗапасов.НалогообложениеНДС = РеквизитыДокумента.НалогообложениеНДС;
			СтруктураВидЗапасов.Поставщик = РеквизитыДокумента.Поставщик;
		Иначе
			Если ЗначениеЗаполнено(РеквизитыДокумента.ТипЗапасов) Тогда
				СтруктураВидЗапасов.ТипЗапасов = РеквизитыДокумента.ТипЗапасов;
			Иначе
				СтруктураВидЗапасов.ТипЗапасов = Перечисления.ТипыЗапасов.Товар;
			КонецЕсли;
			Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПоставщикам") Тогда
				СтруктураВидЗапасов.Поставщик = ?(ЗначениеЗаполнено(РеквизитыДокумента.Поставщик), РеквизитыДокумента.Поставщик, Неопределено);
			КонецЕсли;
			Если РеквизитыДокумента.НалогообложениеНДС = РеквизитыДокумента.НалогообложениеОрганизации
			 ИЛИ Не ПолучитьФункциональнуюОпцию("ИспользоватьРаздельныйУчетПоНалогообложению") Тогда
				СтруктураВидЗапасов.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПустаяСсылка();
			Иначе
				СтруктураВидЗапасов.НалогообложениеНДС = РеквизитыДокумента.НалогообложениеНДС;
			КонецЕсли;
		КонецЕсли;
		
		СтруктураВидЗапасов.ГруппаФинансовогоУчета = РеквизитыДокумента.ГруппаФинансовогоУчета;
		
		Если ЗначениеЗаполнено(РеквизитыДокумента.Назначение) Тогда
			СтруктураВидЗапасов.Назначение = РеквизитыДокумента.Назначение;
			СтруктураВидЗапасов.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляЗаказа;
			СтруктураВидЗапасов.АналитикаПредназначения = РеквизитыДокумента.Назначение;
			
		ИначеЕсли (РеквизитыДокумента.ОбособленныйУчетТоваровПоСделке
		 И ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоСделкам"))
		 ИЛИ РеквизитыДокумента.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляСделки Тогда
			СтруктураВидЗапасов.Сделка = РеквизитыДокумента.Сделка;
			СтруктураВидЗапасов.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляСделки;
			СтруктураВидЗапасов.АналитикаПредназначения = РеквизитыДокумента.Сделка;
			
		ИначеЕсли (РеквизитыДокумента.ВариантОбособленногоУчетаТоваров = Перечисления.ВариантыОбособленногоУчетаТоваров.ПоМенеджерамПодразделения
		 И ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПодразделениямМенеджерам"))
		 ИЛИ РеквизитыДокумента.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляМенеджера Тогда
			СтруктураВидЗапасов.Менеджер = РеквизитыДокумента.Менеджер;
			СтруктураВидЗапасов.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляМенеджера;
			СтруктураВидЗапасов.АналитикаПредназначения = РеквизитыДокумента.Менеджер;
			
		ИначеЕсли (РеквизитыДокумента.ВариантОбособленногоУчетаТоваров = Перечисления.ВариантыОбособленногоУчетаТоваров.ПоПодразделению
		 И ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПодразделениямМенеджерам"))
		 ИЛИ РеквизитыДокумента.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляПодразделения Тогда
			СтруктураВидЗапасов.Подразделение = РеквизитыДокумента.Подразделение;
			СтруктураВидЗапасов.Предназначение = Перечисления.ТипыПредназначенияВидовЗапасов.ПредназначенДляПодразделения;
			СтруктураВидЗапасов.АналитикаПредназначения = РеквизитыДокумента.Подразделение;
		
		КонецЕсли;
				
	КонецЕсли;
	
	Возврат СтруктураВидЗапасов;
	
КонецФункции

Функция СоздатьКлючАналитики(ПараметрыАналитики)

	МенеджерЗаписи = РегистрыСведений.АналитикаВидовЗапасов.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ПараметрыАналитики);
	Если ЗначениеЗаполнено(ПараметрыАналитики.Комитент) Тогда
		МенеджерЗаписи.Поставщик = ПараметрыАналитики.Комитент;
	КонецЕсли;
	
	СправочникОбъект = Справочники.ВидыЗапасов.СоздатьЭлемент();
	ЗаполнитьЗначенияСвойств(СправочникОбъект, ПараметрыАналитики);
	СправочникОбъект.Наименование = ПолучитьНаименованиеВидаЗапасов(СправочникОбъект);
	СправочникОбъект.Записать();

	МенеджерЗаписи.КлючАналитики = СправочникОбъект.Ссылка;
	МенеджерЗаписи.Записать(Ложь);

	Возврат СправочникОбъект.Ссылка;

КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли
