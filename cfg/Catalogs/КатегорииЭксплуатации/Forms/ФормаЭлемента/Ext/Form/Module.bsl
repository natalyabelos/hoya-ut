﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриСозданииЧтенииНаСервере();
	КонецЕсли;
	
	
	Элементы.ГруппаОтражениеВРеглУчета.Видимость = ПолучитьФункциональнуюОпцию("УправлениеПредприятием");
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриСозданииЧтенииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ИнвентарныйУчетПриИзменении(Элемент)
	
	
	Возврат; // Для УТ обработчик пустой
	
КонецПроцедуры

&НаКлиенте
Процедура СпособПогашенияСтоимостиПриИзменении(Элемент)
	
	
	Возврат; // Для УТ обработчик пустой
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НастроитьСчетаРеглУчетаПоОрганизациям(Команда)
	
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриСозданииЧтенииНаСервере()
	
	
	Возврат; // Для УТ обработчик пустой
	
КонецПроцедуры


#КонецОбласти
