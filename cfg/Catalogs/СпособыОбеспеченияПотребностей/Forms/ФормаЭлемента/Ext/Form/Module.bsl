﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	СформироватьЗаголовкиПоясняющихНадписей();

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	ПриЧтенииСозданииНаСервере();

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	Элементы.ПояснениеОрганизация.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций");
	
	ДатаСегодня = НачалоДня(ТекущаяДатаСеанса());
	Справочники.СпособыОбеспеченияПотребностей.АктуализироватьГрафикЗаказовНаСервере(Объект, ДатаСегодня);

	ПереключательЗаказыФормируютсяНеПоПлану = ?(Объект.ФормироватьПлановыеЗаказы, 0, 1);
	ПереключательЗаказыФормируютсяПоПлану   = ?(Объект.ФормироватьПлановыеЗаказы, 1, 0);

	ОдинИсточник = ЗначениеЗаполнено(Объект.ИсточникОбеспеченияПотребностей);
	ПереключательОдинСклад = ?(ОдинИсточник, 1, 0);
	ПереключательНесколькоСкладов = ?(ОдинИсточник, 0, 1);
	ПереключательОдинПоставщик = ?(ОдинИсточник, 1, 0);
	ПереключательНесколькоПоставщиков = ?(ОдинИсточник, 0, 1);

	//Заполнение списка выбора доступных типов обеспечения.
	СписокВыбора = Элементы.ТипОбеспечения.СписокВыбора;

	//Ограничиваем возможные типы обеспечения в зависимости от функциональных опций.
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьПроизводство") Тогда
		СписокВыбора.Удалить(СписокВыбора.НайтиПоЗначению(Перечисления.ТипыОбеспечения.Производство));
	КонецЕсли;
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьПеремещениеТоваров")
			И Не ПолучитьФункциональнуюОпцию("ИспользоватьЗаказыНаПеремещение") Тогда
		СписокВыбора.Удалить(СписокВыбора.НайтиПоЗначению(Перечисления.ТипыОбеспечения.Перемещение));
	КонецЕсли;
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСборкуРазборку")
		И Не ПолучитьФункциональнуюОпцию("ИспользоватьЗаказыНаСборку") Тогда
		СписокВыбора.Удалить(СписокВыбора.НайтиПоЗначению(Перечисления.ТипыОбеспечения.СборкаРазборка));
	КонецЕсли;
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьЗаказыПоставщикам") Тогда
		СписокВыбора.Удалить(СписокВыбора.НайтиПоЗначению(Перечисления.ТипыОбеспечения.Покупка));
	КонецЕсли;

	//Приведение значения типа обеспечения к допустимому.
	Если СписокВыбора.НайтиПоЗначению(Объект.ТипОбеспечения) = Неопределено Тогда
		Если СписокВыбора.НайтиПоЗначению(Перечисления.ТипыОбеспечения.Покупка) <> Неопределено Тогда
			Объект.ТипОбеспечения = Перечисления.ТипыОбеспечения.Покупка;
		ИначеЕсли СписокВыбора.Количество() > 0 Тогда
			Объект.ТипОбеспечения = СписокВыбора[0].Значение;
		КонецЕсли;
	КонецЕсли;

	Если СписокВыбора.Количество() = 1 Тогда
		Элементы.ТипОбеспечения.Видимость = Ложь;
		Элементы.ПояснениеТипОбеспечения.Заголовок =
			?(Объект.ТипОбеспечения = Перечисления.ТипыОбеспечения.Покупка,
				НСтр("ru = 'Данный способ обеспечения позволяет формировать заказы поставщику.'"),
			?(Объект.ТипОбеспечения = Перечисления.ТипыОбеспечения.Перемещение,
				НСтр("ru = 'Данный способ обеспечения позволяет формировать заказы на перемещение.'"),
			?(Объект.ТипОбеспечения = Перечисления.ТипыОбеспечения.СборкаРазборка,
				НСтр("ru = 'Данный способ обеспечения позволяет формировать заказы на сборку.'"),
			?(Объект.ТипОбеспечения = Перечисления.ТипыОбеспечения.Производство,
				НСтр("ru = 'Данный способ обеспечения позволяет формировать заказы на производство.'"), ""))));
	КонецЕсли;

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, ОдинИсточник);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

	Элементы.Поставщик.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.Партнеры");
	Элементы.Склад.ОграничениеТипа     = Новый ОписаниеТипов("СправочникСсылка.Склады");

	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Если ТекущийОбъект.ФормироватьПлановыеЗаказы Тогда
		ТекущийОбъект.ОбеспечиваемыйПериод = 0;
		ТекущийОбъект.ГарантированныйСрокОтгрузки = 0;
	Иначе
		//Очистка дат по графику заказов.
		ТекущийОбъект.ПлановаяДатаЗаказа    = '00010101';
		ТекущийОбъект.ПлановаяДатаПоставки  = '00010101';
		ТекущийОбъект.ДатаСледующейПоставки = '00010101';
	КонецЕсли;

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ПараметрыЗаписи.Вставить("ТипОбеспечения", Объект.ТипОбеспечения);
	Оповестить("Запись_СпособОбеспеченияПотребностей", ПараметрыЗаписи, Объект.Ссылка);

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)

	ПроверитьКорректностьЗаполненияДатПоставки(Объект, Отказ);

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ТипОбеспеченияПриИзменении(Элемент)

	Объект.ИсточникОбеспеченияПотребностей = Неопределено;
	Объект.Соглашение =  ПредопределенноеЗначение("Справочник.СоглашенияСПоставщиками.ПустаяСсылка");
	Объект.ВидЦеныПоставщика = ПредопределенноеЗначение("Справочник.ВидыЦенПоставщиков.ПустаяСсылка");

	Если Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Покупка") Тогда
		ОдинИсточник = ПереключательОдинПоставщик;
	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Перемещение") Тогда
		ОдинИсточник = ПереключательОдинСклад;
	Иначе
		ОдинИсточник = Ложь;
	КонецЕсли;

	
	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, ОдинИсточник);

КонецПроцедуры

&НаКлиенте
Процедура СрокПокупкиПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура СрокПеремещенияПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура СрокСборкиПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура СрокПроизводстваПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура СрокИсполненияЗаказаПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ОбеспечиваемыйПериодПриИзменении(Элемент)

	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ПереключательЗаказыФормируютсяНеПоПлануПриИзменении(Элемент)

	ПереключательЗаказыФормируютсяНеПоПлану = 1;
	ПереключательЗаказыФормируютсяПоПлану   = 0;
	
	Объект.ПлановаяДатаПоставки  = '00010101'; //очистка даты
	Объект.ДатаСледующейПоставки = '00010101'; //очистка даты
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);

	Объект.ФормироватьПлановыеЗаказы        = Ложь;
	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ПереключательЗаказыФормируютсяПоПлануПриИзменении(Элемент)

	ПереключательЗаказыФормируютсяНеПоПлану = 0;
	ПереключательЗаказыФормируютсяПоПлану   = 1;

	Объект.ОбеспечиваемыйПериод = 0;
	Объект.ГарантированныйСрокОтгрузки = 0;

	Объект.ФормироватьПлановыеЗаказы        = Истина;

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура РежимИспользованияНесколькоСкладовПриИзменении(Элемент)

	ПереключательОдинСклад        = 0;
	ПереключательНесколькоСкладов = 1;

	Объект.ИсточникОбеспеченияПотребностей = Неопределено;

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, Ложь);

КонецПроцедуры

&НаКлиенте
Процедура РежимИспользованияОдинСкладПриИзменении(Элемент)

	ПереключательОдинСклад        = 1;
	ПереключательНесколькоСкладов = 0;

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, Истина);

КонецПроцедуры

&НаКлиенте
Процедура РежимИспользованияОдинПоставщикПриИзменении(Элемент)

	ПереключательОдинПоставщик        = 1;
	ПереключательНесколькоПоставщиков = 0;

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, Истина);

КонецПроцедуры

&НаКлиенте
Процедура РежимИспользованияНесколькоПоставщиковПриИзменении(Элемент)

	ПереключательОдинПоставщик        = 0;
	ПереключательНесколькоПоставщиков = 1;

	Объект.ИсточникОбеспеченияПотребностей = Неопределено;
	Объект.Соглашение =  ПредопределенноеЗначение("Справочник.СоглашенияСПоставщиками.ПустаяСсылка");
	Объект.ВидЦеныПоставщика = ПредопределенноеЗначение("Справочник.ВидыЦенПоставщиков.ПустаяСсылка");

	АктивизироватьСтраницы(Элементы, Объект.ТипОбеспечения, Объект.ФормироватьПлановыеЗаказы, Ложь);

КонецПроцедуры

&НаКлиенте
Процедура ДатаБлижайшейПоставкиПриИзменении(Элемент)

	ОчиститьСообщения();
	Объект.ПлановаяДатаЗаказа = ОпределитьДатуЗаказаПоДатеПоставки(Объект.ПлановаяДатаПоставки, Объект.СрокИсполненияЗаказа);
	ПроверитьКорректностьЗаполненияДатПоставки(Объект);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ДатаСледующейПоставкиПриИзменении(Элемент)

	ОчиститьСообщения();
	ПроверитьКорректностьЗаполненияДатПоставки(Объект);
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура СборкаРазборкаДлительностьВДняхПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ПеремещениеДлительностьВДняхПриИзменении(Элемент)

	СкорректироватьСрокИсполненияЗаказа();
	СкорректироватьГарантированныйСрокОтгрузки();
	СформироватьТекстПоясняющихНадписей(ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПроцедурыИФункцииРаботыСДатами

&НаСервереБезКонтекста
Функция ОпределитьДатуЗаказаПоДатеПоставки(ДатаПоставки, СрокИсполненияЗаказа)

	Если Не ЗначениеЗаполнено(ДатаПоставки) Тогда
		Возврат НСтр("ru = 'для расчета заполните дату ближайшей поставки'");
	КонецЕсли;

	Возврат Справочники.СпособыОбеспеченияПотребностей.ОпределитьДатуЗаказаПоДатеПоставки(ДатаПоставки, СрокИсполненияЗаказа);

КонецФункции

&НаСервереБезКонтекста
Функция ОпределитьДатуПоставкиПоДатеЗаказа(СрокИсполненияЗаказа, ОбеспечиваемыйПериод)

	ДатаЗаказа = НачалоДня(ТекущаяДатаСеанса());
	Результат = Новый Структура("ДатаПоставки, ГраницаПериода");

	КалендарьПредприятия = Константы.ОсновнойКалендарьПредприятия.Получить();

	Если ЗначениеЗаполнено(КалендарьПредприятия) Тогда

		Результат.ДатаПоставки = КалендарныеГрафики.ПолучитьДатуПоКалендарю(
			КалендарьПредприятия, ДатаЗаказа, СрокИсполненияЗаказа, Ложь);

	Иначе

		Результат.ДатаПоставки = ДатаЗаказа + СрокИсполненияЗаказа * 86400; //86400 - длительность суток в секундах

	КонецЕсли;

	Если Результат.ДатаПоставки = Неопределено Тогда

		Результат.ДатаПоставки   = НСтр("ru = 'не заполнен график работы предприятия'");
		Результат.ГраницаПериода = НСтр("ru = 'не заполнен график работы предприятия'");

	Иначе

		Если ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоСкладов") Тогда

			Результат.ДатаПоставки = Формат(Результат.ДатаПоставки, "ДЛФ=D");
			Результат.ГраницаПериода = НСтр("ru = '<варьируется по складам>'");

		ИначеЕсли ОбеспечиваемыйПериод = 0 Тогда

			Результат.ДатаПоставки = Формат(Результат.ДатаПоставки, "ДЛФ=D");
			Результат.ГраницаПериода = НСтр("ru = 'не ограничена'");

		Иначе

			СкладПоУмолчанию = Справочники.Склады.СкладПоУмолчанию();
			Если ЗначениеЗаполнено(СкладПоУмолчанию) Тогда

				КалендарьСклада = ОбщегоНазначения.ПолучитьЗначениеРеквизита(СкладПоУмолчанию, "Календарь");
				Если Не ЗначениеЗаполнено(КалендарьСклада) Тогда
					КалендарьСклада = КалендарьПредприятия;
				КонецЕсли;

				Если ЗначениеЗаполнено(КалендарьСклада) тогда

					Результат.ГраницаПериода = КалендарныеГрафики.ПолучитьДатуПоКалендарю(
						КалендарьСклада, Результат.ДатаПоставки, ОбеспечиваемыйПериод, Ложь);

					Если Результат.ГраницаПериода = Неопределено Тогда
						Результат.ГраницаПериода = НСтр("ru = 'не заполнен график работы склада'");
					Иначе
						Результат.ГраницаПериода = Формат(Результат.ГраницаПериода, "ДЛФ=D");
					КонецЕсли;

				Иначе

					Результат.ГраницаПериода = Результат.ДатаПоставки + ОбеспечиваемыйПериод * 86400; //86400 - длительность суток в секундах
					Результат.ГраницаПериода = Формат(Результат.ГраницаПериода, "ДЛФ=D");

				КонецЕсли;

			Иначе

				Результат.ГраницаПериода = НСтр("ru = '<варьируется по складам>'");

			КонецЕсли;

			Результат.ДатаПоставки = Формат(Результат.ДатаПоставки, "ДЛФ=D");

		КонецЕсли;

	КонецЕсли;

	Возврат Результат;

КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ПроверитьКорректностьЗаполненияДатПоставки(Объект, Отказ = Неопределено)

	ДатаПоставки                = Объект.ПлановаяДатаПоставки;
	ДатаСледующейПоставки       = Объект.ДатаСледующейПоставки;
	ФормироватьПлановыеЗаказы   = Объект.ФормироватьПлановыеЗаказы;
	ПлановаяДатаЗаказа          = Объект.ПлановаяДатаЗаказа;

	Если ФормироватьПлановыеЗаказы Тогда

		Если ЗначениеЗаполнено(ДатаПоставки) И ДатаПоставки < НачалоДня(ТекущаяДата()) Тогда

			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				НСтр("ru = 'Недопустимо устанавливать в график прошедшую дату.
				| Если ближайшая поставка не запланирована, необходимо оставить дату пустой.'"),
				, "Объект.ПлановаяДатаПоставки");
			Отказ = Истина;

		КонецЕсли;

		Если ЗначениеЗаполнено(ДатаСледующейПоставки) И НачалоДня(ДатаСледующейПоставки) <= НачалоДня(ДатаПоставки) Тогда

			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				НСтр("ru = 'Дата следующей поставки должна быть позднее даты ближайшей поставки.
				| Если следующая поставка не запланирована, необходимо оставить дату пустой.'"),
				, "Объект.ДатаСледующейПоставки");
			Отказ = Истина;

		КонецЕсли;

		Если ЗначениеЗаполнено(ДатаПоставки) И НачалоДня(ПлановаяДатаЗаказа) < НачалоДня(ТекущаяДата()) Тогда

			Шаблон = НСтр("ru = 'Недопустимо устанавливать в график дату поставки, дата заказа на которую просрочена(%1). Если ближайшая поставка не запланирована, необходимо оставить дату пустой.'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(Шаблон, Формат(ПлановаяДатаЗаказа, "ДЛФ=D"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Объект.ПлановаяДатаПоставки");
			Отказ = Истина;

		КонецЕсли;

	КонецЕсли;

КонецПроцедуры

#КонецОбласти


#Область ПрочиеПроцедурыИФункции

&НаКлиентеНаСервереБезКонтекста
Процедура СформироватьТекстПоясняющихНадписей(Форма)

	Форма.ДатаФормированияЗаказаИнфо = "";
	Форма.ДатаОтгрузкиПоГарантированномуСрокуИнфо = "";
	Форма.ДатаБлижайшейПоставкиИнфо  = "";
	Форма.ГраницаОбеспечиваемогоПериодаИнфо = "";

	Если Форма.Объект.ФормироватьПлановыеЗаказы Тогда

		Форма.ДатаФормированияЗаказаИнфо        = Формат(Форма.Объект.ПлановаяДатаЗаказа,    "ДЛФ=D");
		Форма.ДатаБлижайшейПоставкиИнфо         = Формат(Форма.Объект.ПлановаяДатаПоставки,  "ДЛФ=D");
		Форма.ДатаОтгрузкиПоГарантированномуСрокуИнфо = Формат(Форма.Объект.ПлановаяДатаПоставки, "ДЛФ=D");
		Форма.ГраницаОбеспечиваемогоПериодаИнфо = Формат(Форма.Объект.ДатаСледующейПоставки, "ДЛФ=D");

	Иначе

		ГарантированныйСрок = ОпределитьДатуПоставкиПоДатеЗаказа(
			Форма.Объект.ГарантированныйСрокОтгрузки, Форма.Объект.ОбеспечиваемыйПериод);

		Длительность = ОпределитьДатуПоставкиПоДатеЗаказа(Форма.Объект.СрокИсполненияЗаказа, Форма.Объект.ОбеспечиваемыйПериод);
		Форма.ДатаФормированияЗаказаИнфо        = Формат(ТекущаяДата(), "ДЛФ=D");
		Форма.ДатаБлижайшейПоставкиИнфо         = Длительность.ДатаПоставки;
		Форма.ДатаОтгрузкиПоГарантированномуСрокуИнфо = ГарантированныйСрок.ДатаПоставки;
		Форма.ГраницаОбеспечиваемогоПериодаИнфо = Длительность.ГраницаПериода;

	КонецЕсли;

Конецпроцедуры

&НаСервере
Процедура СформироватьЗаголовкиПоясняющихНадписей()

	НадписьРасчет = НСтр("ru = 'Расчет даты ближайшей возможной поставки и даты отгрузки при приеме заказов к обеспечению.'");
	НадписьДатаОтгрузки = НСтр("ru = 'Дата отгрузки заказов к обеспечению:'");

	Если Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Покупка") Тогда

		НадписьДатаФормированияЗаказа = НСтр("ru = 'Дата формирования заказа поставщику:'");
		НадписьДатаБлижайшейПоставки = НСтр("ru = 'Дата поступления по заказу:'");
		НадписьГраницаОбеспечиваемогоПериода = НСтр("ru = 'Обеспечиваемый период до:'");

	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Перемещение") Тогда

		НадписьДатаФормированияЗаказа = НСтр("ru = 'Дата формирования заказа на перемещение:'");
		НадписьДатаБлижайшейПоставки = НСтр("ru = 'Дата окончания перемещения по заказу:'");
		НадписьГраницаОбеспечиваемогоПериода = НСтр("ru = 'Обеспечиваемый период до:'");

	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.СборкаРазборка") Тогда

		НадписьДатаФормированияЗаказа = НСтр("ru = 'Дата формирования заказа на сборку:'");
		НадписьДатаБлижайшейПоставки = НСтр("ru = 'Дата окончания сборки по заказу:'");
		НадписьГраницаОбеспечиваемогоПериода = НСтр("ru = 'Обеспечиваемый период до:'");

	Иначе

		НадписьДатаФормированияЗаказа = НСтр("ru = 'Дата формирования заказа на производство:'");
		НадписьДатаБлижайшейПоставки = НСтр("ru = 'Дата выпуска продукции по заказу:'");
		НадписьГраницаОбеспечиваемогоПериода = НСтр("ru = 'Обеспечиваемый период до:'");

	КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура АктивизироватьСтраницы(Элементы, ТипОбеспечения, ФормироватьПлановыеЗаказы, ОдинИсточник = Неопределено)

	Если ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Покупка") Тогда

		Элементы.СтраницыТипОбеспечения.ТекущаяСтраница = Элементы.СтраницаПокупка;
		Элементы.СтраницыСрокОбеспечения.ТекущаяСтраница = Элементы.СтраницаСрокПокупки;

	ИначеЕсли ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Перемещение") Тогда

		Элементы.СтраницыТипОбеспечения.ТекущаяСтраница = Элементы.СтраницаПеремещение;
		Элементы.СтраницыСрокОбеспечения.ТекущаяСтраница = Элементы.СтраницаСрокПеремещения;

	ИначеЕсли ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.СборкаРазборка") Тогда

		Элементы.СтраницыТипОбеспечения.ТекущаяСтраница = Элементы.СтраницаСборка;
		Элементы.СтраницыСрокОбеспечения.ТекущаяСтраница = Элементы.СтраницаСрокСборки;


	КонецЕсли;

	Элементы.СтраницыОбеспечиваемыйПериод.ТекущаяСтраница = ?(ФормироватьПлановыеЗаказы,
		Элементы.СтраницаОбеспечиваемыйПериодНедоступен,
		Элементы.СтраницаОбеспечиваемыйПериод);

	Элементы.СтраницыДатыПоставок.ТекущаяСтраница = ?(ФормироватьПлановыеЗаказы,
		Элементы.СтраницаДатыПоставок,
		Элементы.СтраницаДатыПоставокНедоступны);

	Если ОдинИсточник <> Неопределено Тогда

		Элементы.ГруппаСклад.Доступность     = ОдинИсточник;
		Элементы.ГруппаПоставщик.Доступность = ОдинИсточник;

	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Функция ТекстОшибкиСрокаИсполненияЗаказа()

	Если Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Перемещение") Тогда
		Пояснение = НСтр("ru = 'Срок перемещения не может быть меньше длительности перемещения. Срок перемещения увеличен.'");
	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.СборкаРазборка") Тогда
		Пояснение = НСтр("ru = 'Срок сборки не может быть меньше длительности сборки/разборки. Срок сборки увеличен.'");
	Иначе
		Пояснение = "";
	КонецЕсли;

	Возврат Пояснение;

КонецФункции

&НаКлиенте
Процедура СкорректироватьСрокИсполненияЗаказа()

	Если Не Объект.ФормироватьПлановыеЗаказы
		И Объект.СрокИсполненияЗаказа < Объект.ДлительностьВДнях Тогда

		ТекстОшибки = ТекстОшибкиСрокаИсполненияЗаказа();
		Если ТекстОшибки <> "" Тогда

			ПоказатьОповещениеПользователя(НСтр("ru = 'Изменение связанных реквизитов'"),,ТекстОшибки);
			Объект.СрокИсполненияЗаказа = Объект.ДлительностьВДнях;

		КонецЕсли;

	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Функция ТекстОшибкиГарантированногоСрокаОтгрузки()

	Если Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Перемещение") Тогда
		Пояснение = НСтр("ru = 'Гарантированный срок отгрузки не может быть меньше срока перемещения. Гарантированный срок отгрузки увеличен.'");
	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.СборкаРазборка") Тогда
		Пояснение = НСтр("ru = 'Гарантированный срок отгрузки не может быть меньше срока сборки/разборки. Гарантированный срок отгрузки увеличен.'");
	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Покупка") Тогда
		Пояснение = НСтр("ru = 'Гарантированный срок отгрузки не может быть меньше срока покупки. Гарантированный срок отгрузки увеличен.'");
	ИначеЕсли Объект.ТипОбеспечения = ПредопределенноеЗначение("Перечисление.ТипыОбеспечения.Производство") Тогда
		Пояснение = НСтр("ru = 'Гарантированный срок отгрузки не может быть меньше срока производства. Гарантированный срок отгрузки увеличен.'");
	Иначе
		Пояснение = "";
	КонецЕсли;

	Возврат Пояснение;

КонецФункции

&НаКлиенте
Процедура СкорректироватьГарантированныйСрокОтгрузки()

	Если Не Объект.ФормироватьПлановыеЗаказы
		И Объект.ГарантированныйСрокОтгрузки < Объект.СрокИсполненияЗаказа Тогда

		ТекстОшибки = ТекстОшибкиГарантированногоСрокаОтгрузки();
		Если ТекстОшибки <> "" Тогда

			ПоказатьОповещениеПользователя(НСтр("ru = 'Изменение связанных реквизитов'"),,ТекстОшибки);
			Объект.ГарантированныйСрокОтгрузки = Объект.СрокИсполненияЗаказа;

		КонецЕсли;

	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#КонецОбласти
