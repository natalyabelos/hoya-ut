﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоТиповые",               Параметры.ТолькоТиповые);
	//+HLRUDEV-6 (на форму добавлен новый параметр)
	//открытИзЗаказа = ложь;
	//Если  Параметры.Свойство("ОткрытИзЗаказа", открытИзЗаказа) и fs_ОбщийМодульСервер.ИспользоватьФункционалCod() Тогда
	//	Если открытИзЗаказа Тогда
	Если fs_ОбщийМодульСервер.ИспользоватьФункционалCod() Тогда
		вставкаЗапрос = "(СправочникСоглашенияСКлиентами.Типовое или (СправочникСоглашенияСКлиентами.Контрагент = &Контрагент 	И СправочникСоглашенияСКлиентами.ГруппаФинансовогоУчета = &ГруппаФинансовогоУчета))";
		Список.ТекстЗапроса = СтрЗаменить(Список.ТекстЗапроса,"1 = 1", вставкаЗапрос);
		Список.Параметры.УстановитьЗначениеПараметра("Партнер",                Параметры.Партнер);
		Список.Параметры.УстановитьЗначениеПараметра("Контрагент",             Параметры.Партнер.fs_Контрагент);
		Список.Параметры.УстановитьЗначениеПараметра("ГруппаФинансовогоУчета", Параметры.Партнер.fs_ГруппаФинансовогоУчета);
		ПартнерРодитель =  Параметры.Партнер.Родитель;
	Иначе
		Список.Параметры.УстановитьЗначениеПараметра("Партнер",                     Параметры.Партнер);
		ПартнерРодитель = Справочники.Партнеры.ПустаяСсылка();
		Элементы.СоглашенияРодителей.Видимость = Ложь;
	КонецЕсли;
	//Иначе
	//	ЭтаФорма.Элементы.СоглашенияРодителей.Видимость = ложь;
	//	Список.Параметры.УстановитьЗначениеПараметра("Партнер",                     Параметры.Партнер);
	//	ПартнерРодитель = Справочники.Партнеры.ПустаяСсылка();
	//	Элементы.СоглашенияРодителей.Видимость = Ложь;
	//КонецЕсли;
	//-
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоИспользуемыеВРаботеТП", Параметры.ТолькоИспользуемыеВРаботеТП);
	Список.Параметры.УстановитьЗначениеПараметра("ДатаДокумента",               ?(ЗначениеЗаполнено(Параметры.ДатаДокумента), НачалоДня(Параметры.ДатаДокумента), НачалоДня(ТекущаяДата())));
	
	Если Не Параметры.ТолькоТиповые Тогда
		Партнер = Параметры.Партнер;
	КонецЕсли;
		
	Если ЗначениеЗаполнено(Параметры.ХозяйственнаяОперация) ТОгда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ХозяйственнаяОперация", Параметры.ХозяйственнаяОперация, ВидСравненияКомпоновкиДанных.Равно,, Истина);
	КонецЕсли;
	
	// Переопределение выводимых типов соглашений и установка свойств зависимых элементов
	ИспользоватьТиповыеСоглашенияСКлиентами			= ПолучитьФункциональнуюОпцию("ИспользоватьТиповыеСоглашенияСКлиентами");
	ИспользоватьИндивидуальныеСоглашенияСКлиентами	= ПолучитьФункциональнуюОпцию("ИспользоватьИндивидуальныеСоглашенияСКлиентами");
	
	ТолькоТиповые = ИспользоватьТиповыеСоглашенияСКлиентами И НЕ ИспользоватьИндивидуальныеСоглашенияСКлиентами;
	ТолькоИндивидуальные = НЕ ИспользоватьТиповыеСоглашенияСКлиентами И ИспользоватьИндивидуальныеСоглашенияСКлиентами;
	
	Если ТолькоТиповые 
		ИЛИ (Параметры.ТолькоТиповые И НЕ ТолькоИндивидуальные) Тогда
		ТипСоглашения = "Типовое";
		Параметры.ТолькоТиповые = Истина;
		ТолькоТиповые = Истина;
		
		Заголовок = НСтр("ru='Типовые соглашения с клиентами'");
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьИндивидуальноеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокКонтекстноеМенюСоздатьИндивидуальноеСоглашение", "Видимость", Ложь);
		
	ИначеЕсли ТолькоИндивидуальные Тогда
		ТипСоглашения = "Индивидуальное";
		
		Заголовок = НСтр("ru='Индивидуальные соглашения с клиентами'");
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьТиповоеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокКонтекстноеМенюСоздатьТиповоеСоглашение", "Видимость", Ложь);
		
	КонецЕсли;
	
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоТиповые", ТолькоТиповые);
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоИндивидуальные", ТолькоИндивидуальные);
	
	Если Параметры.ТолькоТиповые Тогда
		
		Заголовок = НСтр("ru='Типовые соглашения с клиентами'");
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокТипСоглашения", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьИндивидуальноеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокКонтекстноеМенюСоздатьИндивидуальноеСоглашение", "Видимость", Ложь);
	Иначе
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьТиповоеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокКонтекстноеМенюСоздатьТиповоеСоглашение", "Видимость", Ложь);
	КонецЕсли;
	
	Если НЕ ПравоДоступа("Добавление",Метаданные.Справочники.СоглашенияСКлиентами) Тогда
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьТиповоеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьИндивидуальноеСоглашение", "Видимость", Ложь);
	КонецЕсли;
	//+HLRUDEV-6
	//Если Параметры.Свойство("ОткрытИзЗаказа") Тогда                          
	//	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ПартнерОтбор", СписокПартнеров, ВидСравненияКомпоновкиДанных.ВСписке,, Истина);
	//Иначе	
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ПартнерОтбор", Параметры.Партнер, ВидСравненияКомпоновкиДанных.Равно,, Истина);
	//КонецЕсли;	
	//-
	ОтборыСписковКлиентСервер.СкопироватьСписокВыбораОтбораПоМенеджеру(
		Элементы.Менеджер.СписокВыбора,
		ОбщегоНазначенияУТ.ПолучитьСписокПользователейСПравомДобавления(Метаданные.Справочники.СоглашенияСКлиентами, НСтр("ru = '<Мои соглашения>'")));
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	Если НЕ ПраваПользователяПовтИсп.ИзменениеТиповыхСоглашений() Тогда
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокСоздатьТиповоеСоглашение", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СписокКонтекстноеМенюСоздатьТиповоеСоглашение", "Видимость", Ложь);
	КонецЕсли;
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтаФорма, "СканерШтрихкода");

КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Менеджер      = Настройки.Получить("Менеджер");
	ТипСоглашения = Настройки.Получить("ТипСоглашения");
	Организация   = Настройки.Получить("Организация");
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер", Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
	Если Не Параметры.ТолькоТиповые Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ТипСоглашения", ТипСоглашения, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(ТипСоглашения));
	КонецЕсли;
	
	МассивОрганизаций = Новый Массив();
	МассивОрганизаций.Добавить(Организация);
	МассивОрганизаций.Добавить(ПредопределенноеЗначение("Справочник.Организации.ПустаяСсылка"));
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Организация", МассивОрганизаций, ВидСравненияКомпоновкиДанных.ВСписке,,ЗначениеЗаполнено(Организация));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ТипСоглашенияПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ТипСоглашения", ТипСоглашения, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(ТипСоглашения));
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	МассивОрганизаций = Новый Массив();
	МассивОрганизаций.Добавить(Организация);
	МассивОрганизаций.Добавить(ПредопределенноеЗначение("Справочник.Организации.ПустаяСсылка"));
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Организация", МассивОрганизаций, ВидСравненияКомпоновкиДанных.ВСписке,, ЗначениеЗаполнено(Организация));
	
КонецПроцедуры

&НаКлиенте
Процедура МенеджерПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер", Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Если Копирование Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Истина;
	
	ОткрытьФорму(
		"Справочник.СоглашенияСКлиентами.ФормаОбъекта",
		Новый Структура("Типовое,ЗначенияЗаполнения", Параметры.ТолькоТиповые, Новый Структура("Типовое,Организация,Партнер", Параметры.ТолькоТиповые, Организация, Партнер)),
		,
		,);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьТиповоеСоглашение(Команда)
	
	ОткрытьФорму(
		"Справочник.СоглашенияСКлиентами.ФормаОбъекта",
		Новый Структура("Типовое,ЗначенияЗаполнения", Истина, Новый Структура("Типовое,Организация", Истина, Организация)),
		,
		,);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаКлиенте
Функция СсылкаНаЭлементСпискаПоШтрихкоду(Штрихкод)
	
	Менеджеры = Новый Массив();
	Менеджеры.Добавить(ПредопределенноеЗначение("Справочник.СоглашенияСКлиентами.ПустаяСсылка"));
	Возврат ШтрихкодированиеПечатныхФормКлиент.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(Штрихкод, Менеджеры);
	
КонецФункции

&НаКлиенте
Процедура ОбработатьШтрихкоды(Данные)
	
	МассивСсылок = СсылкаНаЭлементСпискаПоШтрихкоду(Данные.Штрихкод);
	Если МассивСсылок.Количество() > 0 Тогда
		Элементы.Список.ТекущаяСтрока = МассивСсылок[0];
	Иначе
		ШтрихкодированиеПечатныхФормКлиент.ОбъектНеНайден(Данные.Штрихкод);
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры


#КонецОбласти

//+HLRUDEV-6 (на форму добавлен новый реквизит)
&НаКлиенте
Процедура СоглашенияРодителейПриИзменении(Элемент)
	УстановитьОтборПартнеров(СоглашенияРодителей); 
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать


&НаКлиенте
Процедура УстановитьОтборПартнеров(УстановитьДляГруппы = Истина)
	
	ОбщегоНазначенияУТКлиентСервер.ПолучитьОтборДинамическогоСписка(Список).Элементы.Очистить();
	Если УстановитьДляГруппы Тогда
		//создать элемент группы - отбор по партнеру
		УстановитьПараметрОтбора(Список, ПартнерРодитель);
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ПартнерОтбор", ПартнерРодитель, ВидСравненияКомпоновкиДанных.Равно,, Истина);
	Иначе
		УстановитьПараметрОтбора(Список, Партнер);
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "ПартнерОтбор", Партнер, ВидСравненияКомпоновкиДанных.Равно,, Истина);
	КонецЕсли;
	Элементы.Список.Обновить();
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура УстановитьПараметрОтбора(Список,ПартнерОтбор)
	Список.Параметры.УстановитьЗначениеПараметра("Партнер",  ПартнерОтбор);
КонецПроцедуры
//-

#КонецОбласти
