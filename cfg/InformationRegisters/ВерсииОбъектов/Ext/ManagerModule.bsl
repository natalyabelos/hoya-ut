﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура УдалитьИнформациюОбАвтореВерсии(Знач АвторВерсии) Экспорт
	
	Если Не ОбщегоНазначенияПовтИсп.ДоступноИспользованиеРазделенныхДанных() Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	ВерсииОбъектов.Объект,
	|	ВерсииОбъектов.НомерВерсии,
	|	ВерсииОбъектов.ВерсияОбъекта,
	|	НЕОПРЕДЕЛЕНО КАК АвторВерсии,
	|	ВерсииОбъектов.ДатаВерсии,
	|	ВерсииОбъектов.Комментарий,
	|	ВерсииОбъектов.ТипВерсииОбъекта,
	|	ВерсииОбъектов.ВерсияПроигнорирована
	|ИЗ
	|	РегистрСведений.ВерсииОбъектов КАК ВерсииОбъектов
	|ГДЕ
	|	ВерсииОбъектов.АвторВерсии = &АвторВерсии";
	
	Запрос.УстановитьПараметр("АвторВерсии", АвторВерсии);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		НаборЗаписей = РегистрыСведений.ВерсииОбъектов.СоздатьНаборЗаписей();
		
		НаборЗаписей.Отбор["Объект"].Установить(Выборка["Объект"]);
		НаборЗаписей.Отбор["НомерВерсии"].Установить(Выборка["НомерВерсии"]);
		
		ЗаполнитьЗначенияСвойств(НаборЗаписей.Добавить(), Выборка);
		
		НаборЗаписей.Записать();
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

&НаСервере
Функция хоя_ЕстьИзмененияПоВерсии(Ссылка, МассивВерсий) Экспорт

	Перем ВерсияОбъекта;
	
	Перем счетчикУникальныйИд;
	
	СсылкаНаОбъект = Ссылка;
	
	МассивНомеровВерсий = МассивВерсий;
	
	КоличествоВерсийОбъекта = МассивНомеровВерсий.Количество();
	
	ТаблицаИзмененийРеквизитов = Новый ТаблицаЗначений;
	ПодготовитьКолонкиТаблицИзмененийРеквизитов(ТаблицаИзмененийРеквизитов, МассивНомеровВерсий);
	
	ТаблицаИзмененийТабличныхЧастей = Новый Соответствие;
	
	ВерсияОбъекта_Пред = СчитатьНачальныеЗначенияРеквизитовИТабличныхЧастей(
	                               ТаблицаИзмененийРеквизитов,
	                               ТаблицаИзмененийТабличныхЧастей,
	                               КоличествоВерсийОбъекта,
	                               МассивНомеровВерсий, СсылкаНаОбъект);
	
	счетчикУникальныйИд = ПолучитьУникальныеУникальныйИд(ТаблицаИзмененийТабличныхЧастей, "Версия" + Формат(МассивНомеровВерсий[0], "ЧГ=0"));
	
	Для ИндексВерсии = 2 По МассивНомеровВерсий.Количество() Цикл
		НомерВерсии = МассивНомеровВерсий[ИндексВерсии-1];
		НомерПредыдущейВерсии = "Версия" + (Формат(МассивНомеровВерсий[ИндексВерсии-2], "ЧГ=0"));
		ИмяКолонкиТекущейВерсии = "Версия" + Формат(НомерВерсии, "ЧГ=0");
		
		РезультатСравнения = РассчитатьИзменения(НомерВерсии, ВерсияОбъекта_Пред, ВерсияОбъекта, СсылкаНаОбъект);

	КонецЦикла;
	
	ЕстьИзменения = Ложь;
	
	Для каждого Соотв Из РезультатСравнения Цикл
	
		Для каждого Соотв2 Из Соотв.Значение Цикл
		
			 Если Соотв2.Значение.Количество() Тогда
			 
			 	   ЕстьИзменения = Истина;
			 
			 КонецЕсли; 
		
		КонецЦикла; 
	
	КонецЦикла; 
	
	Возврат ЕстьИзменения;
	
КонецФункции

Функция ПолучитьУникальныеУникальныйИд(ТаблицаИзмененийТЧ, ИмяКолонкиВерсии)
	
	СоответствиеУникальныйИд = Новый Соответствие;
	
	Для Каждого ЭлементСоотв Из ТаблицаИзмененийТЧ Цикл
		СоответствиеУникальныйИд[ЭлементСоотв.Ключ] = Число(ЭлементСоотв.Значение[ИмяКолонкиВерсии].Количество());
	КонецЦикла;
	
	Возврат СоответствиеУникальныйИд;
	
КонецФункции

Процедура ПодготовитьКолонкиТаблицИзмененийРеквизитов(ТаблицаЗначений,
                                                      МассивНомеровВерсий)
	
	ТаблицаЗначений = Новый ТаблицаЗначений;
	
	ТаблицаЗначений.Колонки.Добавить("Наименование");
	ТаблицаЗначений.Колонки.Добавить("Версионирование_Модификация");
	ТаблицаЗначений.Колонки.Добавить("Версионирование_ТипЗначения"); // Предполагаемый тип значения.
	
	Для Индекс = 1 По МассивНомеровВерсий.Количество() Цикл
		ТаблицаЗначений.Колонки.Добавить("Версия" + Формат(МассивНомеровВерсий[Индекс-1], "ЧГ=0"));
	КонецЦикла;
	
КонецПроцедуры

Функция СчитатьНачальныеЗначенияРеквизитовИТабличныхЧастей(ТаблицаРеквизитов,
                                                           ТаблицаТЧ,
                                                           КоличествоВерсий,
                                                           МассивНомеровВерсий, СсылкаНаОбъект)
	
	МладшаяВерсияОбъекта = МассивНомеровВерсий[0];
	
	// Выполняем разбор первой версии.
	ВерсияОбъекта  = ВерсионированиеОбъектов.РазборВерсии(СсылкаНаОбъект, МладшаяВерсияОбъекта);
	ДобавитьНомераСтрокВТабличныеЧасти(ВерсияОбъекта.ТабличныеЧасти);
	
	Реквизиты      = ВерсияОбъекта.Реквизиты;
	ТабличныеЧасти = ВерсияОбъекта.ТабличныеЧасти;
	
	Колонка = "Версия" + Формат(МассивНомеровВерсий[0], "ЧГ=0");
	
	Для Каждого СтрокаТаблицыЗначений Из Реквизиты Цикл
		
		НоваяСтрока = ТаблицаРеквизитов.Добавить();
		НоваяСтрока[Колонка] = Новый Структура("ТипИзменения, Значение", "И", СтрокаТаблицыЗначений);
		НоваяСтрока.Наименование = СтрокаТаблицыЗначений.НаименованиеРеквизита;
		НоваяСтрока.Версионирование_Модификация = Ложь;
		НоваяСтрока.Версионирование_ТипЗначения = СтрокаТаблицыЗначений.ТипРеквизита;
		
	КонецЦикла;
	
	Для Каждого ЭлементТЧ Из ТабличныеЧасти Цикл
		
		ТаблицаТЧ.Вставить(ЭлементТЧ.Ключ, Новый Соответствие);
		ПодготовитьКолонкиТаблицИзмененийДляСоответствия(ТаблицаТЧ[ЭлементТЧ.Ключ], МассивНомеровВерсий);
		ТаблицаТЧ[ЭлементТЧ.Ключ]["Версия" + Формат(МладшаяВерсияОбъекта, "ЧГ=0")] = ЭлементТЧ.Значение.Скопировать();
		
		ТекущаяТЗ = ТаблицаТЧ[ЭлементТЧ.Ключ]["Версия" + Формат(МладшаяВерсияОбъекта, "ЧГ=0")];
		
		// Спец идентификатор строки что бы различать строки
		// значение является уникальным в пределах данной таблицы значений.
		
		ТекущаяТЗ.Колонки.Добавить("Версионирование_ИдСтроки");
		ТекущаяТЗ.Колонки.Добавить("Версионирование_Модификация");
		ТекущаяТЗ.Колонки.Добавить("Версионирование_Изменения", Новый ОписаниеТипов("Массив"));
		
		Для Индекс = 1 По ТекущаяТЗ.Количество() Цикл
			ТекущаяТЗ[Индекс-1].Версионирование_ИдСтроки = Индекс;
			ТекущаяТЗ[Индекс-1].Версионирование_Модификация = Ложь;
		КонецЦикла;
	
	КонецЦикла;
	
	Возврат ВерсияОбъекта;
	
КонецФункции

Процедура ДобавитьНомераСтрокВТабличныеЧасти(ТабличныеЧасти)
	
	Для Каждого Соответствие Из ТабличныеЧасти Цикл
		Таблица = Соответствие.Значение;
		Если Таблица.Колонки.Найти("НомерСтроки") <> Неопределено Тогда
			Продолжить;
		КонецЕсли;
		Таблица.Колонки.Вставить(0, "НомерСтроки",,НСтр("ru = '№ строки'"));
		Для НомерСтроки = 1 По Таблица.Количество() Цикл
			Таблица[НомерСтроки-1].НомерСтроки = НомерСтроки;
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры

Функция РассчитатьИзменения(НомерВерсии,
                           РезультатРазбораВерсии_0,
                           РезультатРазбораВерсии_1, СсылкаНаОбъект)
	
	ЭтоДокумент = Ложь;
	
	Если Метаданные.Документы.Содержит(СсылкаНаОбъект.Метаданные()) Тогда
		ЭтоДокумент = Истина;
	КонецЕсли;
	
	// Выполняем разбор предпоследней версии.
	Реквизиты_0      = РезультатРазбораВерсии_0.Реквизиты;
	ТабличныеЧасти_0 = РезультатРазбораВерсии_0.ТабличныеЧасти;
	
	// Выполняем разбор последней версии.
	РезультатРазбораВерсии_1 = ВерсионированиеОбъектов.РазборВерсии(СсылкаНаОбъект, НомерВерсии);
	ДобавитьНомераСтрокВТабличныеЧасти(РезультатРазбораВерсии_1.ТабличныеЧасти);
	
	Реквизиты_1      = РезультатРазбораВерсии_1.Реквизиты;
	ТабличныеЧасти_1 = РезультатРазбораВерсии_1.ТабличныеЧасти;
	
	///////////////////////////////////////////////////////////////////////////////
	//           Формируем список табличных частей, которые изменились           //
	///////////////////////////////////////////////////////////////////////////////
	СписокТабличныхЧастей_0	= СоздатьТаблицуСравнения();
	Для Каждого Элемент Из ТабличныеЧасти_0 Цикл
		НоваяСтрока = СписокТабличныхЧастей_0.Добавить();
		НоваяСтрока.Установить(0, СокрЛП(Элемент.Ключ));
	КонецЦикла;
	
	СписокТабличныхЧастей_1	= СоздатьТаблицуСравнения();
	Для Каждого Элемент Из ТабличныеЧасти_1 Цикл
		НоваяСтрока = СписокТабличныхЧастей_1.Добавить();
		НоваяСтрока.Установить(0, СокрЛП(Элемент.Ключ));
	КонецЦикла;
	
	// Возможно была изменена структура метаданных - добавились или были удалены реквизиты.
	СписокДобавленныхТЧ = ВычестьТаблицу(СписокТабличныхЧастей_1, СписокТабличныхЧастей_0);
	СписокУдаленныхТЧ  = ВычестьТаблицу(СписокТабличныхЧастей_0, СписокТабличныхЧастей_1);
	
	// Список не изменившихся реквизитов, по которым будем искать совпадения / расхождения.
	СписокОставшихсяТЧ = ВычестьТаблицу(СписокТабличныхЧастей_1, СписокДобавленныхТЧ);
	
	// Список реквизитов, которые были изменены.
	СписокИзменившихсяТЧ = НайтиИзменившиесяТабличныеЧасти(СписокОставшихсяТЧ,
	                                                       ТабличныеЧасти_0,
	                                                       ТабличныеЧасти_1);
	
	///////////////////////////////////////////////////////////////////////////////
	//           Формируем список реквизитов, которые изменились                 //
	///////////////////////////////////////////////////////////////////////////////
	СписокРеквизитов0 = СоздатьТаблицуСравнения();
	Для Каждого Реквизит Из РезультатРазбораВерсии_0.Реквизиты Цикл
		НоваяСтрока = СписокРеквизитов0.Добавить();		
		НоваяСтрока.Установить(0, СокрЛП(Строка(Реквизит.НаименованиеРеквизита)));
	КонецЦикла;
	
	СписокРеквизитов1 = СоздатьТаблицуСравнения();
	Для Каждого Реквизит Из РезультатРазбораВерсии_1.Реквизиты Цикл
		НоваяСтрока = СписокРеквизитов1.Добавить();
		НоваяСтрока.Установить(0, СокрЛП(Строка(Реквизит.НаименованиеРеквизита)));
	КонецЦикла;
	
	// Возможно была изменена структура метаданных - добавились или были удалены реквизиты.
	СписокДобавленныхРеквизитов = ВычестьТаблицу(СписокРеквизитов1, СписокРеквизитов0);
	СписокУдаленныхРеквизитов  = ВычестьТаблицу(СписокРеквизитов0, СписокРеквизитов1);
	
	// Список не изменившихся реквизитов, по которым будем искать совпадения / расхождения.
	СписокОставшихсяРеквизитов = ВычестьТаблицу(СписокРеквизитов1, СписокДобавленныхРеквизитов);
	
	// Список реквизитов, которые были изменены.
	СписокИзменившихсяРеквизитов = СоздатьТаблицуСравнения();
	
	ИзмененияВРеквизитах = Новый Соответствие;
	ИзмененияВРеквизитах.Вставить("д", СписокДобавленныхРеквизитов);
	ИзмененияВРеквизитах.Вставить("у", СписокУдаленныхРеквизитов);
	ИзмененияВРеквизитах.Вставить("и", СписокИзменившихсяРеквизитов);
	
	Для Каждого СтрокаТаблицыЗначений Из СписокОставшихсяРеквизитов Цикл
		
		Реквизит = СтрокаТаблицыЗначений.Значение;
		Значение_0 = Реквизиты_0.Найти(Реквизит, "НаименованиеРеквизита").ЗначениеРеквизита;
		Значение_1 = Реквизиты_1.Найти(Реквизит, "НаименованиеРеквизита").ЗначениеРеквизита;
		
		Если ТипЗнч(Значение_0) <> Тип("ХранилищеЗначения")
			И ТипЗнч(Значение_1) <> Тип("ХранилищеЗначения") Тогда
			Если Значение_0 <> Значение_1 Тогда
				НоваяСтрока = СписокИзменившихсяРеквизитов.Добавить();
				НоваяСтрока.Установить(0, Реквизит);
			КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;
	
	ИзмененияВТаблицах = РассчитатьИзмененияТабличныхЧастей(
	                              СписокИзменившихсяТЧ,
	                              ТабличныеЧасти_0,
	                              ТабличныеЧасти_1);
	
	МодификацииТабличныхЧастей = Новый Структура;
	МодификацииТабличныхЧастей.Вставить("д", СписокДобавленныхТЧ);
	МодификацииТабличныхЧастей.Вставить("у", СписокУдаленныхТЧ);
	МодификацииТабличныхЧастей.Вставить("и", ИзмененияВТаблицах);
	
	КомпоновкаИзменений = Новый Соответствие;
	КомпоновкаИзменений.Вставить("Реквизиты",      ИзмененияВРеквизитах);
	КомпоновкаИзменений.Вставить("ТабличныеЧасти", МодификацииТабличныхЧастей);
	
	Возврат КомпоновкаИзменений;
	
КонецФункции

Функция СоздатьТаблицуСравнения(ТаблицаИнициализации = Неопределено,
                                ИмяКолонкиСравнения = "Значение")
	
	Таблица = Новый ТаблицаЗначений;
	Таблица.Колонки.Добавить(ИмяКолонкиСравнения);
	
	Если ТаблицаИнициализации <> Неопределено Тогда
		
		МассивЗначений = ТаблицаИнициализации.ВыгрузитьКолонку(ИмяКолонкиСравнения);
		
		Для Каждого Элемент Из ТаблицаИнициализации Цикл
			НоваяСтрока = Таблица.Добавить();
			НоваяСтрока.Установить(0, Элемент[ИмяКолонкиСравнения]);
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат Таблица;

КонецФункции

Функция ВычестьТаблицу(Знач ТаблицаОсновная,
                       Знач ТаблицаВычитаемая,
                       Знач КолонкаСравненияОсновнойТаблицы = "",
                       Знач КолонкаСравненияВычитаемойТаблицы = "")
	
	Если Не ЗначениеЗаполнено(КолонкаСравненияОсновнойТаблицы) Тогда
		КолонкаСравненияОсновнойТаблицы = "Значение";
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(КолонкаСравненияВычитаемойТаблицы) Тогда
		КолонкаСравненияВычитаемойТаблицы = "Значение";
	КонецЕсли;
	
	ТаблицаРезультат = Новый ТаблицаЗначений;
	ТаблицаРезультат = ТаблицаОсновная.Скопировать();
	
	Для Каждого Элемент Из ТаблицаВычитаемая Цикл
		Значение = Элемент[КолонкаСравненияОсновнойТаблицы];
		НайденнаяСтрока = ТаблицаРезультат.Найти(Значение, КолонкаСравненияОсновнойТаблицы);
		Если НайденнаяСтрока <> Неопределено Тогда
			ТаблицаРезультат.Удалить(НайденнаяСтрока);
		КонецЕсли;
	КонецЦикла;
	
	Возврат ТаблицаРезультат;
	
КонецФункции

Функция НайтиИзменившиесяТабличныеЧасти(СписокОставшихсяТЧ,
                                        ТабличныеЧасти_0,
                                        ТабличныеЧасти_1)
	
	СписокИзменившихсяТЧ = СоздатьТаблицуСравнения();
	
	// Поиск Табличных частей, в которых изменились строки.
	Для Каждого Элемент Из СписокОставшихсяТЧ Цикл
		
		ТЧ_0 = ТабличныеЧасти_0[Элемент.Значение];
		ТЧ_1 = ТабличныеЧасти_1[Элемент.Значение];
		
		Если ТЧ_0.Количество() = ТЧ_1.Количество() Тогда
			
			НайденоОтличие = Ложь;
			
			// Проверяем, что структура колонок осталась прежней (эквивалентна).
			Если ТЧЭквиваленты (ТЧ_0.Колонки, ТЧ_1.Колонки) Тогда
				
				// Ищем различающиеся элементы - строки.
				Для Индекс = 0 По ТЧ_0.Количество() - 1 Цикл
					Строка_0 = ТЧ_0[Индекс];
					Строка_1 = ТЧ_1[Индекс];
					
					Если НЕ СтрокиТЧРавны(Строка_0, Строка_1, ТЧ_0.Колонки) Тогда
						НайденоОтличие = Истина;
						Прервать;
					КонецЕсли
				КонецЦикла;
				
			Иначе
				НайденоОтличие = Истина;
			КонецЕсли;
			
			Если НайденоОтличие Тогда
				НоваяСтрока = СписокИзменившихсяТЧ.Добавить();
				НоваяСтрока.Установить(0, Элемент.Значение);
			КонецЕсли;
			
		Иначе
			НоваяСтрока = СписокИзменившихсяТЧ.Добавить();
			НоваяСтрока.Установить(0, Элемент.Значение);
		КонецЕсли;
			
	КонецЦикла;
	
	Возврат СписокИзменившихсяТЧ;
	
КонецФункции

Функция РассчитатьИзмененияТабличныхЧастей(СписокИзменившихсяТЧ,
                                          ТабличныеЧасти_0,
                                          ТабличныеЧасти_1)
	
	ИзмененияВТаблицах = Новый Соответствие;
	
	// Цикл по количеству табличных частей.
	Для Индекс = 1 По СписокИзменившихсяТЧ.Количество() Цикл
		
		ИзмененияВТаблицах.Вставить(СписокИзменившихсяТЧ[Индекс-1].Значение, Новый Соответствие);
		
		ТаблицаДляАнализа = СписокИзменившихсяТЧ[Индекс-1].Значение;
		ТЧ0 = ТабличныеЧасти_0[ТаблицаДляАнализа];
		ТЧ1 = ТабличныеЧасти_1[ТаблицаДляАнализа];
		
		ТаблицаИзмененныхСтрок = Новый ТаблицаЗначений;
		ТаблицаИзмененныхСтрок.Колонки.Добавить("ИндексВТЧ0");
		ТаблицаИзмененныхСтрок.Колонки.Добавить("ИндексВТЧ1");
		ТаблицаИзмененныхСтрок.Колонки.Добавить("Различия");
		
		СоответствиеСтрокТЧ0СтрокамТЧ1 = НайтиПохожиеСтрокиТаблиц(ТЧ0, ТЧ1);
		СоответствиеСтрокТЧ1СтрокамТЧ0 = Новый Соответствие;
		ПроверяемыеКолонки = НайтиОбщиеКолонки(ТЧ0, ТЧ1);
		Для Каждого Соответствие Из СоответствиеСтрокТЧ0СтрокамТЧ1 Цикл
			СтрокаТаблицы0 = Соответствие.Ключ;
			СтрокаТаблицы1 = Соответствие.Значение;
			РазличияСтрок = РазличияСтрок(СтрокаТаблицы0, СтрокаТаблицы1, ПроверяемыеКолонки);
			Если РазличияСтрок.Количество() > 0 Тогда
				НоваяСтрока = ТаблицаИзмененныхСтрок.Добавить();
				НоваяСтрока["ИндексВТЧ0"] = ИндексСтроки(СтрокаТаблицы0) + 1;
				НоваяСтрока["ИндексВТЧ1"] = ИндексСтроки(СтрокаТаблицы1) + 1;
				НоваяСтрока["Различия"] = РазличияСтрок;
			КонецЕсли;
			СоответствиеСтрокТЧ1СтрокамТЧ0.Вставить(СтрокаТаблицы1, СтрокаТаблицы0);
		КонецЦикла;
		
		ТаблицаДобавленныхСтрок = Новый ТаблицаЗначений;
		ТаблицаДобавленныхСтрок.Колонки.Добавить("ИндексВТЧ1");
		
		Для Каждого СтрокаТаблицы Из ТЧ1 Цикл
			Если СоответствиеСтрокТЧ1СтрокамТЧ0[СтрокаТаблицы] = Неопределено Тогда
				НоваяСтрока = ТаблицаДобавленныхСтрок.Добавить();
				НоваяСтрока.ИндексВТЧ1 = ТЧ1.Индекс(СтрокаТаблицы) + 1;
			КонецЕсли;
		КонецЦикла;
		
		ТаблицаУдаленныхСтрок = Новый ТаблицаЗначений;
		ТаблицаУдаленныхСтрок.Колонки.Добавить("ИндексВТЧ0");
		
		Для Каждого СтрокаТаблицы Из ТЧ0 Цикл
			Если СоответствиеСтрокТЧ0СтрокамТЧ1[СтрокаТаблицы] = Неопределено Тогда
				НоваяСтрока = ТаблицаУдаленныхСтрок.Добавить();
				НоваяСтрока.ИндексВТЧ0 = ТЧ0.Индекс(СтрокаТаблицы) + 1;
			КонецЕсли;
		КонецЦикла;
		
		ИзмененияВТаблицах[СписокИзменившихсяТЧ[Индекс-1].Значение].Вставить("Д", ТаблицаДобавленныхСтрок);
		ИзмененияВТаблицах[СписокИзменившихсяТЧ[Индекс-1].Значение].Вставить("У", ТаблицаУдаленныхСтрок);
		ИзмененияВТаблицах[СписокИзменившихсяТЧ[Индекс-1].Значение].Вставить("И", ТаблицаИзмененныхСтрок);
		
	КонецЦикла;
	
	Возврат ИзмененияВТаблицах;
	
КонецФункции

Процедура ПодготовитьКолонкиТаблицИзмененийДляСоответствия(Соответствие, МассивНомеровВерсий)
	
	Количество = МассивНомеровВерсий.Количество();
	
	Для Индекс = 1 По Количество Цикл
		Соответствие.Вставить("Версия" + Формат(МассивНомеровВерсий[Индекс-1], "ЧГ=0"), Новый ТаблицаЗначений);
	КонецЦикла;
	
КонецПроцедуры

Функция ТЧЭквиваленты(КолонкиПервойТаблицы, КолонкиВторойТаблицы)
	Если КолонкиПервойТаблицы.Количество() <> КолонкиВторойТаблицы.Количество() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Для Каждого Колонка Из КолонкиПервойТаблицы Цикл
		Найденная = КолонкиВторойТаблицы.Найти(Колонка.Имя);
		Если Найденная = Неопределено Или Колонка.ТипЗначения <> Найденная.ТипЗначения Тогда
			Возврат Ложь;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Истина;
КонецФункции

Функция СтрокиТЧРавны(СтрокаТЧ1, СтрокаТЧ2, Колонки)
	
	Для Каждого Колонка Из Колонки Цикл
		ИмяКолонки = Колонка.Имя;
		Если СтрокаТЧ2.Владелец().Колонки.Найти(ИмяКолонки) = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		ЗначениеИзТЧ1 = СтрокаТЧ1[ИмяКолонки];
		ЗначениеИзТЧ2 = СтрокаТЧ2[ИмяКолонки];
		Если ЗначениеИзТЧ1 <> ЗначениеИзТЧ2 Тогда
			Возврат Ложь;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Истина;
	
КонецФункции

Функция НайтиПохожиеСтрокиТаблиц(Таблица1, Таблица2, Знач ТребуемоеКоличествоРазличий = 0, Знач МаксимумРазличий = Неопределено, СоответствиеСтрокТаблицы1СтрокамТаблицы2 = Неопределено)
	
	Если СоответствиеСтрокТаблицы1СтрокамТаблицы2 = Неопределено Тогда
		СоответствиеСтрокТаблицы1СтрокамТаблицы2 = Новый Соответствие;
	КонецЕсли;
	
	Если МаксимумРазличий = Неопределено Тогда
		МаксимумРазличий = МаксимальноеКоличествоРазличийМеждуСтрокамиТаблиц(Таблица1, Таблица2);
	КонецЕсли;
	
	// Вычисляем обратное соответствие для быстрого поиска по значению.
	СоответствиеСтрокТаблицы2СтрокамТаблицы1 = Новый Соответствие; // Ключи - строки таблицы2, значения = строки таблицы1.
	Для Каждого Элемент Из СоответствиеСтрокТаблицы1СтрокамТаблицы2 Цикл
		СоответствиеСтрокТаблицы2СтрокамТаблицы1.Вставить(Элемент.Значение, Элемент.Ключ);
	КонецЦикла;
	
	// Сравниваем строки каждую с каждой.
	Для Каждого СтрокаТаблицы1 Из Таблица1 Цикл
		Для Каждого СтрокаТаблицы2 Из Таблица2 Цикл
			Если СоответствиеСтрокТаблицы1СтрокамТаблицы2[СтрокаТаблицы1] = Неопределено 
			   И СоответствиеСтрокТаблицы2СтрокамТаблицы1[СтрокаТаблицы2] = Неопределено Тогда // Пропускаем найденные строки.
			
				// считаем различия
				КоличествоРазличий = КоличествоРазличийВСтрокахТаблиц(СтрокаТаблицы1, СтрокаТаблицы2);
				
				// Анализируем результат сравнения строк.
				Если КоличествоРазличий = ТребуемоеКоличествоРазличий Тогда
					СоответствиеСтрокТаблицы1СтрокамТаблицы2.Вставить(СтрокаТаблицы1, СтрокаТаблицы2);
					СоответствиеСтрокТаблицы2СтрокамТаблицы1.Вставить(СтрокаТаблицы2, СтрокаТаблицы1);
					Прервать;
				КонецЕсли;
				
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
	Если ТребуемоеКоличествоРазличий < МаксимумРазличий Тогда
		НайтиПохожиеСтрокиТаблиц(Таблица1, Таблица2, ТребуемоеКоличествоРазличий + 1, МаксимумРазличий, СоответствиеСтрокТаблицы1СтрокамТаблицы2);
	КонецЕсли;
	
	Возврат СоответствиеСтрокТаблицы1СтрокамТаблицы2;
	
КонецФункции

Функция НайтиОбщиеКолонки(Таблица1, Таблица2)
	МассивИмен1 = ПолучитьИменаКолонок(Таблица1);
	МассивИмен2 = ПолучитьИменаКолонок(Таблица2);
	Возврат ПересечениеМножеств(МассивИмен1, МассивИмен2);
КонецФункции

Функция ИндексСтроки(СтрокаТаблицы)
	Возврат СтрокаТаблицы.Владелец().Индекс(СтрокаТаблицы);
КонецФункции

Функция РазличияСтрок(Строка1, Строка2, ПроверяемыеКолонки)
	Результат = Новый Массив;
	Для Каждого Колонка Из ПроверяемыеКолонки Цикл
		Если ТипЗнч(Строка1[Колонка]) = Тип("ХранилищеЗначения") Тогда
			Продолжить; // Реквизиты с типом ХранилищеЗначения не сравниваем.
		КонецЕсли;
		Если Строка1[Колонка] <> Строка2[Колонка] Тогда
			Результат.Добавить(Колонка);
		КонецЕсли;
	КонецЦикла;
	Возврат Результат;
КонецФункции

Функция МаксимальноеКоличествоРазличийМеждуСтрокамиТаблиц(Таблица1, Таблица2)
	
	МассивИменКолонокТаблицы1 = ПолучитьИменаКолонок(Таблица1);
	МассивИменКолонокТаблицы2 = ПолучитьИменаКолонок(Таблица2);
	МассивИменКолонокОбеихТаблиц = ОбъединениеМножеств(МассивИменКолонокТаблицы1, МассивИменКолонокТаблицы2);
	ВсегоКолонок = МассивИменКолонокОбеихТаблиц.Количество();
	
	Возврат ?(ВсегоКолонок = 0, 0, ВсегоКолонок - 1);

КонецФункции

Функция ОбъединениеМножеств(Множество1, Множество2)
	
	Результат = Новый Массив;
	
	Для Каждого Элемент Из Множество1 Цикл
		Индекс = Результат.Найти(Элемент);
		Если Индекс = Неопределено Тогда
			Результат.Добавить(Элемент);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого Элемент Из Множество2 Цикл
		Индекс = Результат.Найти(Элемент);
		Если Индекс = Неопределено Тогда
			Результат.Добавить(Элемент);
		КонецЕсли;
	КонецЦикла;	
	
	Возврат Результат;
	
КонецФункции

Функция КоличествоРазличийВСтрокахТаблиц(СтрокаТаблицы1, СтрокаТаблицы2)
	
	Результат = 0;
	
	Таблица1 = СтрокаТаблицы1.Владелец();
	Таблица2 = СтрокаТаблицы2.Владелец();
	
	ОбщиеКолонки = НайтиОбщиеКолонки(Таблица1, Таблица2);
	ОстальныеКолонки = НайтиНесовпадающиеКолонки(Таблица1, Таблица2);
	
	// Каждую колонку, не являющуюся общей считаем как одно различие.
	Результат = Результат + ОстальныеКолонки.Количество();
	
	// Различия считаем по несовпадающим значениям.
	Для Каждого ИмяКолонки Из ОбщиеКолонки Цикл
		Если СтрокаТаблицы1[ИмяКолонки] <> СтрокаТаблицы2[ИмяКолонки] Тогда
			Результат = Результат + 1;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Функция ПолучитьИменаКолонок(Таблица)
	
	Результат = Новый Массив;
	
	Для Каждого Колонка Из Таблица.Колонки Цикл
		Результат.Добавить(Колонка.Имя);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Функция ПересечениеМножеств(Множество1, Множество2)
	
	Результат = Новый Массив;
	
	Для Каждого Элемент Из Множество1 Цикл
		Индекс = Множество2.Найти(Элемент);
		Если Индекс <> Неопределено Тогда
			Результат.Добавить(Элемент);
		КонецЕсли;
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Функция НайтиНесовпадающиеКолонки(Таблица1, Таблица2)
	МассивИмен1 = ПолучитьИменаКолонок(Таблица1);
	МассивИмен2 = ПолучитьИменаКолонок(Таблица2);
	Возврат РазностьМножеств(МассивИмен1, МассивИмен2, Истина);
КонецФункции

Функция РазностьМножеств(Множество1, Знач Множество2, СимметричнаяРазность = Ложь)
	
	Результат = Новый Массив;
	Множество2 = СкопироватьМассив(Множество2);
	
	Для Каждого Элемент Из Множество1 Цикл
		Индекс = Множество2.Найти(Элемент);
		Если Индекс = Неопределено Тогда
			Результат.Добавить(Элемент);
		Иначе
			Множество2.Удалить(Индекс);
		КонецЕсли;
	КонецЦикла;
	
	Если СимметричнаяРазность Тогда
		Для Каждого Элемент Из Множество2 Цикл
			Результат.Добавить(Элемент);
		КонецЦикла;
	КонецЕсли;
	
	Возврат Результат;
КонецФункции

Функция СкопироватьМассив(Массив)
	
	Результат = Новый Массив;
	
	Для Каждого Элемент Из Массив Цикл
		Результат.Добавить(Элемент);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

#КонецЕсли