﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
КонецПроцедуры

&НаКлиенте
Процедура ДиректорияЗаписанныхФайловНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	стрКаталогНач = Запись.ДиректорияЗаписанныхФайлов;
	длг = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.ВыборКаталога);
	длг.Каталог = стрКаталогНач;
	Если длг.Выбрать() = Истина Тогда
		Запись.ДиректорияЗаписанныхФайлов = длг.Каталог;
		Если длг.Каталог <> стрКаталогНач Тогда
			Модифицированность = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ОбновитьДоступность();
	Элементы.ВерсияБитфона.Заголовок = "Версия БИТ.Phone " + БИТфонСервер.ПолучитьВерсиюБИТфон();
	ВнутренниеНомераАстериск.Параметры.УстановитьЗначениеПараметра("Владелец", Запись.Пользователь);
	
	СписокКодеков.Очистить();
	списокСПриоритетом = БИТфонКлиент.СтрРазбить(Запись.СписокКодеков, ";");
	Для Каждого кодекПриорЭл Из списокСПриоритетом Цикл
		стрКодекПриор = кодекПриорЭл.Значение;
		индекс = Найти(стрКодекПриор, "=");
		Если индекс > 0 Тогда
			стрКодек = Лев(стрКодекПриор, индекс-1);
			стрПриор = Сред(стрКодекПриор, индекс+1);
			строкаКодек = СписокКодеков.Добавить();
			строкаКодек.Кодек = стрКодек;
			строкаКодек.Приоритет = Число(стрПриор);
		КонецЕсли;
	КонецЦикла;
	СписокКодеков.Сортировать("Приоритет Убыв");
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьДоступность()
	Элементы.СтационарныйТелефон.Доступность = Запись.ИспользоватьСтационарныйТелефон;
    Элементы.ТипПриемаЗвонка.Доступность = Запись.ИспользоватьСтационарныйТелефон;
	//
	Элементы.ПрефиксВыходаНаВнешнююЛинию.Доступность = НЕ Запись.ИспользоватьПрямойНабор;
	//
	Элементы.СоздаватьСобытияПриВнутреннихЗвонках.Доступность = (Запись.СоздаватьСобытиеПриВходящемЗвонке ИЛИ Запись.СоздаватьСобытиеПриИсходящемЗвонке);
	//
	Элементы.АстерискЛогин.Доступность = Запись.ИнтеграцияСБИТАТС;
	Элементы.АстерискПароль.Доступность = Запись.ИнтеграцияСБИТАТС;
	Элементы.АстерискДиректорияЗаписанныхФайлов.Доступность = Запись.ИнтеграцияСБИТАТС;
	Элементы.АстерискКонтекст.Доступность = Запись.ИнтеграцияСБИТАТС;
	Элементы.ВнутренниеНомераАстериск.Доступность = Запись.ИнтеграцияСБИТАТС;
КонецПроцедуры

&НаКлиенте
Процедура СоздаватьСобытиеПриВходящемЗвонкеПриИзменении(Элемент)
	ОбновитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура СоздаватьСобытиеПриИсходящемЗвонкеПриИзменении(Элемент)
	ОбновитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтационарныйТелефонПриИзменении(Элемент)
	ОбновитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПрямойНаборПриИзменении(Элемент)
	ОбновитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ИнтеграцияСАстерискПриИзменении(Элемент)
	ОбновитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ВнутренниеНомераАстерискСоздать(Команда)
	Заполн = Новый Структура;
	Заполн.Вставить("Владелец", Запись.Пользователь);
	П = Новый Структура;
	П.Вставить("ЗначенияЗаполнения", Заполн);
	ОткрытьФормуМодально("Справочник.бтВнутренниеНомераАстериск.Форма.ФормаЭлементаБезВладельца", П);
КонецПроцедуры

&НаКлиенте
Процедура ВнутренниеНомераАстерискСкопировать(Команда)
	текущ =  Элементы.ВнутренниеНомераАстериск.ТекущаяСтрока;
	П = Новый Структура;
	П.Вставить("ЗначениеКопирования", текущ);
	ОткрытьФормуМодально("Справочник.бтВнутренниеНомераАстериск.Форма.ФормаЭлементаБезВладельца", П);
КонецПроцедуры

&НаКлиенте
Процедура ВнутренниеНомераАстерискИзменитьВнутр(СсылкаНаЭлемент)
	П = Новый Структура("Ключ", СсылкаНаЭлемент);
	ОткрытьФормуМодально("Справочник.бтВнутренниеНомераАстериск.Форма.ФормаЭлементаБезВладельца", П);
КонецПроцедуры

&НаКлиенте
Процедура ВнутренниеНомераАстерискИзменить(Команда)
	текущ =  Элементы.ВнутренниеНомераАстериск.ТекущаяСтрока;
	ВнутренниеНомераАстерискИзменитьВнутр(текущ);
КонецПроцедуры

&НаКлиенте
Процедура ВнутренниеНомераАстерискВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ВнутренниеНомераАстерискИзменитьВнутр(ВыбраннаяСтрока);
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	стрСписокКодеков = Запись.СписокКодеков;
	Если СписокКодеков.Количество() > 0 Тогда
		стрСписокКодеков = "";
		Для Каждого строкаКодек Из СписокКодеков Цикл
			стрКодек = строкаКодек.Кодек;
			стрПриор = Строка(строкаКодек.Приоритет);
			стрСписокКодеков = стрСписокКодеков + стрКодек + "=" + стрПриор + ";";
		КонецЦикла;
	КонецЕсли;
	ТекущийОбъект.СписокКодеков = стрСписокКодеков;
КонецПроцедуры

&НаКлиенте
Процедура ТипПодключенияСтатусовОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ТекущийТипПодключенияСтатусов = Запись.ТипПодключенияСтатусов;
	
	Если ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.AMI") Тогда
		Если НЕ БИТфонСервер.ПолучитьФлагИнтеграцииСБИТАТС() Тогда
			Предупреждение("Подключение через AMI не доступно, так как не включена интеграция с Астериск!");
		Иначе
			Запись.ТипПодключенияСтатусов = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.AMI");
		КонецЕсли;	
	ИначеЕсли ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.НеИспользовать") Тогда 
		Запись.ТипПодключенияСтатусов = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.НеИспользовать");
	ИначеЕсли ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.SIP") Тогда	
		Запись.ТипПодключенияСтатусов = ПредопределенноеЗначение("Перечисление.бтТипПодключенияСтатуса.SIP");
	КонецЕсли;
	ОбновитьДоступность();
	
	Если ТекущийТипПодключенияСтатусов <> Запись.ТипПодключенияСтатусов Тогда
		Модифицированность = Истина;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура СписокКодековПриоритетПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПапкуСЛогФайлом(Команда)
	ЗапуститьПриложение("%APPDATA%\BIT");
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьЗапросВТехподдержку(Команда)
	БИТфонКлиент.ОтправитьЗапросВТехподдержкуСАрхивомЛога();
КонецПроцедуры
