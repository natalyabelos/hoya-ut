﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Функция ТекстЗапросаСторноЗаписейЗаказа() Экспорт

	Текст =
		"ВЫБРАТЬ
		|	Т.Номенклатура     КАК Номенклатура,
		|	Т.Характеристика   КАК Характеристика,
		|	Т.Склад            КАК Склад,
		|	Т.Назначение       КАК Назначение,
		|
		|	- Т.Потребность    КАК Потребность,
		|	- Т.КЗаказу        КАК КЗаказу,
		|	Т.КОтгрузке        КАК НаличиеПодЗаказ
		|
		|ИЗ
		|	РегистрНакопления.ОбеспечениеЗаказов КАК Т
		|
		|ГДЕ
		|	Т.Активность
		|	И Т.Регистратор = &Ссылка
		|	И (Т.Потребность <> 0 ИЛИ Т.КЗаказу <> 0 ИЛИ Т.КОтгрузке <> 0)
		|	И &Отбор
		|;
		|
		|//////////////////////////////////////////////////
		|";

	Возврат Текст;

КонецФункции

#Область ПрограммныйИнтерфейс

Функция ТекстЗапросаОстатков(ИспользоватьКорректировку, Разделы = Неопределено) Экспорт

	Если Не ИспользоватьКорректировку Тогда
		ТекстЗапроса =
			"ВЫБРАТЬ
			|	Т.Номенклатура           КАК Номенклатура,
			|	Т.Характеристика         КАК Характеристика,
			|	Т.Склад                  КАК Склад,
			|	Т.Назначение             КАК Назначение,
			|
			|	Т.НаличиеПодЗаказОстаток КАК Количество,
			|	Т.КЗаказуОстаток         КАК КоличествоКЗаказу
			|
			|ПОМЕСТИТЬ ВтОбеспечениеЗаказов
			|ИЗ
			|	РегистрНакопления.ОбеспечениеЗаказов.Остатки(,
			|		(Номенклатура, Характеристика, Склад, Назначение) В(
			|			ВЫБРАТЬ
			|				Ключи.Номенклатура   КАК Номенклатура,
			|				Ключи.Характеристика КАК Характеристика,
			|				Ключи.Склад          КАК Склад,
			|				Ключи.Назначение     КАК Назначение
			|			ИЗ
			|				ВтТоварыОбособленные КАК Ключи
			|		)) КАК Т
			|
			|ИНДЕКСИРОВАТЬ ПО
			|	Номенклатура, Характеристика, Склад, Назначение
			|;
			|
			|/////////////////////////////////////////////////////////////
			|";
	Иначе
		ТекстЗапроса =
			"ВЫБРАТЬ
			|	НаборДанных.Номенклатура           КАК Номенклатура,
			|	НаборДанных.Характеристика         КАК Характеристика,
			|	НаборДанных.Склад                  КАК Склад,
			|	НаборДанных.Назначение             КАК Назначение,
			|
			|	СУММА(НаборДанных.Количество)         КАК Количество,
			|	СУММА(НаборДанных.КоличествоКЗаказу)  КАК КоличествоКЗаказу
			|
			|ПОМЕСТИТЬ ВтОбеспечениеЗаказов
			|ИЗ (
			|	ВЫБРАТЬ
			|		Т.Номенклатура           КАК Номенклатура,
			|		Т.Характеристика         КАК Характеристика,
			|		Т.Склад                  КАК Склад,
			|		Т.Назначение             КАК Назначение,
			|
			|		Т.НаличиеПодЗаказОстаток - Т.КОтгрузкеОстаток КАК Количество,
			|		Т.КЗаказуОстаток         КАК КоличествоКЗаказу
			|
			|	ИЗ
			|	РегистрНакопления.ОбеспечениеЗаказов.Остатки(,
			|		(Номенклатура, Характеристика, Склад, Назначение) В(
			|			ВЫБРАТЬ
			|				Ключи.Номенклатура   КАК Номенклатура,
			|				Ключи.Характеристика КАК Характеристика,
			|				Ключи.Склад          КАК Склад,
			|				Ключи.Назначение     КАК Назначение
			|			ИЗ
			|				ВтТоварыОбособленные КАК Ключи
			|		)) КАК Т
			|
			|	ОБЪЕДИНИТЬ ВСЕ
			|
			|	ВЫБРАТЬ
			|		Т.Номенклатура            КАК Номенклатура,
			|		Т.Характеристика          КАК Характеристика,
			|		Т.Склад                   КАК Склад,
			|		Т.Назначение              КАК Назначение,
			|
			|		Т.НаличиеПодЗаказ         КАК Количество,
			|		Т.КЗаказу                 КАК КоличествоКЗаказу
			|
			|	ИЗ
			|		ВтОбеспечениеЗаказовКорректировка КАК Т
			|	) КАК НаборДанных
			|
			|СГРУППИРОВАТЬ ПО
			|	НаборДанных.Номенклатура, НаборДанных.Характеристика, НаборДанных.Склад, НаборДанных.Назначение
			|ИМЕЮЩИЕ
			|	СУММА(НаборДанных.Количество) <> 0
			|		ИЛИ СУММА(НаборДанных.КоличествоКЗаказу) <> 0
			|ИНДЕКСИРОВАТЬ ПО
			|	Номенклатура, Характеристика, Склад, Назначение
			|;
			|
			|/////////////////////////////////////////////////////////////
			|";
	КонецЕсли;

	Если Разделы <> Неопределено Тогда
		Разделы.Добавить("ТаблицаОстаткиСкладаОбособленные");
	КонецЕсли;

	Возврат ТекстЗапроса;

КонецФункции

#КонецОбласти

#КонецЕсли
