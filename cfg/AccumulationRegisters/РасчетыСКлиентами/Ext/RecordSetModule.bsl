﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)

	Если ОбменДанными.Загрузка Или Не ПроведениеСервер.РассчитыватьИзменения(ДополнительныеСвойства) Тогда
		Возврат;
	КонецЕсли;

	БлокироватьДляИзменения = Истина;

	// Текущее состояние набора помещается во временную таблицу,
	// чтобы при записи получить изменение нового набора относительно текущего.

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Регистратор",              Отбор.Регистратор.Значение);
	Запрос.УстановитьПараметр("ПериодКонтроляСрокаДолга", Макс(КонецДня(ТекущаяДата()), КонецДня(ДополнительныеСвойства.ДатаРегистратора)));
	Запрос.УстановитьПараметр("ЭтоНовый",                 ДополнительныеСвойства.ЭтоНовый);
	Запрос.МенеджерВременныхТаблиц = ДополнительныеСвойства.ДляПроведения.СтруктураВременныеТаблицы.МенеджерВременныхТаблиц;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Таблица.Регистратор  КАК Регистратор,
	|	Таблица.ЗаказКлиента КАК ЗаказКлиента,
	|	Таблица.Валюта       КАК Валюта,
	|	ВЫБОР КОГДА НЕ Таблица.ИсключатьПриКонтроле ТОГДА
	|			ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|					-Таблица.КОплате
	|				ИНАЧЕ Таблица.КОплате
	|			КОНЕЦ
	|		ИНАЧЕ 0
	|	КОНЕЦ                КАК КОплатеПередЗаписью,
	|	ВЫБОР КОГДА НЕ Таблица.ИсключатьПриКонтроле ТОГДА
	|			ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|					Таблица.Оплачивается
	|				ИНАЧЕ - Таблица.Оплачивается
	|			КОНЕЦ
	|	КОНЕЦ                КАК ОплачиваетсяПередЗаписью,
	|	0                    КАК КОплатеПередЗаписьюКонтрольСрока,
	|	0                    КАК ОплачиваетсяПередЗаписьюКонтрольСрока,
	|	0                    КАК СуммаПередЗаписью,
	|	0                    КАК ОтгружаетсяПередЗаписью,
	|	0                    КАК ДопустимаяСуммаЗадолженностиПередЗаписью
	|
	|ПОМЕСТИТЬ РасчетыСКлиентамиПередЗаписью
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК Таблица
	|ГДЕ
	|	Таблица.Регистратор = &Регистратор
	|	И (Таблица.ЗаказКлиента ССЫЛКА Документ.ЗаказКлиента
	|		ИЛИ Таблица.ЗаказКлиента ССЫЛКА Документ.ЗаявкаНаВозвратТоваровОтКлиента
	|		ИЛИ Таблица.ЗаказКлиента ССЫЛКА Документ.РеализацияТоваровУслуг)
	|	И (Таблица.ЗаказКлиента <> ЗНАЧЕНИЕ(Документ.ЗаказКлиента.ПустаяСсылка)
	|		И Таблица.ЗаказКлиента <> ЗНАЧЕНИЕ(Документ.ЗаявкаНаВозвратТоваровОтКлиента.ПустаяСсылка))
	|	И НЕ &ЭтоНовый
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Таблица.Регистратор                  КАК Регистратор,
	|	Таблица.ЗаказКлиента                 КАК ЗаказКлиента,
	|	Таблица.Валюта                       КАК Валюта,
	|	0                                    КАК КОплатеПередЗаписью,
	|	0                                    КАК ОплачиваетсяПередЗаписью,
	|	ВЫБОР КОГДА Таблица.Период <= &ПериодКонтроляСрокаДолга ТОГДА
	|		ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|				-Таблица.КОплате
	|			ИНАЧЕ Таблица.КОплате
	|		КОНЕЦ
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ                                КАК КОплатеПередЗаписьюКонтрольСрока,
	|	ВЫБОР КОГДА Таблица.Период <= &ПериодКонтроляСрокаДолга ТОГДА
	|		ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|				Таблица.Оплачивается
	|			ИНАЧЕ -Таблица.Оплачивается
	|		КОНЕЦ
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ                                КАК ОплачиваетсяПередЗаписьюКонтрольСрока,
	|	ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|			ВЫБОР КОГДА Таблица.Сумма < 0 ТОГДА
	|					0
	|				ИНАЧЕ -Таблица.Сумма
	|			КОНЕЦ
	|		ИНАЧЕ Таблица.Сумма
	|	КОНЕЦ                                КАК СуммаПередЗаписью,
	|	ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|			0
	|		ИНАЧЕ Таблица.Отгружается
	|	КОНЕЦ                                КАК ОтгружаетсяПередЗаписью,
	|	Таблица.ДопустимаяСуммаЗадолженности КАК ДопустимаяСуммаЗадолженностиПередЗаписью
	|
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК Таблица
	|ГДЕ
	|	Таблица.Регистратор = &Регистратор
	|	И НЕ &ЭтоНовый
	|	И Таблица.ЗаказКлиента <> НЕОПРЕДЕЛЕНО
	|";
	Запрос.Выполнить();

КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)

	Если ОбменДанными.Загрузка Или Не ПроведениеСервер.РассчитыватьИзменения(ДополнительныеСвойства) Тогда
		Возврат;
	КонецЕсли;

	// Рассчитывается изменение нового набора относительно текущего с учетом накопленных изменений
	// и помещается во временную таблицу.

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Регистратор",              Отбор.Регистратор.Значение);
	Запрос.УстановитьПараметр("ПроведениеДокумента",      ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение);
	Запрос.УстановитьПараметр("ПериодКонтроляСрокаДолга", Макс(КонецДня(ТекущаяДата()), КонецДня(ДополнительныеСвойства.ДатаРегистратора)));
	СтруктураВременныеТаблицы = ДополнительныеСвойства.ДляПроведения.СтруктураВременныеТаблицы;
	Запрос.МенеджерВременныхТаблиц = СтруктураВременныеТаблицы.МенеджерВременныхТаблиц;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТаблицаИзменений.ЗаказКлиента КАК ЗаказКлиента,
	|	ТаблицаИзменений.Валюта       КАК Валюта,
	|	СУММА(ТаблицаИзменений.КОплатеИзменение) КАК КОплатеИзменение
	|ПОМЕСТИТЬ ДвиженияРасчетыСКлиентамиИзменение
	|ИЗ
	|	(ВЫБРАТЬ
	|		Таблица.ЗаказКлиента               КАК ЗаказКлиента,
	|		Таблица.Валюта                     КАК Валюта,
	|		Таблица.КОплатеПередЗаписью        КАК КОплатеИзменение,
	|		Таблица.ОплачиваетсяПередЗаписью   КАК ОплачиваетсяИзменение
	|	ИЗ
	|		РасчетыСКлиентамиПередЗаписью КАК Таблица
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		Таблица.ЗаказКлиента КАК ЗаказКлиента,
	|		Таблица.Валюта       КАК Валюта,
	|		ВЫБОР КОГДА Не Таблица.ИсключатьПриКонтроле ТОГДА
	|				ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|						Таблица.КОплате
	|					ИНАЧЕ -Таблица.КОплате
	|				КОНЕЦ
	|			ИНАЧЕ 0
	|		КОНЕЦ,
	|		ВЫБОР КОГДА Не Таблица.ИсключатьПриКонтроле ТОГДА
	|				ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|						-Таблица.Оплачивается
	|					ИНАЧЕ Таблица.Оплачивается
	|				КОНЕЦ
	|			ИНАЧЕ 0
	|		КОНЕЦ
	|	ИЗ
	|		РегистрНакопления.РасчетыСКлиентами КАК Таблица
	|	ГДЕ
	|		Таблица.Регистратор = &Регистратор
	|		И (Таблица.ЗаказКлиента ССЫЛКА Документ.ЗаказКлиента
	|			ИЛИ Таблица.ЗаказКлиента ССЫЛКА Документ.ЗаявкаНаВозвратТоваровОтКлиента
	|			ИЛИ Таблица.ЗаказКлиента ССЫЛКА Документ.РеализацияТоваровУслуг
	|)
	|		И (Таблица.ЗаказКлиента <> ЗНАЧЕНИЕ(Документ.ЗаказКлиента.ПустаяСсылка)
	|			И Таблица.ЗаказКлиента <> ЗНАЧЕНИЕ(Документ.ЗаявкаНаВозвратТоваровОтКлиента.ПустаяСсылка)
	|)
	|
	|) КАК ТаблицаИзменений
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаИзменений.ЗаказКлиента,
	|	ТаблицаИзменений.Валюта
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаИзменений.КОплатеИзменение) + СУММА(ТаблицаИзменений.ОплачиваетсяИзменение) < 0
	|;
	|ВЫБРАТЬ
	|	ТаблицаИзменений.Регистратор                                  КАК Регистратор,
	|	ТаблицаИзменений.ЗаказКлиента                                 КАК ЗаказКлиента,
	|	ТаблицаИзменений.Валюта                                       КАК Валюта,
	|	СУММА(ТаблицаИзменений.СуммаИзменение)                        КАК СуммаИзменение,
	|	СУММА(ТаблицаИзменений.ОтгружаетсяИзменение)                  КАК ОтгружаетсяИзменение,
	|	СУММА(ТаблицаИзменений.ДопустимаяСуммаЗадолженностиИзменение) КАК ДопустимаяСуммаЗадолженностиИзменение
	|ПОМЕСТИТЬ ДвиженияРасчетыСКлиентамиИзменениеСуммыДолга
	|ИЗ
	|	(ВЫБРАТЬ
	|		Таблица.Регистратор                              КАК Регистратор,
	|		Таблица.ЗаказКлиента                             КАК ЗаказКлиента,
	|		Таблица.Валюта                                   КАК Валюта,
	|		Таблица.СуммаПередЗаписью                        КАК СуммаИзменение,
	|		Таблица.ОтгружаетсяПередЗаписью                  КАК ОтгружаетсяИзменение,
	|		Таблица.ДопустимаяСуммаЗадолженностиПередЗаписью КАК ДопустимаяСуммаЗадолженностиИзменение
	|	ИЗ
	|		РасчетыСКлиентамиПередЗаписью КАК Таблица
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		Таблица.Регистратор                   КАК Регистратор,
	|		Таблица.ЗаказКлиента                  КАК ЗаказКлиента,
	|		Таблица.Валюта                        КАК Валюта,
	|		ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|				ВЫБОР КОГДА Таблица.Сумма < 0 ТОГДА
	|						0
	|					ИНАЧЕ Таблица.Сумма
	|				КОНЕЦ
	|			ИНАЧЕ -Таблица.Сумма
	|		КОНЕЦ                                 КАК СуммаИзменение,
	|		ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|				0
	|			ИНАЧЕ -Таблица.Отгружается
	|		КОНЕЦ                                 КАК ОтгружаетсяИзменение,
	|		-Таблица.ДопустимаяСуммаЗадолженности КАК ДопустимаяСуммаЗадолженностиИзменение
	|	ИЗ
	|		РегистрНакопления.РасчетыСКлиентами КАК Таблица
	|	ГДЕ
	|		Таблица.Регистратор = &Регистратор
	|		И Таблица.ЗаказКлиента <> НЕОПРЕДЕЛЕНО
	|
	|) КАК ТаблицаИзменений
	|
	|ГДЕ
	|	&ПроведениеДокумента
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаИзменений.Регистратор,
	|	ТаблицаИзменений.ЗаказКлиента,
	|	ТаблицаИзменений.Валюта
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаИзменений.СуммаИзменение) + СУММА(ТаблицаИзменений.ОтгружаетсяИзменение) < 0
	|	ИЛИ СУММА(ТаблицаИзменений.ДопустимаяСуммаЗадолженностиИзменение) > 0
	|;
	|ВЫБРАТЬ
	|	ТаблицаИзменений.Регистратор                                  КАК Регистратор,
	|	ТаблицаИзменений.ЗаказКлиента                                 КАК ЗаказКлиента,
	|	ТаблицаИзменений.Валюта                                       КАК Валюта,
	|	СУММА(ТаблицаИзменений.СуммаИзменение)                        КАК СуммаИзменение,
	|	СУММА(ТаблицаИзменений.КОплатеИзменениеКонтрольСрока)         КАК КОплатеИзменениеКонтрольСрока,
	|	СУММА(ТаблицаИзменений.ОплачиваетсяИзменениеКонтрольСрока)    КАК ОплачиваетсяИзменениеКонтрольСрока
	|ПОМЕСТИТЬ ДвиженияРасчетыСКлиентамиИзменениеКонтрольСрока
	|ИЗ
	|	(ВЫБРАТЬ
	|		Таблица.Регистратор                              КАК Регистратор,
	|		Таблица.ЗаказКлиента                             КАК ЗаказКлиента,
	|		Таблица.Валюта                                   КАК Валюта,
	|		Таблица.СуммаПередЗаписью                        КАК СуммаИзменение,
	|		Таблица.КОплатеПередЗаписьюКонтрольСрока         КАК КОплатеИзменениеКонтрольСрока,
	|		Таблица.ОплачиваетсяПередЗаписьюКонтрольСрока    КАК ОплачиваетсяИзменениеКонтрольСрока
	|	ИЗ
	|		РасчетыСКлиентамиПередЗаписью КАК Таблица
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		Таблица.Регистратор                   КАК Регистратор,
	|		Таблица.ЗаказКлиента                  КАК ЗаказКлиента,
	|		Таблица.Валюта                        КАК Валюта,
	|		ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|				ВЫБОР КОГДА Таблица.Сумма < 0 ТОГДА
	|						0
	|					ИНАЧЕ Таблица.Сумма
	|				КОНЕЦ
	|			ИНАЧЕ -Таблица.Сумма
	|		КОНЕЦ                                 КАК СуммаИзменение,
	|		ВЫБОР КОГДА Таблица.Период <= &ПериодКонтроляСрокаДолга ТОГДА
	|			ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|					Таблица.КОплате
	|				ИНАЧЕ -Таблица.КОплате
	|			КОНЕЦ
	|		ИНАЧЕ
	|			0
	|		КОНЕЦ                                 КАК КОплатеИзменениеКонтрольСрока,
	|		ВЫБОР КОГДА Таблица.Период <= &ПериодКонтроляСрокаДолга ТОГДА
	|			ВЫБОР КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) ТОГДА
	|					-Таблица.Оплачивается
	|				ИНАЧЕ Таблица.Оплачивается
	|			КОНЕЦ
	|		ИНАЧЕ
	|			0
	|		КОНЕЦ                                 КАК ОплачиваетсяИзменениеКонтрольСрока
	|	ИЗ
	|		РегистрНакопления.РасчетыСКлиентами КАК Таблица
	|	ГДЕ
	|		Таблица.Регистратор = &Регистратор
	|		И Таблица.ЗаказКлиента <> НЕОПРЕДЕЛЕНО
	|
	|) КАК ТаблицаИзменений
	|
	|ГДЕ
	|	&ПроведениеДокумента
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаИзменений.Регистратор,
	|	ТаблицаИзменений.ЗаказКлиента,
	|	ТаблицаИзменений.Валюта
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаИзменений.СуммаИзменение) < 0
	|	ИЛИ (СУММА(ТаблицаИзменений.КОплатеИзменениеКонтрольСрока)
	|			+ СУММА(ТаблицаИзменений.ОплачиваетсяИзменениеКонтрольСрока)) < 0
	|;
	|УНИЧТОЖИТЬ РасчетыСКлиентамиПередЗаписью;
	|";
	МассивРезультатов = Запрос.ВыполнитьПакет();
	ВыборкаИзменение = МассивРезультатов[0].Выбрать();
	ВыборкаИзменение.Следующий();
	ВыборкаИзменениеСуммыДолга = МассивРезультатов[1].Выбрать();
	ВыборкаИзменениеСуммыДолга.Следующий();
	ВыборкаИзменениеКонтрольСрока = МассивРезультатов[2].Выбрать();
	ВыборкаИзменениеКонтрольСрока.Следующий();

	// Добавляется информация о ее существовании и наличии в ней записей об изменении.
	СтруктураВременныеТаблицы.Вставить("ДвиженияРасчетыСКлиентамиИзменение", ВыборкаИзменение.Количество > 0);
	СтруктураВременныеТаблицы.Вставить("ДвиженияРасчетыСКлиентамиИзменениеСуммыДолга", ВыборкаИзменениеСуммыДолга.Количество > 0);
	СтруктураВременныеТаблицы.Вставить("ДвиженияРасчетыСКлиентамиИзменениеКонтрольСрока", ВыборкаИзменениеКонтрольСрока.Количество > 0);

КонецПроцедуры

#КонецОбласти

#КонецЕсли
