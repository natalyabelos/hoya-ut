﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ПрограммныйИнтерфейс

// Формирует структуру серии номенклатуры - какие серии материалов использовались при производстве/сборке
//
// АЛГОРИТМ
// 1. Получить данные о поступлении требуемой серии
// 1.1. Серия может поступить следующими способами
//      - по графику производства - отражается маршрутными листами или выпуском продукции по распоряжению
//      - без графика производства - отражается выпуском продукции без распоряжения
//      - при сборке (разборке) товаров - отражается сборкой (разборкой)
//
// 2. Получить данные о расходе серий на полученные поступления
// 2.1. Расход серии может произойти следующими способами
//      - если серия поступила по графику производства, то
//         - при выполнении маршрутных листов - отражается маршрутными листами
//         - при распределении затрат на график производства - отражается распределением затрат
//      - если серия поступила не по графику производства, то
//         - при распределении затрат на выпуски без распоряжений
//      - если серия поступила при сборке (разборке), то
//         - при сборке (разборке)
//
// Параметры:
//  Параметры	- Структура - содержит параметры необходимые для формирования структуры серии
//
Функция СтруктураСерииНоменклатуры(Параметры) Экспорт
	
	СтруктураСерииНоменклатурыДерево = Новый ДеревоЗначений;
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("Серия", Новый ОписаниеТипов("СправочникСсылка.СерииНоменклатуры"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("Израсходовано", Новый ОписаниеТипов("Число"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("ЕдиницаИзмерения", Новый ОписаниеТипов("Строка"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("НоменклатураПредставление", Новый ОписаниеТипов("Строка"));
	СтруктураСерииНоменклатурыДерево.Колонки.Добавить("СерияПредставление", Новый ОписаниеТипов("Строка"));
	
	СтруктураСерииКоллекция = СтруктураСерииНоменклатурыДерево.Строки;
	
	СписокНоменклатуры = Новый ТаблицаЗначений;
	СписокНоменклатуры.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	СписокНоменклатуры.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	СписокНоменклатуры.Колонки.Добавить("Серия", Новый ОписаниеТипов("СправочникСсылка.СерииНоменклатуры"));
	СписокНоменклатуры.Колонки.Добавить("Строки");
	
	НоваяСтрока = СписокНоменклатуры.Добавить();
	ЗаполнитьЗначенияСвойств(НоваяСтрока, Параметры);
	НоваяСтрока.Строки = СтруктураСерииКоллекция;
	
	// Для предотвращения зацикливания
	ОтработаннаяНоменклатура = Новый ТаблицаЗначений;
	ОтработаннаяНоменклатура.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ОтработаннаяНоменклатура.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	ОтработаннаяНоменклатура.Колонки.Добавить("Серия", Новый ОписаниеТипов("СправочникСсылка.СерииНоменклатуры"));
	
	НоваяСтрока = ОтработаннаяНоменклатура.Добавить();
	ЗаполнитьЗначенияСвойств(НоваяСтрока, Параметры);
	
	УстановитьПривилегированныйРежим(Истина);
	
	Пока СписокНоменклатуры.Количество() <> 0 Цикл
		
		НовыйСписокНоменклатуры = СписокНоменклатуры.СкопироватьКолонки();
		
		ИспользованныеСерии = ИспользованныеСерии(СписокНоменклатуры);
		Для каждого СтрокаНоменклатура Из СписокНоменклатуры Цикл
			СтруктураПоиска = Новый Структура("НоменклатураСписка,СерияСписка", СтрокаНоменклатура.Номенклатура, СтрокаНоменклатура.Серия);
	  		СписокСтрок = ИспользованныеСерии.НайтиСтроки(СтруктураПоиска);
			Для каждого СтрокаСерияМатериала Из СписокСтрок Цикл
				
				НоваяСерия = СтрокаНоменклатура.Строки.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСерия, СтрокаСерияМатериала);
				
				НоваяСерия.НоменклатураПредставление = НоменклатураКлиентСервер.ПредставлениеНоменклатуры(
																СтрокаСерияМатериала.НоменклатураПредставление,
																СтрокаСерияМатериала.ХарактеристикаПредставление);
				
				// Для предотвращения зацикливания
				СтруктураПоиска = Новый Структура("Номенклатура,Характеристика,Серия", 
											НоваяСерия.Номенклатура, НоваяСерия.Характеристика, НоваяСерия.Серия);
			 	СписокСтрок = ОтработаннаяНоменклатура.НайтиСтроки(СтруктураПоиска);
				Если СписокСтрок.Количество() = 0 Тогда
					НоваяНоменклатура = НовыйСписокНоменклатуры.Добавить();
					ЗаполнитьЗначенияСвойств(НоваяНоменклатура, НоваяСерия);
					НоваяНоменклатура.Строки = НоваяСерия.Строки;
				КонецЕсли; 
				
			КонецЦикла; 
		КонецЦикла;
		
		СписокНоменклатуры = НовыйСписокНоменклатуры.Скопировать();
		
	КонецЦикла; 
	
	Возврат СтруктураСерииНоменклатурыДерево;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ИспользованныеСерии(СписокНоменклатуры)

	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ВЫРАЗИТЬ(СписокНоменклатуры.Номенклатура КАК Справочник.Номенклатура) КАК Номенклатура,
	|	ВЫРАЗИТЬ(СписокНоменклатуры.Характеристика КАК Справочник.ХарактеристикиНоменклатуры) КАК Характеристика,
	|	ВЫРАЗИТЬ(СписокНоменклатуры.Серия КАК Справочник.СерииНоменклатуры) КАК Серия
	|ПОМЕСТИТЬ СписокНоменклатуры
	|ИЗ
	|	&СписокНоменклатуры КАК СписокНоменклатуры
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	Характеристика,
	|	Серия
	|;
	|";
	
	
	#Область ПоступлениеСерииПриСборке
	Запрос.Текст = Запрос.Текст + 
	"////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ДвижениеСерии.Номенклатура КАК Номенклатура,
	|	ДвижениеСерии.Характеристика КАК Характеристика,
	|	ДвижениеСерии.Серия КАК Серия,
	|	ВЫРАЗИТЬ(ДвижениеСерии.Документ КАК Документ.СборкаТоваров) КАК Документ,
	|	ДвижениеСерии.КоличествоОборот КАК Количество
	|ПОМЕСТИТЬ ПоступлениеСерииПоСборке
	|ИЗ
	|	РегистрНакопления.ДвиженияСерийТоваров.Обороты(
	|			,
	|			,
	|			,
	|			(Номенклатура, Характеристика, Серия) В
	|					(ВЫБРАТЬ
	|						СписокНоменклатуры.Номенклатура,
	|						СписокНоменклатуры.Характеристика,
	|						СписокНоменклатуры.Серия
	|					ИЗ
	|						СписокНоменклатуры КАК СписокНоменклатуры)
	|				И СкладскаяОперация = ЗНАЧЕНИЕ(Перечисление.СкладскиеОперации.ПриемкаСобранныхКомплектов)) КАК ДвижениеСерии
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	Характеристика,
	|	Серия,
	|	Документ
	|;
	|";
	#КонецОбласти
	
	
	#Область РасходСерий
	Запрос.Текст = Запрос.Текст + 
	"////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ПоступлениеСерииПоСборке.Номенклатура КАК НоменклатураСписка,
	|	ПоступлениеСерииПоСборке.Характеристика КАК ХарактеристикаСписка,
	|	ПоступлениеСерииПоСборке.Серия КАК СерияСписка,
	|	ИспользованныеСерии.Документ КАК Документ,
	|	ИспользованныеСерии.Отправитель КАК Подразделение,
	|	ИспользованныеСерии.Номенклатура КАК Номенклатура,
	|	ИспользованныеСерии.Номенклатура.Представление КАК НоменклатураПредставление,
	|	ИспользованныеСерии.Номенклатура.ЕдиницаИзмерения.Представление КАК ЕдиницаИзмерения,
	|	ИспользованныеСерии.Характеристика КАК Характеристика,
	|	ИспользованныеСерии.Характеристика.Представление КАК ХарактеристикаПредставление,
	|	ИспользованныеСерии.Серия КАК Серия,
	|	ИспользованныеСерии.Серия.Представление КАК СерияПредставление,
	|	ИспользованныеСерии.КоличествоОборот КАК Израсходовано
	|ИЗ
	|	РегистрНакопления.ДвиженияСерийТоваров.Обороты(
	|			,
	|			,
	|			,
	|			Документ В
	|					(ВЫБРАТЬ
	|						ПоступлениеСерииПоСборке.Документ
	|					ИЗ
	|						ПоступлениеСерииПоСборке)
	|				И СкладскаяОперация = ЗНАЧЕНИЕ(Перечисление.СкладскиеОперации.ОтгрузкаКомплектующихДляСборки)) КАК ИспользованныеСерии
	|		ЛЕВОЕ СОЕДИНЕНИЕ ПоступлениеСерииПоСборке КАК ПоступлениеСерииПоСборке
	|		ПО (ПоступлениеСерииПоСборке.Документ = ИспользованныеСерии.Документ)
	|
	|
	|УПОРЯДОЧИТЬ ПО
	|	НоменклатураПредставление,
	|	ХарактеристикаПредставление,
	|	СерияПредставление";
	#КонецОбласти
	
	Запрос.УстановитьПараметр("СписокНоменклатуры", СписокНоменклатуры);
	
	Результат = Запрос.Выполнить().Выгрузить();
	Результат.Индексы.Добавить("НоменклатураСписка,СерияСписка");
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти

#КонецЕсли
