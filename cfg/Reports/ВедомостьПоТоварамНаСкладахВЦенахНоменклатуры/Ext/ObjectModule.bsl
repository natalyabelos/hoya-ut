﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Настройки общей формы отчета подсистемы "Варианты отчетов".
//
// Параметры:
//   Форма - УправляемаяФорма - Форма отчета.
//   КлючВарианта - Строка - Имя предопределенного варианта отчета или уникальный идентификатор пользовательского.
//   Настройки - Структура - см. возвращаемое значение ОтчетыКлиентСервер.ПолучитьНастройкиОтчетаПоУмолчанию().
//
Процедура ОпределитьНастройкиФормы(Форма, КлючВарианта, Настройки) Экспорт
	Настройки.События.ПередЗагрузкойВариантаНаСервере = Истина;
КонецПроцедуры

// Вызывается в одноименном обработчике формы отчета после выполнения кода формы.
//
// Подробнее - см. ОтчетыПереопределяемый.ПередЗагрузкойВариантаНаСервере
//
Процедура ПередЗагрузкойВариантаНаСервере(ЭтаФорма, НовыеНастройкиКД) Экспорт
	
	Отчет = ЭтаФорма.Отчет;
	КомпоновщикНастроекФормы = Отчет.КомпоновщикНастроек;
	
	// Изменение настроек по функциональным опциям
	НастроитьПараметрыОтборыПоФункциональнымОпциям(КомпоновщикНастроекФормы);
	
	НовыеНастройкиКД = КомпоновщикНастроекФормы.Настройки;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	
	ПараметрВидЦены = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(КомпоновщикНастроек, "ВидЦены");
	Если ПараметрВидЦены <> Неопределено Тогда
		Если Не ЗначениеЗаполнено(ПараметрВидЦены.Значение) Тогда
			Если Не ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоВидовЦен") Тогда
				КомпоновкаДанныхКлиентСервер.УстановитьПараметр(КомпоновщикНастроек, "ОценкаЗапасовПоВидуЦен", 3);
				КомпоновкаДанныхКлиентСервер.УстановитьПараметр(КомпоновщикНастроек, "ВидЦены", ЦенообразованиеВызовСервера.ВидЦеныПрайсЛист());
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	ПараметрОценкаЗапасовПоВидуЦен = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(КомпоновщикНастроек, "ОценкаЗапасовПоВидуЦен");
	Если ПараметрОценкаЗапасовПоВидуЦен.Значение = 3 Тогда
		ПараметрВидЦены = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(КомпоновщикНастроек, "ВидЦены");
		Если Не ЗначениеЗаполнено(ПараметрВидЦены.Значение) Тогда
			ВызватьИсключение НСтр("ru= 'Не указан ""Вид цены"".'");
		КонецЕсли;
	КонецЕсли;
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьУпаковкиНоменклатуры") Тогда
		
		СхемаКомпоновкиДанных = Отчеты.ВедомостьПоТоварамНаСкладахВЦенахНоменклатуры.ПолучитьМакет("ОсновнаяСхемаКомпоновкиДанных");
		ТекстЗапроса = СхемаКомпоновкиДанных.НаборыДанных[0].Запрос;
		
		ЗаменяемыйТекст = 
		"	ВЫБОР
		|		КОГДА ЦеныНоменклатурыА.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
		|				ИЛИ ЕСТЬNULL(ЦеныНоменклатурыА.Упаковка.Коэффициент, 0) = 0
		|			ТОГДА ЦеныНоменклатурыА.Цена
		|		ИНАЧЕ ЦеныНоменклатурыА.Цена / ЦеныНоменклатурыА.Упаковка.Коэффициент
		|	КОНЕЦ КАК Цена,
		|	ВЫБОР
		|		КОГДА ЦеныНоменклатурыБ.Цена ЕСТЬ NULL 
		|			ТОГДА 0
		|		КОГДА ЦеныНоменклатурыБ.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
		|				ИЛИ ЕСТЬNULL(ЦеныНоменклатурыБ.Упаковка.Коэффициент, 0) = 0
		|			ТОГДА ЦеныНоменклатурыБ.Цена
		|		ИНАЧЕ ЦеныНоменклатурыБ.Цена / ЦеныНоменклатурыБ.Упаковка.Коэффициент
		|	КОНЕЦ КАК СтараяЦена,
		|	ВЫБОР
		|		КОГДА ЦеныНоменклатурыА.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
		|				ИЛИ ЕСТЬNULL(ЦеныНоменклатурыА.Упаковка.Коэффициент, 0) = 0
		|			ТОГДА ЦеныНоменклатурыА.Цена
		|		ИНАЧЕ ЦеныНоменклатурыА.Цена / ЦеныНоменклатурыА.Упаковка.Коэффициент
		|	КОНЕЦ - ВЫБОР
		|		КОГДА ЦеныНоменклатурыБ.Цена ЕСТЬ NULL 
		|			ТОГДА 0
		|		КОГДА ЦеныНоменклатурыБ.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
		|				ИЛИ ЕСТЬNULL(ЦеныНоменклатурыБ.Упаковка.Коэффициент, 0) = 0
		|			ТОГДА ЦеныНоменклатурыБ.Цена
		|		ИНАЧЕ ЦеныНоменклатурыБ.Цена / ЦеныНоменклатурыБ.Упаковка.Коэффициент
		|	КОНЕЦ КАК Дельта";
		
		Если Найти(ТекстЗапроса, ЗаменяемыйТекст) = 0 Тогда
			ВызватьИсключение НСтр("ru = 'Некорректный текст запроса'");
		КонецЕсли;
		
		ТекстЗамены = 
		"	ЦеныНоменклатурыА.Цена КАК Цена,
		|	ВЫБОР
		|		КОГДА
		|			ЦеныНоменклатурыБ.Цена ЕСТЬ NULL
		|		ТОГДА
		|			0
		|		ИНАЧЕ
		|			ЦеныНоменклатурыБ.Цена
		|	КОНЕЦ КАК СтараяЦена,
		|		ЦеныНоменклатурыА.Цена
		|		- ВЫБОР
		|			КОГДА
		|				ЦеныНоменклатурыБ.Цена ЕСТЬ NULL
		|			ТОГДА
		|				0
		|			ИНАЧЕ
		|				ЦеныНоменклатурыБ.Цена
		|		КОНЕЦ
		|		 КАК Дельта";
		
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, ЗаменяемыйТекст, ТекстЗамены);
		СхемаКомпоновкиДанных.НаборыДанных[0].Запрос = ТекстЗапроса;
		
	КонецЕсли;
	
	СегментыСервер.ВключитьОтборПоСегментуНоменклатурыВСКД(КомпоновщикНастроек);
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновки = КомпоновщикМакета.Выполнить(
		СхемаКомпоновкиДанных,
		КомпоновщикНастроек.ПолучитьНастройки(),
		ДанныеРасшифровки);

	КомпоновкаДанныхСервер.УстановитьЗаголовкиМакетаКомпоновки(ПолучитьСтруктуруЗаголовковПолей(), МакетКомпоновки);
	КомпоновкаДанныхСервер.УстановитьЗаголовкиМакетаКомпоновки(СтруктураДинамическихЗаголовков(), МакетКомпоновки);
	
	ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновки.Инициализировать(
		МакетКомпоновки,
		,
		ДанныеРасшифровки);

	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
	ПроцессорВывода.УстановитьДокумент(ДокументРезультат);
	ПроцессорВывода.Вывести(ПроцессорКомпоновки, Истина);

	КомпоновкаДанныхСервер.СкрытьВспомогательныеПараметрыОтчета(СхемаКомпоновкиДанных, КомпоновщикНастроек, ДокументРезультат, ВспомогательныеПараметрыОтчета());
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции
	
Функция ВспомогательныеПараметрыОтчета()
	ВспомогательныеПараметры = Новый Массив;
	
	ПараметрОценкаЗапасовПоВидуЦен = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(КомпоновщикНастроек, "ОценкаЗапасовПоВидуЦен");
	Если Не ПараметрОценкаЗапасовПоВидуЦен.Значение = 3 Тогда
		ВспомогательныеПараметры.Добавить("ВидЦены");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоВидовЦен") Тогда
		ВспомогательныеПараметры.Добавить("ОценкаЗапасовПоВидуЦен");
	КонецЕсли;
	
	ВспомогательныеПараметры.Добавить("КоличественныеИтогиПоЕдИзм");
		
	КомпоновкаДанныхСервер.ДобавитьВспомогательныеПараметрыОтчетаПоФункциональнымОпциям(ВспомогательныеПараметры);
	
	Возврат ВспомогательныеПараметры;
КонецФункции

Функция ПолучитьСтруктуруЗаголовковПолей()
	
	Возврат КомпоновкаДанныхСервер.ПолучитьСтруктуруЗаголовковПолейЕдиницИзмерений(КомпоновщикНастроек);
	
КонецФункции

Функция СтруктураДинамическихЗаголовков()
	ДинамическиеЗаголовки = Новый Структура;
	
	Параметр = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(КомпоновщикНастроек, "ГруппировкаНоменклатуры");
	ДоступнаяНастройка = ОтчетыКлиентСервер.НайтиДоступнуюНастройку(КомпоновщикНастроек.Настройки, Параметр);
	Если ДоступнаяНастройка <> Неопределено Тогда
		ПредставлениеЗначенияПараметра = ДоступнаяНастройка.ДоступныеЗначения[Параметр.Значение-1];
		ДинамическиеЗаголовки.Вставить("ГруппировкаНоменклатуры", ПредставлениеЗначенияПараметра);
	КонецЕсли;
	
	Возврат ДинамическиеЗаголовки;
КонецФункции

Процедура НастроитьПараметрыОтборыПоФункциональнымОпциям(КомпоновщикНастроекФормы)
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьЕдиницыИзмеренияДляОтчетов") Тогда
		КомпоновкаДанныхСервер.УдалитьПараметрИзПользовательскихНастроекОтчета(КомпоновщикНастроекФормы, "ЕдиницыКоличества");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
