﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УТ 
//
Процедура ЗаполнениеКонстантыУправлениеТорговлей() Экспорт
	
	ЭтоУправлениеПредприятием = Константы.УправлениеПредприятием.Получить();
	
	Константы.УправлениеТорговлей.Установить(НЕ ЭтоУправлениеПредприятием);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

