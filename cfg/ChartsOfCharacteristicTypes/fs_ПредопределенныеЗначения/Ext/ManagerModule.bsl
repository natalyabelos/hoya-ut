﻿// Возвращает значение или список значений по переданному параметру, или имени параметра
// Может вернуть список значений, если указана группа параметров, или значение параметра в ПВХ задано списком


Функция ПолучитьЗначение(Параметр, Первое = Ложь, ТипРезультата = "")  Экспорт
	
	ТЗ = ПолучитьТаблицуЗначений(Параметр);
	
	Если ТЗ.Количество()=0 тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Если Первое тогда
		Возврат ТЗ[0].ЗначениеПараметра;
	КонецЕсли;
	
	ТипРезультата = ВРЕГ(ТипРезультата);
	
	Если ТипРезультата = "" или ТипРезультата = "МАССИВ" тогда
		
		Возврат ТЗ.ВыгрузитьКолонку(0);
		
	ИначеЕсли ТипРезультата = "СПИСОК" тогда
		
		Результат = Новый СписокЗначений;
		Для Каждого СтрокаРезультата из ТЗ цикл
			Результат.Добавить(СтрокаРезультата.ЗначениеПараметра);	
		КонецЦикла;
		
		Возврат Результат;
		
	ИначеЕсли ТипРезультата = "СООТВЕТСТВИЕ" тогда
		
		Результат = Новый Соответствие;
		Для Каждого СтрокаРезультата из ТЗ цикл
			Результат.Вставить(СтрокаРезультата.ЗначениеПараметра, Истина);	
		КонецЦикла;
		
		Возврат Результат;
		
	Иначе
		
		Возврат Неопределено;
		
	КонецЕсли;
	
КонецФункции

Функция ПолучитьТаблицуЗначений(Параметр) 

	Если ТипЗнч(Параметр) = Тип("Строка") Тогда
		ПВХПараметр = ПланыВидовХарактеристик.ард_ПредопределенныеЗначения[Параметр];
	Иначе
		ПВХПараметр = Параметр;
	КонецЕсли; 
	
	МассивЗначений = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ард_ПредопределенныеЗначения.ЗначениеПараметра
	|ИЗ
	|	ПланВидовХарактеристик.ард_ПредопределенныеЗначения КАК ард_ПредопределенныеЗначения
	|ГДЕ
	|	ард_ПредопределенныеЗначения.Ссылка В ИЕРАРХИИ(&Параметр)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ард_ПредопределенныеЗначенияСписокЗначенийПараметров.ЗначениеПараметра
	|ИЗ
	|	ПланВидовХарактеристик.ард_ПредопределенныеЗначения.СписокЗначенийПараметров КАК ард_ПредопределенныеЗначенияСписокЗначенийПараметров
	|ГДЕ
	|	ард_ПредопределенныеЗначенияСписокЗначенийПараметров.Ссылка В ИЕРАРХИИ(&Параметр)";
	
	Запрос.УстановитьПараметр("Параметр", ПВХПараметр);
	
	Возврат Запрос.Выполнить().Выгрузить();

КонецФункции // ПолучитьЗначение() 
 