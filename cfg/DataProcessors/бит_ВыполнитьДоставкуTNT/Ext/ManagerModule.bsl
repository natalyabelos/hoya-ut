﻿// Функция формирует табличный документ с печатной формой накладной,
// разработанной методистами
//
// Возвращаемое значение:
//  Табличный документ - печатная форма накладной
//
Функция ПечатьLabelTNT(Данные)Экспорт

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_TNT_Label";
	Макет = ПолучитьМакет("Label");

	МассивConNumber = Новый Массив();
	Для Каждого Стр Из Данные Цикл
		МассивConNumber.Добавить(Стр.ConNumber);
	КонецЦикла;
	
	ТЗ_Данные = ПолучитьДанныеЭтикеток(МассивConNumber);
	
	Для каждого Строка из ТЗ_Данные Цикл
		ОбластьМакета = Макет.ПолучитьОбласть("Этикетка");
		ОбластьМакета.Параметры.Заполнить(Строка);
		
		ПараметрыШтрихкода = Новый Структура;
		//ПараметрыШтрихкода.Вставить("Ширина",          ОбластьМакета.Рисунки.КартинкаШтрихкода.Ширина);
		//ПараметрыШтрихкода.Вставить("Высота",          ОбластьМакета.Рисунки.КартинкаШтрихкода.Высота);
		ПараметрыШтрихкода.Вставить("Ширина",          150);
		ПараметрыШтрихкода.Вставить("Высота",          60);
		ПараметрыШтрихкода.Вставить("Штрихкод",        Строка.BarCode);
		ПараметрыШтрихкода.Вставить("ТипКода",         4); // Code128
		ПараметрыШтрихкода.Вставить("ОтображатьТекст", Ложь);
		ПараметрыШтрихкода.Вставить("РазмерШрифта",    6);
		
		ОбластьМакета.Рисунки.КартинкаШтрихкода.Картинка = МенеджерОборудованияВызовСервера.ПолучитьКартинкуШтрихкода(ПараметрыШтрихкода);
		
		ТабДокумент.Вывести(ОбластьМакета);
		
		ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	КонецЦикла;
	
	// Зададим параметры макета
	ТабДокумент.ПолеСверху = 0;
	ТабДокумент.ПолеСлева  = 0;
	ТабДокумент.ПолеСнизу  = 0;
	ТабДокумент.ПолеСправа = 0;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	
	Возврат ТабДокумент;

КонецФункции // 

Процедура Печать(МассивОбъектов, ПараметрыПечати,КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "Label") Тогда
		
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
		КоллекцияПечатныхФорм,
		"Label",
		НСтр("ru = 'LabelTNT'"),
		ПечатьLabel(МассивОбъектов,,ПараметрыПечати),
		,
		"Обработка.бит_ВыполнитьДоставкуTNT");
		
	КонецЕсли;
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "Connote") Тогда
		
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
		КоллекцияПечатныхФорм,
		"Connote",
		НСтр("ru = 'Connote'"),
		ПечатьConnote(МассивОбъектов,,ПараметрыПечати),
		,
		"Обработка.бит_ВыполнитьДоставкуTNT");
		
	КонецЕсли;

КонецПроцедуры

// Внешняя функция печати
// Параметры
//  ИмяМакета  – Строка – имя формы печати
//
// Возвращаемое значение:
//   Булево   –Печать прошла успешно - Истина, иначе - Ложь
//
Функция ПечатьLabel(Ссылка, ИмяМакета = "", ПараметрыПечати = Неопределено)	Экспорт
	
	ТабДокумент = ПечатьLabelTNT(Ссылка);
	Если ПараметрыПечати <> Неопределено Тогда
		ТабДокумент.ВысотаСтраницы = ПараметрыПечати.ВысотаСтраницы;
		ТабДокумент.ШиринаСтраницы = ПараметрыПечати.ШиринаСтраницы;
		ТабДокумент.ИмяПринтера = ПараметрыПечати.ИмяПринтера;
	КонецЕсли;
	
	Возврат ТабДокумент;
		
КонецФункции // Печать()

Функция ПолучитьДанныеЭтикеток(МассивConNumber)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	бит_МанифестTNTСрезПоследних.Период,
	|	бит_МанифестTNTСрезПоследних.ConNumber КАК ConNumber,
	|	бит_МанифестTNTСрезПоследних.PackageIndex КАК PackageIndex,
	|	бит_МанифестTNTСрезПоследних.ShipDate,
	|	бит_МанифестTNTСрезПоследних.Sender_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Sender_City,
	|	бит_МанифестTNTСрезПоследних.Sender_Province,
	|	бит_МанифестTNTСрезПоследних.Sender_Postcode,
	|	бит_МанифестTNTСрезПоследних.Sender_Country,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactName,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.Account,
	|	бит_МанифестTNTСрезПоследних.Receiver_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Receiver_City,
	|	бит_МанифестTNTСрезПоследних.Receiver_Province,
	|	бит_МанифестTNTСрезПоследних.Receiver_Postcode,
	|	бит_МанифестTNTСрезПоследних.Receiver_Country,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactName,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.CustomerRef,
	|	бит_МанифестTNTСрезПоследних.Description,
	|	бит_МанифестTNTСрезПоследних.DeliveryInst,
	|	бит_МанифестTNTСрезПоследних.Length,
	|	бит_МанифестTNTСрезПоследних.Height,
	|	бит_МанифестTNTСрезПоследних.Width,
	|	бит_МанифестTNTСрезПоследних.Weight,
	|	бит_МанифестTNTСрезПоследних.Items,
	|	бит_МанифестTNTСрезПоследних.Service,
	|	бит_МанифестTNTСрезПоследних.Option,
	|	бит_МанифестTNTСрезПоследних.BarCode
	|ИЗ
	|	РегистрСведений.бит_МанифестTNT.СрезПоследних(, ConNumber В (&МассивConNumber)) КАК бит_МанифестTNTСрезПоследних
	|
	|УПОРЯДОЧИТЬ ПО
	|	ConNumber,
	|	PackageIndex";
	
	Запрос.УстановитьПараметр("МассивConNumber", МассивConNumber);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат РезультатЗапроса.Выгрузить();
	
КонецФункции

// Функция формирует табличный документ с печатной формой накладной,
// разработанной методистами
//
// Возвращаемое значение:
//  Табличный документ - печатная форма накладной
//
Функция ПечатьConnoteTNT(Данные)Экспорт

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_TNT_Connote";
	Макет = ПолучитьМакет("Connote");
	МакетМанифест = ПолучитьМакет("Manifest");

	МассивConNumber = Новый Массив();
	Для Каждого Стр Из Данные Цикл
		МассивConNumber.Добавить(Стр.ConNumber);
	КонецЦикла;
	
	//ТЗ_Данные = ПолучитьДанныеНакладных(МассивConNumber);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	бит_МанифестTNTСрезПоследних.ConNumber,
	|	бит_МанифестTNTСрезПоследних.ShipDate,
	|	бит_МанифестTNTСрезПоследних.Sender_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Sender_City,
	|	бит_МанифестTNTСрезПоследних.Sender_Province,
	|	бит_МанифестTNTСрезПоследних.Sender_Postcode,
	|	бит_МанифестTNTСрезПоследних.Sender_Country,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactName,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.Account,
	|	бит_МанифестTNTСрезПоследних.Receiver_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Receiver_City,
	|	бит_МанифестTNTСрезПоследних.Receiver_Province,
	|	бит_МанифестTNTСрезПоследних.Receiver_Postcode,
	|	бит_МанифестTNTСрезПоследних.Receiver_Country,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactName,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.CustomerRef,
	|	бит_МанифестTNTСрезПоследних.Description,
	|	бит_МанифестTNTСрезПоследних.DeliveryInst,
	|	бит_МанифестTNTСрезПоследних.Length,
	|	бит_МанифестTNTСрезПоследних.Height,
	|	бит_МанифестTNTСрезПоследних.Width,
	|	бит_МанифестTNTСрезПоследних.Weight,
	|	бит_МанифестTNTСрезПоследних.Items,
	|	бит_МанифестTNTСрезПоследних.Service,
	|	бит_МанифестTNTСрезПоследних.Option,
	|	бит_МанифестTNTСрезПоследних.PackageIndex,
	|	бит_МанифестTNTСрезПоследних.BarCode
	|ПОМЕСТИТЬ ВТ
	|ИЗ
	|	РегистрСведений.бит_МанифестTNT.СрезПоследних(, ConNumber В (&МассивConNumber)) КАК бит_МанифестTNTСрезПоследних
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ.ConNumber КАК ConNumber,
	|	ВТ.ShipDate КАК ShipDate,
	|	ВТ.Sender_CompanyName КАК Sender_CompanyName,
	|	ВТ.Sender_StreetAddress1 КАК Sender_StreetAddress1,
	|	ВТ.Sender_StreetAddress2 КАК Sender_StreetAddress2,
	|	ВТ.Sender_StreetAddress3 КАК Sender_StreetAddress3,
	|	ВТ.Sender_City КАК Sender_City,
	|	ВТ.Sender_Province КАК Sender_Province,
	|	ВТ.Sender_Postcode КАК Sender_Postcode,
	|	ВТ.Sender_Country КАК Sender_Country,
	|	ВТ.Sender_ContactName КАК Sender_ContactName,
	|	ВТ.Sender_ContactDialCode КАК Sender_ContactDialCode,
	|	ВТ.Sender_ContactTelephone КАК Sender_ContactTelephone,
	|	ВТ.Account КАК Account,
	|	ВТ.Receiver_CompanyName КАК Receiver_CompanyName,
	|	ВТ.Receiver_StreetAddress1 КАК Receiver_StreetAddress1,
	|	ВТ.Receiver_StreetAddress2 КАК Receiver_StreetAddress2,
	|	ВТ.Receiver_StreetAddress3 КАК Receiver_StreetAddress3,
	|	ВТ.Receiver_City КАК Receiver_City,
	|	ВТ.Receiver_Province КАК Receiver_Province,
	|	ВТ.Receiver_Postcode КАК Receiver_Postcode,
	|	ВТ.Receiver_Country КАК Receiver_Country,
	|	ВТ.Receiver_ContactName КАК Receiver_ContactName,
	|	ВТ.Receiver_ContactDialCode КАК Receiver_ContactDialCode,
	|	ВТ.Receiver_ContactTelephone КАК Receiver_ContactTelephone,
	|	ВТ.CustomerRef КАК CustomerRef,
	|	ВТ.Description КАК Description,
	|	ВТ.DeliveryInst КАК DeliveryInst,
	|	ВТ.Length КАК Length,
	|	ВТ.Height КАК Height,
	|	ВТ.Width КАК Width,
	|	ВТ.Weight КАК Weight,
	|	ВТ.Items КАК Items,
	|	ВТ.Service КАК Service,
	|	ВТ.Option КАК Option,
	|	ВЫРАЗИТЬ(ВТ.Length * ВТ.Height * ВТ.Width КАК ЧИСЛО(15, 2)) КАК Volume,
	|	ВТ.PackageIndex,
	|	ВТ.BarCode КАК BarCode
	|ИЗ
	|	ВТ КАК ВТ
	|ИТОГИ
	|	МАКСИМУМ(ShipDate),
	|	МАКСИМУМ(Sender_CompanyName),
	|	МАКСИМУМ(Sender_StreetAddress1),
	|	МАКСИМУМ(Sender_StreetAddress2),
	|	МАКСИМУМ(Sender_StreetAddress3),
	|	МАКСИМУМ(Sender_City),
	|	МАКСИМУМ(Sender_Province),
	|	МАКСИМУМ(Sender_Postcode),
	|	МАКСИМУМ(Sender_Country),
	|	МАКСИМУМ(Sender_ContactName),
	|	МАКСИМУМ(Sender_ContactDialCode),
	|	МАКСИМУМ(Sender_ContactTelephone),
	|	МАКСИМУМ(Account),
	|	МАКСИМУМ(Receiver_CompanyName),
	|	МАКСИМУМ(Receiver_StreetAddress1),
	|	МАКСИМУМ(Receiver_StreetAddress2),
	|	МАКСИМУМ(Receiver_StreetAddress3),
	|	МАКСИМУМ(Receiver_City),
	|	МАКСИМУМ(Receiver_Province),
	|	МАКСИМУМ(Receiver_Postcode),
	|	МАКСИМУМ(Receiver_Country),
	|	МАКСИМУМ(Receiver_ContactName),
	|	МАКСИМУМ(Receiver_ContactDialCode),
	|	МАКСИМУМ(Receiver_ContactTelephone),
	|	МАКСИМУМ(CustomerRef),
	|	МАКСИМУМ(Description),
	|	МАКСИМУМ(DeliveryInst),
	|	МАКСИМУМ(Length),
	|	МАКСИМУМ(Height),
	|	МАКСИМУМ(Width),
	|	МАКСИМУМ(Weight),
	|	МАКСИМУМ(Items),
	|	МАКСИМУМ(Service),
	|	МАКСИМУМ(Option),
	|	СУММА(Volume),
	|	МАКСИМУМ(BarCode)
	|ПО
	|	ConNumber";
	
	Запрос.УстановитьПараметр("МассивConNumber", МассивConNumber);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаConNumber = РезультатЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	Пока ВыборкаConNumber.Следующий() Цикл
		ОбластьМакета = Макет.ПолучитьОбласть("Этикетка");
		ОбластьМакета.Параметры.Заполнить(ВыборкаConNumber);
		ОбластьМакета.Параметры.Copy = "Custom";
				
		ТабДокумент.Вывести(ОбластьМакета);
		
		ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
		ОбластьМакета.Параметры.Copy = "Receiver";
		ТабДокумент.Вывести(ОбластьМакета);
		ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
		ОбластьМакетаМанифест = МакетМанифест.ПолучитьОбласть("Этикетка");
		ОбластьМакетаМанифест.Параметры.Заполнить(ВыборкаConNumber);
		ОбластьМакетаМанифест.Параметры.PrintDate = Формат(ТекущаяДата(),"ДФ=dd/MM/yy");
		
		ПараметрыШтрихкода = Новый Структура;
		ПараметрыШтрихкода.Вставить("Ширина",          160);
		ПараметрыШтрихкода.Вставить("Высота",          70);
		ПараметрыШтрихкода.Вставить("Штрихкод",        ВыборкаConNumber.BarCode);
		ПараметрыШтрихкода.Вставить("ТипКода",         4); // Code128
		ПараметрыШтрихкода.Вставить("ОтображатьТекст", Ложь);
		ПараметрыШтрихкода.Вставить("РазмерШрифта",    6);
		
		ОбластьМакетаМанифест.Рисунки.КартинкаШтрихкода.Картинка = МенеджерОборудованияВызовСервера.ПолучитьКартинкуШтрихкода(ПараметрыШтрихкода);
		
		ТабДокумент.Вывести(ОбластьМакетаМанифест);

		ВыборкаДетальныеЗаписи = ВыборкаConNumber.Выбрать();
		
		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			ОбластьМакетаКоробки = МакетМанифест.ПолучитьОбласть("Коробки");
			ОбластьМакетаКоробки.Параметры.Заполнить(ВыборкаДетальныеЗаписи);
			ТабДокумент.Вывести(ОбластьМакетаКоробки);
		КонецЦикла;
		
		ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
	КонецЦикла;

	// Зададим параметры макета
	ТабДокумент.ПолеСверху = 0;
	ТабДокумент.ПолеСлева  = 0;
	ТабДокумент.ПолеСнизу  = 0;
	ТабДокумент.ПолеСправа = 0;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	
	Возврат ТабДокумент;

КонецФункции // 

// Внешняя функция печати
// Параметры
//  ИмяМакета  – Строка – имя формы печати
//
// Возвращаемое значение:
//   Булево   –Печать прошла успешно - Истина, иначе - Ложь
//
Функция ПечатьConnote(Ссылка, ИмяМакета = "", ПараметрыПечати = Неопределено)	Экспорт
	
	ТабДокумент = ПечатьConnoteTNT(Ссылка);
	Если ПараметрыПечати <> Неопределено Тогда
		ТабДокумент.ВысотаСтраницы = ПараметрыПечати.ВысотаСтраницы;
		ТабДокумент.ШиринаСтраницы = ПараметрыПечати.ШиринаСтраницы;
		ТабДокумент.ИмяПринтера = ПараметрыПечати.ИмяПринтера;
	КонецЕсли;
	
	Возврат ТабДокумент;
		
КонецФункции // Печать()

Функция ПолучитьДанныеНакладных(МассивConNumber)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	бит_МанифестTNTСрезПоследних.Период,
	|	бит_МанифестTNTСрезПоследних.ConNumber,
	|	бит_МанифестTNTСрезПоследних.ShipDate,
	|	бит_МанифестTNTСрезПоследних.Sender_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Sender_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Sender_City,
	|	бит_МанифестTNTСрезПоследних.Sender_Province,
	|	бит_МанифестTNTСрезПоследних.Sender_Postcode,
	|	бит_МанифестTNTСрезПоследних.Sender_Country,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactName,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Sender_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.Account,
	|	бит_МанифестTNTСрезПоследних.Receiver_CompanyName,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress1,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress2,
	|	бит_МанифестTNTСрезПоследних.Receiver_StreetAddress3,
	|	бит_МанифестTNTСрезПоследних.Receiver_City,
	|	бит_МанифестTNTСрезПоследних.Receiver_Province,
	|	бит_МанифестTNTСрезПоследних.Receiver_Postcode,
	|	бит_МанифестTNTСрезПоследних.Receiver_Country,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactName,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactDialCode,
	|	бит_МанифестTNTСрезПоследних.Receiver_ContactTelephone,
	|	бит_МанифестTNTСрезПоследних.CustomerRef,
	|	бит_МанифестTNTСрезПоследних.Description,
	|	бит_МанифестTNTСрезПоследних.DeliveryInst,
	|	бит_МанифестTNTСрезПоследних.Length,
	|	бит_МанифестTNTСрезПоследних.Height,
	|	бит_МанифестTNTСрезПоследних.Width,
	|	бит_МанифестTNTСрезПоследних.Weight,
	|	бит_МанифестTNTСрезПоследних.Items,
	|	бит_МанифестTNTСрезПоследних.Service,
	|	бит_МанифестTNTСрезПоследних.Option
	|ПОМЕСТИТЬ ВТ
	|ИЗ
	|	РегистрСведений.бит_МанифестTNT.СрезПоследних(, ConNumber В (&МассивConNumber)) КАК бит_МанифестTNTСрезПоследних
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ.Период,
	|	ВТ.ConNumber,
	|	ВТ.ShipDate,
	|	ВТ.Sender_CompanyName,
	|	ВТ.Sender_StreetAddress1,
	|	ВТ.Sender_StreetAddress2,
	|	ВТ.Sender_StreetAddress3,
	|	ВТ.Sender_City,
	|	ВТ.Sender_Province,
	|	ВТ.Sender_Postcode,
	|	ВТ.Sender_Country,
	|	ВТ.Sender_ContactName,
	|	ВТ.Sender_ContactDialCode,
	|	ВТ.Sender_ContactTelephone,
	|	ВТ.Account,
	|	ВТ.Receiver_CompanyName,
	|	ВТ.Receiver_StreetAddress1,
	|	ВТ.Receiver_StreetAddress2,
	|	ВТ.Receiver_StreetAddress3,
	|	ВТ.Receiver_City,
	|	ВТ.Receiver_Province,
	|	ВТ.Receiver_Postcode,
	|	ВТ.Receiver_Country,
	|	ВТ.Receiver_ContactName,
	|	ВТ.Receiver_ContactDialCode,
	|	ВТ.Receiver_ContactTelephone,
	|	ВТ.CustomerRef,
	|	ВТ.Description,
	|	ВТ.DeliveryInst,
	|	ВТ.Length,
	|	ВТ.Height,
	|	ВТ.Width,
	|	ВТ.Weight,
	|	ВТ.Items,
	|	ВТ.Service,
	|	ВТ.Option,
	|	ВЫРАЗИТЬ(ВТ.Length * ВТ.Height * ВТ.Width * ВТ.Items КАК ЧИСЛО(15, 2)) КАК Volume
	|ИЗ
	|	ВТ КАК ВТ";
	
	Запрос.УстановитьПараметр("МассивConNumber", МассивConNumber);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат РезультатЗапроса.Выгрузить();
	
КонецФункции
