﻿
Функция СведенияОВнешнейОбработке() Экспорт
	ПараметрыРегистрации = Новый Структура;
	
	МассивНазначений = Новый Массив;
	МассивНазначений.Добавить("Документ.РеализацияТоваровУслуг");
	
	ПараметрыРегистрации.Вставить("Вид", "ПечатнаяФорма");
	ПараметрыРегистрации.Вставить("Назначение", МассивНазначений);
	ПараметрыРегистрации.Вставить("Наименование", "Печать накладной");
	ПараметрыРегистрации.Вставить("Версия", "1.1");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);
	ПараметрыРегистрации.Вставить("Информация", "Дополнительная печатная форма к документу РТиУ");
	
	ТаблицаКоманд = Новый ТаблицаЗначений;
	ТаблицаКоманд.Колонки.Добавить("Представление");
	ТаблицаКоманд.Колонки.Добавить("Идентификатор");
	ТаблицаКоманд.Колонки.Добавить("Использование");
	ТаблицаКоманд.Колонки.Добавить("Модификатор");
	ТаблицаКоманд.Колонки.Добавить("ПоказыватьОповещение");
	
	НоваяКоманда = ТаблицаКоманд.Добавить();
	НоваяКоманда.Представление = "ПечатьНакладной";
	НоваяКоманда.Идентификатор = "ПечатьНакладной";
	НоваяКоманда.Использование = "ВызовСерверногоМетода";
	НоваяКоманда.ПоказыватьОповещение = Истина;
	НоваяКоманда.Модификатор = "ПечатьMXL";
	
	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);
	
	Возврат ПараметрыРегистрации;		
КонецФункции	


