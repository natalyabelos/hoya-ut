﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ТипПроцессаXDTO = "DMBusinessProcessPerformance";
	ОбъектXDTO = ИнтеграцияС1СДокументооборот.ПолучитьОбъектXDTOПроцесса(ТипПроцессаXDTO, Параметры);
	ЗаполнитьФормуИзОбъектаXDTO(ОбъектXDTO);
	
	УстановитьВидимостьЭлементов();
	
	Элементы.ВариантИсполнения.ТолькоПросмотр = ЭтаФорма.ТолькоПросмотр;
	
	ИнтеграцияС1СДокументооборотПереопределяемый.ДополнительнаяОбработкаФормыБизнесПроцесса(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ЗаполнитьШаг(Исполнители);
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность Тогда
		Отказ = Истина;
		ТекстВопроса = НСтр("ru = 'Данные были изменены. Сохранить изменения?'");
		Оповещение = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтаФорма);
		Кнопки = новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да,НСтр("ru='Сохранить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Нет,НСтр("ru='Не сохранять'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена,НСтр("ru='Редактировать'"));
		ПоказатьВопрос(Оповещение, ТекстВопроса, Кнопки);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Изменен" И Параметр = Элементы.Предмет Тогда 
		Предмет = Источник.Представление;
	ИначеЕсли ИмяСобытия = "Документооборот_ВыбратьЗначениеИзВыпадающегоСпискаЗавершение" И Источник = ЭтаФорма Тогда
		Если Параметр.Реквизит = "ПорядокИсполнения" Тогда
			ЗаполнитьШаг(Исполнители);
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ГлавнаяЗадачаПредставлениеНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если ЗначениеЗаполнено(ГлавнаяЗадача) Тогда
		ИнтеграцияС1СДокументооборотКлиент.ОткрытьОбъект(ГлавнаяЗадачаТип, ГлавнаяЗадачаID, Элемент);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПредметНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если ЗначениеЗаполнено(Предмет) Тогда
		ИнтеграцияС1СДокументооборотКлиент.ОткрытьОбъект(ПредметТип, ПредметID, Элемент);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВажностьНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ИнтеграцияС1СДокументооборотКлиент.ВыбратьЗначениеИзВыпадающегоСписка("DMBusinessProcessImportance", "Важность", ЭтаФорма); 
	
КонецПроцедуры

&НаКлиенте
Процедура ВажностьАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора(
			"DMBusinessProcessImportance", Данныевыбора, Текст, СтандартнаяОбработка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВажностьОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора(
			"DMBusinessProcessImportance", ДанныеВыбора, Текст, СтандартнаяОбработка);
		
		Если ДанныеВыбора.Количество() = 1 Тогда 
			ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора(
				"Важность", ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма);
			СтандартнаяОбработка = Истина;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВажностьОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора("Важность", ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ВариантИсполненияПереключательПриИзменении(Элемент)
	ОбработатьВыборВариантаИсполнения();
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура АвторНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ИнтеграцияС1СДокументооборотКлиент.ВыбратьПользователяИзДереваПодразделений("Автор", ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура АвторАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора("DMUser", Данныевыбора, Текст, СтандартнаяОбработка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура АвторОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора("DMUser", ДанныеВыбора, Текст, СтандартнаяОбработка);
		
		Если ДанныеВыбора.Количество() = 1 Тогда 
			ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора("Автор", ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма);
			СтандартнаяОбработка = Истина;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура АвторОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора("Автор", ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПроверяющийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Оповещение = Новый ОписаниеОповещения("ПроверяющийНачалоВыбораЗавершение", ЭтаФорма);
	
	ОткрытьФорму("Обработка.ИнтеграцияС1СДокументооборот.Форма.ВыборИсполнителяБизнесПроцесса",, 
		ЭтаФорма,,,, Оповещение, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте 
Процедура ПроверяющийНачалоВыбораЗавершение(Результат, Параметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Результат.Свойство("Исполнитель", Проверяющий);
	Результат.Свойство("ИсполнительID", ПроверяющийID);
	Результат.Свойство("ИсполнительТип", ПроверяющийТип);
	
	Результат.Свойство("ОсновнойОбъектАдресации", ОсновнойОбъектАдресацииПроверяющего);
	Результат.Свойство("ОсновнойОбъектАдресацииID", ОсновнойОбъектАдресацииПроверяющегоID);
	Результат.Свойство("ОсновнойОбъектАдресацииТип", ОсновнойОбъектАдресацииПроверяющегоТип);
	
	Результат.Свойство("ДополнительныйОбъектАдресации", ДополнительныйОбъектАдресацииПроверяющего);
	Результат.Свойство("ДополнительныйОбъектАдресацииID", ДополнительныйОбъектАдресацииПроверяющегоID);
	Результат.Свойство("ДополнительныйОбъектАдресацииТип",ДополнительныйОбъектАдресацииПроверяющегоТип);
	
КонецПроцедуры

&НаКлиенте
Процедура ПроверяющийОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВПоле("Проверяющий", "ОбъектАдресацииПроверяющего", ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура КонтролерНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Оповещение = Новый ОписаниеОповещения("КонтролерНачалоВыбораЗавершение", ЭтаФорма);
		
	ОткрытьФорму("Обработка.ИнтеграцияС1СДокументооборот.Форма.ВыборИсполнителяБизнесПроцесса",,
		ЭтаФорма,,,, Оповещение, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура КонтролерНачалоВыбораЗавершение(Результат, ПараметрыОповещения) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Результат.Свойство("Исполнитель", Контролер);
	Результат.Свойство("ИсполнительID", КонтролерID);
	Результат.Свойство("ИсполнительТип", КонтролерТип);
	
	Результат.Свойство("ОсновнойОбъектАдресации",ОсновнойОбъектАдресацииКонтролера);
	Результат.Свойство("ОсновнойОбъектАдресацииID", ОсновнойОбъектАдресацииКонтролераID);
	Результат.Свойство("ОсновнойОбъектАдресацииТип", ОсновнойОбъектАдресацииКонтролераТип);
	
	Результат.Свойство("ДополнительныйОбъектАдресации", ДополнительныйОбъектАдресацииКонтролера);
	Результат.Свойство("ДополнительныйОбъектАдресацииID", ДополнительныйОбъектАдресацииКонтролераID);
	Результат.Свойство("ДополнительныйОбъектАдресацииТип", ДополнительныйОбъектАдресацииКонтролераТип);
	
	Модифицированность = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура КонтролерОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВПоле("Контролер", "ОбъектАдресацииКонтролера", ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура УчастникБизнесПроцессаАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора(
			"DMUser;DMBusinessProcessExecutorRole", Данныевыбора, Текст, СтандартнаяОбработка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УчастникБизнесПроцессаОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
		
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора(
			"DMUser;DMBusinessProcessExecutorRole", ДанныеВыбора, Текст, СтандартнаяОбработка);
		
		Если ДанныеВыбора.Количество() = 1 Тогда
			Если Элемент = Элементы.Проверяющий  Тогда
			    ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВПоле(
					"Проверяющий", "ОбъектАдресацииПроверяющего", ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма);
			ИначеЕсли Элемент = Элементы.Контролер Тогда
				ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВПоле(
					"Контролер", "ОбъектАдресацииКонтролера", ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма);
			Иначе
				ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВСписке(
					Элемент.Родитель.Родитель, ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма);
			КонецЕсли;
			СтандартнаяОбработка = Истина;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УчастникБизнесПроцессаОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ИнтеграцияС1СДокументооборотКлиент.ПрименитьВыборУчастникаБизнесПроцессаВСписке(Элемент.Родитель.Родитель, ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура СрокДатаПриИзменении(Элемент)
	
	Если Срок = НачалоДня(Срок) Тогда
		Срок = КонецДня(Срок);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыИсполнители

&НаКлиенте
Процедура ИсполнителиИсполнительНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Оповещение = Новый ОписаниеОповещения("ИсполнителиИсполнительНачалоВыбораЗавершение", ЭтаФорма);
	
	ОткрытьФорму("Обработка.ИнтеграцияС1СДокументооборот.Форма.ВыборИсполнителяБизнесПроцесса",, 
		ЭтаФорма,,,, Оповещение, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиИсполнительНачалоВыбораЗавершение(Результат, ПараметрыОповещения) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные = Элементы.Исполнители.ТекущиеДанные;
	
	Результат.Свойство("Исполнитель", ТекущиеДанные.Исполнитель);
	Результат.Свойство("ИсполнительID", ТекущиеДанные.ИсполнительID);
	Результат.Свойство("ИсполнительТип", ТекущиеДанные.ИсполнительТип);
	
	Результат.Свойство("ОсновнойОбъектАдресации", ТекущиеДанные.ОсновнойОбъектАдресации);
	Результат.Свойство("ОсновнойОбъектАдресацииID", ТекущиеДанные.ОсновнойОбъектАдресацииID);
	Результат.Свойство("ОсновнойОбъектАдресацииТип", ТекущиеДанные.ОсновнойОбъектАдресацииТип);
	
	Результат.Свойство("ДополнительныйОбъектАдресации", ТекущиеДанные.ДополнительныйОбъектАдресации);
	Результат.Свойство("ДополнительныйОбъектАдресацииID", ТекущиеДанные.ДополнительныйОбъектАдресацииID);
	Результат.Свойство("ДополнительныйОбъектАдресацииТип", ТекущиеДанные.ДополнительныйОбъектАдресацииТип);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПриАктивизацииСтроки(Элемент)
	
	Если Элементы.Исполнители.ТекущиеДанные <> Неопределено Тогда
		Элементы.ИсполнителиОтветственныйИсполнитель.Пометка = Элементы.Исполнители.ТекущиеДанные.Ответственный;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиСрокПриИзменении(Элемент)
	
	Если Элементы.Исполнители.ТекущиеДанные.Срок = НачалоДня(Элементы.Исполнители.ТекущиеДанные.Срок) Тогда
		Элементы.Исполнители.ТекущиеДанные.Срок = КонецДня(Элементы.Исполнители.ТекущиеДанные.Срок);
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПорядокИсполненияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ИнтеграцияС1СДокументооборотКлиент.ВыбратьЗначениеИзВыпадающегоСписка("DMTaskExecutionOrder", "ПорядокИсполнения", ЭтаФорма,, Истина, Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПорядокИсполненияАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора("DMTaskExecutionOrder", Данныевыбора, Текст, СтандартнаяОбработка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПорядокИсполненияОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		ИнтеграцияС1СДокументооборотВызовСервера.ДанныеДляАвтоПодбора("DMTaskExecutionOrder", ДанныеВыбора, Текст, СтандартнаяОбработка);
		Если ДанныеВыбора.Количество() = 1 Тогда 
			ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора("ПорядокИсполнения", ДанныеВыбора[0].Значение, СтандартнаяОбработка, ЭтаФорма, Истина, Элемент);
			СтандартнаяОбработка = Истина;
			ЗаполнитьШаг(Исполнители);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПорядокИсполненияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	ИнтеграцияС1СДокументооборотКлиент.ОбработкаВыбораДанныхДляАвтоПодбора("ПорядокИсполнения", ВыбранноеЗначение, СтандартнаяОбработка, ЭтаФорма, Истина, Элемент);
	ЗаполнитьШаг(Исполнители);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Записать(Команда)
	
	РезультатЗаписи = ЗаписатьОбъектВыполнить();
	
	Если РезультатЗаписи Тогда
		ИнтеграцияС1СДокументооборотКлиент.Оповестить_ЗаписьБизнесПроцесса(ЭтаФорма, Ложь);
		ЭтаФорма.Заголовок = Представление;
		Состояние(СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru='Бизнес-процесс ""%1"" сохранен.'"), Представление));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтартоватьИЗакрыть(Команда)
	
	РезультатЗапуска = ПодготовитьКПередачеИСтартоватьБизнесПроцесс();
	
	Если РезультатЗапуска Тогда
		ИнтеграцияС1СДокументооборотКлиент.Оповестить_ЗаписьБизнесПроцесса(ЭтаФорма, Истина);
		ТекстСостояния = НСтр("ru = 'Бизнес-процесс ""%Наименование%"" успешно запущен.'");
		ТекстСостояния = СтрЗаменить(ТекстСостояния,"%Наименование%", Представление);
		Состояние(ТекстСостояния);
		Модифицированность = Ложь;
		Если Открыта() Тогда
			Закрыть();
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоШаблону(Команда)
	
	Оповещение = Новый ОписаниеОповещения("ЗаполнитьПоШаблонуЗавершение", ЭтаФорма);
	ИнтеграцияС1СДокументооборотКлиент.НачатьВыборШаблонаБизнесПроцесса(Оповещение, ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтветственныйИсполнитель(Команда)
	
	Если Элементы.Исполнители.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Ответственный = Элементы.Исполнители.ТекущиеДанные.Ответственный;
	Для каждого СтрокаТаблицы Из Исполнители Цикл
		СтрокаТаблицы.Ответственный = Ложь;
	КонецЦикла;
	
	Элементы.Исполнители.ТекущиеДанные.Ответственный = НЕ Ответственный;
	Элементы.ИсполнителиОтветственныйИсполнитель.Пометка = Элементы.Исполнители.ТекущиеДанные.Ответственный;
	
КонецПроцедуры

 &НаКлиенте
Процедура ПереместитьВниз(Команда)
	
	ИндексТекущегоЭлементаКоллекции = Исполнители.Индекс(
		Элементы.Исполнители.ТекущиеДанные);
	КоличествоЭлементовКоллекции = Исполнители.Количество();
	Если ИндексТекущегоЭлементаКоллекции < КоличествоЭлементовКоллекции - 1 Тогда
		Исполнители.Сдвинуть(ИндексТекущегоЭлементаКоллекции, 1);
	КонецЕсли;
	Модифицированность = Истина;
	ЗаполнитьШаг(Исполнители);
	
КонецПроцедуры

&НаКлиенте
Процедура ПереместитьВверх(Команда)
	ИндексТекущегоЭлементаКоллекции = Исполнители.Индекс(
		Элементы.Исполнители.ТекущиеДанные);
	Если ИндексТекущегоЭлементаКоллекции > 0  Тогда
		Исполнители.Сдвинуть(ИндексТекущегоЭлементаКоллекции, -1);
	КонецЕсли;
	Модифицированность = Истина;
	ЗаполнитьШаг(Исполнители);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ЗаполнитьПоШаблонуЗавершение(РезультатВыбораШаблона, ПараметрыОповещения) Экспорт
	
	Если ТипЗнч(РезультатВыбораШаблона) = Тип("Структура") Тогда
		ЗаполнитьКарточкуПоШаблону(РезультатВыбораШаблона);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьЭлементов()
	// условия маршрутизации
	Если ИнтеграцияС1СДокументооборотПовтИсп.ДоступенФункционалВерсииСервиса("1.2.6.2") Тогда
		Элементы.ВариантИсполнения.Видимость = Истина;
		Если ВариантИсполненияID <> "Смешанно" Тогда
			Элементы.ИсполнителиШаг.Видимость = Ложь;
			Элементы.ИсполнителиПорядокИсполнения.Видимость = Ложь;
		Иначе
			Элементы.ИсполнителиШаг.Видимость = Истина;
			Элементы.ИсполнителиПорядокИсполнения.Видимость = Истина;
		КонецЕсли;
		
		Если ВариантИсполненияID <> "Параллельно" Тогда
			Элементы.ИсполнителиГруппаПеремещение.Видимость = Истина;
		Иначе
			Элементы.ИсполнителиГруппаПеремещение.Видимость = Ложь;
		КонецЕсли;
	Иначе
		Элементы.ВариантИсполнения.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьКарточкуПоШаблону(ДанныеШаблона)
	
	РезультатЗаполнения = ИнтеграцияС1СДокументооборотВызовСервера.ЗаполнитьБизнесПроцессПоШаблону(ЭтаФорма, ДанныеШаблона);
	ЗаполнитьФормуИзОбъектаXDTO(РезультатЗаполнения.object);

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьФормуИзОбъектаXDTO(ОбъектXDTO) 
	
	Если ОбъектXDTO.Установлено("objectID") Тогда
		ID = ОбъектXDTO.objectId.id;
		Тип = ОбъектXDTO.objectId.type;
	КонецЕсли;
			
	Обработки.ИнтеграцияС1СДокументооборот.УстановитьНавигационнуюСсылку(ЭтаФорма, ОбъектXDTO);
	Обработки.ИнтеграцияС1СДокументооборот.УстановитьСтандартнуюШапкуБизнесПроцесса(ЭтаФорма, ОбъектXDTO);
	Обработки.ИнтеграцияС1СДокументооборот.УстановитьКонтролераНаКарточке(ЭтаФорма, ОбъектXDTO);
	Обработки.ИнтеграцияС1СДокументооборот.УстановитьПроверяющегоНаКарточке(ЭтаФорма, ОбъектXDTO);
	
	// условия маршрутизации
	Если ИнтеграцияС1СДокументооборотПовтИсп.ДоступенФункционалВерсииСервиса("1.2.6.2") Тогда
		Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектныйРеквизит(ЭтаФорма, ОбъектXDTO.performanceType, "ВариантИсполнения");
	КонецЕсли;
	
	Исполнители.Очистить();
	
	Для Каждого Исполнитель Из ОбъектXDTO.performers Цикл
		НоваяСтрока = Исполнители.Добавить();
		Обработки.ИнтеграцияС1СДокументооборот.УстановитьИсполнителяНаКарточке(НоваяСтрока, Исполнитель);
		НоваяСтрока.Срок = Исполнитель.personalDueDate;
		НоваяСтрока.ОписаниеПоручения = Исполнитель.personalDescription;
		НоваяСтрока.НаименованиеПоручения = Исполнитель.personalTaskName;
		НоваяСтрока.Ответственный = Исполнитель.responsible; 
		// условия маршрутизации
		Если ИнтеграцияС1СДокументооборотПовтИсп.ДоступенФункционалВерсииСервиса("1.2.6.2") Тогда
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектныйРеквизит(НоваяСтрока, Исполнитель.performanceOrder, "ПорядокИсполнения")
		КонецЕсли;
	КонецЦикла;
	
	Элементы.Исполнители.ПодчиненныеЭлементы.ИсполнителиВремя.Видимость = ОбъектXDTO.dueTimeEnabled;
	Элементы.ИсполнителиОтветственныйИсполнитель.Доступность = НЕ ЭтаФорма.ТолькоПросмотр;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьШаг(Таблица)
	
	Для Каждого Строка Из Таблица Цикл
		Строка.Шаг = 0;
	КонецЦикла;
	
	КоличествоСтрок = Таблица.Количество();
	Для Инд = 0 По КоличествоСтрок - 1 Цикл
		Строка = Таблица[Инд];
		Если Не ЗначениеЗаполнено(Строка.ПорядокИсполненияID) Тогда 
			Прервать;
		КонецЕсли;
		
		Если Строка.Ответственный Тогда
			Продолжить;
		КонецЕсли;
		
		Если Инд = 0 Тогда 
			Строка.Шаг = 1;
			Продолжить;
		КонецЕсли;
		
		Если НЕ Таблица[Инд-1].Ответственный Тогда
			ПредыдущаяСтрока = Таблица[Инд-1];
		Иначе
			Если Инд > 1 Тогда
				ПредыдущаяСтрока = Таблица[Инд-2];
			Иначе
				ПредыдущаяСтрока = Таблица[Инд-1];
			КонецЕсли;
		КонецЕсли;
		Если Строка.ПорядокИсполненияID = "ВместеСПредыдущим" Тогда 
			Строка.Шаг = ПредыдущаяСтрока.Шаг; 
			Если ПредыдущаяСтрока.Ответственный И Инд = 1 Тогда
				Строка.Шаг = 1;
			конецЕсли;
		ИначеЕсли Строка.ПорядокИсполненияID = "ПослеПредыдущего" Тогда 
			Строка.Шаг = ПредыдущаяСтрока.Шаг + 1;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьВыборВариантаИсполнения()
	
	Если ВариантИсполненияID <> "Смешанно" Тогда
		Элементы.ИсполнителиШаг.Видимость = Ложь;
		Элементы.ИсполнителиПорядокИсполнения.Видимость = Ложь;
	Иначе
		Элементы.ИсполнителиШаг.Видимость = Истина;
		Элементы.ИсполнителиПорядокИсполнения.Видимость = Истина;
	КонецЕсли;
	
	Если ВариантИсполненияID <> "Параллельно" Тогда
		Элементы.ИсполнителиГруппаПеремещение.Видимость = Истина;
	Иначе
		Элементы.ИсполнителиГруппаПеремещение.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСсылкуБизнесПроцесса(ОбъектXDTO)
	
	ID = ОбъектXDTO.objectId.id;
	Если ОбъектXDTO.objectId.Свойства().Получить("presentation") <> Неопределено Тогда
		Представление = ОбъектXDTO.objectId.presentation;
	Иначе
		Представление = ОбъектXDTO.name;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПодготовитьКПередачеИЗаписатьБизнесПроцесс()
	
	Прокси = ИнтеграцияС1СДокументооборотПовтИсп.ПолучитьПрокси();
	ОбъектXDTO = ПодготовитьБизнесПроцесс(Прокси);
	Если ЗначениеЗаполнено(ID) Тогда
		РезультатЗаписи = ИнтеграцияС1СДокументооборот.ЗаписатьОбъект(Прокси, ОбъектXDTO);
	Иначе
		РезультатСоздания = ИнтеграцияС1СДокументооборот.СоздатьНовыйОбъект(Прокси, ОбъектXDTO);
	КонецЕсли;
	
	Результат = ?(РезультатСоздания = Неопределено, РезультатЗаписи, РезультатСоздания);
	ИнтеграцияС1СДокументооборот.ПроверитьВозвратВебСервиса(Прокси, Результат);

	Если РезультатЗаписи <> Неопределено Тогда
		УстановитьСсылкуБизнесПроцесса(Результат.objects[0]);
	Иначе
		УстановитьСсылкуБизнесПроцесса(Результат.object);
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

&НаСервере
Функция ПодготовитьКПередачеИСтартоватьБизнесПроцесс()
	
	Прокси = ИнтеграцияС1СДокументооборотПовтИсп.ПолучитьПрокси();
	ОбъектXDTO = ПодготовитьБизнесПроцесс(Прокси);
	
	РезультатЗапуска = ИнтеграцияС1СДокументооборот.ЗапуститьБизнесПроцесс(Прокси, ОбъектXDTO);
	ИнтеграцияС1СДокументооборот.ПроверитьВозвратВебСервиса(Прокси, РезультатЗапуска);
	
	УстановитьСсылкуБизнесПроцесса(РезультатЗапуска.businessProcess);
	
	Возврат Истина;
	
КонецФункции

&НаСервере
Функция ПодготовитьБизнесПроцесс(Прокси)
		
	ОбъектXDTO = Обработки.ИнтеграцияС1СДокументооборот.ПодготовитьШапкуБизнесПроцесса(
		Прокси, "DMBusinessProcessPerformance", ЭтаФорма);
	
	//контролер
	Обработки.ИнтеграцияС1СДокументооборот.СоздатьУчастникаБизнесПроцесса(Прокси, 
		ОбъектXDTO.controller, ЭтаФорма, "Контролер", "ОбъектАдресацииКонтролера");
	
	//проверяющий
	Обработки.ИнтеграцияС1СДокументооборот.СоздатьУчастникаБизнесПроцесса(Прокси, 
		ОбъектXDTO.verifier, ЭтаФорма, "Проверяющий", "ОбъектАдресацииПроверяющего");
	
	//исполнители
	Для Каждого Строка Из Исполнители Цикл 
		
		Исполнитель = ИнтеграцияС1СДокументооборот.СоздатьОбъект(Прокси, "DMBusinessProcessPerformanceParticipant");
				
		Если Строка.ИсполнительТип = "DMUser" Тогда
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, Строка, 
				"Исполнитель", Исполнитель.user, "DMUser");
		Иначе
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, Строка, 
				"Исполнитель", Исполнитель.role, "DMBusinessProcessExecutorRole");
			
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, Строка, 
				"ОсновнойОбъектАдресации", Исполнитель.mainAddressingObject, "DMMainAddressingObject");
			
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, Строка, 
				"ДополнительныйОбъектАдресации", Исполнитель.secondaryAddressingObject, "DMSecondaryAddressingObject"); 
		КонецЕсли;
		
		Исполнитель.personalDueDate = Строка.Срок;
		Исполнитель.personalDescription = Строка.ОписаниеПоручения;
		Исполнитель.personalTaskName = Строка.НаименованиеПоручения;
		Исполнитель.responsible = Строка.Ответственный;
		
		// условия маршрутизации
		Если ИнтеграцияС1СДокументооборотПовтИсп.ДоступенФункционалВерсииСервиса("1.2.6.2") Тогда
			Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, Строка,
				"ПорядокИсполнения", Исполнитель.performanceOrder, "DMTaskExecutionOrder");
		КонецЕсли;
		
		ОбъектXDTO.performers.Добавить(Исполнитель);
		
	КонецЦикла;
	
	// условия маршрутизации
	Если ИнтеграцияС1СДокументооборотПовтИсп.ДоступенФункционалВерсииСервиса("1.2.6.2") Тогда
		Обработки.ИнтеграцияС1СДокументооборот.ЗаполнитьОбъектXDTOИзОбъектногоРеквизита(Прокси, ЭтаФорма, 
			"ВариантИсполнения", ОбъектXDTO.performanceType, "DMBusinessProcessRoutingType");
	КонецЕсли;
	
	Возврат ОбъектXDTO;
	
КонецФункции

&НаКлиенте
Функция ЗаписатьОбъектВыполнить()
	
	ПодготовитьКПередачеИЗаписатьБизнесПроцесс();
	ИнтеграцияС1СДокументооборотКлиент.Оповестить_ЗаписьБизнесПроцесса(ЭтаФорма, Ложь);
	Модифицированность = Ложь;
	Возврат Истина;
	
КонецФункции

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Ответ, ПараметрыОповещения) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Нет Тогда
		Модифицированность = Ложь;
		Закрыть();
	ИначеЕсли Ответ = КодВозвратаДиалога.Да И ЗаписатьОбъектВыполнить() Тогда
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры
 
#КонецОбласти
