﻿
// Функция формирует табличный документ с печатной формой накладной,
// разработанной методистами
//
// Возвращаемое значение:
//  Табличный документ - печатная форма накладной
//
Функция ПечатьТОРГ12(ВыбДокумент, ПараметрыПечати = Неопределено)Экспорт
	
	ТабДокумент = Новый ТабличныйДокумент;
	//[[ 31.01.2018 
	НеПроверятьfs_ПечататьDeliveryNote = Ложь;
	Если ПараметрыПечати <> Неопределено Тогда
		ПараметрыПечати.Свойство("НеПроверятьfs_ПечататьDeliveryNote",НеПроверятьfs_ПечататьDeliveryNote);
	КонецЕсли;
	
	Если не ТипЗнч(НеПроверятьfs_ПечататьDeliveryNote) = Тип("Булево") Тогда 
		НеПроверятьfs_ПечататьDeliveryNote = Ложь;
	КонецЕсли;
	//]] 31.01.2018	
	
	
	Для каждого Док Из ВыбДокумент Цикл
		Если ТипЗнч(Док) = Тип("ДокументСсылка.ЗаказНаВнутреннееПотребление") Тогда
			ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЗаказаНаПотребление_DeliveryNote";
			Макет = ПолучитьМакет("Макет");
			Запрос = Новый запрос;
			Запрос.текст = 
			"ВЫБРАТЬ
			|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура,
			|	ЗаказНаВнутреннееПотреблениеТовары.Ссылка.Партнер.fs_КодHoya КАК IlogCustNumb,
			|	ЗаказНаВнутреннееПотреблениеТовары.Количество КАК Количество,
			|	ЗаказНаВнутреннееПотреблениеТовары.Ссылка.Номер КАК IlogordNumb,
			|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура КАК Номенклатура1,
			|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура.fs_Сфера КАК Номfs_Сфера,
			|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура.fs_ДиаметрВнешний КАК Номfs_Диаметр,
			|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура.fs_Цилиндр КАК Номfs_Цилиндр,
			|	0 КАК Цена,
			|	0 КАК Сумма
			|ИЗ
			|	Документ.ЗаказНаВнутреннееПотребление.Товары КАК ЗаказНаВнутреннееПотреблениеТовары
			|ГДЕ
			|	ЗаказНаВнутреннееПотреблениеТовары.Ссылка.Проведен
			|	И ЗаказНаВнутреннееПотреблениеТовары.Ссылка В(&Список)";
			Запрос.УстановитьПараметр("Список",Док);
			Товары = Запрос.Выполнить().Выгрузить();
			
			НеПечататьЦены = Док.Партнер.fs_DeliveryNoteБезЦен;
			
			ОбластьМакета = Макет.ПолучитьОбласть("Шапка");
			ОбластьМакета.Параметры.ДатаРеализации = Формат(Док.Дата,"ДФ=dd.MM.yyyy");
			ОбластьМакета.Параметры.РеализацияНомер = Прав(Док.Номер,6); 
			Если ЗначениеЗаполнено(Док.fs_ЗаказКлиента) Тогда 
				ОбластьМакета.Параметры.ДопСтрока = "РЕКЛАМА КЛИЕНТУ"; 
			Иначе
				ОбластьМакета.Параметры.ДопСтрока = "РЕКЛАМА - ВНУТРЕННЕЕ ПОТРЕБЛЕНИЕ"; 
			КонецЕсли;
			Если ЗначениеЗаполнено(Док.fs_РегиональныйМенеджер) и не ЗначениеЗаполнено(Док.Контрагент) и не ЗначениеЗаполнено(Док.Партнер) Тогда 
				ОбластьМакета.Параметры.Контрагент = Док.fs_РегиональныйМенеджер;
			Иначе
				ОбластьМакета.Параметры.Контрагент = Док.Контрагент;
				ОбластьМакета.Параметры.Клиент = Док.Партнер;
				ОбластьМакета.Параметры.Грузополучатель = ?(ЗначениеЗаполнено(Док.Партнер.fs_грузополучатель),Док.Партнер.fs_грузополучатель,Док.Партнер);
			КонецЕсли;
		
			ТабДокумент.Вывести(ОбластьМакета);
			
			ОбластьМакета = Макет.ПолучитьОбласть("ШапкаТаблицы");
			ТабДокумент.Вывести(ОбластьМакета);
			
			ОбластьМакета = Макет.ПолучитьОбласть("Строка");
			Ном = 0;
			//ЕдИзмерения = Справочники.ЕдиницыИзмерения.НайтиПоНаименованию("Шт");
			ТипЗаказаHLRU = Справочники.fs_ТипыСкладов.HLRU;
			КолвоИтого=0;
			СуммаИтого=0;
			Для каждого Строка из товары цикл
				Ном = Ном + 1;
				Номенклатура = Строка.Номенклатура;
				ОбластьМакета.Параметры.Номер = Ном;
				ЗаполнитьЗначенияСвойств(ОбластьМакета.Параметры,Строка);
				ОбластьМакета.Параметры.IlogordNumb = ?(Найти(Строка.IlogordNumb,"УТ") > 0,Прав(Строка.IlogordNumb,6),Строка.IlogordNumb) ;
				КолвоИтого = КолвоИтого+Строка.Количество;
				СуммаИтого = СуммаИтого + Строка.Сумма;
				
				Если НеПечататьЦены тогда
					ОбластьМакета.Параметры.Цена = "";
					ОбластьМакета.Параметры.Сумма = "";
					СуммаИтого=0;
				КонецЕсли;
				ТабДокумент.Вывести(ОбластьМакета);
			КонецЦикла;
			ОбластьМакета = Макет.ПолучитьОбласть("Итоги");
			ОбластьМакета.Параметры.КолвоИтого = КолвоИтого;
			ОбластьМакета.Параметры.СуммаИтого = СуммаИтого;
			ТабДокумент.Вывести(ОбластьМакета);
			
			Если ЗначениеЗаполнено(док.Комментарий) Тогда 
				ОбластьМакета = Макет.ПолучитьОбласть("Комментарий");
				ОбластьМакета.Параметры.Комментарий = док.Комментарий;
				ТабДокумент.Вывести(ОбластьМакета);
			КонецЕсли;
			
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
			
		ИначеЕсли Док.Партнер.fs_ПечататьDeliveryNote ИЛИ Док.Партнер.fs_ОтправлятьФайлыDeliveryNoteНаЭлПочту или НеПроверятьfs_ПечататьDeliveryNote Тогда
			ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_РеализацияТоваровУслуг_DeliveryNote";
			Макет = ПолучитьМакет("Макет");
			//[[ 31.01.2018 
			Если ТипЗнч(Док) = Тип("ДокументСсылка.ЗаказКлиента") Тогда 
				СписокЗаказов = новый Массив;
				СписокЗаказов.Добавить(Док);
			Иначе
				СписокЗаказов = Док.Товары.ВыгрузитьКолонку("ЗаказКлиента");
			КонецЕсли;
			//]] 31.01.2018
			Запрос = Новый запрос;
			Запрос.текст = 
			"ВЫБРАТЬ
			|	ЗаказКлиентаТовары.Номенклатура,
			|	ЗаказКлиентаТовары.fs_Диаметр КАК Diam,
			|	ЗаказКлиентаТовары.fs_Сфера КАК Sph,
			|	ЗаказКлиентаТовары.fs_Цилиндр КАК Cyl,
			|	ЗаказКлиентаТовары.fs_Axis КАК Ax,
			|	ЗаказКлиентаТовары.fs_Add КАК Add,
			|	ЗаказКлиентаТовары.fs_Сторона КАК Сторона,
			|	ЗаказКлиентаТовары.Ссылка.Партнер.fs_КодHoya КАК IlogCustNumb,
			|	ЗаказКлиентаТовары.Количество КАК Количество,
			|	(ЗаказКлиентаТовары.СуммаСНДС - ЗаказКлиентаТовары.СуммаНДС) / ЗаказКлиентаТовары.Количество КАК Цена,
			|	ЗаказКлиентаТовары.СуммаСНДС КАК Сумма,
			|	ЗаказКлиентаТовары.fs_Reference КАК Reference,
			|	ЗаказКлиентаТовары.fs_Patient КАК Patient,
			|	ЗаказКлиентаТовары.Ссылка.Номер КАК IlogordNumb,
			|	ЗаказКлиентаТовары.fs_НомерЗаказаКлиента КАК OrdRef,
			|	ЗаказКлиентаТовары.Номенклатура КАК Номенклатура1,
			|	ЗаказКлиентаТовары.Ссылка.fs_ТипЗаказаHOYA КАК ТипЗаказаHOYA,
			|	ЗаказКлиентаТовары.Номенклатура.fs_Сфера КАК Номfs_Сфера,
			|	ЗаказКлиентаТовары.Номенклатура.fs_ДиаметрВнешний КАК Номfs_Диаметр,
			|	ЗаказКлиентаТовары.Номенклатура.fs_Цилиндр КАК Номfs_Цилиндр
			|ИЗ
			|	Документ.ЗаказКлиента.Товары КАК ЗаказКлиентаТовары
			|ГДЕ
			|	ЗаказКлиентаТовары.Ссылка.Проведен
			|	И ЗаказКлиентаТовары.Ссылка В(&Список)";
			Запрос.УстановитьПараметр("Список",СписокЗаказов);
			Товары = Запрос.Выполнить().Выгрузить();
			
			Запрос = Новый запрос;
			Запрос.Текст = "ВЫБРАТЬ
			|	СчетФактураВыданный.Номер
			|ИЗ
			|	Документ.СчетФактураВыданный КАК СчетФактураВыданный
			|ГДЕ
			|	СчетФактураВыданный.ДокументОснование = &ДокументОснование
			|	И СчетФактураВыданный.Проведен";
			Запрос.УстановитьПараметр("ДокументОснование",Док);
			ТЗ= Запрос.Выполнить().Выгрузить();
			
			
			НеПечататьЦены = Док.Партнер.fs_DeliveryNoteБезЦен;
			
			ОбластьМакета = Макет.ПолучитьОбласть("Шапка");
			ОбластьМакета.Параметры.Контрагент = Док.Контрагент;
			ОбластьМакета.Параметры.Клиент = Док.Партнер;
			ОбластьМакета.Параметры.Грузополучатель = ?(ЗначениеЗаполнено(Док.Партнер.fs_грузополучатель),Док.Партнер.fs_грузополучатель,Док.Партнер);
			ОбластьМакета.Параметры.ДатаРеализации = Формат(Док.Дата,"ДФ=dd.MM.yyyy");
			ОбластьМакета.Параметры.РеализацияНомер = Прав(Док.Номер,6);
			//[[ 31.01.2018 
			Если ТипЗнч(Док) = Тип("ДокументСсылка.ЗаказКлиента") Тогда 
				ОбластьМакета.Параметры.Ячейка = Док.fs_НомерБоксаКлиента;
			Иначе //Бит_SladkovAV 20190222 
				Если ТипЗнч(Док) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда 
					Если ЗначениеЗаполнено(Док.Товары[0].ЗаказКлиента) Тогда 
						ОбластьМакета.Параметры.Ячейка = Док.Товары[0].ЗаказКлиента.fs_НомерБоксаКлиента;
					КонецЕсли;
				КонецЕсли;
				//***
			КонецЕсли;
			//]] 31.01.2018
			
			
			Если ТЗ.Количество() > 0 тогда
				ОбластьМакета.Параметры.СчетФактураНомер = Прав(ТЗ[0].Номер,6);
			КонецЕсли;
			
			ТабДокумент.Вывести(ОбластьМакета);
			
			
			ОбластьМакета = Макет.ПолучитьОбласть("ШапкаТаблицы");
			ТабДокумент.Вывести(ОбластьМакета);
			
			ОбластьМакета = Макет.ПолучитьОбласть("Строка");
			Ном = 0;
			//ЕдИзмерения = Справочники.ЕдиницыИзмерения.НайтиПоНаименованию("Шт");
			ТипЗаказаHLRU = Справочники.fs_ТипыСкладов.HLRU;
			КолвоИтого=0;
			СуммаИтого=0;
			Для каждого Строка из товары цикл
				Ном = Ном + 1;
				Номенклатура = Строка.Номенклатура;
				ОбластьМакета.Параметры.Номер = Ном;
				ЗаполнитьЗначенияСвойств(ОбластьМакета.Параметры,Строка);
				ОбластьМакета.Параметры.IlogordNumb = ?(Найти(Строка.IlogordNumb,"УТ") > 0,Прав(Строка.IlogordNumb,6),Строка.IlogordNumb) ;
				ОбластьМакета.Параметры.Sph = Формат(Строка.Sph,"ЧДЦ=2");
				ОбластьМакета.Параметры.Cyl = Формат(Строка.Cyl,"ЧДЦ=2");
				ОбластьМакета.Параметры.Ax = Формат(Строка.Ax,"ЧДЦ=2");
				ОбластьМакета.Параметры.Add = Формат(Строка.Add,"ЧДЦ=2");
				КолвоИтого = КолвоИтого+Строка.Количество;
				СуммаИтого = СуммаИтого + Строка.Сумма;
				Если Строка.ТипЗаказаHOYA = ТипЗаказаHLRU тогда
					ОбластьМакета.Параметры.Sph = Формат(Строка.Номfs_Сфера,"ЧДЦ=2");
					ОбластьМакета.Параметры.Cyl = Формат(Строка.Номfs_Цилиндр,"ЧДЦ=2");
					ОбластьМакета.Параметры.Diam = Строка.Номfs_Диаметр;
				КонецЕсли;
				
				Если НеПечататьЦены тогда
					ОбластьМакета.Параметры.Цена = "";
					ОбластьМакета.Параметры.Сумма = "";
					СуммаИтого=0;
				КонецЕсли;
				ТабДокумент.Вывести(ОбластьМакета);
			КонецЦикла;
			ОбластьМакета = Макет.ПолучитьОбласть("Итоги");
			ОбластьМакета.Параметры.КолвоИтого = КолвоИтого;
			ОбластьМакета.Параметры.СуммаИтого = СуммаИтого;
			ТабДокумент.Вывести(ОбластьМакета);
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
			
		КонецЕсли;	
	КонецЦикла; 	
	//ОбластьМакета = Макет.ПолучитьОбласть("Подвал");
	//ОбластьМакета.Параметры.Дата  = Формат(ТекущаяДата(),"ДФ=dd.MM.yyyy");
	
	//ТабДокумент.Вывести(ОбластьМакета);
	
	// Зададим параметры макета
	ТабДокумент.ПолеСверху = 0;
	ТабДокумент.ПолеСлева  = 0;
	ТабДокумент.ПолеСнизу  = 0;
	ТабДокумент.ПолеСправа = 0;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	//ТабДокумент.Показать();
	Возврат ТабДокумент;
	
КонецФункции // ПечатьТОРГ12()

Процедура Печать(МассивОбъектов,ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	//Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "Delivery note") Тогда
	Для каждого Строка из КоллекцияПечатныхФорм цикл
		Строка.Экземпляров = 1
	КонецЦикла;
	
	УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
	КоллекцияПечатныхФорм,
	"Deliverynote",
	НСтр("ru = 'Deliverynote'"),
	ПечатьТорг(МассивОбъектов,,ПараметрыПечати),
	,
	"ВнешняяОбработка.fs_DeliveryNote");
	
	//ТабличныйДокумент = ПечатьТорг12(МассивОбъектов);
	//ТабличныйДокумент.Напечатать(РежимИспользованияДиалогаПечати.Использовать);
	//КонецЕсли;
	
КонецПроцедуры

// Внешняя функция печати
// КТ-2000: (095)789-3070
// Параметры
//  ИмяМакета  – Строка – имя формы печати
//
// Возвращаемое значение:
//   Булево   –Печать прошла успешно - Истина, иначе - Ложь
//
Функция ПечатьТорг(МассивОбъектов, ИмяМакета = "", ПараметрыПечати = Неопределено)	Экспорт
	
	
	
	НаПринтер = истина;
	КоличествоЭкземпляров = 1;
	//Для Каждого ОбъектНаПечать из МассивОбъектов Цикл
	ТабДокумент = ПечатьТорг12(МассивОбъектов, ПараметрыПечати);
	
	Если ПараметрыПечати <> Неопределено Тогда
		КоличествоЭкземпляров = 1;
		Если НЕ(ПараметрыПечати.Свойство("КоличествоЭкземпляров",КоличествоЭкземпляров)) Тогда
			КоличествоЭкземпляров = 1;			
		КонецЕсли;
		НаПринтер = Ложь;
		Если НЕ(ПараметрыПечати.Свойство("НаПринтер",НаПринтер)) Тогда
			НаПринтер = Ложь;			
		КонецЕсли;
	КонецЕсли;
	Если КоличествоЭкземпляров = Неопределено Тогда
		КоличествоЭкземпляров = 1;
	КонецЕсли;
	Если НаПринтер = Неопределено Тогда
		НаПринтер = Ложь;
	КонецЕсли;
	
	
	ТабДокумент.КоличествоЭкземпляров = 1;
	//ТабДокумент.НаПринтер = Истина;
	Возврат ТабДокумент;
	
	
КонецФункции // Печать()
