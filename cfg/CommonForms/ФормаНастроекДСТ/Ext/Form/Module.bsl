﻿

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Настройки = Константы.НастройкаПараметровДСТ.Получить().Получить();
	Если ЗначениеЗаполнено(Настройки) Тогда 
		ЗаполнитьЗначенияСвойств(ЭтотОбъект,Настройки);
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
Процедура КнопкаОтмена(Команда)
	
	Модифицированность = Ложь;
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура КнопкаОкНаСервере()
	
	Настройка = Новый Структура;
	
	Настройка.Вставить("client_id", client_id);
	Настройка.Вставить("Token"      , Token);
	Настройка.Вставить("КаталогЗапросов"            , КаталогЗапросов);
	Настройка.Вставить("СерверHTTPS"            , СерверHTTPS);
	
	Константы.НастройкаПараметровДСТ.Установить(Новый ХранилищеЗначения(Настройка));
	
	
КонецПроцедуры

&НаКлиенте
Процедура КнопкаОк(Команда)
	КнопкаОкНаСервере();
	Закрыть();
КонецПроцедуры
