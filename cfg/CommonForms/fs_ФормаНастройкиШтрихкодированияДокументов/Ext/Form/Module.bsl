﻿
&НаСервере
Процедура ЗаписатьНаСервере()

	Константы.fs_ОтключениеПечатиШтрихкодовНаДокументах.Установить(fs_ОтключениеПечатиШтрихкодовНаДокументах);
	Константы.fs_ВысотаКолонтитулаСоШтрихкодом.Установить(fs_ВысотаКолонтитулаСоШтрихкодом);
	Константы.fs_РамерШтрихкодаВКолонтитуле.Установить(fs_РамерШтрихкодаВКолонтитуле);
	Константы.fs_РазмерШрифтаШтрихкода.Установить(fs_РазмерШрифтаШтрихкода);
	Константы.fs_УдалятьДугиеШтрихкодыИзПечатнойФормы.Установить(fs_УдалятьДугиеШтрихкодыИзПечатнойФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	
	ЗаписатьНаСервере();
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	fs_ОтключениеПечатиШтрихкодовНаДокументах = Константы.fs_ОтключениеПечатиШтрихкодовНаДокументах.Получить();
	fs_ВысотаКолонтитулаСоШтрихкодом   = Константы.fs_ВысотаКолонтитулаСоШтрихкодом.Получить();
	fs_РамерШтрихкодаВКолонтитуле   = Константы.fs_РамерШтрихкодаВКолонтитуле.Получить();
	fs_РазмерШрифтаШтрихкода   = Константы.fs_РазмерШрифтаШтрихкода.Получить();
	fs_УдалятьДугиеШтрихкодыИзПечатнойФормы   = Константы.fs_УдалятьДугиеШтрихкодыИзПечатнойФормы.Получить();

КонецПроцедуры
