﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	ИспользоватьНесколькоКасс = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоКасс");
	УправлениеЭлементамиФормы();
	
	ИспользоватьНачислениеЗарплаты = Константы.ИспользоватьНачислениеЗарплаты.Получить();
	
	ИспользоватьЗаявкиНаРасходованиеДенежныхСредств = ПолучитьФункциональнуюОпцию("ИспользоватьЗаявкиНаРасходованиеДенежныхСредств");
	Элементы.СтраницаРаспоряженияНаОплату.Видимость = ИспользоватьЗаявкиНаРасходованиеДенежныхСредств;
	Элементы.СтраницаЗаказыКОплате.Видимость = НЕ ИспользоватьЗаявкиНаРасходованиеДенежныхСредств;
	Элементы.СтраницаВыплатаЗарплатыПоЗаявкам.Видимость = ИспользоватьНачислениеЗарплаты И ИспользоватьЗаявкиНаРасходованиеДенежныхСредств;
	Элементы.СтраницаВыплатаЗарплаты.Видимость = ИспользоватьНачислениеЗарплаты И НЕ ИспользоватьЗаявкиНаРасходованиеДенежныхСредств;
	Элементы.РасходныеКассовыеОрдераКассаОтбор.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоКасс");
	
	ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыплатаЗарплатыЧерезКассу;
	
	ЗаказыКОплате.ТекстЗапроса = ДенежныеСредстваСервер.ТекстЗапросаЗаказыКОплате();
	
	
	УстановитьПараметрыДинамическихСписков();
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаГлобальныеКоманды);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// МеханизмВнешнегоОборудования
	Если ИспользоватьПодключаемоеОборудование // Проверка на включенную ФО "Использовать ВО"
	   И МенеджерОборудованияКлиент.ОбновитьРабочееМестоКлиента() Тогда // Проверка на определенность рабочего места ВО
		ОписаниеОшибки = "";

		ПоддерживаемыеТипыВО = Новый Массив();
		ПоддерживаемыеТипыВО.Добавить("СканерШтрихкода");

		Если Не МенеджерОборудованияКлиент.ПодключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыВО, ОписаниеОшибки) Тогда
			ТекстСообщения = НСтр("ru = 'При подключении оборудования произошла ошибка:
			                      |""%ОписаниеОшибки%"".'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОписаниеОшибки%", ОписаниеОшибки);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		КонецЕсли;
	КонецЕсли;
	// Конец МеханизмВнешнегоОборудования

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование"
		И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			//Преобразуем предварительно к ожидаемому формату
			Если Параметр[1] = Неопределено Тогда
				ОбработатьШтрихкоды(Новый Структура("Штрихкод, Количество", Параметр[0], 1)); // Достаем штрихкод из основных данных
			Иначе
				ОбработатьШтрихкоды(Новый Структура("Штрихкод, Количество", Параметр[1][1], 1)); // Достаем штрихкод из дополнительных данных
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	Если ИмяСобытия = "Запись_РасходныйКассовыйОрдер" Тогда
		ОбновитьДиначескиеСписки();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	// МеханизмВнешнегоОборудования
	ПоддерживаемыеТипыВО = Новый Массив();
	ПоддерживаемыеТипыВО.Добавить("СканерШтрихкода");

	МенеджерОборудованияКлиент.ОтключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыВО);
	// Конец МеханизмВнешнегоОборудования
	
	СохранитьРабочиеЗначенияПолейФормы(Истина);
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Организация = Настройки.Получить("Организация");
	Касса = Настройки.Получить("Касса");
	Работник = Настройки.Получить("Работник");
	ХозяйственнаяОперация = Настройки.Получить("ХозяйственнаяОперация");
	УстановитьОтборДинамическихСписков();
	
	Вариант = Настройки.Получить("ДатаПлатежа.Вариант");
	Если ЗначениеЗаполнено(Вариант) Тогда
		ДатаПлатежа.Вариант = Вариант;
	КонецЕсли;
	УстановитьПараметрыДинамическихСписков();
	
	ВыплатаРаботнику = ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыплатаЗарплатыРаботнику;
	
	Элементы.РаботникОтбор.Доступность = ВыплатаРаботнику;
	Элементы.ВыплатаЗарплатыРаботникОтбор.Доступность = ВыплатаРаботнику;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ГруппаСтраницыПриСменеСтраницы(Элемент, ТекущаяСтраница)
	
	ОбновитьДиначескиеСписки();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаявкиКОплатеВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПоказатьЗначение(Неопределено, Элементы.ЗаявкиКОплате.ТекущиеДанные.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаказыКОплатеВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПоказатьЗначение(Неопределено, Элементы.ЗаказыКОплате.ТекущиеДанные.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыПоЗаявкамВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПоказатьЗначение(Неопределено, Элементы.ВыплатаЗарплатыПоЗаявкам.ТекущиеДанные.Ведомость);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПоказатьЗначение(Неопределено, Элементы.ВыплатаЗарплаты.ТекущиеДанные.Ведомость);
	
КонецПроцедуры

&НаКлиенте
Процедура РасходныеКассовыеОрдераКассаОтборПриИзменении(Элемент)
	КассаОтборПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаОплатуКассаОтборПриИзменении(Элемент)
	КассаОтборПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаОплатуДатаПлатежаОтборПриИзменении(Элемент)
	УстановитьПараметрыДинамическихСписков();
КонецПроцедуры

&НаКлиенте
Процедура ЗаказыКОплатеКассаОтборПриИзменении(Элемент)
	КассаОтборПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ЗаказыКОплатеДатаПлатежаОтборПриИзменении(Элемент)
	УстановитьПараметрыДинамическихСписков();
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыПоЗаявкамКассаОтборПриИзменении(Элемент)
	КассаОтборПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыКассаПриИзменении(Элемент)
	КассаОтборПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаПоВедомостямПриИзменении(Элемент)
	
	Работник = ПредопределенноеЗначение("Справочник.ФизическиеЛица.ПустаяСсылка");
	
	УстановитьПараметрыДинамическихСписков();
	
	Элементы.РаботникОтбор.Доступность = ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаЗарплатыРаботнику");
	
КонецПроцедуры

&НаКлиенте
Процедура РаботникОтборПриИзменении(Элемент)
	УстановитьПараметрыДинамическихСписков();
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыВыплатаПоВедомостямПриИзменении(Элемент)
	
	Работник = ПредопределенноеЗначение("Справочник.ФизическиеЛица.ПустаяСсылка");
	
	УстановитьПараметрыДинамическихСписков();
	
	Элементы.ВыплатаЗарплатыРаботникОтбор.Доступность = ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаЗарплатыРаботнику");
	
КонецПроцедуры

&НаКлиенте
Процедура ВыплатаЗарплатыРаботникОтборПриИзменении(Элемент)
	УстановитьПараметрыДинамическихСписков();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьВыдачаДенежныхСредствПодотчетнику(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыдачаДенежныхСредствПодотчетнику"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВозвратОплатыКлиенту(Команда)

	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьПрочиеРасходы(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПрочиеРасходы"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьПрочаяВыдачаДенежныхСредств(Команда)

	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПрочаяВыдачаДенежныхСредств"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьСдачаДенежныхСредствВБанк(Команда)

	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.СдачаДенежныхСредствВБанк"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьИнкассацию(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ИнкассацияДенежныхСредствВБанк"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВыдачаДенежныхСредствВДругуюКассу(Команда)

	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыдачаДенежныхСредствВДругуюКассу"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВыдачаДенежныхСредствВКассуККМ(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыдачаДенежныхСредствВКассуККМ"));

КонецПроцедуры

&НаКлиенте
Процедура СоздатьОплатаДенежныхСредствВДругуюОрганизацию(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ОплатаДенежныхСредствВДругуюОрганизацию"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВозвратДенежныхСредствВДругуюОрганизацию(Команда)
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВозвратДенежныхСредствВДругуюОрганизацию"));
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВнутреннююПередачуДенежныхСредств(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВнутренняяПередачаДенежныхСредств"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьКонвертацияВалюты(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.КонвертацияВалюты"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВыплатаЗаработнойПлатыПоВедомостям(Команда)
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаЗарплатыЧерезКассу"));
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВыплатуЗарплатыРаздатчиком(Команда)
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаЗарплатыРаздатчиком"));
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВыплатаЗаработнойПлатыРаботнику(Команда)
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаЗарплатыРаботнику"));
КонецПроцедуры

&НаКлиенте
Процедура ОплатитьДокумент(Команда)
	
	СтрокаТаблицы = Элементы.ЗаказыКОплате.ТекущиеДанные;
	Если СтрокаТаблицы <> Неопределено Тогда
	
		СтруктураОснование = Новый Структура("Организация, ЗаказПоставщику, СуммаКОплате",
			СтрокаТаблицы.Организация,
			СтрокаТаблицы.Ссылка,
			СтрокаТаблицы.СуммаКОплате);
		СтруктураПараметры = Новый Структура("Основание", СтруктураОснование);
		ОткрытьФорму("Документ.РасходныйКассовыйОрдер.ФормаОбъекта", СтруктураПараметры, Элементы.РасходныеКассовыеОрдера);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыплатитьЗарплатуПоЗаявкам(Команда)
	
	СоздатьОрдерНаВыплатуЗарплаты(Элементы.ВыплатаЗарплатыПоЗаявкам.ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыплатитьЗарплату(Команда)
	
	СоздатьОрдерНаВыплатуЗарплаты(Элементы.ВыплатаЗарплаты.ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьОплатуПоКредитам(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ОплатаПоКредитам"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьОплатуПоЗаймам(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВыплатаПоЗаймамВыданным"));
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьОплатуЛизингодателю(Команда)
	
	СоздатьРасходныйКассовыйОрдер(ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ОплатаЛизингодателю"));
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.РасходныеКассовыеОрдера);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Элементы.РасходныеКассовыеОрдера);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаСервере
Процедура КассаОтборПриИзмененииНаСервере()
	
	Организация = Справочники.Кассы.ПолучитьРеквизитыКассы(Касса).Организация;
	УстановитьОтборДинамическихСписков();
	
КонецПроцедуры

#КонецОбласти

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Функция СсылкаНаЭлементСпискаПоШтрихкоду(Штрихкод)
	
	Менеджеры = Новый Массив();
	Менеджеры.Добавить(ПредопределенноеЗначение("Документ.РасходныйКассовыйОрдер.ПустаяСсылка"));
	Возврат ШтрихкодированиеПечатныхФормКлиент.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(Штрихкод, Менеджеры);
	
КонецФункции

&НаКлиенте
Процедура ОбработатьШтрихкоды(Данные)
	
	МассивСсылок = СсылкаНаЭлементСпискаПоШтрихкоду(Данные.Штрихкод);
	Если МассивСсылок.Количество() > 0 Тогда
		
		Ссылка = МассивСсылок[0];
		Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.РасходныйКассовыйОрдер") Тогда
			Элементы.РасходныеКассовыеОрдера.ТекущаяСтрока = Ссылка;
			Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаСтраницы.ПодчиненныеЭлементы.СтраницаРасходныеКассовыеОрдера;
		КонецЕсли;
		
		ПоказатьЗначение(, Ссылка);
		
	Иначе
		ШтрихкодированиеПечатныхФормКлиент.ОбъектНеНайден(Данные.Штрихкод);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УстановитьВидимостьКассы()
	
	КассаВидимость = Не ЗначениеЗаполнено(Касса);
	Элементы.РасходныеКассовыеОрдераКасса.Видимость = КассаВидимость;
	
	МассивЭлементов = Новый Массив;
	МассивЭлементов.Добавить("ЗаявкиКОплатеКасса");
	
	ДенежныеСредстваСервер.УстановитьВидимостьЭлементовПоМассиву(
		Элементы,
		МассивЭлементов,
		?(КассаВидимость, МассивЭлементов, Новый Массив)); // МассивВидимыхРеквизитов
	
КонецПроцедуры

#КонецОбласти

#Область СозданиеДокументов

&НаСервере
Процедура УправлениеЭлементамиФормы()
	
	Элементы.СоздатьВыдачаДенежныхСредствВДругуюКассу.Видимость = ИспользоватьНесколькоКасс;
	Элементы.СоздатьОплатуЛизингодателю.Видимость = ИспользоватьЛизинг;
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьРасходныйКассовыйОрдер(ХозяйственнаяОперация)

	СтруктураПараметры = Новый Структура;
	СтруктураПараметры.Вставить("Основание", Новый Структура("ХозяйственнаяОперация", ХозяйственнаяОперация));
	ОткрытьФорму("Документ.РасходныйКассовыйОрдер.ФормаОбъекта", СтруктураПараметры, Элементы.РасходныеКассовыеОрдера);

КонецПроцедуры

&НаКлиенте
Процедура СоздатьОрдерНаВыплатуЗарплаты(СтрокаТаблицы)
	
	Если СтрокаТаблицы <> Неопределено Тогда
		
		Если СтрокаТаблицы.Свойство("Касса") Тогда
			Если ЗначениеЗаполнено(СтрокаТаблицы.Касса) Тогда
				ОснованиеКасса = СтрокаТаблицы.Касса;
			Иначе
				ОснованиеКасса = Касса;
			КонецЕсли;
		Иначе
			ОснованиеКасса = Неопределено;
		КонецЕсли;
		
		Если СтрокаТаблицы.Свойство("Организация") Тогда
			Если ЗначениеЗаполнено(СтрокаТаблицы.Организация) Тогда
				ОснованиеОрганизация = СтрокаТаблицы.Организация;
			Иначе
				ОснованиеОрганизация = Неопределено;
			КонецЕсли;
		Иначе
			ОснованиеОрганизация = Неопределено;
		КонецЕсли;
		
		СтруктураОснование = Новый Структура("Касса, Организация, Работник, Ведомость, Заявка",
			ОснованиеКасса,
			ОснованиеОрганизация,
			Работник,
			СтрокаТаблицы.Ведомость,
			СтрокаТаблицы.Заявка);
			
		СтруктураОснование.Вставить("ХозяйственнаяОперация", ХозяйственнаяОперация);
		
		
		СтруктураПараметры = Новый Структура("Основание", СтруктураОснование);
		ОткрытьФорму("Документ.РасходныйКассовыйОрдер.ФормаОбъекта", СтруктураПараметры, Элементы.РасходныеКассовыеОрдера);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура СохранитьРабочиеЗначенияПолейФормы(СохранитьНеопределено = Ложь)
	
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить("ТекущаяОрганизация", , ?(СохранитьНеопределено, Неопределено, Организация));
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить("ТекущаяКасса", , ?(СохранитьНеопределено, Неопределено, Касса));
	
КонецПроцедуры

&НаСервере
Процедура УстановитьОтборДинамическихСписков()
	
	СписокОрганизаций = Новый СписокЗначений;
	СписокОрганизаций.Добавить(Организация);
	
	Если ЗначениеЗаполнено(Организация)
		И ПолучитьФункциональнуюОпцию("ИспользоватьОбособленныеПодразделенияВыделенныеНаБаланс") Тогда
		
		Запрос = Новый Запрос("ВЫБРАТЬ
		|	Организации.Ссылка
		|ИЗ
		|	Справочник.Организации КАК Организации
		|ГДЕ
		|	Организации.ОбособленноеПодразделение
		|	И Организации.ГоловнаяОрганизация = &Организация
		|	И Организации.ДопускаютсяВзаиморасчетыЧерезГоловнуюОрганизацию");
		Запрос.УстановитьПараметр("Организация", Организация);
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СписокОрганизаций.Добавить(Выборка.Ссылка);
			
		КонецЦикла;
		
	КонецЕсли;
	
	СписокКасс = Новый СписокЗначений;
	СписокКасс.Добавить(Касса);
	СписокКасс.Добавить(Справочники.Кассы.ПустаяСсылка());
	
	Для Каждого ДинамическийСписок Из МассивДинамическихСписков() Цикл
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(ДинамическийСписок, "Организация", СписокОрганизаций, ВидСравненияКомпоновкиДанных.ВСписке,, ЗначениеЗаполнено(Организация));
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(ДинамическийСписок, "Касса", СписокКасс, ВидСравненияКомпоновкиДанных.ВСписке,, ЗначениеЗаполнено(Касса));
	КонецЦикла;
	
	СохранитьРабочиеЗначенияПолейФормы();
	УстановитьВидимостьКассы();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПараметрыДинамическихСписков()
	
	ДатаОстатков = ?(ЗначениеЗаполнено(ДатаПлатежа.Дата), ДатаПлатежа.Дата, Дата(2299,1,1));
	Граница = Новый Граница(КонецДня(ДатаОстатков), ВидГраницы.Включая);
	ЗаявкиКОплате.Параметры.УстановитьЗначениеПараметра("ДатаПлатежа", Граница);
	ЗаказыКОплате.Параметры.УстановитьЗначениеПараметра("ДатаПлатежа", Граница);
	
	Если Константы.ИспользоватьНачислениеЗарплаты.Получить() Тогда
		ВыплатаЗарплатыПоЗаявкам.Параметры.УстановитьЗначениеПараметра("Работник", Работник);
		ВыплатаЗарплатыПоЗаявкам.Параметры.УстановитьЗначениеПараметра(
			"ВыплатаПоВедомости",
			ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыплатаЗарплатыЧерезКассу);
		
		ВыплатаЗарплаты.Параметры.УстановитьЗначениеПараметра("Работник", Работник);
		ВыплатаЗарплаты.Параметры.УстановитьЗначениеПараметра(
			"ВыплатаПоВедомости",
			ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыплатаЗарплатыЧерезКассу);
	КонецЕсли;
	
	Элементы.ЗаявкиКОплате.Обновить();
	Элементы.ЗаказыКОплате.Обновить();
	Элементы.ВыплатаЗарплатыПоЗаявкам.Обновить();
	Элементы.ВыплатаЗарплаты.Обновить();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьДиначескиеСписки()
	
	Элементы.ЗаявкиКОплате.Обновить();
	Элементы.ЗаказыКОплате.Обновить();
	Элементы.ВыплатаЗарплатыПоЗаявкам.Обновить();
	Элементы.ВыплатаЗарплаты.Обновить();
	
КонецПроцедуры

&НаСервере
Функция МассивДинамическихСписков()

	МассивСписков = Новый Массив;
	МассивСписков.Добавить(РасходныеКассовыеОрдера);
	МассивСписков.Добавить(ЗаявкиКОплате);
	МассивСписков.Добавить(ЗаказыКОплате);
	МассивСписков.Добавить(ВыплатаЗарплатыПоЗаявкам);
	МассивСписков.Добавить(ВыплатаЗарплаты);
	
	Возврат МассивСписков;

КонецФункции


#КонецОбласти

#КонецОбласти
