﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОтборСписка = Параметры.Отбор;
	Регистратор = Параметры.Регистратор;
	Склад       = Параметры.Склад;
	Если Параметры.Отбор.Свойство("СкрыватьДопОтборы") Тогда
		Элементы.ПоказатьВсе.Видимость = НЕ Параметры.СкрыватьДопОтборы;
	КонецЕсли; 
	
	УстановитьСнятьОтборыСписка();
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПоказатьВсеПриИзменении(Элемент)
	
	УстановитьСнятьОтборыСписка();
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьСнятьОтборыСписка()
	Перем ЗначениеОтбора;
	
	ИспользоватьОтборы = НЕ ПоказатьВсе;
	
	Если ОтборСписка.Свойство("ХозяйственнаяОперация") Тогда
		
		Если ЗначениеЗаполнено(ОтборСписка.ХозяйственнаяОперация) Тогда
			
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
				Список,
				"ХозяйственнаяОперация",
				ОтборСписка.ХозяйственнаяОперация,
				,, Истина);
			
		Иначе
			
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
				Список,
				"ХозяйственнаяОперация",
				Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию,
				ВидСравненияКомпоновкиДанных.НеРавно,, Истина);
			
		КонецЕсли;
		
		ОтборСписка.Удалить("ХозяйственнаяОперация");
		
	КонецЕсли;
	
	МассивОтборов = Новый Массив;
	МассивОтборов.Добавить("Организация");
	МассивОтборов.Добавить("Валюта");
	МассивОтборов.Добавить("Контрагент");
	МассивОтборов.Добавить("Договор");
	МассивОтборов.Добавить("НалогообложениеНДС");
	МассивОтборов.Добавить("Партнер");
	МассивОтборов.Добавить("Соглашение");
	МассивОтборов.Добавить("ЦенаВключаетНДС");
	МассивОтборов.Добавить("ВернутьМногооборотнуюТару");
	
	Для каждого ИмяОтбора из МассивОтборов Цикл
		Если ОтборСписка.Свойство(ИмяОтбора, ЗначениеОтбора) Тогда
			
			ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
				Список,
				ИмяОтбора,
				ЗначениеОтбора);
			
			ОтборСписка.Удалить(ИмяОтбора);
			
		КонецЕсли;
	КонецЦикла;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"Регистратор",
		Регистратор);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"Склад",
		Склад);
		
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"ИспользоватьОтборы",
		ИспользоватьОтборы);
	
КонецПроцедуры

#КонецОбласти