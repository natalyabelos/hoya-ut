﻿
&НаКлиенте
Процедура ПодборКонтрагентов(Команда)
	формаПодбор = ПолучитьФорму("Обработка.бтБИТфон.Форма.ПодборКонтрагентов", , Элементы.Список); 
	формаПодбор.Открыть();
КонецПроцедуры

&НаКлиенте
Процедура СписокОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(ВыбранноеЗначение) Тогда
		строка = Объект.Список.Добавить();
		строка.Контрагент = ВыбранноеЗначение;
		массивНомераТелефонов = БИТфонСерверУТ.НайтиНомераКонтрагента(строка.Контрагент);
		Если массивНомераТелефонов.Количество() > 0 Тогда
			строка.НомерТелефона = массивНомераТелефонов[0];
		КонецЕсли;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура СписокКонтрагентНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	описаниеТипа = Новый ОписаниеТипов("СправочникСсылка." + БИТфонСерверУТ.ПолучитьИмяСправочникаКонтрагентов());
	Элемент.ОграничениеТипа = описаниеТипа;
	Элемент.ВыбиратьТип = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьФлагОбработки(Команда)
	Для Каждого строкаСписка Из Объект.Список Цикл
		Если строкаСписка.Обработан Тогда
			строкаСписка.Обработан = Ложь;
			Модифицированность = Истина;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура СписокКонтактноеЛицоНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	описаниеТипа = Новый ОписаниеТипов("СправочникСсылка." + БИТфонСерверУТ.ПолучитьИмяСправочникаКонтактныхЛиц());
	Элемент.ОграничениеТипа = описаниеТипа;
	Элемент.ВыбиратьТип = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура СписокКонтрагентПриИзменении(Элемент)
	массивНомераТелефонов = БИТфонСерверУТ.НайтиНомераКонтрагента(Элементы.Список.ТекущиеДанные.Контрагент);
	Если массивНомераТелефонов.Количество() > 0 Тогда
		Элементы.Список.ТекущиеДанные.НомерТелефона = массивНомераТелефонов[0];
	КонецЕсли;
	Элементы.Список.ТекущиеДанные.Обработан = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура СписокКонтактноеЛицоПриИзменении(Элемент)
	массивНомераТелефонов = БИТфонСерверУТ.НайтиНомераКонтактногоЛица(Элементы.Список.ТекущиеДанные.КонтактноеЛицо);
	Если массивНомераТелефонов.Количество() > 0 Тогда
		Элементы.Список.ТекущиеДанные.НомерТелефона = массивНомераТелефонов[0];
	КонецЕсли;
	Элементы.Список.ТекущиеДанные.Обработан = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура СписокНомерТелефонаНачалоВыбораИзСписка(Элемент, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Элементы.Список.ТекущиеДанные.КонтактноеЛицо) Тогда
		массивНомераТелефонов = БИТфонСерверУТ.НайтиНомераКонтактногоЛица(Элементы.Список.ТекущиеДанные.КонтактноеЛицо);
		Элемент.СписокВыбора.ЗагрузитьЗначения(массивНомераТелефонов);
	ИначеЕсли ЗначениеЗаполнено(Элементы.Список.ТекущиеДанные.Контрагент) Тогда
		массивНомераТелефонов = БИТфонСерверУТ.НайтиНомераКонтрагента(Элементы.Список.ТекущиеДанные.Контрагент);
		Элемент.СписокВыбора.ЗагрузитьЗначения(массивНомераТелефонов);
	КонецЕсли;
КонецПроцедуры

