﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);

	Документы.ЗаписьКнигиПродаж.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ДоходыИРасходыСервер.ОтразитьНДСЗаписиКнигиПродаж(ДополнительныеСвойства, Движения, Отказ);
	ВзаиморасчетыСервер.ОтразитьСуммыДокументаВВалютеРегл(ДополнительныеСвойства, Движения, Отказ);
	
	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
	Документы.СчетФактураВыданный.АктуализироватьСчетФактуру(ЭтотОбъект.Ссылка, Истина, ТребуетсяСчетФактура());
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если НЕ ЗаписьДополнительногоЛиста Тогда
		МассивНепроверяемыхРеквизитов.Добавить("КорректируемыйПериод");
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(ДокументРасчетов) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("Ценности.Номенклатура");
	Иначе
		МассивНепроверяемыхРеквизитов.Добавить("Ценности.ВидЦенности");
		МассивНепроверяемыхРеквизитов.Добавить("Ценности.Событие");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
	Документы.СчетФактураВыданный.АктуализироватьСчетФактуру(ЭтотОбъект.Ссылка, Ложь, ТребуетсяСчетФактура());
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	СуммаДокумента = Ценности.Итог("Сумма") + Ценности.Итог("СуммаНДС");
	
	Если НЕ ЗначениеЗаполнено(ДокументРасчетов) И ДокументРасчетов <> Неопределено Тогда
		ДокументРасчетов = Неопределено
	КонецЕсли;
	
	Если НЕ ЗаписьДополнительногоЛиста И ЗначениеЗаполнено(КорректируемыйПериод) Тогда
		КорректируемыйПериод = '00010101'
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		ВзаиморасчетыСервер.ЗаполнитьИдентификаторыСтрокВТабличнойЧасти(Ценности);
	КонецЕсли;
	
	РеквизитыПроверки = Документы.СчетФактураВыданный.РевизитыПроверкиСчетаФактуры();
	ЗаполнитьЗначенияСвойств(РеквизитыПроверки, ЭтотОбъект);
	Документы.СчетФактураВыданный.ПроверитьРеквизитыСчетФактуры(Ссылка, ПометкаУдаления, РеквизитыПроверки);

КонецПроцедуры

#Область ЗаполнениеДокумента

Процедура ЗаполнитьРеквизитыПоУмолчанию()

	Ответственный	= Пользователи.ТекущийПользователь();
	Организация		= ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Валюта			= ЗначениеНастроекПовтИсп.ПолучитьВалютуРегламентированногоУчета(Валюта);
	Подразделение	= ЗначениеНастроекПовтИсп.ПодразделениеПользователя(Ответственный, Подразделение);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область Прочее

Функция ТребуетсяСчетФактура()
	
	Возврат (ДокументРасчетов = Неопределено);
	
КонецФункции

#КонецОбласти

#КонецЕсли
