﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	УстановитьВидимостьЭлементовПоФО();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	ЗаполнитьСписокХозяйственныхОпераций();
	
	СписокРаспоряженияНаОформление.ТекстЗапроса = ТекстЗапросаСписокРаспоряженияНаОформление();
	
	ДатаОтгрузки = ТекущаяДата();
	СтруктураБыстрогоОтбора = Неопределено;
	Параметры.Свойство("СтруктураБыстрогоОтбора", СтруктураБыстрогоОтбора);
	Если СтруктураБыстрогоОтбора <> Неопределено Тогда
		Если СтруктураБыстрогоОтбора.Свойство("Склад", Склад) Тогда
			СкладПриИзмененииСервер();
		КонецЕсли;
		ОтборыСписковКлиентСервер.ОтборПоЗначениюСпискаПриСозданииНаСервере(
			СписокДокументы,
			"Статус",
			Статус,
			СтруктураБыстрогоОтбора);
		ОтборыСписковКлиентСервер.ОтборПоЗначениюСпискаПриСозданииНаСервере(
			СписокДокументы,
			"ХозяйственнаяОперация",
			ХозяйственнаяОперация,
			СтруктураБыстрогоОтбора);
		ОтборыСписковКлиентСервер.ОтборПоЗначениюСпискаПриСозданииНаСервере(
			СписокРаспоряженияНаОформление,
			"ХозяйственнаяОперация",
			ХозяйственнаяОперация,
			СтруктураБыстрогоОтбора);
		Если НЕ СтруктураБыстрогоОтбора.Свойство("ДатаОтгрузки",ДатаОтгрузки) Тогда
			ДатаОтгрузки = ТекущаяДата();
		КонецЕсли;
	КонецЕсли;
	УстановитьОтборПоДатеОформления(СписокРаспоряженияНаОформление, ПолучитьДатуОформления(ДатаОтгрузки));
	
	
	УстановитьТекущуюСтраницу();
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаГлобальныеКоманды);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтаФорма, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			ОбработатьШтрихкоды(Новый ОписаниеОповещения("ОбработкаОповещенияЗавершение", ЭтотОбъект, Новый Структура("ИмяСобытия", ИмяСобытия)), МенеджерОборудованияКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
            Возврат;
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	ОбработкаОповещенияФрагмент(ИмяСобытия);
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещенияЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    ИмяСобытия = ДополнительныеПараметры.ИмяСобытия;
    
    
    ОбработкаОповещенияФрагмент(ИмяСобытия);

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещенияФрагмент(Знач ИмяСобытия)
    
    Если ИмяСобытия = "Запись_ВнутреннееПотреблениеТоваров"
        ИЛИ ИмяСобытия = "Запись_ЗаказНаВнутреннееПотребление"
        ИЛИ ИмяСобытия = "Запись_ЗаказНаРемонт" Тогда
        Элементы.СписокРаспоряженияНаОформление.Обновить();
    КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Если СтруктураБыстрогоОтбора = Неопределено Тогда
		Склад = Настройки.Получить("Склад");
		СкладПриИзмененииСервер();
		ХозяйственнаяОперация = Настройки.Получить("ХозяйственнаяОперация");
		Статус = Настройки.Получить("Статус");
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			СписокДокументы,
			"Статус",
			Статус,
			ВидСравненияКомпоновкиДанных.Равно,
			,
			ЗначениеЗаполнено(Статус));
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			СписокДокументы,
			"ХозяйственнаяОперация",
			ХозяйственнаяОперация,
			ВидСравненияКомпоновкиДанных.Равно,
			,
			ЗначениеЗаполнено(ХозяйственнаяОперация));
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			СписокРаспоряженияНаОформление,
			"ХозяйственнаяОперация",
			ХозяйственнаяОперация,
			ВидСравненияКомпоновкиДанных.Равно,
			,
			ЗначениеЗаполнено(ХозяйственнаяОперация));
	Иначе
		СтруктураБыстрогоОтбора.Свойство("Склад", Склад);
		СтруктураБыстрогоОтбора.Свойство("ХозяйственнаяОперация", ХозяйственнаяОперация);
		СтруктураБыстрогоОтбора.Свойство("Статус", Статус);
	КонецЕсли;
	Настройки.Удалить("Склад");
	Настройки.Удалить("ХозяйственнаяОперация");
	Настройки.Удалить("Статус");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	СкладПриИзмененииСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура СтатусПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокДокументы,
		"Статус",
		Статус,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(Статус));
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаОтгрузкиПриИзменении(Элемент)
	
	УстановитьОтборПоДатеОформления(СписокРаспоряженияНаОформление, ПолучитьДатуОформления(ДатаОтгрузки));
	
КонецПроцедуры

&НаКлиенте
Процедура ХозяйственнаяОперацияПриИзменении(Элемент)
	
	ХозяйственнаяОперацияПриИзмененииСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокДокументыПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Если Копирование Тогда 
		Возврат;
	КонецЕсли;

	СтруктураОснование = Новый Структура("ХозяйственнаяОперация,Склад",
		ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.СписаниеТоваровПоТребованию"), Склад);
	СтруктураПараметры = Новый Структура("Основание", СтруктураОснование);
	ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.ФормаОбъекта", СтруктураПараметры, Элементы.СписокДокументы);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокРаспоряженияНаОформлениеВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПоказатьЗначение(Неопределено, Элемент.ТекущиеДанные.Ссылка);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьПередачуВЭксплуатацию(Команда)
	
	СоздатьВнутреннееПотреблениеТоваровСОперацией(1);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьПередачуВПроизводство(Команда)
	
	СоздатьВнутреннееПотреблениеТоваровСОперацией(2);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьВнутреннееПотреблениеТоваровПоРаспоряжению(Команда)
	
	Если Элементы.СписокРаспоряженияНаОформление.ТекущиеДанные = Неопределено Тогда
		
		ТекстПредупреждения = НСтр("ru = 'Команда не может быть выполнена для указанного объекта!'");
		ПоказатьПредупреждение(Неопределено, ТекстПредупреждения);
		Возврат;
		
	КонецЕсли;
	
	ВыделенныеСтроки = Элементы.СписокРаспоряженияНаОформление.ВыделенныеСтроки;
	
	КоличествоВыделенныхСтрок = ВыделенныеСтроки.Количество();
	
	МассивСсылок = Новый Массив;
	
	Если КоличествоВыделенныхСтрок = 1 ИЛИ НЕ ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам Тогда
		
		ДанныеСтроки = Элементы.СписокРаспоряженияНаОформление.ТекущиеДанные;
		ТипДокументаОснования = ДанныеСтроки.Тип;
		МассивСсылок.Добавить(ДанныеСтроки.Ссылка);
		
	Иначе
		
		ПредыдущийТипДокументаОснования = Неопределено;
		Для Каждого НомерСтроки Из ВыделенныеСтроки Цикл
			
			ДанныеСтроки = Элементы.СписокРаспоряженияНаОформление.ДанныеСтроки(НомерСтроки);
			
			ТипДокументаОснования = ДанныеСтроки.Тип;
			
			Если ПредыдущийТипДокументаОснования <> Неопределено
				И ТипДокументаОснования <> ПредыдущийТипДокументаОснования Тогда
				
				ТекстПредупреждения = НСтр("ru = 'Оформление одного документа на основании различных типов распоряжений не предусмотрено.'");
				ПоказатьПредупреждение(Неопределено, ТекстПредупреждения);
				Возврат;
				
			КонецЕсли;
			ПредыдущийТипДокументаОснования = ТипДокументаОснования;
			
			
			МассивСсылок.Добавить(ДанныеСтроки.Ссылка);
			
		КонецЦикла;
		
	КонецЕсли;

	ТекстПредупреждения = Неопределено;
		РеквизитыШапки = ПараметрыОформленияНаОснованииЗаказа(МассивСсылок, ТекстПредупреждения);
	
	Если РеквизитыШапки = Неопределено Тогда
		Если ТекстПредупреждения <> Неопределено Тогда
			ПоказатьПредупреждение(, ТекстПредупреждения);
		КонецЕсли; 
		Возврат;
	КонецЕсли;
	
	// Откроем форму
	ПараметрыОснования = Новый Структура;
	ПараметрыОснования.Вставить("РеквизитыШапки",    РеквизитыШапки);
	ПараметрыОснования.Вставить("ДокументОснование", МассивСсылок);
	ПараметрыОснования.Вставить("ДатаОтгрузки",      ДатаОтгрузки);
	
	ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.Форма.ФормаДокумента", Новый Структура("Основание", ПараметрыОснования));
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусКОтгрузке(Команда)
	
	УстановитьСтатус("КОтгрузке", НСтр("ru='К отгрузке'"));
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусОтгружено(Команда)
	
	УстановитьСтатус("Отгружено", НСтр("ru='Отгружено'"));
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьРаспоряжение(Команда)

	Если Элементы.СписокРаспоряженияНаОформление.ТекущиеДанные <> Неопределено Тогда
		ПоказатьЗначение(Неопределено, Элементы.СписокРаспоряженияНаОформление.ТекущиеДанные.Ссылка);
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура СостояниеОбеспечения(Команда)

	ОбеспечениеКлиент.ОткрытьФормуСостояниеОбеспечения(Элементы.СписокРаспоряженияНаОформление, ЭтаФорма);

КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.СписокДокументы);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Элементы.СписокДокументы);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УстановитьВидимостьЭлементовПоФО()

	ИспользуютсяРаспоряжения = ПолучитьФункциональнуюОпцию("ИспользоватьЗаказыНаВнутреннееПотребление");

	Если Не ИспользуютсяРаспоряжения Тогда
		Элементы.Страницы.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	КонецЕсли;

	Элементы.СкладРаспоряжений.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоСкладов")
		И ИспользуютсяРаспоряжения;

	Элементы.СкладВнутреннихПотреблений.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоСкладов");

	ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам = ПолучитьФункциональнуюОпцию("ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам");
	Если НЕ ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам Тогда
		Элементы.СписокРаспоряженияНаОформление.РежимВыделения = РежимВыделенияТаблицы.Одиночный;
	КонецЕсли;

	ЭтоБазоваяВерсия = ПолучитьФункциональнуюОпцию("БазоваяВерсия");
	Если ЭтоБазоваяВерсия Тогда
		АвтоЗаголовок = Ложь;
		Заголовок = НСтр("ru = 'Списание товаров на хознужды'");
		Элементы.ХозяйственнаяОперация.Видимость = Ложь;
	КонецЕсли;

	Элементы.СписокДокументыГруппаСоздать.Видимость = Не ЭтоБазоваяВерсия;
	Элементы.СписокДокументыСоздать.Видимость = ЭтоБазоваяВерсия;

КонецПроцедуры

#КонецОбласти

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Функция СсылкаНаЭлементСпискаПоШтрихкоду(Штрихкод)
	
	Менеджеры = Новый Массив();
	Менеджеры.Добавить(ПредопределенноеЗначение("Документ.ПеремещениеТоваров.ПустаяСсылка"));
	Менеджеры.Добавить(ПредопределенноеЗначение("Документ.ЗаказНаПеремещение.ПустаяСсылка"));
	Возврат ШтрихкодированиеПечатныхФормКлиент.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(Штрихкод, Менеджеры);
	
КонецФункции

&НаКлиенте
Процедура ОбработатьШтрихкоды(Знач Оповещение, Данные)
	
	МассивСсылок = СсылкаНаЭлементСпискаПоШтрихкоду(Данные.Штрихкод);
	Если МассивСсылок.Количество() > 0 Тогда
		
		Ссылка = МассивСсылок[0];
		Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
			Элементы.СписокДокументыПоступления.ТекущаяСтрока = Ссылка;
			Элементы.Страницы.ТекущаяСтраница = Элементы.Страницы.ПодчиненныеЭлементы.СтраницаПеремещенияТоваров;
		ИначеЕсли ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗаказНаПеремещение") Тогда
			Элементы.СписокРаспоряженияНаОформление.ТекущаяСтрока = Ссылка;
			Элементы.Страницы.ТекущаяСтраница = Элементы.Страницы.ПодчиненныеЭлементы.СтраницаРаспоряженияНаОформление;
		КонецЕсли;
		
		ПоказатьЗначение(Новый ОписаниеОповещения("ОбработатьШтрихкодыЗавершение", ЭтотОбъект, Новый Структура("Данные, Оповещение", Данные, Оповещение)), Ссылка);
        Возврат;
		
	Иначе
		ШтрихкодированиеПечатныхФормКлиент.ОбъектНеНайден(Данные.Штрихкод);
	КонецЕсли;
	
	ОбработатьШтрихкодыФрагмент(Оповещение);
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкодыЗавершение(ДополнительныеПараметры) Экспорт
    
    Данные = ДополнительныеПараметры.Данные;
    Оповещение = ДополнительныеПараметры.Оповещение;
    
    
    ОбработатьШтрихкодыФрагмент(Оповещение);

КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкодыФрагмент(Знач Оповещение)
    
    ВыполнитьОбработкуОповещения(Оповещение);

КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	Документы.ЗаказНаВнутреннееПотребление.УстановитьУсловноеОформлениеСписка(СписокРаспоряженияНаОформление);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокХозяйственныхОпераций()
	
	СписокХозяйственныхОпераций.Очистить();
	СписокХозяйственныхОпераций.Добавить(Перечисления.ХозяйственныеОперации.СписаниеТоваровПоТребованию);
	СписокХозяйственныхОпераций.Добавить(Перечисления.ХозяйственныеОперации.ПередачаВЭксплуатацию);
	
КонецПроцедуры


&НаСервере
Функция ПараметрыОформленияНаОснованииЗаказа(Знач МассивСсылок, ТекстПредупреждения)

	Возврат Документы.ЗаказНаВнутреннееПотребление.ПараметрыОформленияВнутреннегоПотребления(МассивСсылок, ТекстПредупреждения);

КонецФункции

&НаСервере
Процедура УстановитьТекущуюСтраницу()
	
	ИмяТекущейСтраницы = "";
	
	Если Параметры.Свойство("ИмяТекущейСтраницы", ИмяТекущейСтраницы) Тогда
		Если ЗначениеЗаполнено(ИмяТекущейСтраницы) Тогда
			ТекущийЭлемент = Элементы[ИмяТекущейСтраницы];
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатус(СтатусСтрока, ПредставлениеСтатуса = Неопределено)
	
	ПредставлениеСтатуса = ?(ПредставлениеСтатуса = Неопределено, СтатусСтрока, ПредставлениеСтатуса);
	ВыделенныеСтроки = РаботаСДиалогамиКлиент.ПроверитьПолучитьВыделенныеВСпискеСсылки(Элементы.СписокДокументы);
	Если ВыделенныеСтроки.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ТекстВопроса = НСтр("ru='У выделенных в списке документов будет установлен статус ""%ПредставлениеСтатуса%"". Продолжить?'");
	ТекстВопроса = СтрЗаменить(ТекстВопроса, "%ПредставлениеСтатуса%", ПредставлениеСтатуса);
	Режим = Новый СписокЗначений;
	Режим.Добавить(КодВозвратаДиалога.Да,НСтр("ru='Установить'"));
	Режим.Добавить(КодВозвратаДиалога.Нет,НСтр("ru='Не устанавливать'"));
	Ответ = Неопределено;

	ПоказатьВопрос(Новый ОписаниеОповещения("УстановитьСтатусЗавершение", ЭтотОбъект, Новый Структура("ВыделенныеСтроки, ПредставлениеСтатуса, СтатусСтрока", ВыделенныеСтроки, ПредставлениеСтатуса, СтатусСтрока)), ТекстВопроса,Режим);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
    
    ВыделенныеСтроки = ДополнительныеПараметры.ВыделенныеСтроки;
    ПредставлениеСтатуса = ДополнительныеПараметры.ПредставлениеСтатуса;
    СтатусСтрока = ДополнительныеПараметры.СтатусСтрока;
    
    
    Ответ = РезультатВопроса;
    
    Если Ответ = КодВозвратаДиалога.Нет Тогда
        Возврат;
    КонецЕсли;
    
    ОчиститьСообщения();
    КоличествоОбработанных = ОбщегоНазначенияУТВызовСервера.УстановитьСтатусДокументов(ВыделенныеСтроки, СтатусСтрока);
    ОбщегоНазначенияУТКлиент.ОповеститьПользователяОбУстановкеСтатуса(Элементы.СписокДокументы, КоличествоОбработанных, ВыделенныеСтроки.Количество(), ПредставлениеСтатуса);

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьОтборПоДатеОформления(СписокДокументов, ЗначениеПараметра)

	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		СписокДокументов,
		"ДатаОтгрузки",
		ЗначениеПараметра);

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьДатуОформления(Дата)

	Если Не ЗначениеЗаполнено(Дата) Тогда

		Результат = '00010101';

	ИначеЕсли Дата < НачалоДня(ТекущаяДата()) Тогда

		Дата = КонецДня(ТекущаяДата());
		Результат = Дата + 1;

	Иначе

		Результат = КонецДня(Дата) + 1;

	КонецЕсли;

	Возврат Результат;

КонецФункции

&НаКлиенте
Процедура СоздатьВнутреннееПотреблениеТоваровСОперацией(ХозяйственнаяОперацияИндекс)
	
	СтруктураОснование = Новый Структура("ХозяйственнаяОперация,Склад",
		СписокХозяйственныхОпераций[ХозяйственнаяОперацияИндекс].Значение, Склад);
		
	СтруктураПараметры = Новый Структура("Основание", СтруктураОснование);
	ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.ФормаОбъекта", СтруктураПараметры, Элементы.СписокДокументы);
	
КонецПроцедуры

&НаСервере
Процедура СкладПриИзмененииСервер()
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокДокументы,
		"Склад",
		Склад,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(Склад));
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокРаспоряженияНаОформление,
		"Склад",
		Склад,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(Склад));
	
КонецПроцедуры

&НаСервере
Процедура ХозяйственнаяОперацияПриИзмененииСервер()
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокДокументы,
		"ХозяйственнаяОперация",
		ХозяйственнаяОперация,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(ХозяйственнаяОперация));
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокРаспоряженияНаОформление,
		"ХозяйственнаяОперация",
		ХозяйственнаяОперация,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(ХозяйственнаяОперация));
	
КонецПроцедуры

&НаСервере
Функция ТекстЗапросаСписокРаспоряженияНаОформление()
	
	Возврат
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	ЗаказыОстатки.ЗаказНаВнутреннееПотребление КАК Ссылка,
		|	ЗаказыОстатки.ЗаказНаВнутреннееПотребление.Организация КАК Организация,
		|	ТИПЗНАЧЕНИЯ(ЗаказыОстатки.ЗаказНаВнутреннееПотребление) КАК Тип,
		|	ЗаказыОстатки.Склад КАК Склад,
		|			ЗаказыОстатки.ЗаказНаВнутреннееПотребление.Подразделение 
		|	КАК Подразделение,
		|	ЗаказыОстатки.ЗаказНаВнутреннееПотребление.Дата КАК Дата,
		|	ЗаказыОстатки.ЗаказНаВнутреннееПотребление.Номер КАК Номер,
		|			ВЫРАЗИТЬ(ЗаказыОстатки.ЗаказНаВнутреннееПотребление КАК Документ.ЗаказНаВнутреннееПотребление).ХозяйственнаяОперация
		|	КАК ХозяйственнаяОперация
		|ИЗ
		|	РегистрНакопления.ЗаказыНаВнутреннееПотребление.Остатки(&ДатаОтгрузки, {(ЗаказНаВнутреннееПотребление) КАК Распоряжение}) КАК ЗаказыОстатки
		|	
		|ГДЕ
		|	ЗаказыОстатки.КОформлениюОстаток > 0
		|	И ЕСТЬNULL(ЗаказыОстатки.ЗаказНаВнутреннееПотребление.Организация, НЕОПРЕДЕЛЕНО) <> НЕОПРЕДЕЛЕНО
		|	";
		
КонецФункции

#КонецОбласти

#КонецОбласти
