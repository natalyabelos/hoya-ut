﻿&НаКлиенте
Перем КэшированныеЗначения; //используется механизмом обработки изменения реквизитов ТЧ

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);

	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	Валюта = Константы.ВалютаУправленческогоУчета.Получить();
	ПриЧтенииСозданииНаСервере();
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	ПланыВидовХарактеристик.СтатьиРасходов.ЗаполнитьПризнакАналитикаРасходовОбязательна(Объект.Расходы);

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_РаспределениеРасходовНаСебестоимостьТоваров");

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СтатьяРасходовПриИзменении(Элемент)
	
	РасходыСтатьяРасходовПриИзмененииНаСервере(КэшированныеЗначения);
	
КонецПроцедуры

&НаСервере
Процедура РасходыСтатьяРасходовПриИзмененииНаСервере(КэшированныеЗначения)
	
	СтрокаТаблицы = Объект.Расходы.НайтиПоИдентификатору(Элементы.Расходы.ТекущаяСтрока);
	
	Если Не ЗначениеЗаполнено(СтрокаТаблицы.СтатьяРасходов) Тогда
		СтрокаТаблицы.АналитикаРасходов = Неопределено;
	Иначе
		СтрокаТаблицы.ПравилоРаспределения = ПолучитьПравилоРаспределенияСтатьиРасходов(СтрокаТаблицы.СтатьяРасходов);
	КонецЕсли;
	
	СтруктураДействий = Новый Структура("ЗаполнитьПризнакАналитикаРасходовОбязательна");
	ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(СтрокаТаблицы, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ВариантРаспределенияПоВсемОрганизациямПриИзменении(Элемент)
	
	ВариантРаспределенияПоОрганизациямПриИзменении();
	
КонецПроцедуры

&НаКлиенте
Процедура ВариантРаспределенияПоВыбраннымОрганизациямПриИзменении(Элемент)
	
	ВариантРаспределенияПоОрганизациямПриИзменении();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаполнитьРасходы()
	
	СтруктураРеквизитов = Новый Структура;
	Если Не Объект.РаспределениеПоВсемОрганизациям Тогда
		СтруктураРеквизитов.Вставить("Организация");
	КонецЕсли;
	
	Если ОбщегоНазначенияУТКлиент.ВозможноЗаполнениеТабличнойЧасти(ЭтаФорма, Объект.Расходы, СтруктураРеквизитов) Тогда
		ЗаполнитьРасходыПоОстаткам();
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(
		УсловноеОформление, Новый Структура("Расходы"));
	
КонецПроцедуры

#Область ПриИзмененииРеквизитов

&НаСервере
Процедура ВариантРаспределенияПоОрганизациямПриИзменении()
	
	Объект.РаспределениеПоВсемОрганизациям = (ВариантРаспределенияПоОрганизациям = 1);
	УправлениеЭлементамиФормы();
	
	Если Объект.РаспределениеПоВсемОрганизациям
	 И ЗначениеЗаполнено(Объект.Организация) Тогда
	 
		Для Каждого СтрокаТаблицы Из Объект.Расходы Цикл
			Если Не ЗначениеЗаполнено(СтрокаТаблицы.Организация) Тогда
				СтрокаТаблицы.Организация = Объект.Организация;
			КонецЕсли;
		КонецЦикла;
	 
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	Если Объект.РаспределениеПоВсемОрганизациям Тогда
		ВариантРаспределенияПоОрганизациям = 1;
	Иначе
		ВариантРаспределенияПоОрганизациям = 0;
	КонецЕсли;
	УправлениеЭлементамиФормы();
	
	ПланыВидовХарактеристик.СтатьиРасходов.ЗаполнитьПризнакАналитикаРасходовОбязательна(Объект.Расходы);
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭлементамиФормы()
	
	Элементы.Организация.Доступность = Не Объект.РаспределениеПоВсемОрганизациям;
	Элементы.РасходыОрганизация.Видимость = Объект.РаспределениеПоВсемОрганизациям;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРасходыПоОстаткам()

	Запрос = Новый Запрос("
	| ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ПрочиеРасходы.Организация КАК Организация,
	|	ПрочиеРасходы.Подразделение КАК Подразделение,
	|	ПрочиеРасходы.СтатьяРасходов КАК СтатьяРасходов,
	|	ПрочиеРасходы.АналитикаРасходов КАК АналитикаРасходов,
	|	ПрочиеРасходы.СтатьяРасходов.ПравилоРаспределенияНаСебестоимость КАК ПравилоРаспределения,
	|	ПрочиеРасходы.СуммаОстаток КАК Сумма,
	|	ПрочиеРасходы.СуммаБезНДСОстаток КАК СуммаБезНДС,
	|	ПрочиеРасходы.СуммаРеглОстаток КАК СуммаРегл
	|ИЗ
	|	РегистрНакопления.ПрочиеРасходы.Остатки(&Граница,
	|		(Организация = &Организация
	|			ИЛИ &ПоВсемОрганизациям)
	|		И СтатьяРасходов.ВариантРаспределенияРасходов = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|	) КАК ПрочиеРасходы
	|ГДЕ
	|	ПрочиеРасходы.СуммаОстаток <> 0
	|	ИЛИ ПрочиеРасходы.СуммаБезНДСОстаток <> 0
	|	ИЛИ ПрочиеРасходы.СуммаРеглОстаток <> 0
	|");
	Запрос.УстановитьПараметр("Граница", Новый Граница(КонецМесяца(Объект.Дата), ВидГраницы.Включая));
	Запрос.УстановитьПараметр("Организация", Объект.Организация);
	Запрос.УстановитьПараметр("ПоВсемОрганизациям", Объект.РаспределениеПоВсемОрганизациям);
	Объект.Расходы.Загрузить(Запрос.Выполнить().Выгрузить());

КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьПравилоРаспределенияСтатьиРасходов(СтатьяРасходов)
	
	Возврат ПланыВидовХарактеристик.СтатьиРасходов.ПолучитьПравилоРаспределения(СтатьяРасходов);

КонецФункции

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

#КонецОбласти
