﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Функция определяет реквизиты документа.
//
// Параметры:
//  ДокументСсылка - ДокументСсылка.ОтчетКомитентуОСписании - Ссылка на документ
//
// Возвращаемое значение:
//	Структура - Реквизиты выбранного документа
//
Функция РеквизитыДокумента(ДокументСсылка) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДанныеДокумента.Организация КАК Организация,
	|	ДанныеДокумента.Партнер КАК Партнер,
	|	ДанныеДокумента.Контрагент КАК Контрагент,
	|	ДанныеДокумента.Валюта КАК ВалютаВзаиморасчетов,
	|	ДанныеДокумента.СуммаДокумента КАК СуммаДокумента,
	|	ДанныеДокумента.СуммаДокумента КАК СуммаВзаиморасчетов,
	|	ДанныеДокумента.Проведен КАК Проведен,
	|	ДанныеДокумента.НачалоПериода КАК НачалоПериода,
	|	ДанныеДокумента.КонецПериода КАК КонецПериода,
	|	ДанныеДокумента.Договор КАК Договор,
	|	ДанныеДокумента.ПорядокРасчетов КАК ПорядокРасчетов
	|
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &ДокументСсылка
	|");
	
	Запрос.УстановитьПараметр("ДокументСсылка", ДокументСсылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Организация = Выборка.Организация;
		Партнер = Выборка.Партнер;
		Контрагент = Выборка.Контрагент;
		Договор = Выборка.Договор;
		ПорядокРасчетов = Выборка.ПорядокРасчетов;
		ВалютаВзаиморасчетов = Выборка.ВалютаВзаиморасчетов;
		СуммаДокумента = Выборка.СуммаДокумента;
		СуммаВзаиморасчетов = ?(Выборка.Проведен, Выборка.СуммаВзаиморасчетов, 0);
		НачалоПериода = Выборка.НачалоПериода;
		КонецПериода = Выборка.КонецПериода;
	Иначе
		Организация = Справочники.Организации.ПустаяСсылка();
		Партнер = Справочники.Партнеры.ПустаяСсылка();
		Контрагент = Справочники.Контрагенты.ПустаяСсылка();
		Договор = Справочники.ДоговорыКонтрагентов.ПустаяСсылка();
		ПорядокРасчетов = Перечисления.ПорядокРасчетов.ПустаяСсылка();
		ВалютаВзаиморасчетов = Справочники.Валюты.ПустаяСсылка();
		СуммаДокумента = 0;
		СуммаВзаиморасчетов = 0;
		НачалоПериода = Неопределено;
		КонецПериода = Неопределено;
	КонецЕсли;
	
	СтруктураРеквизитов = Новый Структура("Организация, Партнер, Контрагент, Договор, ПорядокРасчетов, ВалютаВзаиморасчетов, ХозяйственнаяОперация, СуммаДокумента, СуммаВзаиморасчетов, НачалоПериода, КонецПериода",
		Организация,
		Партнер,
		Контрагент,
		Договор,
		ПорядокРасчетов,
		ВалютаВзаиморасчетов,
		Перечисления.ХозяйственныеОперации.ОтчетКомитенту,
		СуммаДокумента,
		СуммаВзаиморасчетов,
		НачалоПериода,
		КонецПериода);
	
	Возврат СтруктураРеквизитов;

КонецФункции


#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, СтруктураДополнительныеСвойства) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ДанныеДокумента.Дата КАК Период,
	|	ДанныеДокумента.Ссылка КАК Ссылка,
	|	ДанныеДокумента.Валюта КАК Валюта,
	|	ДанныеДокумента.Организация КАК Организация,
	|	ДанныеДокумента.Партнер КАК Партнер,
	|	ДанныеДокумента.Контрагент КАК Контрагент,
	|	ДанныеДокумента.Договор КАК Договор,
	|	ВЫБОР КОГДА ДанныеДокумента.ПорядокРасчетов = ЗНАЧЕНИЕ(Перечисление.ПорядокРасчетов.ПоДоговорамКонтрагентов) ТОГДА
	|		ИСТИНА
	|	ИНАЧЕ
	|		ЛОЖЬ
	|	КОНЕЦ КАК РасчетыПоДоговорам,
	|
	|	ВЫБОР КОГДА ДанныеДокумента.КонецПериода = ДАТАВРЕМЯ(1, 1, 1)
	|		ИЛИ ДанныеДокумента.КонецПериода > ДанныеДокумента.Дата
	|	ТОГДА
	|		ДанныеДокумента.Дата
	|	ИНАЧЕ
	|		ДанныеДокумента.КонецПериода
	|	КОНЕЦ КАК КонецПериода,
	|	ДанныеДокумента.Соглашение,
	|	ДанныеДокумента.Подразделение,
	|	ДанныеДокумента.Менеджер,
	|	ДанныеДокумента.ГруппаФинансовогоУчета
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК ДанныеДокумента
	|	
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|";
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Коэффициенты = РаботаСКурсамивалютУТ.ПолучитьКоэффициентыПересчетаВалюты(
		Реквизиты.Валюта, 
		, // ВалютаВзаиморасчетов
		Реквизиты.Период);
	
	ПересчитатьТаблицуТоваровВВалютуРегл(Реквизиты, Запрос.МенеджерВременныхТаблиц);
	
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.УстановитьПараметр("Период", Реквизиты.Период);
	Запрос.УстановитьПараметр("КонецПериода", КонецДня(Реквизиты.КонецПериода));
	Запрос.УстановитьПараметр("Валюта", Реквизиты.Валюта);
	Запрос.УстановитьПараметр("Организация", Реквизиты.Организация);
	Запрос.УстановитьПараметр("Партнер", Реквизиты.Партнер);
	Запрос.УстановитьПараметр("ХозяйственнаяОперация", Перечисления.ХозяйственныеОперации.ОтчетКомитентуОСписании);
	Запрос.УстановитьПараметр("АналитикаУчетаПоПартнерам", РегистрыСведений.АналитикаУчетаПоПартнерам.ЗначениеКлючаАналитики(Реквизиты));
	Запрос.УстановитьПараметр("КоэффициентПересчетаВВалютуУпр", Коэффициенты.КоэффициентПересчетаВВалютуУПР);
	Запрос.УстановитьПараметр("КоэффициентПересчетаВВалютуРегл", Коэффициенты.КоэффициентПересчетаВВалютуРегл);
	Запрос.УстановитьПараметр("ВалютаРегламентированногоУчета", Константы.ВалютаРегламентированногоУчета.Получить());
	Запрос.УстановитьПараметр("УчитыватьСебестоимостьТоваровПоВидамЗапасов", ПолучитьФункциональнуюОпцию("УчитыватьСебестоимостьТоваровПоВидамЗапасов"));
	Запрос.УстановитьПараметр("Договор", Реквизиты.Договор);
	Запрос.УстановитьПараметр("РасчетыПоДоговорам", Реквизиты.РасчетыПоДоговорам);
	Запрос.УстановитьПараметр("КорВидЗапасов", Справочники.ВидыЗапасов.ВидЗапасовДокумента(Реквизиты.Организация, Перечисления.ХозяйственныеОперации.ОтчетКомитентуОСписании, Неопределено));
	Запрос.УстановитьПараметр("ИспользоватьПартионныйУчет", ПолучитьФункциональнуюОпцию("ИспользоватьПартионныйУчет"));
	Запрос.УстановитьПараметр("Контрагент", Реквизиты.Контрагент);
	Запрос.УстановитьПараметр("Соглашение", Реквизиты.Соглашение);
	Запрос.УстановитьПараметр("Подразделение", Реквизиты.Подразделение);
	Запрос.УстановитьПараметр("Менеджер", Реквизиты.Менеджер);
	Запрос.УстановитьПараметр("ГруппаФинансовогоУчета", Реквизиты.ГруппаФинансовогоУчета);
	Запрос.УстановитьПараметр("ФормироватьВидыЗапасовПоГруппамФинансовогоУчета", ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоГруппамФинансовогоУчета"));
	
	Запрос.Текст = ТекстЗапросаТаблицаВтВидыЗапасов()
		+ ТекстЗапросаТаблицаТоварыКОформлениюОтчетовКомитенту()
		+ ТекстЗапросаТаблицаРасчетыСПоставщиками()
		+ ТекстЗапросаТаблицаРасчетыСПоставщикамиПоследовательность()
		+ ТекстЗапросаТаблицаСебестоимостьТоваров()
		+ ТекстЗапросаТаблицаСуммыДокументовВВалютеРегл()
		+ ТекстЗапросаТаблицаПартииТоваровОрганизаций()
		+ ТекстЗапросаТаблицаПартииТоваровОрганизацийПоследовательность()
		+ ТекстЗапросаТаблицаЗакупки()
		;
	
	МассивРезультатов = Запрос.ВыполнитьПакет();
	
	ТаблицыДляДвижений = СтруктураДополнительныеСвойства.ТаблицыДляДвижений;
	// МассивРезультатов[0] - ВтВидыЗапасов
	ТаблицыДляДвижений.Вставить("ТаблицаТоварыКОформлениюОтчетовКомитенту",          МассивРезультатов[1].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаРасчетыСПоставщиками",                       МассивРезультатов[2].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаРасчетыСПоставщикамиПоследовательность",     МассивРезультатов[3].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаСебестоимостьТоваров",                       МассивРезультатов[4].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаСуммыДокументовВВалютеРегл",                 МассивРезультатов[5].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаПартииТоваровОрганизаций",                   МассивРезультатов[6].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаПартииТоваровОрганизацийПоследовательность", МассивРезультатов[7].Выгрузить());
	ТаблицыДляДвижений.Вставить("ТаблицаЗакупки",									 МассивРезультатов[8].Выгрузить());
	
КонецПроцедуры

Функция ТекстЗапросаТаблицаВтВидыЗапасов()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки                          КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры           КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.НомерГТД                             КАК НомерГТД,
	|	ТаблицаВидыЗапасов.ВидЗапасов.ТипЗапасов                КАК ТипЗапасов,
	|	ТаблицаВидыЗапасов.ВидЗапасов                           КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.Количество                           КАК Количество,
	|	ВЫРАЗИТЬ(ТаблицаВидыЗапасов.СуммаСНДС
	|		* &КоэффициентПересчетаВВалютуУпр КАК ЧИСЛО(15,2))  КАК Стоимость,
	|	ВЫРАЗИТЬ((ТаблицаВидыЗапасов.СуммаСНДС - ТаблицаВидыЗапасов.СуммаНДС)
	|		* &КоэффициентПересчетаВВалютуУпр КАК ЧИСЛО(15,2))  КАК СтоимостьБезНДС,
	|	ВЫРАЗИТЬ((ТаблицаВидыЗапасов.СуммаСНДС)
	|		* &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15,2)) КАК СтоимостьРегл,
	|	ВЫРАЗИТЬ(ТаблицаВидыЗапасов.СуммаНДС
	|		* &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15,2)) КАК НДСРегл,
	|	Аналитика.Номенклатура                                  КАК Номенклатура,
	|	Аналитика.Характеристика                                КАК Характеристика,
	|	Аналитика.Склад											КАК Склад,
	|	ТаблицаВидыЗапасов.СуммаСНДС							КАК СуммаСНДС,
	|	ТаблицаВидыЗапасов.СуммаНДС								КАК СуммаНДС,
	|	ТаблицаВидыЗапасов.АналитикаУчетаПартий					КАК АналитикаУчетаПартий
	|ПОМЕСТИТЬ ВтВидыЗапасов
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании.ВидыЗапасов КАК ТаблицаВидыЗапасов
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		РегистрСведений.АналитикаУчетаНоменклатуры КАК Аналитика
	|	ПО
	|		ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры = Аналитика.КлючАналитики
	|ГДЕ
	|	ТаблицаВидыЗапасов.Ссылка = &Ссылка
	|;
	|//////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;	
	
КонецФункции

Функция ТекстЗапросаТаблицаТоварыКОформлениюОтчетовКомитенту()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки                КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)        КАК ВидДвижения,
	|	&КонецПериода                                 КАК Период,
	|	ТаблицаВидыЗапасов.ВидЗапасов                 КАК ВидЗапасов,
	|	&Валюта                                       КАК Валюта,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.Номенклатура               КАК Номенклатура,
	|	ТаблицаВидыЗапасов.Характеристика             КАК Характеристика,
	|	ТаблицаВидыЗапасов.НомерГТД                   КАК НомерГТД,
	|	ТаблицаВидыЗапасов.Количество                 КАК КоличествоСписано,
	|	&ХозяйственнаяОперация                        КАК ХозяйственнаяОперация,
	|	&КорВидЗапасов                                КАК КорВидЗапасов
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|ГДЕ
	|	ТаблицаВидыЗапасов.Количество > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки                КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)        КАК ВидДвижения,
	|	&КонецПериода                                 КАК Период,
	|	ТаблицаВидыЗапасов.ВидЗапасов                 КАК ВидЗапасов,
	|	&Валюта                                       КАК Валюта,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.Номенклатура               КАК Номенклатура,
	|	ТаблицаВидыЗапасов.Характеристика             КАК Характеристика,
	|	ТаблицаВидыЗапасов.НомерГТД                   КАК НомерГТД,
	|	-ТаблицаВидыЗапасов.Количество                КАК КоличествоСписано,
	|	&ХозяйственнаяОперация                        КАК ХозяйственнаяОперация,
	|	&КорВидЗапасов                                КАК КорВидЗапасов
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|ГДЕ
	|	ТаблицаВидыЗапасов.Количество < 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаРасчетыСПоставщиками()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	ДанныеДокумента.ДатаПлатежа КАК ДатаПлатежа,
	|	&Период КАК ДатаРегистратора,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|
	|	&АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|
	|	ВЫБОР КОГДА &РасчетыПоДоговорам ТОГДА
	|		&Договор
	|	ИНАЧЕ
	|		&Ссылка
	|	КОНЕЦ КАК ЗаказПоставщику,
	|
	|	&Валюта КАК Валюта,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	
	|	ДанныеДокумента.СуммаДокумента КАК Сумма,
	|	0 КАК КОплате,
	|	ВЫРАЗИТЬ(ДанныеДокумента.СуммаДокумента * &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15, 2)) КАК СуммаРегл,
	|	ВЫРАЗИТЬ(ДанныеДокумента.СуммаДокумента * &КоэффициентПересчетаВВалютуУпр КАК ЧИСЛО(15, 2)) КАК СуммаУпр
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК ДанныеДокумента
	|	
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	КОНЕЦПЕРИОДА(ДанныеДокумента.ДатаПлатежа, День) КАК Период,
	|	ДанныеДокумента.ДатаПлатежа КАК ДатаПлатежа,
	|	&Период КАК ДатаРегистратора,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|
	|	&АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|
	|	ВЫБОР КОГДА &РасчетыПоДоговорам ТОГДА
	|		&Договор
	|	ИНАЧЕ
	|		&Ссылка
	|	КОНЕЦ КАК ЗаказПоставщику,
	|
	|	&Валюта КАК Валюта,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	
	|	0 КАК Сумма,
	|	ДанныеДокумента.СуммаДокумента КАК КОплате,
	|	0 КАК СуммаРегл,
	|	0 КАК СуммаУпр
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК ДанныеДокумента
	|	
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
		
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаРасчетыСПоставщикамиПоследовательность()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Ссылка КАК Регистратор,
	|	&АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаСебестоимостьТоваров()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ВидыЗапасов.НомерСтроки КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)КАК ВидДвижения,
	|	&Период КАК Период,
	|	ВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	&Организация КАК Организация,
	|	ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыНаСкладах) КАК РазделУчета,
	|
	|	ВЫБОР КОГДА &УчитыватьСебестоимостьТоваровПоВидамЗапасов ТОГДА
	|		ВЫБОР
	|			КОГДА
	|				ВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар)
	|			ТОГДА
	|				&КорВидЗапасов
	|			ИНАЧЕ
	|				ВидыЗапасов.ВидЗапасов
	|		КОНЕЦ
	|	ИНАЧЕ
	|		Неопределено
	|	КОНЕЦ КАК ВидЗапасов,
	|
	|	ВидыЗапасов.Количество КАК Количество,
	|	ВидыЗапасов.Стоимость КАК Стоимость,
	|	ВидыЗапасов.СтоимостьБезНДС КАК СтоимостьБезНДС,
	|	ВидыЗапасов.СтоимостьРегл КАК СтоимостьРегл,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация
	|
	|ИЗ
	|	ВтВидыЗапасов КАК ВидыЗапасов
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаСуммыДокументовВВалютеРегл()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Валюта КАК Валюта,
	|	ТаблицаВидыЗапасов.ИдентификаторСтроки КАК ИдентификаторСтроки,
	|	ТаблицаВидыЗапасов.СтавкаНДС КАК СтавкаНДС,
	|	ТаблицаВидыЗапасов.СуммаСНДС - ТаблицаВидыЗапасов.СуммаНДС КАК СуммаБезНДС,
	|	ТаблицаВидыЗапасов.СуммаНДС КАК СуммаНДС,
	|	ТаблицаДокументаРегл.СуммаБезНДС КАК СуммаБезНДСРегл,
	|	ТаблицаДокументаРегл.СуммаНДС КАК СуммаНДСРегл,
	|	НЕОПРЕДЕЛЕНО КАК ТипРасчетов
	|
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании.ВидыЗапасов КАК ТаблицаВидыЗапасов
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		ТаблицаТоваровПредварительная КАК ТаблицаДокументаРегл
	|	ПО
	|		ТаблицаВидыЗапасов.ИдентификаторСтроки = ТаблицаДокументаРегл.ИдентификаторСтроки
	|
	|ГДЕ
	|	ТаблицаВидыЗапасов.Ссылка = &Ссылка
	|	И &Валюта <> &ВалютаРегламентированногоУчета
	|
	|УПОРЯДОЧИТЬ ПО
	|	ТаблицаВидыЗапасов.НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаТаблицаСуммыДокументовВВалютеРегл()

Функция ТекстЗапросаТаблицаПартииТоваровОрганизаций()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки                КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)        КАК ВидДвижения,
	|	&КонецПериода                                 КАК Период,
	|	&Ссылка                                       КАК Регистратор,
	|	&Организация                                  КАК Организация,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	&Ссылка                                       КАК ДокументПоступления,
	|	&КорВидЗапасов                                КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.АналитикаУчетаПартий       КАК АналитикаУчетаПартий,
	|	ТаблицаВидыЗапасов.Количество                 КАК Количество,
	|	ТаблицаВидыЗапасов.Стоимость                  КАК Стоимость,
	|	ТаблицаВидыЗапасов.СтоимостьБезНДС            КАК СтоимостьБезНДС,
	|	ТаблицаВидыЗапасов.СтоимостьРегл              КАК СтоимостьРегл,
	|	0                                             КАК НДСРегл,
	|	ТаблицаВидыЗапасов.Номенклатура               КАК Номенклатура,
	|	ТаблицаВидыЗапасов.Характеристика             КАК Характеристика,
	|	&ХозяйственнаяОперация                        КАК ХозяйственнаяОперация,
	|	ИСТИНА                                        КАК Первичное,
	|	НЕОПРЕДЕЛЕНО                                  КАК ДокументИсточник
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|ГДЕ
	|	&ИспользоватьПартионныйУчет
	|	И ТаблицаВидыЗапасов.Количество > 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаПартииТоваровОрганизацийПоследовательность()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	&КонецПериода                     КАК Период,
	|	&Ссылка                           КАК Регистратор,
	|	ТаблицаВидыЗапасов.Номенклатура   КАК Номенклатура,
	|	ТаблицаВидыЗапасов.Характеристика КАК Характеристика
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|ГДЕ
	|	&ИспользоватьПартионныйУчет
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаЗакупки()

	ТекстЗапроса =
	// Отражение задолженности перед комитентом 
	"ВЫБРАТЬ
	|	&Период КАК Период,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ОтражениеЗадолженностиПередКомитентом) КАК ХозяйственнаяОперация,
	|	&Организация КАК Организация,
	|	&Подразделение КАК Подразделение,
	|	&Менеджер КАК Менеджер,
	|
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.Склад КАК Склад,
	|	ВЫБОР КОГДА &УчитыватьСебестоимостьТоваровПоВидамЗапасов ТОГДА
	|		ВЫБОР
	|			КОГДА ТаблицаВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|				ВЫРАЗИТЬ(&КорВидЗапасов КАК Справочник.ВидыЗапасов).ТипЗапасов
	|			ИНАЧЕ ТаблицаВидыЗапасов.ТипЗапасов
	|		КОНЕЦ
	|	ИНАЧЕ
	|		НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК ТипЗапасов,
	|	ВЫБОР КОГДА &УчитыватьСебестоимостьТоваровПоВидамЗапасов ТОГДА
	|		ВЫБОР
	|			КОГДА ТаблицаВидыЗапасов.ТипЗапасов = ЗНАЧЕНИЕ(Перечисление.ТипыЗапасов.КомиссионныйТовар) ТОГДА
	|				&КорВидЗапасов
	|			ИНАЧЕ ТаблицаВидыЗапасов.ВидЗапасов
	|		КОНЕЦ
	|	ИНАЧЕ
	|		НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК ВидЗапасов,
	|
	|	&Партнер КАК Партнер,
	|	&Контрагент КАК Контрагент,
	|	&Соглашение КАК Соглашение,
	|	&Договор КАК Договор,
	|	НЕОПРЕДЕЛЕНО КАК Заказ,
	|
	|	ТаблицаВидыЗапасов.Количество КАК Количество,
	|
	|	ТаблицаВидыЗапасов.Стоимость КАК Сумма,
	|	ТаблицаВидыЗапасов.СтоимостьБезНДС КАК СуммаБезНДС,
	|	ТаблицаВидыЗапасов.СтоимостьРегл КАК СуммаРегл,
	|	ТаблицаВидыЗапасов.СтоимостьРегл - ТаблицаВидыЗапасов.НДСРегл КАК СуммаРеглБезНДС,
	|	0 КАК СуммаСкидки,
	|
	|	ТаблицаВидыЗапасов.Стоимость КАК Стоимость,
	|	ТаблицаВидыЗапасов.СтоимостьБезНДС КАК СтоимостьБезНДС,
	|	ТаблицаВидыЗапасов.СтоимостьРегл КАК СтоимостьРегл,
	|	0 КАК СуммаДопРасходов,
	|	0 КАК СуммаДопРасходовБезНДС,
	|
	|	&Валюта КАК ВалютаДокумента,
	|	ТаблицаВидыЗапасов.СуммаСНДС КАК СуммаВВалютеДокумента,
	|	ТаблицаВидыЗапасов.СуммаСНДС - ТаблицаВидыЗапасов.СуммаНДС КАК СуммаБезНДСВВалютеДокумента,
	|
	|	&Валюта КАК ВалютаВзаиморасчетов,
	|	ТаблицаВидыЗапасов.СуммаСНДС КАК СуммаВВалютеВзаиморасчетов,
	|	ТаблицаВидыЗапасов.СуммаСНДС - ТаблицаВидыЗапасов.СуммаНДС КАК СуммаБезНДСВВалютеВзаиморасчетов,
	|
	|	ВЫБОР
	|		КОГДА &ФормироватьВидыЗапасовПоГруппамФинансовогоУчета
	|			ТОГДА ТаблицаВидыЗапасов.ВидЗапасов
	|		ИНАЧЕ ТаблицаВидыЗапасов.Номенклатура
	|	КОНЕЦ КАК ИсточникГФУНоменклатуры,
	|	ВЫБОР
	|		КОГДА &ГруппаФинансовогоУчета <> Значение(Справочник.ГруппыФинансовогоУчетаРасчетов.ПустаяСсылка)
	|			ТОГДА &Ссылка
	|		КОГДА &РасчетыПоДоговорам
	|			ТОГДА &Договор
	|		ИНАЧЕ &Ссылка
	|	КОНЕЦ КАК ИсточникГФУРасчетов
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|;
	|"; 
	
	Возврат ТекстЗапроса;

КонецФункции

Процедура ПересчитатьТаблицуТоваровВВалютуРегл(Реквизиты, МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ТаблицаДокумента.Ссылка                                КАК Ссылка,
	|	ТаблицаДокумента.НомерСтроки                           КАК НомерСтроки,
	|	ТаблицаДокумента.ИдентификаторСтроки                   КАК ИдентификаторСтроки,
	|	ТаблицаДокумента.СуммаСНДС - ТаблицаДокумента.СуммаНДС КАК СуммаБезНДС,
	|	ТаблицаДокумента.СтавкаНДС                             КАК СтавкаНДС,
	|	ТаблицаДокумента.СуммаНДС                              КАК СуммаНДС,
	|	&Валюта                                                КАК Валюта,
	|	&Период                                                КАК Дата
	|
	|ПОМЕСТИТЬ ТаблицаТоваровПредварительная
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании.ВидыЗапасов КАК ТаблицаДокумента
	|
	|ГДЕ
	|	ТаблицаДокумента.Ссылка = &Ссылка
	|	И &Валюта <> &ВалютаРегламентированногоУчета
	|";
	Запрос.УстановитьПараметр("Ссылка",                         Реквизиты.Ссылка);
	Запрос.УстановитьПараметр("Период",                         Реквизиты.Период);
	Запрос.УстановитьПараметр("Валюта",                         Реквизиты.Валюта);
	Запрос.УстановитьПараметр("ВалютаРегламентированногоУчета", Константы.ВалютаРегламентированногоУчета.Получить());
	
	Запрос.Выполнить();
	
	ОбщегоНазначенияУТ.ПересчитатьТаблицуТоваровВВалютуРегл(МенеджерВременныхТаблиц);
	
КонецПроцедуры

#КонецОбласти

#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	// Отчет комитенту о списании
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.МенеджерПечати = "Обработка.ПечатьОбщихФорм";
	КомандаПечати.Идентификатор = "ОтчетКомиссионераСписание";
	КомандаПечати.Представление = НСтр("ru = 'Отчет комитенту о списании'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;

КонецПроцедуры

// Формирует печатные формы.
//
// Параметры:
//  МассивОбъектов  - Массив    - ссылки на объекты, которые нужно распечатать;
//  ПараметрыПечати - Структура - дополнительные настройки печати;
//  КоллекцияПечатныхФорм - ТаблицаЗначений - сформированные табличные документы (выходной параметр)
//  ОбъектыПечати         - СписокЗначений  - значение - ссылка на объект;
//                                            представление - имя области в которой был выведен объект (выходной параметр);
//  ПараметрыВывода       - Структура       - дополнительные параметры сформированных табличных документов (выходной параметр).
//
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
КонецПроцедуры

Функция ПолучитьДанныеДляПечатнойФормыОтчетПоКомиссииОСписании(ПараметрыПечати, МассивОбъектов) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка КАК Ссылка,
	|	ДанныеДокумента.Номер КАК Номер,
	|	ДанныеДокумента.Дата КАК Дата,
	|	ДанныеДокумента.Контрагент КАК Комитент,
	|	ДанныеДокумента.Организация КАК Комиссионер,
	|	ДанныеДокумента.Организация.Префикс КАК Префикс,
	|	ДанныеДокумента.Валюта КАК Валюта,
	|	"""" КАК ПредставительКомитента,
	|	ДанныеДокумента.Менеджер.ФизическоеЛицо КАК ПредставительКомиссионера
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка В(&МассивДокументов)
	|	И ДанныеДокумента.Проведен
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВложенныйЗапрос.Ссылка КАК Ссылка,
	|	ВложенныйЗапрос.Номенклатура КАК Номенклатура,
	|	ВложенныйЗапрос.Номенклатура.НаименованиеПолное КАК ТоварНаименование,
	|	ВложенныйЗапрос.Номенклатура.Код КАК Код,
	|	ВложенныйЗапрос.Номенклатура.Артикул КАК Артикул,
	|	ВложенныйЗапрос.ЕдиницаИзмерения.Представление КАК ЕдиницаИзмеренияНаименование,
	|	ВложенныйЗапрос.Характеристика.НаименованиеПолное КАК Характеристика,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(ВложенныйЗапрос.Упаковка.Коэффициент, 1) = 1
	|			ТОГДА НЕОПРЕДЕЛЕНО
	|		ИНАЧЕ ВложенныйЗапрос.Упаковка.Наименование
	|	КОНЕЦ КАК Упаковка,
	|	ВложенныйЗапрос.Цена КАК Цена,
	|	СУММА(ВложенныйЗапрос.Количество) КАК Количество,
	|	СУММА(ВложенныйЗапрос.Сумма) КАК Сумма,
	|	МАКСИМУМ(ВложенныйЗапрос.НомерСтроки) КАК НомерСтроки
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТаблицаТовары.Ссылка КАК Ссылка,
	|		ТаблицаТовары.Номенклатура КАК Номенклатура,
	|		ВЫБОР
	|			КОГДА ТаблицаТовары.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)
	|				ТОГДА ТаблицаТовары.Номенклатура.ЕдиницаИзмерения
	|			ИНАЧЕ ТаблицаТовары.Упаковка
	|		КОНЕЦ КАК ЕдиницаИзмерения,
	|		ТаблицаТовары.Характеристика КАК Характеристика,
	|		ТаблицаТовары.Упаковка КАК Упаковка,
	|		ТаблицаТовары.КоличествоУпаковок КАК Количество,
	|		ТаблицаТовары.Цена КАК Цена,
	|		ТаблицаТовары.СуммаСНДС КАК Сумма,
	|		ТаблицаТовары.НомерСтроки КАК НомерСтроки
	|	ИЗ
	|		Документ.ОтчетКомитентуОСписании.Товары КАК ТаблицаТовары
	|	ГДЕ
	|		ТаблицаТовары.Ссылка В(&МассивДокументов)
	|		И ТаблицаТовары.Номенклатура.ТипНоменклатуры В
	|			(ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар),ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.МногооборотнаяТара))) КАК ВложенныйЗапрос
	|
	|СГРУППИРОВАТЬ ПО
	|	ВложенныйЗапрос.Ссылка,
	|	ВложенныйЗапрос.Номенклатура,
	|	ВложенныйЗапрос.ЕдиницаИзмерения,
	|	ВложенныйЗапрос.Характеристика,
	|	ВложенныйЗапрос.Упаковка,
	|	ВложенныйЗапрос.Цена,
	|	ВложенныйЗапрос.Номенклатура.НаименованиеПолное,
	|	ВложенныйЗапрос.Номенклатура.Код,
	|	ВложенныйЗапрос.Номенклатура.Артикул,
	|	ВложенныйЗапрос.ЕдиницаИзмерения.Представление,
	|	ВложенныйЗапрос.Характеристика.НаименованиеПолное,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(ВложенныйЗапрос.Упаковка.Коэффициент, 1) = 1
	|			ТОГДА НЕОПРЕДЕЛЕНО
	|		ИНАЧЕ ВложенныйЗапрос.Упаковка.Наименование
	|	КОНЕЦ
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ
	|	СУММА(Сумма)
	|ПО
	|	Ссылка");
	
	Запрос.УстановитьПараметр("МассивДокументов", МассивОбъектов);
	Заголовок 					= НСтр("ru = 'Отчет комитенту о списании'");
	МассивРезультатов 			= Запрос.ВыполнитьПакет();
	РезультатПоШапке			= МассивРезультатов[0];
	РезультатПоТабличнойЧасти 	= МассивРезультатов[1];
	СтруктураДанныхДляПечати 	= Новый Структура(
		"РезультатПоШапке, РезультатПоТабличнойЧасти, Заголовок",
		РезультатПоШапке,
		РезультатПоТабличнойЧасти,
		Заголовок);
	
	Возврат СтруктураДанныхДляПечати;
	
КонецФункции

Процедура ЗаполнитьСтруктуруПолучателейПечатныхФорм(СтруктураДанныхОбъектаПечати) Экспорт
	
	СтруктураДанныхОбъектаПечати.ОсновнойПолучатель = "Партнер";
	
	СтруктураДанныхОбъектаПечати.МассивРеквизитовПолучателей.Добавить("Партнер");
	Если ПолучитьФункциональнуюОпцию("ИспользоватьПартнеровИКонтрагентов") Тогда 
		СтруктураДанныхОбъектаПечати.МассивРеквизитовПолучателей.Добавить("Контрагент");
	КонецЕсли;
	СтруктураДанныхОбъектаПечати.МассивРеквизитовПолучателей.Добавить("КонтактноеЛицо");
	
КонецПроцедуры

Функция ДоступныеДляШаблоновПечатныеФормы() Экспорт

	МассивДоступныхПечатныхФорм = Новый Массив;
	
	МассивДоступныхПечатныхФорм.Добавить(ШаблоныСообщенийСервер.СтруктураПараметровДоступнойПечатнойФормы(
	                                     "ОтчетКомиссионераСписание", НСтр("ru = 'Отчет комиссионера о списании'"),
	                                     "Обработка.ПечатьОбщихФорм", Неопределено));
	
	Возврат МассивДоступныхПечатныхФорм;

КонецФункции

#КонецОбласти

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УТ 11.1.3
//
// В табличных частях заполняется новый реквизит "Аналитика учета номенклатуры" 
//
Процедура ЗаполнитьАналитикуУчетаНоменклатурыВТабличныхЧастях(Параметры) Экспорт
	
	ТекстСобытия = НСтр("ru = 'Заполнение аналитик учета номенклатуры в документах'", ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка());
	ЗаписьЖурналаРегистрации(ТекстСобытия, УровеньЖурналаРегистрации.Информация, , ,
				НСтр("ru = 'Запуск задания заполнения аналитики учета номенклатуры в документах: Отчет комитенту о списании'"));
				
	Запрос = Новый Запрос;
	ТекстЗапроса = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	Операция.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.ОтчетКомитентуОСписании КАК Операция
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		Документ.ОтчетКомитентуОСписании.Товары КАК Строки
	|	ПО
	|		Операция.Ссылка = Строки.Ссылка
	|		И Строки.АналитикаУчетаНоменклатуры = ЗНАЧЕНИЕ(Справочник.КлючиАналитикиУчетаНоменклатуры.ПустаяСсылка)
	|ГДЕ
	|	Операция.Проведен
	|	И Операция.Дата МЕЖДУ &НачалоПериода И &КонецПериода
	|УПОРЯДОЧИТЬ ПО
	|	Операция.Дата УБЫВ
	|";
	
	Если НЕ Параметры.ОтложенныйЗапуск Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "ПЕРВЫЕ 1000", "");
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "УБЫВ", "");
		Запрос.УстановитьПараметр("НачалоПериода", Параметры.НачалоПериода);
		Запрос.УстановитьПараметр("КонецПериода", Параметры.КонецПериода);
	Иначе
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "И Операция.Дата МЕЖДУ &НачалоПериода И &КонецПериода", "");
	КонецЕсли;

	Запрос.Текст = ТекстЗапроса;
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		НачатьТранзакцию();
		Попытка
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("Документ.ОтчетКомитентуОСписании");
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			Блокировка.Заблокировать();

			ДокОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			МестаУчета = РегистрыСведений.АналитикаУчетаНоменклатуры.МестаУчета(Перечисления.ХозяйственныеОперации.ОтчетКомитенту, Неопределено, ДокОбъект.Подразделение, ДокОбъект.Партнер);
			ИменаПолей = РегистрыСведений.АналитикаУчетаНоменклатуры.ИменаПолейКоллекцииПоУмолчанию();
			ИменаПолей.СтатусУказанияСерий = "";
			РегистрыСведений.АналитикаУчетаНоменклатуры.ЗаполнитьВКоллекции(ДокОбъект.Товары, МестаУчета, ИменаПолей);
			
			ИменаПолей.Номенклатура		= "УдалитьНоменклатура";
			ИменаПолей.Характеристика	= "УдалитьХарактеристика";
			РегистрыСведений.АналитикаУчетаНоменклатуры.ЗаполнитьВКоллекции(ДокОбъект.ВидыЗапасов, МестаУчета, ИменаПолей);
		
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(ДокОбъект); 
			
			ЗафиксироватьТранзакцию();
		Исключение
			ОтменитьТранзакцию();
			ТекстСообщения = НСтр("ru = 'Не удалось обработать: %ОтчетКомитентуОСписании% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ОтчетКомитентуОСписании%", Выборка.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ЗаписьЖурналаРегистрации(ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(), УровеньЖурналаРегистрации.Предупреждение,
									Метаданные.Документы.ОтчетКомитентуОСписании, Выборка.Ссылка, ТекстСообщения);
		КонецПопытки;
	КонецЦикла;
	
	ЗаписьЖурналаРегистрации(ТекстСобытия, УровеньЖурналаРегистрации.Информация, , ,
				НСтр("ru = 'Задание заполнения аналитики учета номенклатуры в документах: Отчет комитенту о списании завершено'"));
	
	Параметры.ОбработкаЗавершена = (Выборка.Количество() = 0);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
