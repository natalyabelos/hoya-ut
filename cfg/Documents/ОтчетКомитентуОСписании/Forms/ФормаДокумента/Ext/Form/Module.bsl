﻿&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();

	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);

	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		
		ПриЧтенииСозданииНаСервере();
		Если Параметры.Свойство("Основание")
		 И ТипЗнч(Параметры.Основание) = Тип("Структура")
		 И Параметры.Основание.Свойство("ЗаполнятьПоТоварамКОформлению") Тогда
			ЗаполнятьПоТоварамКОформлению = Параметры.Основание.ЗаполнятьПоТоварамКОформлению;
		КонецЕсли;
		
	КонецЕсли;
	
	Если ТекущийВариантИнтерфейсаКлиентскогоПриложения() = ВариантИнтерфейсаКлиентскогоПриложения.Версия8_2 Тогда
		Элементы.ГруппаИтого.ЦветФона = Новый Цвет();
	КонецЕсли;
	
	Элементы.ГруппаСостояние.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьОбменЭД");
	
	// Подсистема "ЭлектронныеДокументы"
	УстановитьТекстСостоянияЭДНаСервере();
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом
	
	// Обработчик подсистемы "Свойства"
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтаФорма, Объект, "ГруппаДополнительныеРеквизиты");
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка)
	 И ЗаполнятьПоТоварамКОформлению Тогда
		КомиссионнаяТорговляКлиент.ПроверитьЗаполнениеДокументаПоОстаткам(Объект);	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)

	Если ИсточникВыбора.ИмяФормы = "ОбщаяФорма.ПодборПоТоварамКОформлениюОтчетаКомитента" Тогда
		
		ПолучитьТоварыИзХранилища(ВыбранноеЗначение);
		
	ИначеЕсли ИсточникВыбора.ИмяФормы = "Справочник.ВидыЗапасов.Форма.ФормаВводаВидовЗапасов" Тогда	
		
		ПолучитьВидыЗапасовИзХранилища(ВыбранноеЗначение);
		Объект.ВидыЗапасовУказаныВручную = ИсточникВыбора.ВидыЗапасовУказаныВручную;
		Модифицированность = Истина;	
		
	КонецЕсли;
	
	Если Окно <> Неопределено Тогда
		Окно.Активизировать();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// Подсистема "Свойства"
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтаФорма, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
	КонецЕсли;
	
	// Подсистема "ЭлектронныеДокументы"
	Если ИмяСобытия = "ОбновитьСостояниеЭД" Тогда
		УстановитьТекстСостоянияЭДНаСервере();
	КонецЕсли;
	
	Если ИмяСобытия = "Запись_СоглашенияСПоставщиками" Тогда
		
		УстановитьДоступностьСоглашений();
		
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтаФорма, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если ПараметрыЗаписи.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		ЗакупкиКлиент.ПроверитьСопоставленнуюНоменклатуруПоставщика(Объект, Отказ, НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	Элементы.СтраницаКомментарий.Картинка = ОбщегоНазначения.ПолучитьКартинкуКомментария(Объект.Комментарий);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются, ЗаполнитьПризнакАртикул",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются"),
			Новый Структура("Номенклатура", "Артикул")));
	
	// Подсистема "ЭлектронныеДокументы"
	УстановитьТекстСостоянияЭДНаСервере();

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ОтчетКомитентуОСписании", ПараметрыЗаписи, Объект.Ссылка);

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика = Настройки.Получить("НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика");
	
	Если НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика Тогда
		Элементы.ФормаНеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика.Пометка = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// Обработчик механизма "Свойства"
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтаФорма, ТекущийОбъект);
	
	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	
	Если ЦенообразованиеКлиент.ЗадатьВопросПересчетаЦеныПриИзмененииДаты(Объект) Тогда
		ЦеныРассчитаны = ЗаполнитьЦеныПоСоглашениюСервер();
		ЗакупкиКлиент.ОповеститьОбОкончанииЗаполненияЦенПоСоглашению(ЦеныРассчитаны);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.Организация) Тогда
		ОрганизацияПриИзмененииСервер();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	ПартнерПриИзмененииСервер();
		
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
		Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
			Если Объект.Товары.Итог("Цена")<>0 И ВопросПользователюПередЗаполнениемЦенПоСоглашению() Тогда
				ЗаполнитьЦеныПоСоглашениюСервер();
			КонецЕсли;
			ЗакупкиКлиент.ОповеститьОбОкончанииЗаполненияУсловийЗакупокПоУмолчанию();
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КонтактноеЛицоПриИзменении(Элемент)
	
	Если Объект.КонтактноеЛицо.Пустая() Тогда
		Возврат;
	ИначеЕсли Не Объект.Партнер.Пустая() Тогда
		Возврат;
	КонецЕсли;
	
	ПартнерИзменился = Ложь;
	КонтактноеЛицоПриИзмененииСервер(ПартнерИзменился);
	
	Если Не ПартнерИзменился Тогда
		Возврат;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
		Если Объект.Товары.Итог("Цена") <> 0 И ВопросПользователюПередЗаполнениемЦенПоСоглашению() Тогда
			ЗаполнитьЦеныПоСоглашениюСервер();
		КонецЕслИ;
		ЗакупкиКлиент.ОповеститьОбОкончанииЗаполненияУсловийЗакупокПоУмолчанию();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашениеПриИзменении(Элемент)
	
	СоглашениеПриИзмененииСервер(
		ЗначениеЗаполнено(Объект.Соглашение) И Объект.Товары.Итог("Цена")<>0 И ВопросПользователюПередЗаполнениемЦенПоСоглашению());
	
	Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
		ПродажиКлиент.ОповеститьОбОкончанииЗаполненияУсловийПродаж();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашениеНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтруктураДополнительногоОтбора = Новый Структура;
	СтруктураДополнительногоОтбора.Вставить("ХозяйственнаяОперация", ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПриемНаКомиссию"));

	ЗакупкиКлиент.НачалоВыбораСоглашенияСПоставщиком(
		Элемент,
		СтандартнаяОбработка,
		Объект.Партнер,
		Объект.Соглашение,
		Объект.Дата,
		,
		СтруктураДополнительногоОтбора);

КонецПроцедуры

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	КонтрагентПриИзмененииСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.Валюта) Тогда
		
		ВалютаПриИзмененииСервер(
			Объект.Валюта,
			ЦенообразованиеКлиент.НеобходимПересчетВВалюту(Объект, ВалютаДокумента));
		ЦенообразованиеКлиент.ОповеститьОбОкончанииПересчетаСуммВВалюту(ВалютаДокумента, Объект.Валюта);
		
	КонецЕсли;
	
	ВалютаДокумента = Объект.Валюта;
	
КонецПроцедуры

&НаКлиенте
Процедура НалогообложениеНДСПриИзменении(Элемент)
	
	НалогообложениеНДСПриИзмененииСервер(КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенаВключаетНДСПриИзменении(Элемент)
	
	ЦенаВключаетНДСПриИзмененииСервер(КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура СостояниеЭДНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ЭлектронныеДокументыКлиент.ОткрытьСписокЭД(Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура МенеджерПриИзменении(Элемент)
	ЗаполнитьПодчиненныеСвойстваПоСтатистике("Менеджер");
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары

&НаКлиенте
Процедура ТоварыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ЗакупкиКлиент.ПриВыбореНоменклатурыПоставщика(Объект, Поле, "ТоварыНоменклатураПоставщика", НСтр("ru = 'Комитент'"));
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПоставщикаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьНоменклатуруПоНоменклатуреПоставщика");
	СтруктураДействий.Вставить(
		"ПроверитьСопоставленнуюНоменклатуруПоставщика",
		ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПроверкиСопоставленнойНоменклатурыПоставщикаВСтрокеТЧ(
			Объект,
			НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика));
	
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	СтруктураДействий.Вставить("ЗаполнитьЦенуЗакупки", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияЦеныЗакупкиВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС", Объект.НалогообложениеНДС);
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСумму");
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцу", ТекущаяСтрока.Характеристика);
	СтруктураДействий.Вставить("ЗаполнитьПризнакАртикул", Новый Структура("Номенклатура", "Артикул"));
	СтруктураДействий.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу", ТекущаяСтрока.Упаковка);
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	СтруктураДействий.Вставить("ЗаполнитьЦенуЗакупки", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияЦеныЗакупкиВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС", Объект.НалогообложениеНДС);
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
	СтруктураДействий.Вставить("ЗаполнитьНоменклатуруПоставщикаПоНоменклатуре", Объект.Партнер);
	СтруктураДействий.Вставить(
		"ПроверитьСопоставленнуюНоменклатуруПоставщика",
		ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПроверкиСопоставленнойНоменклатурыПоставщикаВСтрокеТЧ(
			Объект,
			НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика));

	СтруктураДействий.Вставить("НоменклатураПриИзмененииПереопределяемый", Новый Структура("ИмяФормы, ИмяТабличнойЧасти",
		ЭтаФорма.ИмяФормы, "Товары"));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыХарактеристикаПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьЦенуЗакупки", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияЦеныЗакупкиВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
	СтруктураДействий.Вставить("ЗаполнитьНоменклатуруПоставщикаПоНоменклатуре", Объект.Партнер);
	СтруктураДействий.Вставить(
		"ПроверитьСопоставленнуюНоменклатуруПоставщика",
		ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПроверкиСопоставленнойНоменклатурыПоставщикаВСтрокеТЧ(
			Объект,
			НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика));

	СтруктураДействий.Вставить("ХарактеристикаПриИзмененииПереопределяемый", Новый Структура("ИмяФормы, ИмяТабличнойЧасти",
		ЭтаФорма.ИмяФормы, "Товары"));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыУпаковкаПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	
	Если ТекущаяСтрока.Количество > 0 Тогда
		СтруктураДействий.Вставить("ПересчитатьЦенуЗаУпаковку", ТекущаяСтрока.Количество);
	Иначе
		СтруктураДействий.Вставить("ЗаполнитьЦенуЗакупки", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияЦеныЗакупкиВСтрокеТЧ(Объект));
	КонецЕсли;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
	СтруктураДействий.Вставить("ЗаполнитьНоменклатуруПоставщикаПоНоменклатуре", Объект.Партнер);
	СтруктураДействий.Вставить(
		"ПроверитьСопоставленнуюНоменклатуруПоставщика",
		ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПроверкиСопоставленнойНоменклатурыПоставщикаВСтрокеТЧ(
			Объект,
			НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоУпаковокПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий, Объект);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСтавкаНДСПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	
	СтруктураДействий.Вставить("ПересчитатьЦенуСкидкуПоСуммеВПродажах");
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС",  ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриАктивизацииЯчейки(Элемент)
	Если Элемент.ТекущийЭлемент = Неопределено Тогда
		Возврат
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "ТоварыНоменклатураПоставщика" Тогда
		ЗакупкиКлиент.ЗаполнитьСписокВыбораНоменклатурыПоставщика(Объект.Партнер,
			Элементы.Товары.ТекущиеДанные,
			Элементы.ТоварыНоменклатураПоставщика.СписокВыбора);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура УстановитьИнтервалВыполнить(Команда)
	
	ОбщегоНазначенияУТКлиент.РедактироватьПериод(Объект, 
		Новый Структура("ДатаНачала, ДатаОкончания", "НачалоПериода", "КонецПериода"));

КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоРезультатамСписаний(Команда)
	
	СтруктураРеквизитов = Новый Структура;
	СтруктураРеквизитов.Вставить("Организация");
	СтруктураРеквизитов.Вставить("Партнер");
	СтруктураРеквизитов.Вставить("Валюта");
	
	Если ОбщегоНазначенияУТКлиент.ВозможноЗаполнениеТабличнойЧасти(ЭтаФорма, Объект.Товары, СтруктураРеквизитов) Тогда
	
		ЗаполнитьПоРезультатамСписанийСервер();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьПоРезультатамСписаний(Команда)
	
	СтруктураРеквизитов = Новый Структура;
	СтруктураРеквизитов.Вставить("Организация");
	СтруктураРеквизитов.Вставить("Партнер");
	СтруктураРеквизитов.Вставить("Валюта");
	
	Если ОбщегоНазначенияУТКлиент.ВозможноЗаполнениеТабличнойЧасти(ЭтаФорма, Неопределено, СтруктураРеквизитов) Тогда

		АдресТоварыВХранилище = ПоместитьТоварыВХранилище(
			Объект.Товары,
			УникальныйИдентификатор);
		ПараметрыПодбора = Новый Структура("
			|АдресТоварыВХранилище, 
			|Организация, 
			|Партнер,
			|Соглашение,
			|Валюта,
			|НалогообложениеНДС,
			|НачалоПериода,
			|КонецПериода,
			|ЭтоОтчетОПродажах
			|",
			АдресТоварыВХранилище,
			Объект.Организация, 
			Объект.Партнер,
			Объект.Соглашение,
			Объект.Валюта,
			Объект.НалогообложениеНДС,
			Объект.НачалоПериода,
			Объект.КонецПериода,
			Ложь);
		ОткрытьФорму(
			"ОбщаяФорма.ПодборПоТоварамКОформлениюОтчетаКомитента",
			ПараметрыПодбора, 
			ЭтаФорма);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьЦеныПоСоглашению(Команда)

	Если ЗакупкиКлиент.НеобходимоЗаполнениеЦенПоСоглашению(Объект, "Товары", НСтр("ru='Товары'")) Тогда
		
		ЦеныРассчитаны = ЗаполнитьЦеныПоСоглашениюСервер();
		ЗакупкиКлиент.ОповеститьОбОкончанииЗаполненияЦенПоСоглашению(ЦеныРассчитаны);
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПоказатьНоменклатуруПоставщика(Команда)
	
	ЗакупкиКлиент.ПоказатьНоменклатуруПоставщика(Объект.Партнер, Объект.Ссылка, Элементы.Товары.ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтроку(Команда)
	
	ТаблицаФормы  = Элементы.Товары;
	ДанныеТаблицы = Объект.Товары;
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	НоваяСтрока = ОбщегоНазначенияУТКлиент.РазбитьСтрокуТЧ(ДанныеТаблицы, ТаблицаФормы);
	
	Если НоваяСтрока <> Неопределено Тогда
		
		СтруктураДействий = Новый Структура;
		ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий, Объект);
		
		ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, КэшированныеЗначения);
		
		РассчитатьИтоговыеПоказателиОтчетаКомитентуОСписании(ЭтаФорма);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьВидыЗапасов(Команда)
	
	Перем АдресТоваровВХранилище;
	Перем АдресВидовЗапасовВХранилище;
	
	ПоместитьТоварыИВидыЗапасовВХранилище(
		АдресТоваровВХранилище,
		АдресВидовЗапасовВХранилище);
	ФинансыКлиент.ОткрытьВидыЗапасов(
		Объект,
		АдресТоваровВХранилище,
		АдресВидовЗапасовВХранилище,
		ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика(Команда)
	
	НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика = Не НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика;
	Элементы.ФормаНеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика.Пометка = НеВыполнятьПроверкуСопоставленнойНоменклатурыПоставщика;
	
КонецПроцедуры

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	НоменклатураСервер.УстановитьУсловноеОформлениеЕдиницИзмерения(ЭтаФорма);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыСтавкаНДС.Имя);

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыСумма.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.НалогообложениеНДС");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Перечисления.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС;

	Элемент.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);

	//

	НоменклатураСервер.УстановитьУсловноеОформлениеХарактеристикНоменклатуры(ЭтаФорма);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыНоменклатураПоставщика.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Партнер");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеЗаполнено;
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.НезаполненноеПолеТаблицы);
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<выберите поставщика>'"));
	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.Договор.Имя);

	ГруппаОтбора1 = Элемент.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбора1.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли;

	ОтборЭлемента = ГруппаОтбора1.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Соглашение");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеЗаполнено;

	ОтборЭлемента = ГруппаОтбора1.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Договор");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Заполнено;
	Элемент.Оформление.УстановитьЗначениеПараметра("ОтметкаНезаполненного", Ложь);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.Договор.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Соглашение");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Заполнено;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Договор");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеЗаполнено;
	Элемент.Оформление.УстановитьЗначениеПараметра("ОтметкаНезаполненного", Истина);

КонецПроцедуры

#Область ПриИзмененииРеквизитов

&НаКлиентеНаСервереБезКонтекста
Функция ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий, Объект)
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСумму");
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
КонецФункции

&НаСервере
Процедура ОрганизацияПриИзмененииСервер()
	
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Элементы.Договор.Видимость, Объект.Договор);
	
	Если ЗначениеЗаполнено(Объект.Организация) Тогда
		ЗаполнитьДоговорПоУмолчанию();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПартнерПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
			
		УсловияЗакупок = ЗакупкиСервер.ПолучитьУсловияЗакупокПоУмолчанию(
			Объект.Партнер,
			Новый Структура("ВыбранноеСоглашение", Объект.Соглашение));
		Если УсловияЗакупок <> Неопределено Тогда
			Если Объект.Соглашение <> УсловияЗакупок.Соглашение
				И ЗначениеЗаполнено(УсловияЗакупок.Соглашение) Тогда

				Объект.Соглашение = УсловияЗакупок.Соглашение;
				СоглашениеПриИзмененииСервер(Ложь);
					
			Иначе
				Объект.Соглашение = УсловияЗакупок.Соглашение;
			КонецЕсли;
		Иначе
			Объект.Соглашение = Неопределено;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Объект.Контрагент) Тогда
			Объект.Контрагент = ПартнерыИКонтрагенты.ПолучитьКонтрагентаПартнераПоУмолчанию(Объект.Партнер);
		КонецЕсли;
		
		ЗакупкиСервер.ЗаполнитьНоменклатуруПоставщикаВТаблице(Объект.Товары, Объект.Партнер);
		ПартнерыИКонтрагенты.ЗаполнитьКонтактноеЛицоПартнераПоУмолчанию(Объект.Партнер, Объект.КонтактноеЛицо);
		
	КонецЕсли;
	
	ЗаполнитьПодчиненныеСвойстваПоСтатистике("Контрагент");
	
	УстановитьДоступностьСоглашений();
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Элементы.Договор.Видимость, Объект.Договор);
	
КонецПроцедуры

&НаСервере
Процедура КонтактноеЛицоПриИзмененииСервер(ПартнерИзменился)
	
	ВладелецКонтактногоЛица = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.КонтактноеЛицо, "Владелец");
	Если ВладелецКонтактногоЛица <> Объект.Партнер Тогда
		Объект.Партнер = ВладелецКонтактногоЛица;
		ПартнерПриИзмененииСервер();
		ПартнерИзменился = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СоглашениеПриИзмененииСервер(ПересчитатьЦены = Истина)
	
	Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
		
		УсловияЗакупок = ЗакупкиСервер.ПолучитьУсловияЗакупок(Объект.Соглашение);
		ЗаполнитьЗначенияСвойств(Объект, УсловияЗакупок);
		
		ЗаполнитьДоговорПоУмолчанию();
		
		ВалютаДокумента = Объект.Валюта;
		
		Если ПересчитатьЦены Тогда
			СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
			ЗакупкиСервер.ЗаполнитьЦены(
				Объект.Товары,
				, // Массив строк
				Новый Структура( // Параметры заполнения
					"ПоляЗаполнения, Дата, Валюта, Соглашение",
					"Цена, СтавкаНДС",
					Объект.Дата,
					Объект.Валюта,
					Объект.Соглашение
				),
				Новый Структура( // Структура действий с измененныими строками
					"ПересчитатьСумму, ПересчитатьСуммуСНДС, ПересчитатьСуммуНДС",
					"КоличествоУпаковок", СтруктураПересчетаСуммы, СтруктураПересчетаСуммы));
		КонецЕсли;
		
		КомиссионнаяТорговляСервер.ЗаполнитьСуммуСНДС(
			Объект.Товары,
			Объект.ЦенаВключаетНДС);
		УправлениеЭлементамиФормы();
		РассчитатьИтоговыеПоказателиОтчетаКомитентуОСписании(ЭтаФорма);
		
	КонецЕсли;
		
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Элементы.Договор.Видимость, Объект.Договор);
	
КонецПроцедуры

&НаСервере
Процедура КонтрагентПриИзмененииСервер()
	
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Элементы.Договор.Видимость, Объект.Договор);
	
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		ЗаполнитьДоговорПоУмолчанию();
	КонецЕсли;
	
	ЗаполнитьПодчиненныеСвойстваПоСтатистике("Контрагент");
	
КонецПроцедуры

&НаСервере
Процедура ВалютаПриИзмененииСервер(НоваяВалюта, ПересчитатьСуммы)
	
	ЗаполнитьДоговорПоУмолчанию();
	
	Если ПересчитатьСуммы Тогда
		ПродажиСервер.ПересчитатьСуммуДокументаВВалюту(
			Объект, 
			ВалютаДокумента, 
			Объект.Валюта,
			Ложь); // ЕстьСуммаПродажи
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НалогообложениеНДСПриИзмененииСервер(КэшированныеЗначения)
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС",  Объект.НалогообложениеНДС);
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
	
	ОбработкаТабличнойЧастиСервер.ОбработатьТЧ(Объект.Товары, СтруктураДействий, КэшированныеЗначения);
	РассчитатьИтоговыеПоказателиОтчетаКомитентуОСписании(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ЦенаВключаетНДСПриИзмененииСервер(КэшированныеЗначения)
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВТЧ(Объект));
	СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", Новый Структура("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС));
	
	ОбработкаТабличнойЧастиСервер.ОбработатьТЧ(Объект.Товары, СтруктураДействий, КэшированныеЗначения);
	
	Элементы.ТоварыСумма.Видимость = Не Объект.ЦенаВключаетНДС;
	
КонецПроцедуры

#КонецОбласти

#Область ПодборыИОбработкаПроверкиКоличества

// Функция используется в автотесте процесса продаж.
//
&НаСервере
Функция ПоместитьТоварыВХранилище(Знач Товары, УникальныйИдентификатор)

	АдресПлатежейВХранилище = ПоместитьВоВременноеХранилище(
		Товары.Выгрузить(,"Номенклатура, Характеристика, Количество"),
		УникальныйИдентификатор);
		
	Возврат АдресПлатежейВХранилище;
	
КонецФункции

&НаСервере
Процедура ПоместитьТоварыИВидыЗапасовВХранилище(АдресТоваровВХранилище, АдресВидовЗапасовВХранилище)
	
	ЗапасыСервер.ПоместитьТоварыИВидыЗапасовВХранилище(
		Объект.Товары,
		Объект.ВидыЗапасов,
		УникальныйИдентификатор,
		АдресТоваровВХранилище,
		АдресВидовЗапасовВХранилище);
		
КонецПроцедуры

&НаСервере
Процедура ПолучитьВидыЗапасовИзХранилища(АдресВидовЗапасовВХранилище)
	
	Объект.ВидыЗапасов.Загрузить(ПолучитьИзВременногоХранилища(АдресВидовЗапасовВХранилище));
	
КонецПроцедуры

&НаСервере
Процедура ПолучитьТоварыИзХранилища(АдресПлатежейВХранилище)

	Объект.Товары.Загрузить(ПолучитьИзВременногоХранилища(АдресПлатежейВХранилище));
	
	Если ЗначениеЗаполнено(Объект.Соглашение) Тогда
		
		СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
		ЗакупкиСервер.ЗаполнитьЦены(
			Объект.Товары,
			, // Массив строк
			Новый Структура( // Параметры заполнения
				"ПоляЗаполнения, Дата, Валюта, Соглашение",
				"Цена, СтавкаНДС",
				Объект.Дата,
				Объект.Валюта,
				Объект.Соглашение
			),
			Новый Структура( // Структура действий с измененныими строками
				"ПересчитатьСумму, ПересчитатьСуммуСНДС, ПересчитатьСуммуНДС",
				"КоличествоУпаковок", СтруктураПересчетаСуммы, СтруктураПересчетаСуммы));
		
	КонецЕсли;
	КомиссионнаяТорговляСервер.ЗаполнитьСуммуСНДС(
		Объект.Товары,
		Объект.ЦенаВключаетНДС);
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются, ЗаполнитьПризнакАртикул",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются"),
			Новый Структура("Номенклатура", "Артикул")));
	
КонецПроцедуры

#КонецОбласти

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УправлениеЭлементамиФормы()
	
	Элементы.ТоварыСумма.Видимость = Не Объект.ЦенаВключаетНДС;
	
КонецПроцедуры

#КонецОбласти

#Область ПодсистемаЭлектронныедокументы

&НаСервере
Процедура УстановитьТекстСостоянияЭДНаСервере()
	
	ТекстСостоянияЭД = ЭлектронныеДокументыКлиентСервер.ПолучитьТекстСостоянияЭД(Объект.Ссылка, ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	УстановитьДоступностьСоглашений();
	ВалютаДокумента = Объект.Валюта;
	УправлениеЭлементамиФормы();
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются, ЗаполнитьПризнакАртикул",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются"),
			Новый Структура("Номенклатура", "Артикул")));
	РассчитатьИтоговыеПоказателиОтчетаКомитентуОСписании(ЭтаФорма);
	Элементы.СтраницаКомментарий.Картинка = ОбщегоНазначения.ПолучитьКартинкуКомментария(Объект.Комментарий);
	
	ЗакупкиСервер.УстановитьДоступностьДоговора(Объект, Элементы.Договор.Доступность, Элементы.Договор.Видимость);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДоговорПоУмолчанию()
	
	Объект.Договор = ЗакупкиСервер.ПолучитьДоговорПоУмолчанию(
		Объект,
		Перечисления.ХозяйственныеОперации.ПриемНаКомиссию,
		Объект.Валюта);
	
КонецПроцедуры

&НаСервере
Функция ЗаполнитьЦеныПоСоглашениюСервер()
	
	СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект);
	ЦеныРассчитаны = ЗакупкиСервер.ЗаполнитьЦены(
		Объект.Товары,
		, // Массив строк
		Новый Структура( // Параметры заполнения
			"ПоляЗаполнения, Дата, Валюта, Соглашение",
			"Цена, СтавкаНДС",
			Объект.Дата,
			Объект.Валюта,
			Объект.Соглашение
		),
		Новый Структура( // Структура действий с измененныими строками
			"ПересчитатьСумму, ПересчитатьСуммуСНДС, ПересчитатьСуммуНДС",
			"КоличествоУпаковок", СтруктураПересчетаСуммы, СтруктураПересчетаСуммы));
	
	КомиссионнаяТорговляСервер.ЗаполнитьСуммуСНДС(
		Объект.Товары,
		Объект.ЦенаВключаетНДС);
	
	Возврат ЦеныРассчитаны;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьПоРезультатамСписанийСервер()
	
	КомиссионнаяТорговляСервер.ЗаполнитьТоварыПоОстаткамКОформлениюОтчетовКомитентуОСписании(Объект);
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Товары,
		Новый Структура("ЗаполнитьПризнакХарактеристикиИспользуются, ЗаполнитьПризнакАртикул",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются"),
			Новый Структура("Номенклатура", "Артикул")));
	
КонецПроцедуры

&НаКлиенте
Функция ВопросПользователюПередЗаполнениемЦенПоСоглашению()
	
	ВариантыОтветов = Новый СписокЗначений;
	ВариантыОтветов.Добавить(Истина, НСтр("ru='Перезаполнить'"));
	ВариантыОтветов.Добавить(Ложь, НСтр("ru='Не перезаполнять'"));
	
	Возврат Вопрос(НСтр("ru='Перезаполнить цены по соглашению?'"), ВариантыОтветов);
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьИтоговыеПоказателиОтчетаКомитентуОСписании(Форма)
	
	Если Форма.Объект.НалогообложениеНДС = ПредопределенноеЗначение("Перечисление.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС") Тогда
		Форма.Элементы.ГруппаСтраницыНДС.ТекущаяСтраница   = Форма.Элементы.СтраницаБезНДС;
		Форма.Элементы.ГруппаСтраницыВсего.ТекущаяСтраница = Форма.Элементы.СтраницаВсегоБезНДС;
	Иначе
		Форма.Элементы.ГруппаСтраницыНДС.ТекущаяСтраница   = Форма.Элементы.СтраницаСНДС;
		Форма.Элементы.ГруппаСтраницыВсего.ТекущаяСтраница = Форма.Элементы.СтраницаВсегоСНДС;
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьСоглашений()
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
		КоличествоСоглашенийСПоставшиком  = ЗакупкиВызовСервера.ПолучитьКоличествоСоглашенийСПоставщиком(Объект.Партнер);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Соглашение", "Видимость", КоличествоСоглашенийСПоставшиком > 0);
	КонецЕсли; 
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств(Команда)
	
	// Подсистема "Свойства"
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьПодчиненныеСвойстваПоСтатистике(ИмяРеквизитаРодителя)
	ЗаполнениеСвойствПоСтатистикеСервер.ЗаполнитьПодчиненныеСвойства(Объект, ИмяРеквизитаРодителя);
КонецПроцедуры

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	
	// Подсистема "Свойства"
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти
