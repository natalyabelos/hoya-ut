﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Если Параметры.Свойство("ПериодРегистрации") Тогда
		Объект.Дата = Параметры.ПериодРегистрации;
	КонецЕсли;
	
	// Контроль создания документа в подчиенном узле РИБ с фильтрами
	ОбменДаннымиУТУП.КонтрольСозданияДокументовВРаспределеннойИБ(Объект, Отказ);

	// Обработчик подсистемы "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	
	// Обработчик подсистемы "Дополнительные отчеты и обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОбновитьНадписьПериод();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)

	Оповестить("Запись_РаспределениеНДС");
	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	
	ОбновитьНадписьПериод();
	ПериодПриИзмененииСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ОткрытьФормуРедактированияКомментария(Элемент.ТекстРедактирования, Объект.Комментарий, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыручкаНеНДСПриИзменении(Элемент)
	
	УправлениеЭлементамиФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыручкаЕНВДПриИзменении(Элемент)
	
	УправлениеЭлементамиФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура СписатьНДСКакЦенностиПриИзменении(Элемент)
	
	УправлениеЭлементамиФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОрганизацияПриИзмененииСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовНеНДСПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовНеНДС) Тогда
		СтатьяРасходовНеНДСПриИзмененииНаСервере();
	Иначе
		АналитикаРасходовНеНДСОбязательна = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СтатьяРасходовНеНДСПриИзмененииНаСервере()
	
	АналитикаРасходовНеНДСОбязательна = 
		ЗначениеЗаполнено(Объект.СтатьяРасходовНеНДС)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.СтатьяРасходовНеНДС, "КонтролироватьЗаполнениеАналитики");
		
	АналитикаРасходовЗаказРеализацияНеНДС = 
		ЗначениеЗаполнено(Объект.СтатьяРасходовНеНДС)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.СтатьяРасходовНеНДС, "АналитикаРасходовЗаказРеализация");
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовЕНВДПриИзменении(Элемент)
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВД) Тогда
		СтатьяРасходовЕНВДПриИзмененииНаСервере();
	Иначе
		АналитикаРасходовЕНВДОбязательна = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СтатьяРасходовЕНВДПриИзмененииНаСервере()
	
	АналитикаРасходовЕНВДОбязательна = 
		ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВД)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.СтатьяРасходовЕНВД, "КонтролироватьЗаполнениеАналитики");
		
	АналитикаРасходовЗаказРеализацияЕНВД = 
		ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВД)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.СтатьяРасходовЕНВД, "АналитикаРасходовЗаказРеализация");
	
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовНеНДСНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЗаказРеализацияНеНДС Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовНеНДСОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовНеНДС = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовНеНДСАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовНеНДСОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЗаказРеализацияЕНВД Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовЕНВД = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВариантОпределенияБазыРаспределенияНДСПриИзменении(Элемент)
	
	ВариантОпределенияБазыРаспределенияНДСПриИзмененииСервер();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Заполнить(Команда)
	
	ЗаполнитьБазуРаспределенияПоВыручкеСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура Обновить(Команда)
	
	ОбновитьДанныеПоВыручкеСервер();
	
КонецПроцедуры

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст)
	
	ДанныеВыбора = Новый СписокЗначений;
	ПродажиСервер.ЗаполнитьДанныеВыбораАналитикиРасходов(ДанныеВыбора, Текст);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	//
	
	РеквизитыПроверкиАналитик = Новый Массив;
	РеквизитыПроверкиАналитик.Добавить("СтатьяРасходовНеНДС, АналитикаРасходовНеНДС, СтатьяРасходовЕНВД, АналитикаРасходовЕНВД");
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(УсловноеОформление, РеквизитыПроверкиАналитик);
	
КонецПроцедуры

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

#Область Прочее

#КонецОбласти

#Область ИнициализацияИЗаполнение

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ПараметрыУчетнойПолитики = РегистрыСведений.УчетнаяПолитикаОрганизаций.ПараметрыУчетнойПолитики(Объект.Организация, Объект.Дата);
	Если ПараметрыУчетнойПолитики <> Неопределено Тогда
		ПрименяетсяЕНВД = ПараметрыУчетнойПолитики.ПрименяетсяЕНВД;
		УчитыватьПорог5Процентов = ПараметрыУчетнойПолитики.Учитывать5ПроцентныйПорог;
	КонецЕсли;
	
	ВалютаРегламентированногоУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	ИспользуетсяУчетПрочихДоходовРасходов = ПолучитьФункциональнуюОпцию("ИспользоватьУчетПрочихДоходовРасходов");
	
	ВариантПримененияПравила5Процентов = ?(Объект.ПрименитьПравило5Процентов, 1, 0);
	ВариантОпределенияБазыРаспределенияНДС = ?(Объект.БазаРаспределенияУстанавливаетсяВручную, 1, 0);
	
	УправлениеЭлементамиФормы();
	СформироватьПояснениеПримененияПорога5Процентов();
	
	Если НЕ Объект.БазаРаспределенияУстанавливаетсяВручную Тогда
		ОбновитьДанныеПоВыручкеСервер();
	КонецЕсли;
	
	СтатьяРасходовНеНДСПриИзмененииНаСервере();
	СтатьяРасходовЕНВДПриИзмененииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭлементамиФормы()
	
	ЭтоКонецКвартала = (Месяц(Объект.Дата)%3 = 0);
	
	Элементы.ГруппаПрименениеПравила5Процентов.Видимость = УчитыватьПорог5Процентов И ЭтоКонецКвартала;
	Элементы.ПараметрыРаспределениеНДС.Доступность = (НЕ ЭтоКонецКвартала) ИЛИ (НЕ Объект.ПрименитьПравило5Процентов);
	
	Если НЕ Объект.ПрименитьПравило5Процентов Тогда
		
		Элементы.ГруппаБазаРаспределения.ТекущаяСтраница = 
			?(Объект.БазаРаспределенияУстанавливаетсяВручную,
				Элементы.ГруппаБазаУказанаВручную,
				Элементы.ГруппаВыручка);
		Элементы.БазаВыручкаЕНВД.ТолькоПросмотр = НЕ ПрименяетсяЕНВД;
		
		Элементы.ГруппаСписаниеРасходов.Доступность = НЕ Объект.СписатьНДСКакЦенности;
		
		ЗначениеВыручкаНеНДС = ?(ВариантОпределенияБазыРаспределенияНДС = 0, ВыручкаНеНДС, Объект.ВыручкаНеНДС);
		ЗначениеВыручкаЕНВД = ?(ВариантОпределенияБазыРаспределенияНДС = 0, ВыручкаЕНВД, Объект.ВыручкаЕНВД);
		
		Элементы.СтатьяЗатратНеНДС.АвтоОтметкаНезаполненного = (ЗначениеВыручкаНеНДС <> 0);
		Если ЗначениеВыручкаНеНДС = 0 И Элементы.СтатьяЗатратНеНДС.ОтметкаНезаполненного Тогда
			 Элементы.СтатьяЗатратНеНДС.ОтметкаНезаполненного = Ложь;
		КонецЕсли;
		
		Элементы.СтатьяЗатратЕНВД.АвтоОтметкаНезаполненного  = (ЗначениеВыручкаЕНВД <> 0);
		Если ЗначениеВыручкаЕНВД = 0 И Элементы.СтатьяЗатратЕНВД.ОтметкаНезаполненного Тогда
			 Элементы.СтатьяЗатратЕНВД.ОтметкаНезаполненного = Ложь;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииСервер()
	
	ПравилаСписания = Документы.РаспределениеНДС.ЗаполнитьПравилаСписанияНДС(Объект.Организация);
	ЗаполнитьЗначенияСвойств(Объект,ПравилаСписания);
	
	ПараметрыУчетнойПолитики = РегистрыСведений.УчетнаяПолитикаОрганизаций.ПараметрыУчетнойПолитики(Объект.Организация, Объект.Дата);
	Если ПараметрыУчетнойПолитики <> Неопределено Тогда
		ПрименяетсяЕНВД = ПараметрыУчетнойПолитики.ПрименяетсяЕНВД;
		УчитыватьПорог5Процентов = ПараметрыУчетнойПолитики.Учитывать5ПроцентныйПорог;
	КонецЕсли;
	
	УправлениеЭлементамиФормы();
	СформироватьПояснениеПримененияПорога5Процентов();
	
КонецПроцедуры

&НаСервере
Процедура ПериодПриИзмененииСервер()
	
	ПараметрыУчетнойПолитики = РегистрыСведений.УчетнаяПолитикаОрганизаций.ПараметрыУчетнойПолитики(Объект.Организация, Объект.Дата);
	Если ПараметрыУчетнойПолитики <> Неопределено Тогда
		ПрименяетсяЕНВД = ПараметрыУчетнойПолитики.ПрименяетсяЕНВД;
		УчитыватьПорог5Процентов = ПараметрыУчетнойПолитики.Учитывать5ПроцентныйПорог;
	КонецЕсли;
	
	УправлениеЭлементамиФормы();
	СформироватьПояснениеПримененияПорога5Процентов();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьНадписьПериод()
	
	ПредставлениеПериода = ПредставлениеПериода(НачалоМесяца(Объект.Дата), КонецМесяца(Объект.Дата), "ДЛФ=D");
	Если Месяц(Объект.Дата)%3=0 Тогда
		ПредставлениеПериода = ПредставлениеПериода(НачалоКвартала(Объект.Дата), КонецКвартала(Объект.Дата), "ДЛФ=D");
	КонецЕсли;
	
	НадписьПериод = ПредставлениеПериода;
	
КонецПроцедуры

#КонецОбласти

#Область Автозаполнение

&НаСервере
Процедура ЗаполнитьБазуРаспределенияПоВыручкеСервер()
	
	Если НЕ ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	ТаблицыБазы = Документы.РаспределениеНДС.ПолучитьБазуРаспределения(Объект.Дата,Объект.Организация);
	
	БазаРаспределения = Новый Структура;
	БазаРаспределения.Вставить("ВыручкаНДС",   ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаНДС"));
	БазаРаспределения.Вставить("ВыручкаНеНДС", ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаНеНДС"));
	БазаРаспределения.Вставить("ВыручкаЕНВД",  ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаЕНВД"));
	БазаРаспределения.Вставить("ВыручкаНДС0",  ТаблицыБазы.ТаблицаДокументыЭкспорт.Итог("Сумма"));
	
	ЗаполнитьЗначенияСвойств(Объект, БазаРаспределения);
	Объект.ДокументыЭкспорт.Загрузить(ТаблицыБазы.ТаблицаДокументыЭкспорт);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеПоВыручкеСервер()
	
	ТаблицыБазы = Документы.РаспределениеНДС.ПолучитьБазуРаспределения(Объект.Дата,Объект.Организация);
	
	БазаРаспределения = Новый Структура;
	БазаРаспределения.Вставить("ВыручкаНДС",   ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаНДС"));
	БазаРаспределения.Вставить("ВыручкаНеНДС", ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаНеНДС"));
	БазаРаспределения.Вставить("ВыручкаЕНВД",  ТаблицыБазы.ТаблицаВыручка.Итог("ВыручкаЕНВД"));
	БазаРаспределения.Вставить("ВыручкаНДС0",  ТаблицыБазы.ТаблицаДокументыЭкспорт.Итог("Сумма"));
	
	ЗаполнитьЗначенияСвойств(ЭтаФорма, БазаРаспределения);
	
КонецПроцедуры

&НаКлиенте
Процедура ВариантПримененияПравила5ПроцентовПриИзменении(Элемент)
	
	ВариантПримененияПравила5ПроцентовПриИзмененииСервер();
	
КонецПроцедуры

&НаСервере
Процедура СформироватьПояснениеПримененияПорога5Процентов()
	
	Если НЕ УчитыватьПорог5Процентов Тогда
		 Элементы.ГруппаПояснениеПримененияПравила5Процентов.Видимость = Ложь;
		 Возврат;
	КонецЕсли;
	
	Элементы.ГруппаПояснениеПримененияПравила5Процентов.Видимость = Истина;
	
	Состояние = ЗакрытиеМесяцаУТ.СостояниеРасчетаСебестоимости(Объект.Организация, Объект.Дата);
	Если Состояние <> Перечисления.СостоянияОперацийЗакрытияМесяца.ВыполненоУспешно Тогда
		КартинкаПояснение = БиблиотекаКартинок.Информация;
		ТекстПояснения = НСтр("ru = 'Расчет себестоимости не выполнен, решение следует принять на основании предварительной оценки.'");
	Иначе
		Оценка = Документы.РаспределениеНДС.ОценкаПримененияПравила5Процентов(Объект.Организация, Объект.Дата);
		Если Оценка.РасходыПоДеятельностиНеОблагаемойНДС = 0 Тогда
			Если НЕ Объект.ПрименитьПравило5Процентов Тогда
				КартинкаПояснение = БиблиотекаКартинок.Внимание16;
			Иначе
				КартинкаПояснение = БиблиотекаКартинок.Информация;
			КонецЕсли;
			ТекстПояснения = НСтр("ru = 'Отсутствуют расходы по реализации не облагаемой НДС. Весь НДС может быть принят к вычету.'");
		Иначе
			Если НЕ Объект.ПрименитьПравило5Процентов И Оценка.Доля <= 5 Тогда
				КартинкаПояснение = БиблиотекаКартинок.Внимание16;
				ШаблонПояснения = НСтр("ru = 'Расходы по реализациии не облагаемой НДС (%1 %2) составляют %4% от общих расходов (%3 %2). 
										|Весь НДС может быть принят к вычету.'");
			ИначеЕсли Объект.ПрименитьПравило5Процентов И Оценка.Доля > 5 Тогда
				КартинкаПояснение = БиблиотекаКартинок.ВниманиеКрасный;
				ШаблонПояснения = НСтр("ru = 'Расходы по реализациии не облагаемой НДС (%1 %2) составляют %4% от общих расходов (%3 %2). 
										|НДС должен быть распределен между видами деятельности.'");
			Иначе
				КартинкаПояснение = БиблиотекаКартинок.Информация;
				ШаблонПояснения = НСтр("ru = 'Расходы по реализациии не облагаемой НДС (%1 %2) составляют %4% от общих расходов (%3 %2).'");
			КонецЕсли;
			ТекстПояснения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонПояснения,  
								Оценка.РасходыПоДеятельностиНеОблагаемойНДС,
								ВалютаРегламентированногоУчета,
								Оценка.РасходыВсего,
								Формат(Оценка.Доля, "ЧДЦ=2"));
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ВариантПримененияПравила5ПроцентовПриИзмененииСервер() Экспорт
	
	Объект.ПрименитьПравило5Процентов = ?(ВариантПримененияПравила5Процентов = 1, Истина, Ложь);
	УправлениеЭлементамиФормы();
	СформироватьПояснениеПримененияПорога5Процентов();
	
КонецПроцедуры

&НаСервере
Процедура ВариантОпределенияБазыРаспределенияНДСПриИзмененииСервер()
	
	Объект.БазаРаспределенияУстанавливаетсяВручную = 
		?(ВариантОпределенияБазыРаспределенияНДС = 1, Истина, Ложь);
		
	Если Объект.БазаРаспределенияУстанавливаетсяВручную Тогда
		ЗаполнитьБазуРаспределенияПоВыручкеСервер();
	Иначе
		ОбновитьДанныеПоВыручкеСервер();
	КонецЕсли;
	
	УправлениеЭлементамиФормы();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
