﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
	ОтборыСписковКлиентСервер.СкопироватьСписокВыбораОтбораПоМенеджеру(
		Элементы.Менеджер.СписокВыбора,
		ОбщегоНазначенияУТ.ПолучитьСписокПользователейСПравомДобавления(Метаданные.Документы.СчетНаОплатуКлиенту));
		
	Элементы.Аннулировать.Видимость = ПравоДоступа("Изменение", Метаданные.Документы.СчетНаОплатуКлиенту);
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаГлобальныеКоманды);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтаФорма, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	// Подсистема "ЭлектронныеДокументы"
	Если ИмяСобытия = "ОбновитьСостояниеЭД" Тогда
		Элементы.Список.Обновить();
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Состояние = Настройки.Получить("Состояние");
	Менеджер  = Настройки.Получить("Менеджер");
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура МенеджерПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Аннулировать(Команда)
	
	Если Не РаботаСДиалогамиКлиент.ПроверитьНаличиеВыделенныхВСпискеСтрок(Элементы.Список) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстВопроса = НСтр("ru='Выделенные в списке счетов на оплату будут аннулированы. Продолжить?'");
	Ответ = Неопределено;

	ПоказатьВопрос(Новый ОписаниеОповещения("АннулироватьЗавершение", ЭтотОбъект), ТекстВопроса,РежимДиалогаВопрос.ДаНет);
	
КонецПроцедуры

&НаКлиенте
Процедура АннулироватьЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
    
    Ответ = РезультатВопроса;
    
    Если Ответ = КодВозвратаДиалога.Нет Тогда
        Возврат;
    КонецЕсли;
    
    КоличествоОбработанных = УстановитьПризнакАннулироваСервер(Элементы.Список.ВыделенныеСтроки);
    
    Если КоличествоОбработанных > 0 Тогда
        
        Элементы.Список.Обновить();
        
        ТекстСообщения = НСтр("ru='%КоличествоОбработанных% из %КоличествоВсего% выделенных в списке счетов на оплату аннулированы.'");
        ТекстСообщения = СтрЗаменить(ТекстСообщения,"%КоличествоОбработанных%", КоличествоОбработанных);
        ТекстСообщения = СтрЗаменить(ТекстСообщения,"%КоличествоВсего%",        Элементы.Список.ВыделенныеСтроки.Количество());
        ПоказатьОповещениеПользователя(НСтр("ru='Счета на оплату аннулированы'"),, ТекстСообщения, БиблиотекаКартинок.Информация32);
        
    Иначе
        
        ТекстСообщения = НСтр("ru='Не аннулирован ни один счет на оплату.'");
        ПоказатьОповещениеПользователя(НСтр("ru='Счета на оплату не аннулированы'"),, ТекстСообщения, БиблиотекаКартинок.Информация32);
        
    КонецЕсли;

КонецПроцедуры

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Состояние");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = НСтр("ru = 'Аннулирован'");

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЗакрытыйДокумент);

КонецПроцедуры

#Область ПриИзмененииРеквизитов

&НаКлиенте
Процедура ОтборСостояниеПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	
КонецПроцедуры

#КонецОбласти

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Функция СсылкаНаЭлементСпискаПоШтрихкоду(Штрихкод)
	
	Менеджеры = Новый Массив();
	Менеджеры.Добавить(ПредопределенноеЗначение("Документ.СчетНаОплатуКлиенту.ПустаяСсылка"));
	Возврат ШтрихкодированиеПечатныхФормКлиент.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(Штрихкод, Менеджеры);
	
КонецФункции

&НаКлиенте
Процедура ОбработатьШтрихкоды(Данные)
	
	МассивСсылок = СсылкаНаЭлементСпискаПоШтрихкоду(Данные.Штрихкод);
	Если МассивСсылок.Количество() > 0 Тогда
		Элементы.Список.ТекущаяСтрока = МассивСсылок[0];
		ПоказатьЗначение(Неопределено, МассивСсылок[0]);
	Иначе
		ШтрихкодированиеПечатныхФормКлиент.ОбъектНеНайден(Данные.Штрихкод);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервереБезКонтекста
Функция УстановитьПризнакАннулироваСервер (Знач СчетаНаОплату)
	
	Возврат Документы.СчетНаОплатуКлиенту.УстановитьПризнакАннулирован(СчетаНаОплату);
	
КонецФункции

// СтандартныеПодсистемы.Печать
// /+\ fs_AALeshin 19.02.2016
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	  	///\ +FirstBit_Sportivnaya | Aleshin <13.01.2016>
	Если Команда.Имя = "ПодменюПечатьКомандаПечати3" ИЛИ Команда.Имя = "ПодменюПечатьКомандаПечати4"   Тогда
		//Если  Объект.СозданНаОснванииНескольких = Истина  Тогда
		//	
		//Если Не Элементы.Список.ТекущаяСтрока.Проведен Тогда
		//	Предупреждение("Для печати документа необходимо провести документ!",,"Необходимо провести документ!");
		//	Возврат;
		//КонецЕсли;
		ТабДок = ПечатьСчетаНаОплатуПонесколькимРеализациям();
		
		//Типовая часть вывода в стандартное окно++
		ИдентификаторПечатнойФормы = "ПФ_MXL_СчетНаОплату";
		НазваниеПечатнойФормы = НСтр("ru = 'Счет на оплату клиенту по нескольким реализациям'");
		
		Если Не ОбщегоНазначенияКлиент.ПодсистемаСуществует("СтандартныеПодсистемы.Печать") Тогда
			ТабДок.Показать(НазваниеПечатнойФормы);
			ДокументыПечатались = Истина;
			Возврат;
		КонецЕсли;     
		МодульУправлениеПечатьюКлиент = ОбщегоНазначенияКлиент.ОбщийМодуль("УправлениеПечатьюКлиент");     
		КоллекцияПечатныхФорм = МодульУправлениеПечатьюКлиент.НоваяКоллекцияПечатныхФорм(ИдентификаторПечатнойФормы);
		ПечатнаяФорма = МодульУправлениеПечатьюКлиент.ОписаниеПечатнойФормы(КоллекцияПечатныхФорм, ИдентификаторПечатнойФормы);
		ПечатнаяФорма.СинонимМакета = НазваниеПечатнойФормы;
		ПечатнаяФорма.ТабличныйДокумент = ТабДок;
		ПечатнаяФорма.ИмяФайлаПечатнойФормы = НазваниеПечатнойФормы;
		
		ОбластиОбъектов = Новый СписокЗначений;
		МодульУправлениеПечатьюКлиент.ПечатьДокументов(КоллекцияПечатныхФорм, ОбластиОбъектов);
		
		ДокументыПечатались = Истина;
		
	Иначе
		//\/ -fb_Sportivnaya
		
		УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.Список);
	КонецЕсли;
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаСервере
Функция ПечатьСчетаНаОплатуПонесколькимРеализациям()
	         пДанныеДляЗапроса = Новый ТаблицаЗначений;
	пДанныеДляЗапроса.Колонки.Добавить("Счет", 		 Новый ОписаниеТипов ("ДокументСсылка.СчетнаОплатуКлиенту"));
	
	
ТабДокумент = Новый ТабличныйДокумент;	
Если ЗначениеЗаполнено (Элементы.Список.ТекущаяСтрока.ДокументыОснования) ТОгда
	пДанныеДляЗапроса.Колонки.Добавить("Реализация", Новый ОписаниеТипов ("ДокументСсылка.РеализацияТоваровУслуг"));
	ЭтоРеализация = Истина;
	Для каждого Строка Из Элементы.Список.ТекущаяСтрока.ДокументыОснования Цикл
		НовСтр = пДанныеДляЗапроса.Добавить();	
		НовСтр.Реализация = Строка.Реализация;
		НовСтр.Счет = Элементы.Список.ТекущаяСтрока.Ссылка;
	КонецЦикла;
Иначе
	
	Если типЗнч(Элементы.Список.ТекущаяСтрока.ДокументОснование) = Тип("ДокументСсылка.ЗаказКлиента") Тогда
		пДанныеДляЗапроса.Колонки.Добавить("Реализация", Новый ОписаниеТипов ("ДокументСсылка.ЗаказКлиента"));
		ЭтоРеализация = Ложь;
	Иначе
		пДанныеДляЗапроса.Колонки.Добавить("Реализация", Новый ОписаниеТипов ("ДокументСсылка.РеализацияТоваровУслуг"));
		ЭтоРеализация = Истина;
	КонецЕсли;	
	НовСтр = пДанныеДляЗапроса.Добавить();	
	НовСтр.Реализация = Элементы.Список.ТекущаяСтрока.ДокументОснование;
	НовСтр.Счет = Элементы.Список.ТекущаяСтрока.Ссылка;
КонецЕсли;

	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Вт_Реализации.Реализация,
		|	Вт_Реализации.Счет
		|ПОМЕСТИТЬ ВТ_Реализация
		|ИЗ
		|	&Вт_Реализации КАК Вт_Реализации
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////";
		
	Если НЕ ЭтоРеализация Тогда Запрос.Текст = Запрос.Текст+"	
		
|		ВЫБРАТЬ
|	ЗаказКлиента.НалогообложениеНДС,
|	ВТ_Реализация.Счет,
|	ЗаказКлиента.Ссылка
|ПОМЕСТИТЬ ВТ_СчетНалогооблож
|ИЗ
|	ВТ_Реализация КАК ВТ_Реализация
|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказКлиента КАК ЗаказКлиента
|		ПО ВТ_Реализация.Реализация = ЗаказКлиента.Ссылка
|ГДЕ
|	НЕ ЗаказКлиента.ПометкаУдаления
|;
|
|//////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	СУММА(ЗаказКлиентаТовары.СуммаНДС) КАК СуммаНДС,
|	ЗаказКлиентаТовары.Ссылка,
|	ВТ_Реализация.Реализация
|ПОМЕСТИТЬ вт_СуммаНДС
|ИЗ
|	ВТ_Реализация КАК ВТ_Реализация
|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказКлиента.Товары КАК ЗаказКлиентаТовары
|		ПО ВТ_Реализация.Реализация = ЗаказКлиентаТовары.Ссылка
|
|СГРУППИРОВАТЬ ПО
|	ЗаказКлиентаТовары.Ссылка,
|	ВТ_Реализация.Реализация
|;";
	Иначе Запрос.Текст = Запрос.Текст+"	
		|ВЫБРАТЬ
		|	РеализацияТоваровУслуг.НалогообложениеНДС,
		|	ВТ_Реализация.Счет,
		|	РеализацияТоваровУслуг.Ссылка
		|ПОМЕСТИТЬ ВТ_СчетНалогооблож
		|ИЗ
		|	ВТ_Реализация КАК ВТ_Реализация
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.РеализацияТоваровУслуг КАК РеализацияТоваровУслуг
		|		ПО ВТ_Реализация.Реализация = РеализацияТоваровУслуг.Ссылка
		|ГДЕ
		|	НЕ РеализацияТоваровУслуг.ПометкаУдаления
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	СУММА(РеализацияТоваровУслугТовары.СуммаНДС) КАК СуммаНДС,
		|	РеализацияТоваровУслугТовары.Ссылка,
		|	ВТ_Реализация.Реализация
		|ПОМЕСТИТЬ вт_СуммаНДС
		|ИЗ
		|	ВТ_Реализация КАК ВТ_Реализация
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.РеализацияТоваровУслуг.Товары КАК РеализацияТоваровУслугТовары
		|		ПО ВТ_Реализация.Реализация = РеализацияТоваровУслугТовары.Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	РеализацияТоваровУслугТовары.Ссылка,
		|	ВТ_Реализация.Реализация
		|;";
	КонецЕсли;
	Запрос.Текст = Запрос.Текст+"
		|
		|////////////////////////////////////////////////////////////////////////////////";
		Если ЭтоРеализация Тогда Запрос.Текст = Запрос.Текст+"
		|ВЫБРАТЬ
		|	СчетНаОплатуКлиентуДокументыОснования.Ссылка,
		|	СчетНаОплатуКлиентуДокументыОснования.НомерСтроки,
		|	СчетНаОплатуКлиентуДокументыОснования.Реализация,
		|	СчетНаОплатуКлиентуДокументыОснования.ДатаДокумента,
		|	СчетНаОплатуКлиентуДокументыОснования.СуммаДокумента,
		|	СчетНаОплатуКлиентуДокументыОснования.Контрагент,
		|	СчетНаОплатуКлиентуДокументыОснования.Склад,
		|	СчетНаОплатуКлиентуДокументыОснования.ВключаетНДС,
		|	ВТ_СчетНалогооблож.Ссылка КАК Ссылка1
		|ПОМЕСТИТЬ ВТ_ДокументыОснования
		|ИЗ
		|	ВТ_СчетНалогооблож КАК ВТ_СчетНалогооблож
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СчетНаОплатуКлиенту.ДокументыОснования КАК СчетНаОплатуКлиентуДокументыОснования
		|		ПО ВТ_СчетНалогооблож.Ссылка = СчетНаОплатуКлиентуДокументыОснования.Реализация
		|			И ВТ_СчетНалогооблож.Счет = СчетНаОплатуКлиентуДокументыОснования.Ссылка
		|ГДЕ
		|	СчетНаОплатуКлиентуДокументыОснования.Ссылка.Проведен";
	Иначе Запрос.Текст = Запрос.Текст+"
		|		ВЫБРАТЬ
		|	ВТ_СчетНалогооблож.Ссылка КАК Ссылка,
		|	ВТ_СчетНалогооблож.Ссылка КАК Ссылка1,
		|	ВТ_СчетНалогооблож.Ссылка.Ссылка КАК Реализация,
		|	ВТ_СчетНалогооблож.Ссылка.Дата КАК ДатаДокумента,
		|	ВТ_СчетНалогооблож.Ссылка.СуммаДокумента КАК СуммаДокумента,
		|	ВТ_СчетНалогооблож.Ссылка.Контрагент КАК Контрагент,
		|	ВТ_СчетНалогооблож.Ссылка.Склад КАК Склад,
		|	ВТ_СчетНалогооблож.Ссылка.ЦенаВключаетНДС КАК ВключаетНДС,
		|	0 КАК НомерСтроки
		|ПОМЕСТИТЬ ВТ_ДокументыОснования
		|ИЗ
		|	ВТ_СчетНалогооблож КАК ВТ_СчетНалогооблож";
		КонецЕсли;
		Запрос.Текст = Запрос.Текст+"
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	СчетНаОплатуКлиенту.Номер,
		|	СчетНаОплатуКлиенту.Дата,
		|	СчетНаОплатуКлиенту.Организация,
		|	СчетНаОплатуКлиенту.Организация.ИНН,
		|	СчетНаОплатуКлиенту.Организация.КПП,
		|	СчетНаОплатуКлиенту.Организация.НаименованиеСокращенное,
		|	СчетНаОплатуКлиенту.Валюта,
		|	СчетНаОплатуКлиенту.ФормаОплаты,
		|	СчетНаОплатуКлиенту.БанковскийСчет,
		|	СчетНаОплатуКлиенту.БанковскийСчет.Наименование,
		|	СчетНаОплатуКлиенту.БанковскийСчет.НомерСчета,
		|	СчетНаОплатуКлиенту.Менеджер,
		|	СчетНаОплатуКлиенту.НазначениеПлатежа,
		|	СчетНаОплатуКлиенту.Контрагент,
		|	СчетНаОплатуКлиенту.Контрагент.ИНН,
		|	СчетНаОплатуКлиенту.Контрагент.КПП,
		|	СчетНаОплатуКлиенту.Комментарий,
		|	СчетНаОплатуКлиенту.Руководитель,
		|	СчетНаОплатуКлиенту.ГлавныйБухгалтер,
		|	СчетНаОплатуКлиенту.ИдентификаторПлатежа,
		|	СчетНаОплатуКлиенту.Ссылка,
		|	СчетНаОплатуКлиенту.ЧастичнаяОплата,
		|	СчетНаОплатуКлиенту.ДополнительнаяИнформация,
		|	ВТ_СчетНалогооблож.НалогообложениеНДС КАК НалогообложениеНДС1,
		|	ВТ_СчетНалогооблож.Ссылка КАК Ссылка1
		|ПОМЕСТИТЬ ВТ_СчетаНаОплату
		|ИЗ
		|	ВТ_СчетНалогооблож КАК ВТ_СчетНалогооблож
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СчетНаОплатуКлиенту КАК СчетНаОплатуКлиенту
		|		ПО ВТ_СчетНалогооблож.Счет = СчетНаОплатуКлиенту.Ссылка
		|ГДЕ
		|	СчетНаОплатуКлиенту.Проведен
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.НомерСтроки,
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.ДатаПлатежа КАК ДатаПлатежа1,
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.ПроцентПлатежа,
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.СуммаПлатежа,
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.ЭтоЗалогЗаТару,
		|	СчетНаОплатуКлиентуЭтапыГрафикаОплаты.Ссылка,
		|	ВТ_Реализация.Реализация
		|ПОМЕСТИТЬ ЭтапыОплаты
		|ИЗ
		|	ВТ_Реализация КАК ВТ_Реализация
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СчетНаОплатуКлиенту.ЭтапыГрафикаОплаты КАК СчетНаОплатуКлиентуЭтапыГрафикаОплаты
		|		ПО ВТ_Реализация.Счет = СчетНаОплатуКлиентуЭтапыГрафикаОплаты.Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ДокументыОснования.Ссылка,
		|	ВТ_ДокументыОснования.НомерСтроки,
		|	ВТ_ДокументыОснования.Реализация,
		|	ВТ_ДокументыОснования.ДатаДокумента,
		|	ВТ_ДокументыОснования.СуммаДокумента,
		|	ВТ_ДокументыОснования.Контрагент,
		|	ВТ_ДокументыОснования.Склад,
		|	ВТ1.Номер,
		|	ВТ1.Дата,
		|	ВТ1.Организация,
		|	ВТ1.ОрганизацияИНН,
		|	ВТ1.ОрганизацияКПП,
		|	ВТ1.ОрганизацияНаименованиеСокращенное,
		|	ВТ1.Валюта,
		|	ВТ1.ФормаОплаты,
		|	ВТ1.БанковскийСчет,
		|	ВТ1.БанковскийСчетНаименование,
		|	ВТ1.БанковскийСчетНомерСчета,
		|	ВТ1.Менеджер,
		|	ВТ1.НазначениеПлатежа,
		|	ВТ1.Контрагент КАК Контрагент1,
		|	ВТ1.КонтрагентИНН,
		|	ВТ1.КонтрагентКПП,
		|	ВТ1.Комментарий,
		|	ВТ1.Руководитель,
		|	ВТ1.ГлавныйБухгалтер,
		|	ВТ1.ИдентификаторПлатежа,
		|	ВТ1.БанковскийСчет.ГородБанка,
		|	ВТ1.БанковскийСчет.Банк.КоррСчет,
		|	ВТ1.БанковскийСчет.Банк.Код,
		|	ВТ1.Ссылка КАК СсылкаСчет,
		|	ВТ1.ЧастичнаяОплата,
		|	ЭтапыОплаты.НомерСтроки КАК НомерСтроки1,
		|	ЭтапыОплаты.ДатаПлатежа1,
		|	ЭтапыОплаты.ПроцентПлатежа,
		|	ЭтапыОплаты.СуммаПлатежа,
		|	ЭтапыОплаты.ЭтоЗалогЗаТару,
		|	ЭтапыОплаты.Ссылка КАК Ссылка2,
		|	ВТ1.Контрагент.Наименование,
		|	ВТ_ДокументыОснования.ВключаетНДС,
		|	ВТ1.ДополнительнаяИнформация,
		|	ВТ1.НалогообложениеНДС1 КАК Налогообложение,
		|	вт_СуммаНДС.СуммаНДС
		|ИЗ
		|	ВТ_СчетаНаОплату КАК ВТ1
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ДокументыОснования КАК ВТ_ДокументыОснования
		|		ПО ВТ1.Ссылка1 = ВТ_ДокументыОснования.Ссылка1
		|		ЛЕВОЕ СОЕДИНЕНИЕ ЭтапыОплаты КАК ЭтапыОплаты
		|		ПО ВТ1.Ссылка1 = ЭтапыОплаты.Реализация
		|		ЛЕВОЕ СОЕДИНЕНИЕ вт_СуммаНДС КАК вт_СуммаНДС
		|		ПО ВТ1.Ссылка1 = вт_СуммаНДС.Реализация
		|ИТОГИ ПО
		|	СсылкаСчет";
	Запрос.УстановитьПараметр("Вт_Реализации",пДанныеДляЗапроса);

	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	
	пСуммаПоДокументам = 0;
	пНомерСтроки = 0;
	пСчетчик = 0;
	пКоличествоЗаписей = РезультатЗапроса.Количество();
	//СписокДляОтправки = Новый ТаблицаЗначений;      //ТЗ для передачи данныых в механизм отправки почты
	//СписокДляОтправки.Колонки.Добавить("Организация",Новый ОписаниеТипов("СправочникСсылка.Организации"));
	//СписокДляОтправки.Колонки.Добавить("ПутьКФайлу", Новый ОписаниеТипов("Строка"));

	
	Пока пСчетчик <= пКоличествоЗаписей-1  Цикл
			
	ТабДокумент.Очистить();	
	
	Выборка = РезультатЗапроса[пСчетчик];	
 	
	Если ЗначениеЗаполнено(Выборка.Реализация) Тогда
		
		
		пНомерСтроки = 0;		 
		пСуммаВТЧНДС = 0;
		пСуммаНДС = 0;
		
		Счет =  Выборка.СсылкаСчет.ПолучитьОбъект();
		Макет = Счет.ПолучитьМакет("ПФ_MXL_СчетНаОплату");	
		
	 	ЗаголовокСчета           			= Макет.ПолучитьОбласть("ЗаголовокСчета");
	 	ОбразецЗаполненияПП 				= Макет.ПолучитьОбласть("ОбразецЗаполненияПП");
	 	ОбразецЗаполненияРеквизитыБанка     = Макет.ПолучитьОбласть("ОбразецЗаполненияРеквизитыБанка");
	 	ОбразецЗаполненияНазначениеПлатежа  = Макет.ПолучитьОбласть("ОбразецЗаполненияНазначениеПлатежа");
	 	Заголовок1                  		= Макет.ПолучитьОбласть("Заголовок");
	 	Поставщик                   		= Макет.ПолучитьОбласть("Поставщик");
	 	Покупатель                 			= Макет.ПолучитьОбласть("Покупатель");
	 	ШапкаТаблицы              		    = Макет.ПолучитьОбласть("ШапкаТаблицы");
	 	СтрокаТаблицы						= Макет.ПолучитьОбласть("СтрокаТаблицы");
	 	ПодвалТаблицы          				= Макет.ПолучитьОбласть("ПодвалТаблицы");
	 	ПодвалТаблицыНДС          		    = Макет.ПолучитьОбласть("ПодвалТаблицыНДС");
	 	ПодвалТаблицыВсего        		    = Макет.ПолучитьОбласть("ПодвалТаблицыВсего");
	 	СуммаПрописьюЧастичныйСчет          = Макет.ПолучитьОбласть("СуммаПрописьюЧастичныйСчет");	
	 	ШапкаТаблицыЭтапыОплаты     		= Макет.ПолучитьОбласть("ШапкаТаблицыЭтапыОплаты");
	 	СтрокаТаблицыЭтапыОплаты    		= Макет.ПолучитьОбласть("СтрокаТаблицыЭтапыОплаты");
	 	ИтогоЭтапыОплаты            		= Макет.ПолучитьОбласть("ИтогоЭтапыОплаты");
	 	ДополнительнаяИнформация    		= Макет.ПолучитьОбласть("ДополнительнаяИнформация");
	 	ПодвалСчета         	     		= Макет.ПолучитьОбласть("ПодвалСчета");
	 	ОбластьФаксимиле           			= Макет.ПолучитьОбласть("ОбластьФаксимиле");
		ОбластьДно							= Макет.ПолучитьОбласть("ОбластьДно");
	 		
		ФормированиеПечатныхФорм.ВывестиЛоготипВТабличныйДокумент(Макет, ЗаголовокСчета, "ЗаголовокСчета", Выборка.Организация);
		ШтрихкодированиеПечатныхФорм.ВывестиШтрихкодВТабличныйДокумент(ЗаголовокСчета, Макет, ЗаголовокСчета, Выборка.СсылкаСчет);
		//ЗаголовокСчета.Параметры.СрокДействия =" Счет действителен до " +  Формат(Выборка.ДатаПлатежа1,"ДЛФ=Д")+"."; //Дата из ТЧ частичной оплаты
		ТабДокумент.Вывести(ЗаголовокСчета);
		//
		//СведенияОПоставщике = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Выборка.Организация, Выборка.Дата);
		
		ОбразецЗаполненияПП.Параметры.БанкПолучателяПредставление 			  = ?(ЗначениеЗаполнено(Выборка.БанковскийСчетНаименование),Строка(Выборка.БанковскийСчетНаименование) +", "+ Выборка.БанковскийСчетГородБанка,"");
		ОбразецЗаполненияПП.Параметры.БИКБанкаПолучателя 					  = Выборка.БанковскийСчетБанкКоррСчет;
		ОбразецЗаполненияПП.Параметры.СчетБанкаПолучателяПредставление 		  = Выборка.БанковскийСчетБанкКод;
		ОбразецЗаполненияПП.Параметры.ИНН 									  = Выборка.ОрганизацияИНН;
		ОбразецЗаполненияПП.Параметры.КПП 									  = Выборка.ОрганизацияКПП;         //Заполняем образец заполнения бан реквизитов
		ОбразецЗаполненияПП.Параметры.ПредставлениеПоставщикаДляПлатПоручения = Выборка.ОрганизацияНаименованиеСокращенное;
		ОбразецЗаполненияПП.Параметры.ИдентификаторПлатежа 					  = Выборка.ИдентификаторПлатежа;
		ОбразецЗаполненияПП.Параметры.НазначениеПлатежа 					  = Выборка.НазначениеПлатежа;
		ТабДокумент.Вывести(ОбразецЗаполненияПП);
		
		Заголовок1.Параметры.ТекстЗаголовка = ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(Выборка, НСтр("ru='Счет на оплату'"));
		ТабДокумент.Вывести(Заголовок1);
		
 
		Поставщик.Параметры.ПредставлениеПоставщика = ФормированиеПечатныхФорм.ОписаниеОрганизации(ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Выборка.Организация, Выборка.Дата),
			"ПолноеНаименование,ИНН,КПП,ЮридическийАдрес,Телефоны,");
		ТабДокумент.Вывести(Поставщик);
		
		Покупатель.Параметры.ПредставлениеПолучателя = ФормированиеПечатныхФорм.ОписаниеОрганизации(ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Выборка.Контрагент1, Выборка.Дата), 
			"ПолноеНаименование,ИНН,КПП,ЮридическийАдрес,Телефоны,");
		ТабДокумент.Вывести(Покупатель);
		
		ТабДокумент.Вывести(ШапкаТаблицы);
		
		пНомерСтроки						 = пНомерСтроки +1;
		пСуммаПоДокументам 					 = Выборка.СуммаДокумента;
		СтрокаТаблицы.Параметры.НомерСтроки  = пНомерСтроки;
		СтрокаТаблицы.Параметры.Реализация 	 = Выборка.Реализация;
		Если Выборка.ВключаетНДС Тогда
			пСуммаВТЧНДС = Выборка.СуммаНДС;
			СтрокаТаблицы.Параметры.Сумма 	 = Выборка.СуммаДокумента;
		Иначе
			пСуммаНДС = Выборка.СуммаНДС;
			СтрокаТаблицы.Параметры.Сумма    = Выборка.СуммаДокумента;
		КонецЕсли;
		
		СтрокаТаблицы.Параметры.НДС     	 = Выборка.СуммаНДС;
		
		ТабДокумент.Вывести(СтрокаТаблицы);
		
		Пока пСчетчик < пКоличествоЗаписей-1 Цикл		
		 
			Если Выборка.СсылкаСчет = РезультатЗапроса[пСчетчик+1].СсылкаСчет И ЗначениеЗаполнено(Выборка.Реализация) Тогда
				пНомерСтроки						  = пНомерСтроки +1;                                                    //Если в следующей строке результата запроса
				пСуммаПоДокументам 					  = пСуммаПоДокументам + РезультатЗапроса[пСчетчик+1].СуммаДокумента;   //Ссылка на счет равна ссылке на счет текущей строки
				СтрокаТаблицы.Параметры.НомерСтроки   = пНомерСтроки;                                                       //то заполняем следующей строкой табличную часть
				СтрокаТаблицы.Параметры.Реализация 	  = РезультатЗапроса[пСчетчик+1].Реализация;                            //иначе идем дальше
				                                                                                                  
					Если РезультатЗапроса[пСчетчик+1].ВключаетНДС Тогда
						пСуммаВТЧНДС                  = пСуммаВТЧНДС + РезультатЗапроса[пСчетчик+1].СуммаНДС;
						СтрокаТаблицы.Параметры.Сумма = РезультатЗапроса[пСчетчик+1].СуммаДокумента;                        //Подсчитываем сумму по документам с и без ндс
					Иначе
						пСуммаНДС                     = пСуммаНДС + РезультатЗапроса[пСчетчик+1].СуммаНДС;
						СтрокаТаблицы.Параметры.Сумма = РезультатЗапроса[пСчетчик+1].СуммаДокумента;
					КонецЕсли; 
				
				СтрокаТаблицы.Параметры.НДС           = РезультатЗапроса[пСчетчик+1].СуммаНДС;				

				ТабДокумент.Вывести(СтрокаТаблицы);

				пСчетчик = пСчетчик + 1;
			Иначе
				Прервать;
			КонецЕсли; 
		КонецЦикла;	
		
		ПодвалТаблицы.Параметры.Всего = пСуммаПоДокументам -  пСуммаНДС;
		ТабДокумент.Вывести(ПодвалТаблицы);
		
		Если ЗначениеЗаполнено(Выборка.Налогообложение) Тогда
			
			Если Выборка.Налогообложение = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяНДС  Тогда
				Если ЗначениеЗаполнено(пСуммаВТЧНДС) Тогда
					 ПодвалТаблицыНДС.Параметры.НДС      = "В т.ч. НДС:";
					 ПодвалТаблицыНДС.Параметры.ВсегоНДС = пСуммаВТЧНДС;
					 ТабДокумент.Вывести(ПодвалТаблицыНДС);
				КонецЕсли; 
				
				Если ЗначениеЗаполнено(пСуммаНДС) Тогда
					 ПодвалТаблицыНДС.Параметры.НДС      = "НДС:";                       //В зависимости от налогообложения задаем представление ндс
					 ПодвалТаблицыНДС.Параметры.ВсегоНДС = пСуммаНДС;
					 ТабДокумент.Вывести(ПодвалТаблицыНДС);
				КонецЕсли;
				
			ИначеЕсли Выборка.Налогообложение = Перечисления.ТипыНалогообложенияНДС.ПродажаНаЭкспорт  Тогда
			    	  ПодвалТаблицыНДС.Параметры.НДС      = "НДС(0%):";
					  ПодвалТаблицыНДС.Параметры.ВсегоНДС = "-";
					  ТабДокумент.Вывести(ПодвалТаблицыНДС);
					
			ИначеЕсли Выборка.Налогообложение = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД Тогда
				      ПодвалТаблицыНДС.Параметры.НДС      = "НДС(Без НДС):";
					  ПодвалТаблицыНДС.Параметры.ВсегоНДС = "-";
					  ТабДокумент.Вывести(ПодвалТаблицыНДС);
			КонецЕсли;
				
		КонецЕсли; 
		
		ПодвалТаблицыВсего.Параметры.ВсегоСНДС = пСуммаПоДокументам;
		ТабДокумент.Вывести(ПодвалТаблицыВсего);
		
		СуммаПрописьюЧастичныйСчет.Параметры.СуммаПрописью = РаботаСкурсамиВалют.СформироватьСуммуПрописью(пСуммаПоДокументам, Выборка.Валюта); 
	    ТабДокумент.Вывести(СуммаПрописьюЧастичныйСчет);
		
		Если ЗначениеЗаполнено(Выборка.ДополнительнаяИнформация) Тогда
		ДополнительнаяИнформация.Параметры.ДополнительнаяИнформация = Выборка.ДополнительнаяИнформация;
		ТабДокумент.Вывести(ДополнительнаяИнформация);
		КонецЕсли; 
		
		ФайлКартинки = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Выборка.Организация, "ФайлФаксимильнаяПечать");

		Если ЗначениеЗаполнено(ФайлКартинки) Тогда
			
			ФормированиеПечатныхФорм.ВывестиФаксимилеВТабличныйДокумент(Макет, ОбластьФаксимиле, Выборка.Организация, Новый Структура("ОтображатьФаксимиле", Истина));	
			ТабДокумент.Вывести(ОбластьФаксимиле);
		Иначе
			ОтветственныеЛица = ОтветственныеЛицаСервер.ПолучитьОтветственныеЛицаОрганизации(Выборка.Организация);
			ПодвалСчета.Параметры.ФИОРуководителя = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ОтветственныеЛица.Руководитель);
			ПодвалСчета.Параметры.ФИОБухгалтера   = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ОтветственныеЛица.ГлавныйБухгалтер);
			ПодвалСчета.Параметры.ФИОМенеджер     = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Выборка.Менеджер);
			ТабДокумент.Вывести(ПодвалСчета);
		КонецЕсли;
		
		ОбластьДно.Параметры.Согласие = "Оплата данного счета-оферты свидетельствует о заключении сделки-купли продажи в письменной форме (п.3 ст.434 и п.3 ст.438 ГК РФ)";
		ТабДокумент.Вывести(ОбластьДно);   //Пожелание клиента добавить эту строку
		
		ТабДокумент.АвтоМасштаб = Истина;
		
	КонецЕсли;
	 ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	 пСчет    = "";
	 пСчетчик = пСчетчик +1;
 КонецЦикла;
 

Возврат ТабДокумент;	

КонецФункции // ()
// \-/ fs_AALeshin
 

#КонецОбласти

#КонецОбласти

