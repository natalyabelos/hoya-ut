﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Возвращает массив имен ролей с правом "Добавление" для данного документа
//
// Возвращаемое значение:
//	Массив - Массив с именами ролей
//
Функция ИменаРолейСПравомДобавления() Экспорт
	
	МассивРолей = Новый Массив;
	МассивРолей.Добавить("ПолныеПрава");
	Возврат МассивРолей;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции
// Формирует запрос проверки при смене статуса списка документов
//
// Параметры:
//	МассивДокументов - Массив - Массив ссылок на документы, которые надо проверять
//	НовыйСтатус - Строка - Имя нового статуса
//	ДополнительныеПараметры - Структура - Структура дополнительных параметров
//
// Возвращаемое значение:
//	Запрос - Запрос проверки перед сменой статуса
//
Функция СформироватьЗапросПроверкиПриСменеСтатуса(МассивДокументов, НовыйСтатус, ДополнительныеПараметры) Экспорт
	
	ЗначениеНовогоСтатуса = Перечисления.СтатусыРассылокКлиентам[НовыйСтатус];
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ТаблицаДокументов.Ссылка КАК Ссылка,
	|	ПРЕДСТАВЛЕНИЕ(ТаблицаДокументов.Ссылка) КАК Представление,
	|	ПРЕДСТАВЛЕНИЕ(ТаблицаДокументов.Статус) КАК ПредставлениеТекущегоСтатуса,
	|	ПРЕДСТАВЛЕНИЕ(&Статус) КАК ПредставлениеНовогоСтатуса,
	|	ВЫБОР
	|		КОГДА ТаблицаДокументов.Статус = &Статус
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК СтатусСовпадает,
	|	ИСТИНА КАК Проведен,
	|	ТаблицаДокументов.ПометкаУдаления КАК ПометкаУдаления,
	|	ЛОЖЬ КАК ЗаписьПроведением,
	|	ТаблицаДокументов.Статус КАК ТекущийСтатус
	|ИЗ
	|	Документ.РассылкаКлиентам КАК ТаблицаДокументов
	|ГДЕ
	|	ТаблицаДокументов.Ссылка В(&МассивДокументов)";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("Статус", ЗначениеНовогоСтатуса);
	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);
	
	Возврат Запрос;
	
КонецФункции

// Возвращает результат проверки при смене статуса документа
//
// Параметры:
//	ВыборкаПроверки - ВыборкаИзРезультатаЗапроса - Текущая строка выборки
//	НовыйСтатус - Перечисление - Новый статус
//	ДополнительныеПараметры - Структура - Структура дополнительных параметров
//
// Возвращаемое значение:
//	Булево - Истина, в случае успешного завершения проверки
//
Функция ПроверкаПередСменойСтатуса(ВыборкаПроверки, НовыйСтатус, ДополнительныеПараметры) Экспорт
	
	Отказ = Ложь;
	
	Если ВыборкаПроверки.ТекущийСтатус = Перечисления.СтатусыРассылокКлиентам.Обрабатывается
		ИЛИ ВыборкаПроверки.ТекущийСтатус = Перечисления.СтатусыРассылокКлиентам.Выполнена Тогда
		
		ТекстОшибки = НСтр("ru='У документа %Документ% статус ""%Статус%"" не установлен, т.к. текущий статус отличается от ""Черновик"" И ""К отправке""'");
		ТекстОшибки = СтрЗаменить(ТекстОшибки, "%Документ%", ВыборкаПроверки.Ссылка);
		ТекстОшибки = СтрЗаменить(ТекстОшибки, "%Статус%", ВыборкаПроверки.ПредставлениеНовогоСтатуса);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки, ВыборкаПроверки.Ссылка);
		Отказ = Истина;
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

#КонецОбласти

#КонецЕсли
