﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, СтруктураДополнительныеСвойства) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ВозвратПодарочныхСертификатов.Дата КАК Дата,
	|	ВозвратПодарочныхСертификатов.Валюта КАК Валюта,
	|	ВозвратПодарочныхСертификатов.Организация КАК Организация,
	|	ВозвратПодарочныхСертификатов.НомерЧекаККМ КАК НомерЧекаККМ
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов КАК ВозвратПодарочныхСертификатов
	|ГДЕ
	|	ВозвратПодарочныхСертификатов.Ссылка = &Ссылка");

	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);

	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	Выборка.Следующий();
	
	Коэффициенты = РаботаСКурсамивалютУТ.ПолучитьКоэффициентыПересчетаВалюты(Выборка.Валюта, , Выборка.Дата);
	
	ТекстЗапроса = ТекстЗапросаПодарочныеСертификаты()
	             + ТекстЗапросаИсторияПодарочныхСертификатов()
	             + ТекстЗапросаТаблицаДенежныеСредстваВКассахККМ()
	             + ТекстЗапросаТаблицаРасчетыПоЭквайрингу();

	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("Ссылка",      ДокументСсылка);
	Запрос.УстановитьПараметр("Период",      Выборка.Дата);
	Запрос.УстановитьПараметр("Валюта",      Выборка.Валюта);
	Запрос.УстановитьПараметр("Организация", Выборка.Организация);
	Запрос.УстановитьПараметр("ХозяйственнаяОперация",           ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.РеализацияВРозницу"));
	Запрос.УстановитьПараметр("ЧекПробит",                       ЗначениеЗаполнено(Выборка.НомерЧекаККМ));
	Запрос.УстановитьПараметр("КоэффициентПересчетаВВалютуУпр",  Коэффициенты.КоэффициентПересчетаВВалютуУпр);
	Запрос.УстановитьПараметр("КоэффициентПересчетаВВалютуРегл", Коэффициенты.КоэффициентПересчетаВВалютуРегл);
	
	МассивРезультатов = Запрос.ВыполнитьПакет();
	
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаПодарочныеСертификаты",         МассивРезультатов[0].Выгрузить());
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаИсторияПодарочныхСертификатов", МассивРезультатов[1].Выгрузить());
	// МассивРезультатов[2] - ТаблицаСуммаПлатежнымиКартами
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаДенежныеСредстваВКассахККМ",    МассивРезультатов[3].Выгрузить());
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаРасчетыПоЭквайрингу",           МассивРезультатов[4].Выгрузить());
	
КонецПроцедуры

Функция ТекстЗапросаПодарочныеСертификаты()
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&Период                                                 КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)                  КАК ВидДвижения,
	|	ТабличнаяЧасть.ПодарочныйСертификат                     КАК ПодарочныйСертификат,
	|	ТабличнаяЧасть.ПодарочныйСертификат.Владелец.Номинал    КАК Сумма,
	|	ТабличнаяЧасть.Сумма * &КоэффициентПересчетаВВалютуРегл КАК СуммаРегл
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов.ПодарочныеСертификаты КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|	И &ЧекПробит
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаИсторияПодарочныхСертификатов()
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&Период                                                        КАК Период,
	|	ТабличнаяЧасть.ПодарочныйСертификат                            КАК ПодарочныйСертификат,
	|	ЗНАЧЕНИЕ(Перечисление.СтатусыПодарочныхСертификатов.Возвращен) КАК Статус
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов.ПодарочныеСертификаты КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|	И &ЧекПробит
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаДенежныеСредстваВКассахККМ()
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	СУММА(ТабличнаяЧасть.Сумма) КАК Сумма
	|ПОМЕСТИТЬ ТаблицаСуммаПлатежнымиКартами
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов.ОплатаПлатежнымиКартами КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ДанныеДокумента.Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	ДанныеДокумента.КассаККМ.Владелец КАК Организация,
	|	ДанныеДокумента.КассаККМ КАК КассаККМ,
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) КАК Сумма,
	|	ВЫРАЗИТЬ((ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0)) * &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15, 2)) КАК СуммаРегл,
	|	ВЫРАЗИТЬ((ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0)) * &КоэффициентПересчетаВВалютуУпр КАК ЧИСЛО(15, 2)) КАК СуммаУпр
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов КАК ДанныеДокумента
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаСуммаПлатежнымиКартами КАК ТаблицаСуммаПлатежнымиКартами
	|		ПО (ИСТИНА)
	|ГДЕ
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) <> 0
	|	И ДанныеДокумента.Ссылка = &Ссылка
	|	И &ЧекПробит
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаРасчетыПоЭквайрингу()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаПлатежей.НомерСтроки КАК НомерСтроки,
	|	&Период КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыДенежныхСредствПоЭквайрингу.ПоступлениеПоПлатежнойКарте) КАК ТипДенежныхСредств,
	|	&Организация КАК Организация,
	|	&Валюта КАК Валюта,
	|	ТаблицаПлатежей.ЭквайринговыйТерминал КАК ЭквайринговыйТерминал,
	|	ТаблицаПлатежей.КодАвторизации КАК КодАвторизации,
	|	ТаблицаПлатежей.НомерПлатежнойКарты КАК НомерПлатежнойКарты,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	&Период КАК ДатаПлатежа,
	|	ТаблицаПлатежей.Сумма
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов.ОплатаПлатежнымиКартами КАК ТаблицаПлатежей
	|
	|ГДЕ
	|	&ЧекПробит
	|	И ТаблицаПлатежей.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции


#КонецОбласти

#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	Если ПолучитьФункциональнуюОпцию("ИспользоватьПодарочныеСертификаты") Тогда
		
		// КМ-3
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюУТКлиент.ПечатьКМ3";
		КомандаПечати.МенеджерПечати = "";
		КомандаПечати.Идентификатор = "КМ3";
		КомандаПечати.Представление = НСтр("ru = 'КМ-3'");
		
	КонецЕсли;

КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
КонецПроцедуры

// Функция получает данные для формирования печатной формы КМ-3.
//
Функция ПолучитьДанныеДляПечатнойФормыКМ3(ПараметрыПечати, МассивОбъектов) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	ОтветственныеЛицаСервер.СформироватьВременнуюТаблицуОтветственныхЛицДокументов(МассивОбъектов, МенеджерВременныхТаблиц);
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ДокЧек.Ссылка КАК Ссылка,
	|	ДокЧек.Номер КАК Номер,
	|	ДокЧек.Дата КАК ДатаДокумента,
	|	ДокЧек.КассаККМ КАК КассаККМ,
	|	ДокЧек.КассаККМ.ТипКассы КАК ТипКассы,
	|	ДокЧек.КассаККМ.Представление КАК Покупатель,
	|	ДокЧек.Кассир.ФизическоеЛицо КАК КассирККМ,
	|	ДокЧек.Валюта КАК Валюта,
	|	ДокЧек.Организация КАК Организация,
	|	ТаблицаОтветственныеЛица.РуководительНаименование КАК Руководитель,
	|	ТаблицаОтветственныеЛица.РуководительДолжность КАК ДолжностьРуководителя,
	|	ТаблицаОтветственныеЛица.ГлавныйБухгалтерНаименование КАК ГлавныйБухгалтер,
	|	ДокЧек.Организация.Представление КАК Поставщик,
	|	ДокЧек.КассаККМ.СерийныйНомер КАК СерийныйНомерККМ,
	|	ДокЧек.КассаККМ.РегистрационныйНомер КАК РегистрационныйНомерККМ,
	|	ДокЧек.КассаККМ.Наименование КАК ПредставлениеККМ
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов КАК ДокЧек
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаОтветственныеЛица КАК ТаблицаОтветственныеЛица
	|		ПО ДокЧек.Ссылка = ТаблицаОтветственныеЛица.Ссылка
	|ГДЕ
	|	ДокЧек.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДокЧек.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ДокЧек.Ссылка         КАК Документ,
	|	ДокЧек.РеализацияПодарочныхСертификатов.НомерЧекаККМ КАК НомерЧека,
	|	ДокЧек.СуммаДокумента КАК СуммаДокумента
	|ИЗ
	|	Документ.ВозвратПодарочныхСертификатов КАК ДокЧек
	|ГДЕ
	|	ДокЧек.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Документ
	|ИТОГИ ПО
	|	Документ";
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	СтруктураДанныхДляПечати    = Новый Структура("РезультатЗапроса, ЗаголовокДокумента",
		РезультатЗапроса, НСтр("ru = 'КМ-3'"));
	
	Если ПривилегированныйРежим() Тогда
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
	Возврат СтруктураДанныхДляПечати;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли
