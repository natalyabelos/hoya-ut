﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	Склад = Справочники.Склады.СкладПоУмолчанию();
			
	Если Параметры.Свойство("Статус", Статус) Тогда
		СтатусПриИзмененииНаКлиентеНаСервере(ЭтаФорма);
	Иначе
		Статус = "Все";
	КонецЕсли;
	
	Параметры.Свойство("СтруктураБыстрогоОтбора", СтруктураБыстрогоОтбора);
	
	Если СтруктураБыстрогоОтбора <> Неопределено Тогда
		СтруктураБыстрогоОтбора.Свойство("Склад", Склад);
		СтруктураБыстрогоОтбора.Свойство("Помещение", Помещение);	
	Иначе
		СтруктураБыстрогоОтбора = Новый Структура("Склад, Помещение", Склад, Помещение);
	КонецЕсли;
	
	ОтборыСписковКлиентСервер.ОтборПоЗначениюСпискаПриСозданииНаСервере(СписокПересчетов, 
			"Склад", 
			Склад, 
			СтруктураБыстрогоОтбора,
			Истина);
			
	ОтборыСписковКлиентСервер.ОтборПоЗначениюСпискаПриСозданииНаСервере(СписокПересчетов, 
			"Помещение", 
			Помещение, 
			СтруктураБыстрогоОтбора,
			Истина);
	
	УстановитьВидимостьПомещения();
	
	Если ПравоДоступа("Использование", Метаданные.Обработки.ПомощникОформленияСкладскихАктов) Тогда
		ОбновитьИнформациюОРасхождениях(Элементы, Склад);
		Элементы.СтраницыИнформацияОРасхождениях.Видимость = Истина;
	Иначе
		Элементы.СтраницыИнформацияОРасхождениях.Видимость = Ложь;
	КонецЕсли;
	
	Если Не ПравоДоступа("Добавление", Метаданные.Документы.ПересчетТоваров) Тогда
		
		Элементы.СоздатьЗаданиеНаПересчетНеадресныйСклад.Видимость = Ложь;
		Элементы.СкопироватьНеадресныйСклад.Видимость = Ложь;
		Элементы.УстановитьПометкуУдаленияНеадресныйСклад.Видимость = Ложь;
		Элементы.НазначитьИсполнителя.Видимость = Ложь;
		
		Элементы.СоздатьЗаданиеНаПересчетАдресныйСклад.Видимость = Ложь;
		Элементы.СгенерироватьЗаданияНаПересчетАдресныйСклад.Видимость = Ложь;
		Элементы.СкопироватьАдресныйСклад.Видимость = Ложь;
		Элементы.УстановитьПометкуУдаленияАдресныйСклад.Видимость = Ложь;
		Элементы.НазначитьИсполнителяАдресныйСклад.Видимость = Ложь;
		
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечатьНеадресныйСклад);
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечатьАдресныйСклад);
	// Конец СтандартныеПодсистемы.Печать
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаКоманднаяПанельЗаданияНаПересчетНеадресныйСклад);
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма, Элементы.ГруппаКоманднаяПанельЗаданияНаПересчетАдресныйСклад);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	УстановитьВидимостьКомандКонтекстныхОтчетов();
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтаФорма, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	МенеджерОборудованияКлиентПереопределяемый.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
		КонецЕсли;
	КонецЕсли;
	
	Если ИмяСобытия = "Запись_ПересчетТоваров" Тогда
		Если Элементы.СтраницыИнформацияОРасхождениях.Видимость Тогда
			ОбновитьСписокНаСервере()
		Иначе
			Элементы.СписокПересчетов.Обновить();
		КонецЕсли;
	ИначеЕсли ИмяСобытия = "Создание_СкладскиеАкты" Тогда
		Если Элементы.СтраницыИнформацияОРасхождениях.Видимость Тогда
			ОбновитьИнформациюОРасхождениях(Элементы, Склад);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора) 
	
	Если ИсточникВыбора.ИмяФормы = "Справочник.Пользователи.Форма.ФормаСписка" 
		И ТипЗнч(ВыбранноеЗначение) = Тип("СправочникСсылка.Пользователи") Тогда
		// Назначение исполнителя - выбрали пользователя
		
		МассивИзмененныхДокументов = ОбработкаВыбораИсполнителяНаСервере(ВыбранноеЗначение);
		
		Если МассивИзмененныхДокументов.Количество() > 0 Тогда
			
			ТекстСообщения = НСтр("ru='Для %КоличествоОбработанных% из %КоличествоВсего% выделенных в списке заданий на пересчет назначен исполнитель %Исполнитель%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%КоличествоОбработанных%", МассивИзмененныхДокументов.Количество());
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%КоличествоВсего%",        СписокВыделенныхДокументов.Количество());
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Исполнитель%",            ВыбранноеЗначение);
			
			ТекстЗаголовка = НСтр("ru='Исполнитель %Исполнитель% назначен'");
			ТекстЗаголовка = СтрЗаменить(ТекстЗаголовка, "%Исполнитель%", ВыбранноеЗначение);
			ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
			
		Иначе
			
			ТекстСообщения = НСтр("ru='Исполнитель %Исполнитель% не назначен ни для одного задания на пересчет'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Исполнитель%", ВыбранноеЗначение);
			
			ТекстЗаголовка = НСтр("ru='Исполнитель %Исполнитель% не назначен'");
			ТекстЗаголовка = СтрЗаменить(ТекстЗаголовка, "%Исполнитель%", ВыбранноеЗначение);
			ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Склад = Настройки.Получить("Склад");
	Помещение = Настройки.Получить("Помещение");
	
	Если СтруктураБыстрогоОтбора <> Неопределено Тогда
		
		Если СтруктураБыстрогоОтбора.Свойство("Склад") Тогда
			Склад = СтруктураБыстрогоОтбора.Склад;
			Настройки.Удалить("Склад");
		КонецЕсли;
		
		Если СтруктураБыстрогоОтбора.Свойство("Помещение") Тогда
			Помещение = СтруктураБыстрогоОтбора.Помещение;
			Настройки.Удалить("Помещение");
		КонецЕсли;
		
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокПересчетов, 
			"Склад", 
			Склад, 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);
			
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокПересчетов, 
			"Помещение", 
			Помещение, 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);
	
	УстановитьВидимостьПомещения();
	УстановитьВидимостьСтраницКоманднойПанелиСписокПересчетов();
	ОбновитьИнформациюОРасхождениях(Элементы, Склад);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокПересчетов, 
		"Склад", 
		Склад, 
		ВидСравненияКомпоновкиДанных.Равно,, 
		Истина);
		
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокПересчетов, 
		"Помещение", 
		Помещение, 
		ВидСравненияКомпоновкиДанных.Равно,, 
		Истина);

	УстановитьВидимостьПомещения();
	УстановитьВидимостьСтраницКоманднойПанелиСписокПересчетов();
	ОбновитьИнформациюОРасхождениях(Элементы, Склад);
	
КонецПроцедуры

&НаКлиенте
Процедура ПомещениеПриИзменении(Элемент)

	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокПересчетов, 
		"Помещение", 
		Помещение, 
		ВидСравненияКомпоновкиДанных.Равно,, 
		Истина);
		
	УстановитьВидимостьСтраницКоманднойПанелиСписокПересчетов();

КонецПроцедуры

&НаКлиенте
Процедура СтатусПриИзменении(Элемент)
	СтатусПриИзмененииНаКлиентеНаСервере(ЭтаФорма);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СгенерироватьЗаданияНаПересчет(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Склад", Склад);
	ПараметрыФормы.Вставить("Помещение", Помещение);
	ОткрытьФорму("Документ.ПересчетТоваров.Форма.ФормаНастроекСозданияЗаданийНаПересчет", 
			ПараметрыФормы, 
			ЭтаФорма,,,,, 
			РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьЗаданиеНаПересчет(Команда)
	
	ЗначенияЗаполнения = Новый Структура;
	ЗначенияЗаполнения.Вставить("Склад", Склад);
	ЗначенияЗаполнения.Вставить("Помещение", Помещение); 
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ЗначенияЗаполнения", ЗначенияЗаполнения);	
		
	ОткрытьФорму("Документ.ПересчетТоваров.ФормаОбъекта", ПараметрыФормы);
		
КонецПроцедуры

&НаКлиенте
Процедура НазначитьИсполнителя(Команда)
	
	ОчиститьСообщения();
	                                  
	МассивВыделенныхДокументов = РаботаСДиалогамиКлиент.ПроверитьПолучитьВыделенныеВСпискеСсылки(Элементы.СписокПересчетов);
	
	Если МассивВыделенныхДокументов.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	СписокВыделенныхДокументов.Очистить();
	
	Для Каждого ВыделеннаяСтрока Из МассивВыделенныхДокументов Цикл
		СписокВыделенныхДокументов.Добавить(ВыделеннаяСтрока);	
	КонецЦикла;
	
	ОткрытьФорму("Справочник.Пользователи.ФормаВыбора", Новый Структура("РежимВыбора", Истина), ЭтаФорма,,,,,
					РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.СписокПересчетов);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьОтчетТоварыВПроцессеОтгрузки(Команда)
	ПараметрыФормы = Новый Структура("КлючПользовательскихНастроек, КлючНазначенияИспользования, КлючВарианта, СформироватьПриОткрытии, Отбор, НеИспользоватьФиксированныеНастройки");
	ПараметрыФормы.КлючПользовательскихНастроек	= "ПересчетТоваровФормаСписка";
	ПараметрыФормы.КлючНазначенияИспользования	= "ПересчетТоваровФормаСписка";
	ПараметрыФормы.КлючВарианта					= "ОтгружаемыеТовары";
	ПараметрыФормы.СформироватьПриОткрытии		= Истина;
	ПараметрыФормы.Отбор						= Новый Структура;
	Если Не Склад.Пустая() Тогда
		ПараметрыФормы.Отбор.Вставить("Склад",		Склад);
	КонецЕсли;
	Если Не Помещение.Пустая() Тогда
		ПараметрыФормы.Отбор.Вставить("Помещение",	Помещение);
	КонецЕсли;

	ОткрытьФорму("Отчет.ВедомостьПоТоварамНаСкладах.Форма", ПараметрыФормы, ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСписок(Команда)
	Если Элементы.СтраницыИнформацияОРасхождениях.Видимость Тогда
		ОбновитьСписокНаСервере();
	Иначе
		Элементы.СписокПересчетов.Обновить();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Функция СсылкаНаЭлементСпискаПоШтрихкоду(Штрихкод)
	
	Менеджеры = Новый Массив();
	Менеджеры.Добавить(ПредопределенноеЗначение("Документ.ПересчетТоваров.ПустаяСсылка"));
	Возврат ШтрихкодированиеПечатныхФормКлиент.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(Штрихкод, Менеджеры);
	
КонецФункции

&НаКлиенте
Процедура ОбработатьШтрихкоды(Данные)
	
	МассивСсылок = СсылкаНаЭлементСпискаПоШтрихкоду(Данные.Штрихкод);
	Если МассивСсылок.Количество() > 0 Тогда
		Элементы.СписокПересчетов.ТекущаяСтрока = МассивСсылок[0];
		ПоказатьЗначение(,МассивСсылок[0]);
	Иначе
		ШтрихкодированиеПечатныхФормКлиент.ОбъектНеНайден(Данные.Штрихкод);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура УстановитьВидимостьПомещения()
	
	Если СкладыСервер.ИспользоватьСкладскиеПомещения(Склад) Тогда
		Элементы.ГруппаФильтры.ТекущаяСтраница = Элементы.ГруппаФильтрыСПомещением;
		Элементы.СписокПересчетовПомещение.Видимость = Истина;
	Иначе
		Элементы.ГруппаФильтры.ТекущаяСтраница = Элементы.ГруппаФильтрыБезПомещения;
		Элементы.СписокПересчетовПомещение.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьСтраницКоманднойПанелиСписокПересчетов()
	
	Если СкладыСервер.ИспользоватьАдресноеХранение(Склад, Помещение) Тогда
		Элементы.ГруппаСтраницыШапкаЗаданияНаПересчет.ТекущаяСтраница = Элементы.ГруппаСтраницаШапкаЗаданияНаПересчетАдресныйСклад;
	Иначе
		Элементы.ГруппаСтраницыШапкаЗаданияНаПересчет.ТекущаяСтраница = Элементы.ГруппаСтраницаШапкаЗаданияНаПересчетНеадресныйСклад;
	КонецЕсли;	
	
КонецПроцедуры

&НаСервере
Функция ОбработкаВыбораИсполнителяНаСервере(ВыбранноеЗначение)
			
	Запрос = Новый Запрос;
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПересчетТоваров.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Исполнитель <> &Исполнитель
	|	И ПересчетТоваров.Ссылка В(&МассивДокументов)";
	
	Запрос.УстановитьПараметр("Исполнитель", ВыбранноеЗначение);
	Запрос.УстановитьПараметр("МассивДокументов", СписокВыделенныхДокументов);
		
	ВыборкаДокументов = Запрос.Выполнить().Выбрать();
	
	МассивИзмененныхДокументов = Новый Массив;
	
	Пока ВыборкаДокументов.Следующий() Цикл
		
		Попытка
			ДокументОбъект = ВыборкаДокументов.Ссылка.ПолучитьОбъект();
			
			ДокументОбъект.Заблокировать();
			
			ДокументОбъект.Исполнитель = ВыбранноеЗначение;
			
			ДокументОбъект.Записать(РежимЗаписиДокумента.Запись);
			МассивИзмененныхДокументов.Добавить(ВыборкаДокументов.Ссылка);
		Исключение
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		КонецПопытки;
		
	КонецЦикла;
	
	Элементы.СписокПересчетов.Обновить();
		
	Возврат МассивИзмененныхДокументов;

КонецФункции

&НаКлиенте
Процедура СкладОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если Склад = ВыбранноеЗначение тогда
		СтандартнаяОбработка = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаСервереБезКонтекста
Функция ЕстьРасхождения(Склад)
	Возврат Обработки.ПомощникОформленияСкладскихАктов.ПолучитьСписокТоваровКОформлениюСкладскихАктов(Склад).Количество() > 0;
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьИнформациюОРасхождениях(Элементы, Склад)
	
	Если Не ЗначениеЗаполнено(Склад) Тогда
		Элементы.СтраницыИнформацияОРасхождениях.ТекущаяСтраница = Элементы.СтраницаНетСклада;
	ИначеЕсли ЕстьРасхождения(Склад) Тогда
		Элементы.СтраницыИнформацияОРасхождениях.ТекущаяСтраница = Элементы.СтраницаЕстьРасхождения;
	Иначе
		Элементы.СтраницыИнформацияОРасхождениях.ТекущаяСтраница = Элементы.СтраницаНетРасхождений; 
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДекорацияЕстьРасхожденияОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)

	СтандартнаяОбработка = Ложь;
	
	ПараметрыФормы = Новый Структура("Склад", Склад);
	
	ОткрытьФорму("Обработка.ПомощникОформленияСкладскихАктов.Форма", ПараметрыФормы, ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура СтатусПриИзмененииНаКлиентеНаСервере(Форма)
	Если Форма.Статус = "Все" Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЭлементыГруппыОтбораДинамическогоСписка(Форма.СписокПересчетов, "Статус");
	ИначеЕсли Форма.Статус = "ТолькоНевыполненные" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Форма.СписокПересчетов, 
			"Статус", 
			ПредопределенноеЗначение("Перечисление.СтатусыПересчетовТоваров.Выполнено"), 
			ВидСравненияКомпоновкиДанных.НеРавно,, 
			Истина);	
	ИначеЕсли Форма.Статус = "ВРаботе" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Форма.СписокПересчетов, 
			"Статус", 
			ПредопределенноеЗначение("Перечисление.СтатусыПересчетовТоваров.ВРаботе"), 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);		
	ИначеЕсли Форма.Статус = "ВнесениеРезультатов" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Форма.СписокПересчетов, 
			"Статус", 
			ПредопределенноеЗначение("Перечисление.СтатусыПересчетовТоваров.ВнесениеРезультатов"), 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);
	ИначеЕсли Форма.Статус = "Выполнено" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Форма.СписокПересчетов, 
			"Статус", 
			ПредопределенноеЗначение("Перечисление.СтатусыПересчетовТоваров.Выполнено"), 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);
	ИначеЕсли Форма.Статус = "Подготовлено" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Форма.СписокПересчетов, 
			"Статус", 
			ПредопределенноеЗначение("Перечисление.СтатусыПересчетовТоваров.Подготовлено"), 
			ВидСравненияКомпоновкиДанных.Равно,, 
			Истина);
	Иначе
		Форма.Статус = "Все";
		ОбщегоНазначенияКлиентСервер.УдалитьЭлементыГруппыОтбораДинамическогоСписка(Форма.СписокПересчетов, "Статус");
	КонецЕсли;	
КонецПроцедуры

&НаСервере 
Процедура УстановитьВидимостьКомандКонтекстныхОтчетов()
	КоличествоДоступныхОтчетов = 0;
	
	Если ПравоДоступа("Просмотр", Метаданные.Отчеты.ВедомостьПоТоварамНаСкладах) Тогда
		Элементы.ОткрытьОтчетТоварыВПроцессеОтгрузки.Видимость = Истина;
		КоличествоДоступныхОтчетов = КоличествоДоступныхОтчетов + 1;
	Иначе
		Элементы.ОткрытьОтчетТоварыВПроцессеОтгрузки.Видимость = Ложь;
	КонецЕсли;
	
	Если КоличествоДоступныхОтчетов > 0 Тогда
		Элементы.СтраницыКонтекстныеОтчеты.ТекущаяСтраница = Элементы.СтраницаДоступныеОтчеты;
	Иначе
		Элементы.СтраницыКонтекстныеОтчеты.ТекущаяСтраница = Элементы.СтраницаНетДоступныхОтчетов;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОбновитьСписокНаСервере()
	ОбновитьИнформациюОРасхождениях(Элементы, Склад);
	Элементы.СписокПересчетов.Обновить();
КонецПроцедуры

#КонецОбласти

#КонецОбласти
