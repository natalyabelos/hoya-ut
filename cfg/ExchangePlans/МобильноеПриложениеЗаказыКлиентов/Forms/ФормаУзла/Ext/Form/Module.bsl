﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		Объект.ПрефиксДляДанныхМобильногоУстройства = 
			МобильноеПриложениеЗаказыКлиентовПереопределяемый.НовыйПрефиксДляДанныхМобильногоУстройства();
	КонецЕсли;
	ПриЧтенииСозданииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	НастройкаКомпоновки = ТекущиеНастройкиКомпоновкиСервер();
	ТекущийОбъект.НастройкиОбмена = Новый ХранилищеЗначения(НастройкаКомпоновки, Новый СжатиеДанных(9));
	
	Если ЗначениеЗаполнено(ТекущийОбъект.Ссылка) И ЗарегистрироватьИзмененияДляВсехПартнеров Тогда
		СообщениеОбмена = "";
		МобильноеПриложениеЗаказыКлиентовПереопределяемый.ЗарегистрироватьИзмененияКлиентовДляПланаОбмена(ТекущийОбъект.Ссылка,
			Истина, "", Отказ, СообщениеОбмена);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриЧтенииСозданииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если ЗначениеЗаполнено(Объект.Пользователь) Тогда
		ПроверитьПользователяМобильногоПриложения();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если Булево(РегистрироватьОплатыПоПлатежнымКартам) И
		НЕ ЗначениеЗаполнено(Объект.ЭквайринговыйТерминал) Тогда
		ТекстСообщения =
			НСтр("ru = 'Укажите эквайринговый терминал для создания документов ""Эквайринговая операция""'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения, Объект.Ссылка, "ЭквайринговыйТерминал", "ЭквайринговыйТерминал", Отказ);
	КонецЕсли;
	
	Если Объект.РегистрироватьНаличные И
		НЕ Булево(РегистрироватьОплатыПоДоверенностям) И
		НЕ ЗначениеЗаполнено(Объект.Касса) Тогда
		ТекстСообщения =
			НСтр("ru = 'Укажите кассу для создания документов ""Приходный кассовый ордер""'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения, Объект.Ссылка, "Касса", "Касса", Отказ);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийШапкиФормы

&НаКлиенте
Процедура ТаблицаНастроекКомпоновкиПриИзменении(Элемент)
	
	ТекущаяНастройка = Элементы.ТаблицаНастроекКомпоновки.ТекущиеДанные;
	Если ТекущаяНастройка = Неопределено Тогда
		Возврат;
	КонецЕсли;
	Модифицированность = Истина;
	ТекущаяНастройкаНастройка = ТекущаяНастройка.Настройка;
	Если ТекущаяНастройкаНастройка = "Сегмент партнеров"
		ИЛИ ТекущаяНастройкаНастройка = "Организация"
		ИЛИ ТекущаяНастройкаНастройка = "Соглашение" Тогда
		ЗарегистрироватьИзмененияДляВсехПартнеров = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура РегистрироватьОплатыПоПлатежнымКартамПриИзменении(Элемент)
	
	Если НЕ Булево(РегистрироватьОплатыПоПлатежнымКартам) Тогда
		Объект.ЭквайринговыйТерминал = "";
		Объект.СтатьяДДСЭквайринг = "";
	КонецЕсли;
	УстановитьВидимостьДоступность();
	ПроверитьПользователяМобильногоПриложения();
КонецПроцедуры

&НаКлиенте
Процедура РегистрироватьНаличныеПриИзменении(Элемент)
	
	Объект.РегистрироватьНаличные = Булево(РегистрироватьОплатыНаличными);
	Если НЕ Объект.РегистрироватьНаличные Тогда
		Объект.Касса = "";
		Объект.СтатьяДДСНаличные = "";
		РегистрироватьОплатыПоДоверенностям = 0;
	КонецЕсли;
	УстановитьВидимостьДоступность();
	ПроверитьПользователяМобильногоПриложения();
КонецПроцедуры

&НаКлиенте
Процедура РегистрироватьОплатыПоДоверенностямПриИзменении(Элемент)
	
	Если Булево(РегистрироватьОплатыПоДоверенностям) Тогда
		Объект.Касса = "";
		Объект.СтатьяДДСНаличные = "";
	КонецЕсли;
	УстановитьВидимостьДоступность();
	ПроверитьПользователяМобильногоПриложения();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СтандартныеНастройки(Команда)
	
	Модифицированность = Истина;
	ЗагрузитьСтандартныеНастройки();
КонецПроцедуры

&НаКлиенте
Процедура ОтвязатьУстройство(Команда)
	
	ОтвязатьУстройствоНаСервере();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	Заголовок = Нстр("ru = 'Настройка синхронизации для пользователя мобильного приложения ""1С:Заказы""'");
	ИспользоватьОплатуПлатежнымиКартами = ПолучитьФункциональнуюОпцию("ИспользоватьОплатуПлатежнымиКартами");
	РегистрироватьОплатыПоПлатежнымКартам = Число(ЗначениеЗаполнено(Объект.ЭквайринговыйТерминал));
	
	ИспользоватьДоверенности = ПолучитьФункциональнуюОпцию("ИспользоватьДоверенностиНаПолучениеТМЦ");
	РегистрироватьОплатыПоДоверенностям = Число(Объект.РегистрироватьНаличные И Не ЗначениеЗаполнено(Объект.Касса));
	РегистрироватьОплатыНаличными = Число(Объект.РегистрироватьНаличные);
	УстановитьВидимостьДоступность();
	ИнициализироватьКомпоновщикСервер();
КонецПроцедуры

&НаСервере
Процедура УстановитьОтборыКомпоновщикаПоФункциональнымОпциям()
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСегментыНоменклатуры") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"СегментНоменклатуры");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСегментыПартнеров") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"СегментПартнеров");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"Организация");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоВидовЦен") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"ВидЦены");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоСкладов") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"Склад");
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСоглашенияСКлиентами") Тогда
		КомпоновкаДанныхСервер.УдалитьЭлементОтбораИзВсехНастроекОтчета(КомпоновщикНастроекКомпоновкиДанных,
			"Соглашение");
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ИнициализироватьКомпоновщикСервер()
	
	СхемаВыгрузкиДанных = ПланыОбмена.МобильноеПриложениеЗаказыКлиентов.ПолучитьМакет("СхемаКомпоновкиНастроекОбмена");
	АдресСхемы = ПоместитьВоВременноеХранилище(СхемаВыгрузкиДанных, УникальныйИдентификатор);
	КомпоновщикНастроекКомпоновкиДанных.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемы)); 
	
	НастройкаКомпоновки = Объект.Ссылка.НастройкиОбмена.Получить();
	
	Если НастройкаКомпоновки = Неопределено Тогда
		КомпоновщикНастроекКомпоновкиДанных.ЗагрузитьНастройки(СхемаВыгрузкиДанных.НастройкиПоУмолчанию);
	Иначе
		КомпоновщикНастроекКомпоновкиДанных.ЗагрузитьНастройки(НастройкаКомпоновки);
		КомпоновщикНастроекКомпоновкиДанных.Восстановить(СпособВосстановленияНастроекКомпоновкиДанных.ПроверятьДоступность);
	КонецЕсли;
	
	УстановитьОтборыКомпоновщикаПоФункциональнымОпциям();
КонецПроцедуры

&НаСервере
Функция ТекущиеНастройкиКомпоновкиСервер()
	
	Возврат КомпоновщикНастроекКомпоновкиДанных.ПолучитьНастройки();
КонецФункции

&НаСервере
Процедура ЗаполнитьНастройкиКомпоновщика(СхемаКомпоновки, НастройкиКомпоновки)
	
	Адрес = Новый УникальныйИдентификатор();
	URLСхемы = ПоместитьВоВременноеХранилище(СхемаКомпоновки, Адрес);
	
	ИсточникНастроек = Новый ИсточникДоступныхНастроекКомпоновкиДанных(URLСхемы);
	КомпоновщикНастроекКомпоновкиДанных.Инициализировать(ИсточникНастроек);
	КомпоновщикНастроекКомпоновкиДанных.ЗагрузитьНастройки(НастройкиКомпоновки);
КонецПроцедуры	

&НаСервере
Процедура ЗагрузитьСтандартныеНастройки()
	
	СхемаКомпоновки = ПланыОбмена.МобильноеПриложениеЗаказыКлиентов.ПолучитьМакет("СхемаКомпоновкиНастроекОбмена");
	НастройкиКомпоновки = СхемаКомпоновки.НастройкиПоУмолчанию;
	ЗаполнитьНастройкиКомпоновщика(СхемаКомпоновки, НастройкиКомпоновки);
	КомпоновщикНастроекКомпоновкиДанных.ЗагрузитьПользовательскиеНастройки(
		КомпоновщикНастроекКомпоновкиДанных.ПользовательскиеНастройки);
	
	УстановитьОтборыКомпоновщикаПоФункциональнымОпциям();
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ГруппаПлатежныеКарты.Видимость = ИспользоватьОплатуПлатежнымиКартами;
	Элементы.ЭквайринговыйТерминал.Доступность = Булево(РегистрироватьОплатыПоПлатежнымКартам);
	Элементы.ЭквайринговыйТерминал.АвтоОтметкаНезаполненного = ИспользоватьОплатуПлатежнымиКартами 
		И Булево(РегистрироватьОплатыПоПлатежнымКартам);
	Элементы.ЭквайринговыйТерминал.ОтметкаНезаполненного = ЗначениеЗаполнено(Объект.ЭквайринговыйТерминал);
	Элементы.СтатьяДДСЭквайринг.Доступность = Булево(РегистрироватьОплатыПоПлатежнымКартам);
	
	Элементы.РегистрироватьОплатыПоДоверенностям.Доступность = Объект.РегистрироватьНаличные И ИспользоватьДоверенности;
	Элементы.РегистрироватьОплатыПоКассе.Доступность = Объект.РегистрироватьНаличные;
	Элементы.Касса.Доступность = Объект.РегистрироватьНаличные И НЕ Булево(РегистрироватьОплатыПоДоверенностям);
	Элементы.Касса.АвтоОтметкаНезаполненного = Объект.РегистрироватьНаличные
		И НЕ Булево(РегистрироватьОплатыПоДоверенностям);
	Элементы.Касса.ОтметкаНезаполненного = ЗначениеЗаполнено(Объект.Касса);
	Элементы.СтатьяДДСНаличные.Доступность = Объект.РегистрироватьНаличные
		И НЕ Булево(РегистрироватьОплатыПоДоверенностям);
	Элементы.ГруппаКомментарийОплатаПоДоверенностям.Видимость = НЕ ИспользоватьДоверенности;
КонецПроцедуры

&НаСервере
Функция ПроверкаПользователяМобильногоПриложения(Пользователь)
	
	Возврат МобильноеПриложениеЗаказыКлиентовПереопределяемый.ПроверкаПользователяМобильногоПриложения(Пользователь);
КонецФункции

&НаКлиенте
Процедура ПроверитьПользователяМобильногоПриложения()
	
	ПроверкаПользователя = ПроверкаПользователяМобильногоПриложения(Объект.Пользователь);
	ТекстСообщения = "";
	Если НЕ ПроверкаПользователя.ПользовательМобильногоПриложения Тогда
		ТекстСообщения = ТекстСообщения
		+ НСтр("ru = 'Пользователь не входит в группу пользователей мобильного приложения.'") + Символы.ПС;
	КонецЕсли;
	Если Объект.РегистрироватьНаличные И НЕ Булево(РегистрироватьОплатыПоДоверенностям)
			И Не ПроверкаПользователя.Касса Тогда
		ТекстСообщения = ТекстСообщения
		+ НСтр("ru = 'У пользователя недостаточно прав для создания документов ""Приходный кассовый ордер"".'") + Символы.ПС;
	КонецЕсли;
	Если Булево(РегистрироватьОплатыПоПлатежнымКартам) И Не ПроверкаПользователя.ПлатежнаяКарта Тогда
		ТекстСообщения = ТекстСообщения
		+ НСтр("ru = 'У пользователя недостаточно прав для создания документов ""Эквайринговая операция"".'");
	КонецЕсли;
	Если ЗначениеЗаполнено(ТекстСообщения) Тогда
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = ТекстСообщения;
		Сообщение.Поле = СокрЛП("Пользователь");
		Сообщение.УстановитьДанные(Объект.Пользователь);
		Сообщение.Сообщить();
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОтвязатьУстройствоНаСервере()
	
	Объект.Код = СокрЛП(Объект.Пользователь.УникальныйИдентификатор());
КонецПроцедуры

#КонецОбласти