﻿////////////////////////////////////////////////////////////////////////////////
// Общий серверный модуль подсистемы БИТ.Phone
// Содержит функции, не зависящие от конфигурации
////////////////////////////////////////////////////////////////////////////////

Функция ПолучитьВерсиюБИТфон() ЭКСПОРТ
	Возврат "1.2.22.79";
КонецФункции

Функция ПолучитьВерсиюСофтфона() ЭКСПОРТ
	Возврат "2.1.196";
КонецФункции

//---------------------------------------------------------------------------------------------------------
Функция ЗаписатьОшибкуВЖурналРегистрации(стрОшибка, стрОписание) ЭКСПОРТ
	ЗаписьЖурналаРегистрации("БИТ.Phone", УровеньЖурналаРегистрации.Ошибка, , стрОшибка, стрОписание);
КонецФункции

Функция ПолучитьТекущегоПользователя() ЭКСПОРТ
	//ПользовательБД = ИмяПользователя();
	//Пользователь = Справочники.Пользователи.НайтиПоКоду(ПользовательБД);
	Пользователь = ПараметрыСеанса.ТекущийПользователь;
	Возврат Пользователь;
КонецФункции

// Возвращаемое значение - булево,
//   Истина если в регистре сведений есть настройки для текущего пользователя
Функция ПроверитьНастройкиТекущегоПользователя() ЭКСПОРТ
	Запрос = Новый Запрос();
	Запрос.Текст = "ВЫБРАТЬ
	               |	бтНастройки.АдресСервера,
	               |	бтНастройки.Логин,
	               |	бтНастройки.Пароль
	               |ИЗ
	               |	РегистрСведений.бтНастройки КАК бтНастройки
	               |ГДЕ
	               |	бтНастройки.Пользователь = &Пользователь";
	Запрос.УстановитьПараметр("Пользователь", ПолучитьТекущегоПользователя());
	резт = Запрос.Выполнить();
	Если резт.Пустой() Тогда
		Возврат Ложь;
	КонецЕсли;
	табл = резт.Выгрузить();
	строкаРезт = табл[0];
	настроен = ЗначениеЗаполнено(строкаРезт.АдресСервера) И ЗначениеЗаполнено(строкаРезт.Логин);
	Возврат настроен;
КонецФункции

Процедура ЗаполнитьНастройкиПоУмолчаниюПользователя(настройки, ПользовательСсылка)
	настройки.Пользователь					= ПользовательСсылка;
	настройки.Протокол						= Перечисления.бтПротоколы.UDP;
	настройки.ИнтервалПеререгистрации		= 300;
	настройки.АвтоопределениеNAT			= Истина;
	настройки.ФорматЗаписи					= Перечисления.бтФорматЗаписи.wav;
	настройки.КомандаПереадресации			= "#9";
	настройки.КомандаОтправкиФакса			= "#7";
	настройки.ГлубинаИсторииЗвонков			= 30;
	настройки.ПрефиксВыходаНаВнешнююЛинию	= "9";
	настройки.КомандаНеБеспокоить			= "#78";
	настройки.КомандаОтменаНеБеспокоить		= "#79";
	настройки.СерверЛицензийАдрес			= "127.0.0.1";
	настройки.СерверЛицензийПорт			= 10601;
	настройки.ОтправкаСМСОтправитель		= "SMS-TEST";
	настройки.УровеньЛоггирования			= 4;
	настройки.РазворачиватьОкноПриВходящемЗвонке = Истина;
	настройки.ТипПодключенияСтатусов		= Перечисления.бтТипПодключенияСтатуса.НеИспользовать;
	настройки.ТипПриемаЗвонка				= Перечисления.бтТипПриемаЗвонка.ПриниматьВручную;
	настройки.ТипПереводаЗвонка				= Перечисления.бтТипПереводаЗвонка.Условный;
	настройки.RTPПорт						= 4000;
КонецПроцедуры

Процедура УстановитьНастройку(стрНастройка, значениеНастройки)
	ПользовательЗап =  БИТфонСервер.ПолучитьТекущегоПользователя();
	зап = РегистрыСведений.бтНастройки.СоздатьМенеджерЗаписи();
	зап.Пользователь = ПользовательЗап;
	зап.Прочитать();
	обновитьЗапись = Ложь;
	Если НЕ зап.Выбран() Тогда
		ЗаполнитьНастройкиПоУмолчаниюПользователя(зап, ПользовательЗап);
		обновитьЗапись = Истина;
	КонецЕсли;
	Если зап[стрНастройка] <> значениеНастройки Тогда
		зап[стрНастройка] = значениеНастройки;
		обновитьЗапись = Истина;
	КонецЕсли;
	Если обновитьЗапись Тогда
		зап.Записать();
	КонецЕсли;
КонецПроцедуры

Функция ДоступнаРольБитФонДляПользователя() ЭКСПОРТ
	Возврат (РольДоступна(Метаданные.Роли.ПользовательБИТфон) ИЛИ РольДоступна("ПолныеПрава"));
КонецФункции

Функция ПолучитьПараметрыСоединения() ЭКСПОРТ
	Пользователь = ПолучитьТекущегоПользователя();
	СтруктураОтбора = Новый Структура("Пользователь", Пользователь);
	Возврат РегистрыСведений.бтНастройки.Получить(СтруктураОтбора);
КонецФункции

Функция ПолучитьДиректориюОтправкиФаксов() ЭКСПОРТ
	настрFTPДиректория = ПолучитьПараметрыСоединения().ДиректорияОтправкиФаксов;
	Возврат настрFTPДиректория;
КонецФункции

Функция ПолучитьПользователяОтправкиФаксов() ЭКСПОРТ
	настрFTPПользователь = ПолучитьПараметрыСоединения().ПользовательОтправкиФаксов;
	Возврат настрFTPПользователь;
КонецФункции

Функция ПолучитьПарольОтправкиФаксов() ЭКСПОРТ
	настрFTPПароль = ПолучитьПараметрыСоединения().ПарольОтправкиФаксов;
	Возврат настрFTPПароль;
КонецФункции

Функция ПолучитьДиректориюЗаписанныхФайлов() ЭКСПОРТ
	настрДиректория = ПолучитьПараметрыСоединения().ДиректорияЗаписанныхФайлов;
	Возврат настрДиректория;
КонецФункции

Функция ПолучитьФорматЗаписи() ЭКСПОРТ
	форматЗап = ПолучитьПараметрыСоединения().ФорматЗаписи;
	Возврат форматЗап;
КонецФункции

Функция ПолучитьКомандуПереадресации() ЭКСПОРТ
	команда = ПолучитьПараметрыСоединения().КомандаПереадресации;
	Возврат команда;
КонецФункции

Функция ПолучитьКомандуОтправкиФакса() ЭКСПОРТ
	команда = ПолучитьПараметрыСоединения().КомандаОтправкиФакса;
	Возврат команда;
КонецФункции

Функция ПолучитьГлубинуИсторииЗвонков() ЭКСПОРТ
	глубина = ПолучитьПараметрыСоединения().ГлубинаИсторииЗвонков;
	Возврат глубина;
КонецФункции

Функция ПолучитьПрефиксВыходаНаВнешнююЛинию() ЭКСПОРТ
	префикс = ПолучитьПараметрыСоединения().ПрефиксВыходаНаВнешнююЛинию;
	Возврат префикс;
КонецФункции

Функция ПолучитьФлагИспользоватьПрямойНабор() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ИспользоватьПрямойНабор;
	Возврат флаг;
КонецФункции

Функция ПолучитьКомандуНеБеспокоить() ЭКСПОРТ
	команда = ПолучитьПараметрыСоединения().КомандаНеБеспокоить;
	Возврат команда;
КонецФункции

Функция ПолучитьКомандуОтменаНеБеспокоить() ЭКСПОРТ
	команда = ПолучитьПараметрыСоединения().КомандаОтменаНеБеспокоить;
	Возврат команда;
КонецФункции

Функция ПолучитьАдресСервераЛицензий() ЭКСПОРТ
	адрес = ПолучитьПараметрыСоединения().СерверЛицензийАдрес;
	Возврат адрес;
КонецФункции

Функция ПолучитьПортСервераЛицензий() ЭКСПОРТ
	порт = ПолучитьПараметрыСоединения().СерверЛицензийПорт;
	Возврат порт;
КонецФункции

Функция ПолучитьУровеньЛоггирования() ЭКСПОРТ
	уровеньЛог = ПолучитьПараметрыСоединения().УровеньЛоггирования;
	Возврат уровеньЛог;
КонецФункции

Функция ПолучитьФлагСоздаватьСобытиеВходящийЗвонок() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().СоздаватьСобытиеПриВходящемЗвонке;
	Возврат флаг;
КонецФункции

Функция ПолучитьФлагСоздаватьСобытиеИсходящийЗвонок() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().СоздаватьСобытиеПриИсходящемЗвонке;
	Возврат флаг;
КонецФункции

Функция ПолучитьФлагСоздаватьСобытияПриВнутреннихЗвонках() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().СоздаватьСобытияПриВнутреннихЗвонках;
	Возврат флаг;
КонецФункции

Функция ПолучитьФлагВсегдаЗаписыватьРазговор() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ВсегдаЗаписыватьРазговор;
	Возврат флаг;
КонецФункции

Процедура ПолучитьПараметрыОтправкиСМС(стрСМСЛогин, стрСМСПароль) ЭКСПОРТ
	параметрыСоед = ПолучитьПараметрыСоединения();
	стрСМСЛогин = параметрыСоед.ОтправкаСМСЛогин;
	стрСМСПароль = параметрыСоед.ОтправкаСМСПароль;
КонецПроцедуры

Функция ПолучитьОтправителяСМСПоУмолчанию() ЭКСПОРТ
	отправитель = ПолучитьПараметрыСоединения().ОтправкаСМСОтправитель;
	Возврат отправитель;
КонецФункции

Функция ПолучитьСтационарныйТелефон() ЭКСПОРТ
	стрСтацТел = ПолучитьПараметрыСоединения().СтационарныйТелефон;
	Возврат стрСтацТел;
КонецФункции

Функция ПолучитьФлагИспользоватьСтационарныйТелефон() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ИспользоватьСтационарныйТелефон;
	Возврат флаг;
КонецФункции

Процедура УстановитьФлагИспользоватьСтационарныйТелефон(использовать) ЭКСПОРТ
	УстановитьНастройку("ИспользоватьСтационарныйТелефон", использовать);
КонецПроцедуры

Функция ПолучитьФлагАвтостартаПриЗапускеСистемы() ЭКСПОРТ
	Если НЕ БИТфонСерверУТ.ЕстьВозможностьАвтозапуска() Тогда
		Возврат Ложь;
	КонецЕсли;
	флаг = ПолучитьПараметрыСоединения().АвтозапускПриСтартеСистемы;
	Возврат флаг;
КонецФункции	

Функция ПолучитьСвойНомер() ЭКСПОРТ
	номер = ПолучитьПараметрыСоединения().СвойНомер;
	Возврат номер;
КонецФункции  

Функция ПолучитьФлагИнтеграцииСБИТАТС() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ИнтеграцияСБИТАТС;
	Возврат флаг;
КонецФункции  

Функция ПолучитьФлагРазворачиватьОкноПриВходящемЗвонке() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().РазворачиватьОкноПриВходящемЗвонке;
	Возврат флаг;
КонецФункции  

Функция ПолучитьФлагОткрыватьКартуЯндексПриВходящемЗвонке() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ОткрыватьКартуЯндексПриВходящемЗвонке;
	Возврат флаг;
КонецФункции  

Функция ПолучитьВерсиюСофтфонаТекущегоПользователя() ЭКСПОРТ
	версия = ПолучитьПараметрыСоединения().ВерсияСофтфона;
	Возврат версия;
КонецФункции

Процедура УстановитьВерсиюСофтфонаТекущегоПользователя(стрВерсия) ЭКСПОРТ
	УстановитьНастройку("ВерсияСофтфона", стрВерсия);
КонецПроцедуры

Функция ПолучитьАстерискШпионЛогин() ЭКСПОРТ
	логин = "";
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	               |	бтЛогиныАстериск.SIPлогин
	               |ИЗ
	               |	Справочник.бтВнутренниеНомераАстериск КАК бтВнутренниеНомераАстериск
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.бтЛогиныАстериск КАК бтЛогиныАстериск
	               |		ПО бтВнутренниеНомераАстериск.Ссылка = бтЛогиныАстериск.ВнутреннийНомер
	               |ГДЕ
	               |	бтВнутренниеНомераАстериск.Владелец = &Владелец";
	Запрос.УстановитьПараметр("Владелец", ПолучитьТекущегоПользователя());
	табл = Запрос.Выполнить().Выгрузить();
	Если табл.Количество() > 0 Тогда
		логин = табл[0].SIPлогин;
	КонецЕсли;
	Возврат логин;
КонецФункции  

Функция ПолучитьАстерискКонтекст() ЭКСПОРТ
	контекст = ПолучитьПараметрыСоединения().АстерискКонтекст;
	Возврат контекст;
КонецФункции  

Функция ПолучитьТипПодключенияСтатусаАбонентов() ЭКСПОРТ
	тип = ПолучитьПараметрыСоединения().ТипПодключенияСтатусов;
	Возврат тип;
КонецФункции 

Функция ПолучитьФлагНеИскатьКонтрагента() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().НеИскатьКонтрагента;
	Возврат флаг;
КонецФункции  

Функция ПолучитьФлагПриниматьНесколькоВходящих() ЭКСПОРТ
	флаг = ПолучитьПараметрыСоединения().ПриниматьНесколькоВходящих;
	Возврат флаг;
КонецФункции  

Функция ПолучитьТипПриемаЗвонка() ЭКСПОРТ
	тип = ПолучитьПараметрыСоединения().ТипПриемаЗвонка;
	Если НЕ ЗначениеЗаполнено(тип) Тогда
		тип = Перечисления.бтТипПриемаЗвонка.ПриниматьВручную;
		УстановитьНастройку("ТипПриемаЗвонка", тип);
	КонецЕсли;
	Возврат тип;
КонецФункции 

Функция ПолучитьТипПереводаЗвонка() ЭКСПОРТ
	тип = ПолучитьПараметрыСоединения().ТипПереводаЗвонка;
	Если НЕ ЗначениеЗаполнено(тип) Тогда
		тип = Перечисления.бтТипПереводаЗвонка.Условный;
		УстановитьНастройку("ТипПереводаЗвонка", тип);
	КонецЕсли;
	Возврат тип;
КонецФункции 

Функция ПолучитьСписокКодеков() ЭКСПОРТ
	список = ПолучитьПараметрыСоединения().СписокКодеков;
	Возврат список;
КонецФункции 

Процедура УстановитьСписокКодеков(стрСписок) ЭКСПОРТ
	УстановитьНастройку("СписокКодеков", стрСписок);
КонецПроцедуры

//---------------------------------------------------------------------------------------------------------
//************ Работа с историей звонков *****************

Процедура ДобавитьВИсториюВходящие(НомерВходящий, Абонент, КонтактноеЛицо, Успешность, датаНачалоРазговора, Длительность, стрПутьКЗаписи) ЭКСПОРТ
	запись = РегистрыСведений.бтИсторияЗвонков.СоздатьМенеджерЗаписи();
	запись.Пользователь	= БИТфонСервер.ПолучитьТекущегоПользователя();
	запись.Дата			= датаНачалоРазговора;
	запись.Номер		= НомерВходящий;
	запись.Абонент		= Абонент;
	запись.ТипЗвонка	= Перечисления.бтТипЗвонка.Входящий;
	запись.Успешность	= Успешность;
	запись.КонтактноеЛицо		= КонтактноеЛицо;
	запись.ДлительностьЗвонка	= Длительность;
	запись.ЗаписьРазговора		= стрПутьКЗаписи;
	запись.Записать();
КонецПроцедуры

Процедура ДобавитьВИсториюИсходящие(НомерИсходящий, Абонент, КонтактноеЛицо, датаНачалоРазговора, Длительность, стрПутьКЗаписи) ЭКСПОРТ
	запись = РегистрыСведений.бтИсторияЗвонков.СоздатьМенеджерЗаписи();
	запись.Пользователь	= ПолучитьТекущегоПользователя();
	запись.Дата			= датаНачалоРазговора;
	запись.Номер		= НомерИсходящий;
	запись.ТипЗвонка	= Перечисления.бтТипЗвонка.Исходящий;
	запись.Успешность	= Истина;
	запись.Абонент		= Абонент;
	запись.КонтактноеЛицо		= КонтактноеЛицо;
	запись.ДлительностьЗвонка	= Длительность;
	запись.ЗаписьРазговора		= стрПутьКЗаписи;
	запись.Записать();
КонецПроцедуры

//---------------------------------------------------------------------------------------------------------
Функция ПолучитьНомерДляОператора(стрНомер, контрагент, контактноеЛицо) ЭКСПОРТ
	стрНомер = "";
	
	структОтбор = Новый Структура("Пользователь", ПолучитьТекущегоПользователя());
	выборка = РегистрыСведений.бтСпискиОбзвона.Выбрать(структОтбор);
	Если НЕ выборка.Следующий() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	СписокОбзвонаТекущий = выборка.Список;
	Если НЕ ЗначениеЗаполнено(СписокОбзвонаТекущий) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	НачатьТранзакцию();
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	               |	бтСписокОбзвонаСписок.Ссылка,
	               |	бтСписокОбзвонаСписок.НомерСтроки,
	               |	бтСписокОбзвонаСписок.НомерТелефона,
	               |	бтСписокОбзвонаСписок.Контрагент,
	               |	бтСписокОбзвонаСписок.КонтактноеЛицо,
	               |	бтСписокОбзвонаСписок.Обработан
	               |ИЗ
	               |	Документ.бтСписокОбзвона.Список КАК бтСписокОбзвонаСписок
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.бтСписокОбзвона КАК бтСписокОбзвона
	               |		ПО бтСписокОбзвонаСписок.Ссылка = бтСписокОбзвона.Ссылка
	               |ГДЕ
	               |	бтСписокОбзвона.Ссылка = &Ссылка
	               |	И бтСписокОбзвонаСписок.Обработан = ЛОЖЬ
	               |
	               |ДЛЯ ИЗМЕНЕНИЯ
	               |	Документ.бтСписокОбзвона";
	Запрос.УстановитьПараметр("Ссылка", СписокОбзвонаТекущий);
	выгрузка = Запрос.Выполнить().Выбрать();
	Если выгрузка.Количество() = 0 Тогда
		ОтменитьТранзакцию();
		Возврат Истина;
	КонецЕсли;
	
	выгрузка.Следующий();
	
	стрНомер			= выгрузка.НомерТелефона;
	контрагент			= выгрузка.Контрагент;
	контактноеЛицо		= выгрузка.КонтактноеЛицо;
	
	списокОбзвона = выгрузка.Ссылка;
	списокОбзвонаОб = списокОбзвона.ПолучитьОбъект();
	строка = списокОбзвонаОб.Список.Найти(выгрузка.НомерСтроки, "НомерСтроки");
	Если строка <> Неопределено Тогда
		строка.Обработан = Истина;
	КонецЕсли;
	списокОбзвонаОб.Записать();
	
	ЗафиксироватьТранзакцию();
	
	Возврат Истина;
КонецФункции

//---------------------------------------------------------------------------------------------------------
Функция ПолучитьПараметрыОткрытияФормыНастроек() ЭКСПОРТ
	ПользовательЗап =  БИТфонСервер.ПолучитьТекущегоПользователя();
	менеджЗапись = РегистрыСведений.бтНастройки.СоздатьМенеджерЗаписи();
	менеджЗапись.Пользователь = ПользовательЗап;
	менеджЗапись.Прочитать();
	Если НЕ менеджЗапись.Выбран() Тогда
		ЗаполнитьНастройкиПоУмолчаниюПользователя(менеджЗапись, ПользовательЗап);
		менеджЗапись.Записать();
	КонецЕсли;
	Отбор = Новый Структура("Пользователь", ПользовательЗап);
	ключзап = РегистрыСведений.бтНастройки.СоздатьКлючЗаписи(Отбор);
	П = Новый Структура("Ключ", ключзап);
	Возврат П;
КонецФункции

//---------------------------------------------------------------------------------------------------------
Функция ПолучитьНомерПользователя(ПользовательСсылка) ЭКСПОРТ
	менеджЗапись = РегистрыСведений.бтНастройки.СоздатьМенеджерЗаписи();
	менеджЗапись.Пользователь = ПользовательСсылка;
	менеджЗапись.Прочитать();
	стрНомер = "";
	Если менеджЗапись.Выбран() Тогда
		стрНомер = менеджЗапись.СвойНомер;
	КонецЕсли;
	Возврат стрНомер;
КонецФункции

//---------------------------------------------------------------------------------------------------------
Функция ОтправитьПочтовоеСообщениеВТехподдержку(стрТема, стрТекст, стрНаименованиеВложения, двоичнВложение, сообщение) ЭКСПОРТ
	Письмо = Новый ИнтернетПочтовоеСообщение;
	
	Письмо.Тема = стрТема;
	Текст = Письмо.Тексты.Добавить(стрТекст, ТипТекстаПочтовогоСообщения.ПростойТекст);
	Письмо.Отправитель = "BIT.Phonesupport@1cbit.ru";
	Письмо.ИмяОтправителя = "БИТ.Phone техническая поддержка";
	
	Письмо.Вложения.Добавить(двоичнВложение, стрНаименованиеВложения);
	
	Письмо.Получатели.Добавить("bitphone@1cbit.ru");
	
	Профиль = Новый ИнтернетПочтовыйПрофиль;
	Профиль.АдресСервераSMTP = "mx.1cbit.ru";
	Профиль.ПортSMTP = 587;
	Профиль.ПользовательSMTP = "BIT.Phonesupport@1cbit.ru";
	Профиль.ПарольSMTP = "NYF7ayjyu";
	Профиль.АутентификацияSMTP = СпособSMTPАутентификации.ПоУмолчанию;
	
	Почта = Новый ИнтернетПочта;
	
	успешностьОтправки = Истина;
	 
	Попытка
		Почта.Подключиться(Профиль);
		сообщение.Текст = сообщение.Текст + Символы.ПС + "Подключение к почтовому серверу БИТ успешно установлено";
		
		Почта.Послать(Письмо);
		сообщение.Текст = сообщение.Текст + Символы.ПС + "Письмо отправлено";
		
		Почта.Отключиться();
	Исключение
		успешностьОтправки = Ложь;
		сообщение.Текст = сообщение.Текст + Символы.ПС + "Не удалось подключиться к почтовому серверу БИТ - " + ОписаниеОшибки();
	КонецПопытки;
	
	Возврат успешностьОтправки;
КонецФункции
