﻿//бит_АЮКузнецов – Процедура устанавливает дату запрета всем пользователям и каждому в частности на последний день предидущего месяца (считать от даты запуска)
Процедура ЗакрытьПериод() Экспорт
	
	ПерезаписьРегистра = РегистрыСведений.ДатыЗапретаИзменения.СоздатьНаборЗаписей();
	ПерезаписьРегистра.Прочитать();
	Для Каждого Запись Из ПерезаписьРегистра Цикл
		
		Если Запись.Пользователь <> Перечисления.ВидыНазначенияДатЗапрета.ДляВсехИнформационныхБаз Тогда
			
			Запись.ДатаЗапрета = КонецМесяца(ДобавитьМесяц(ТекущаяДата(), -1));
			
		КонецЕсли;
		
	КонецЦикла;
	
	ПерезаписьРегистра.Записывать = Истина;
	
	ПерезаписьРегистра.Записать(Истина);
	
КонецПроцедуры