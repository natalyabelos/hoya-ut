﻿// Удалит строки из таблицы значений не удовлетворяющие условию:
//	Категория скидки(наценки) партнера дожна совпадать с Категорией скидки(наценки) ТаблицыСкидкиНаценки
//
//	Выполняется только для Документы.ЗаказКлиента
//
//	Метод реализован в первую очередь для изменения поведения метода 
//		СкидкиНаценкиСервер.РассчитатьДеревоСкидокНаценок()
//
// Параметры:
//	ТаблицаСкидкиНаценки - ТаблицаЗначений - Обязательный. 
//		В составе колонок обязательно "СкидкаНаценка"
//
//	ПараметрыРасчета - Структура, СправочникСсылка.Партнеры, ДокументСсылка.ЗаказКлиента - Обязательный.
//		Для структуры обязательные реквизиты Партнер, Регистратор
//
Процедура ПрименитьКатегорииСкидкиНаценки(ТаблицаСкидкиНаценки, знач ПараметрыРасчета) Экспорт
	
	// бит_ЕШабалин, 2017.12.15, создан новый метод
	
	Перем КатегорияСкидкиНаценки;
	Перем Индекс, НомерСтроки, КоличествоСтрок, ТекущаяСтрока;	

	МассивСтандартный = Новый массив;
	//МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.DirectDiscount);
	МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualStandart);
	//МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualSpecial);
	//МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualIndividual);
	МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.Global);
	МассивСтандартный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.PromotionalDiscount);

	МассивСпециальный = Новый массив;
	МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.DirectDiscount);
	//МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualStandart);
	МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualSpecial);
	//МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualIndividual);
	МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.Global);
	МассивСпециальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.PromotionalDiscount);
	
	МассивИндивидуальный = Новый массив;
	МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.DirectDiscount);
	//МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualStandart);
	//МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualSpecial);
	МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.ContractualIndividual);
	МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.Global);
	МассивИндивидуальный.Добавить(Справочники.fs_КатегорииСкидкиНаценки.PromotionalDiscount);
	
	Если Не ЭтоЗаказКлиента(ПараметрыРасчета) Тогда 
		Возврат;
	КонецЕсли;
	
	КатегорияСкидкиНаценки = ПолучитьКатегориюСкидкиНаценки(ПараметрыРасчета);
	
	Если КатегорияСкидкиНаценки = Справочники.бит_ТипСкидки.Standart тогда
		МассивСкидок = МассивСтандартный;
	ИначеЕсли КатегорияСкидкиНаценки = Справочники.бит_ТипСкидки.Special тогда
		МассивСкидок = МассивСпециальный;
	ИначеЕсли КатегорияСкидкиНаценки = Справочники.бит_ТипСкидки.Individual тогда
		МассивСкидок = МассивИндивидуальный;
	КонецЕсли;
	
	// бит_ЕШабалин, fix, 2018.02.12 {{
	// исключаем дамп метода МассивСкидок.Найти
	Если ТипЗнч(МассивСкидок) <> Тип("Массив") Тогда
		Возврат;
	КонецЕсли;
	// }} бит_ЕШабалин

	// Удаление обратным перебором (с конца)
	КоличествоСтрок = ТаблицаСкидкиНаценки.Количество();
	Для НомерСтроки = 1 По КоличествоСтрок Цикл			
		ТекущаяСтрока = ТаблицаСкидкиНаценки.Получить(КоличествоСтрок - НомерСтроки);
		//Если ПолучитьКатегориюСкидкиНаценки(ТекущаяСтрока.СкидкаНаценка) <> КатегорияСкидкиНаценки Тогда
		//	ТаблицаСкидкиНаценки.Удалить(ТекущаяСтрока);
		//КонецЕсли;
		Если МассивСкидок.Найти(ПолучитьКатегориюСкидкиНаценки(ТекущаяСтрока.СкидкаНаценка)) = неопределено тогда
			ТаблицаСкидкиНаценки.Удалить(ТекущаяСтрока);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры // ПрименитьКатегориюСкидкиНаценкиКРезультатуРасчета()

// Функция вернет признак выполнять модификацию ТаблицыСкидкиНаценки
//
// Параметры:
//	ПараметрыРасчета - Произвольный - Обязательный.
//
// Возвращаемое значение:
//	- Булево -
Функция ЭтоЗаказКлиента(ПараметрыРасчета) Экспорт
	
	Перем Регистратор;
	
	Возврат ТипЗнч(ПараметрыРасчета) = Тип("ДокументСсылка.ЗаказКлиента")
		Или ТипЗнч(ПараметрыРасчета) = Тип("ДокументОбъект.ЗаказКлиента")
		Или ТипЗнч(ПараметрыРасчета) = Тип("Структура")
		И ПараметрыРасчета.Свойство("Регистратор", Регистратор)
		И ТипЗнч(Регистратор) = Тип("ДокументСсылка.ЗаказКлиента");	
КонецФункции // ВыполнитьОтборПоКатегорииСкидкиНаценки()

// Вернет СправочникСсылка.
Функция ПолучитьКатегориюСкидкиНаценки(Источник, НеНайден = Ложь) 
	
	Перем ТипИстончика;
	
	НеНайден = Ложь;
	
	ТипИсточника = ТипЗнч(Источник);
	
	Если ТипИсточника = Тип("СправочникСсылка.Партнеры")
		Или ТипИсточника = Тип("СправочникОбъект.Партнеры") Тогда
		
		РодительВерхнегоУровня = Источник.Родитель;
		//Пока Не РодительВерхнегоУровня.Родитель.ПустаяСсылка() Цикл
		//	РодительВерхнегоУровня =  РодительВерхнегоУровня.Родитель;
		//КонецЦикла;
		
		Возврат РодительВерхнегоУровня.fs_КатегорияСкидкиНаценки;		
	КонецЕсли;
	
	// объекты данных с реквизитом fs_КатегорияСкидкиНаценки 
	Если ТипИсточника = Тип("СправочникСсылка.СкидкиНаценки")
		Или ТипИсточника = Тип("СправочникОбъект.СкидкиНаценки") Тогда
		
		Возврат Источник.fs_КатегорияСкидкиНаценки;		
	КонецЕсли;
	
	// оъекты данных с реквизитом Партнер 
	Если ТипИсточника = Тип("ДокументСсылка.ЗаказКлиента")
		Или ТипИсточника = Тип("ДокументОбъект.ЗаказКлиента")
		Или ТипИсточника = Тип("Структура") И Источник.Свойство("Партнер") Тогда 
		
		Возврат ПолучитьКатегориюСкидкиНаценки(Источник.Партнер, НеНайден);		
	КонецЕсли;
	
	// сам справочник категорий скидки наценки
	Если ТипИсточника = Тип("СправочникСсылка.fs_КатегорииСкидкиНаценки")
		Или ТипИсточника = Тип("СправочникОбъект.fs_КатегорииСкидкиНаценки") Тогда
		
		Возврат Источник.Ссылка;		
	КонецЕсли; 
	
	НеНайден = Истина;
	
	Возврат Справочники.fs_КатегорииСкидкиНаценки.ПустаяСсылка();
	
КонецФункции // ПолучитьКатегориюСкидкиНаценки()